package def.engine.logic;

public abstract class StateUpdateTracker <E> {
	private boolean updated = false;
	
	public void stateUpdated() {
		updated = true;
		callAtStateUpdate();
	}
	
	public void stateUpdateHandled(){
		updated = false;
	}
	
	/**
	 * Implementation should return default value if state is not changed.
	 */
	public abstract E callIfNotUpdated();
	
	/**
	 * Operation to be called if state has changed.
	 * @return
	 */
	public abstract E callIfUpdated();
	
	public E updateCheckCall(){
		if(updated){
			updated = false;
			return callIfUpdated();
		}
		else{
			return callIfNotUpdated();
		}
	}

	/**
	 * Optional action do be executed when a update is received
	 */
	public void callAtStateUpdate(){}
}
