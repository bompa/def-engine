package def.engine.logic;

public interface IAnimation {
	/**
	 * Update the animations state.

	 * @return true if animation is complete, false if it is ongoing.
	 */
	public boolean updateAnimationState();

}
