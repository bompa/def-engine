package def.engine.logic.math;

public class VectorArray3f {
	public static final int VEC_SZ = 3;
	private float[] value = new float[VEC_SZ];
	
	public float[] getArray(){
		return value; 
	}
}
