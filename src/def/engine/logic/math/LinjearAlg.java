package def.engine.logic.math;

import java.nio.FloatBuffer;
import java.util.Arrays;

import javax.vecmath.AxisAngle4f;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Quat4f;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

import def.engine.graphics.ViewPort;
import def.shared.math.Degree;
import def.shared.math.Radian;
import def.util.LogHandler;

import android.opengl.Matrix;

public class LinjearAlg {
	public static LogHandler l = LogHandler.createInstanceShared();
	
	public static final Vector3f X_AXIS = new Vector3f(1.0f, 0.0f, 0.0f);
	public static final Vector3f Y_AXIS = new Vector3f(0.0f, 1.0f, 0.0f);
	public static final Vector3f Z_AXIS = new Vector3f(0.0f, 0.0f, 1.0f);
	public final static Vector3f ORIGO = new Vector3f(0, 0, 0);
	
	public static final int X = 0;
	public static final int Y = 1;
	public static final int Z = 2;
	
	public static final int A = 3;
	public static final int W = 3;
	
	public static final int U = 0;
	public static final int V = 1;
	
	public static final Matrix3f M3_IDENTIY = new Matrix3f();
	public static final Matrix4f M4_IDENTIY = new Matrix4f();
	static{
		M3_IDENTIY.setIdentity();
		M4_IDENTIY.setIdentity();
	}
	
	public static Quat4f createQuat(Vector3f axis, Degree angle){
		AxisAngle4f axisAngle = new AxisAngle4f(axis.x, axis.y, axis.z, angle.value);
		Quat4f q = new Quat4f();
		q.set(axisAngle);
		return q;
	}

	/**
	 * 
	 * @param fovy
	 * @param aspectRatio
	 * @param zNear
	 * @param zFar
	 * @return
	 */
	public static MatrixArray4f calcualteProjectionMatrix(	final float fovy, final float aspectRatio, 
															final float zNear, final float zFar) {
		
		final Double height, width;
		final float fovyHalf = fovy / 2.0f;
		height = Math.tan(Radian.degreeValue(fovyHalf)) * zNear;
		width = height * aspectRatio;
	
		final MatrixArray4f projectionMtx = new MatrixArray4f();
		final float[] projMtx = projectionMtx.getArray();		
	    Matrix.frustumM(projMtx, 0, -width.floatValue(), width.floatValue(), -height.floatValue(), height.floatValue(), zNear, zFar);
		return projectionMtx;
	}
	
	/**
	 * Temp default view matrix. TODO: add vector parameters
	 * @return
	 */
	public static MatrixArray4f calculateViewMatrix(final Vector3f eyePos, final Vector3f lookPos){
		MatrixArray4f viewMatrix = new MatrixArray4f();

		// Set our up vector. This is where our head would be pointing were we holding the camera.
		final float upX = 0.0f;
		final float upY = 1.0f;
		final float upZ = 0.0f;

		// Set the view matrix. This matrix can be said to represent the camera position.
		// NOTE: In OpenGL 1, a ModelView matrix is used, which is a combination of a model and
		// view matrix. In OpenGL 2, we can keep track of these matrices separately if we choose.
		Matrix.setLookAtM(viewMatrix.getArray(), 0, eyePos.x, eyePos.y, eyePos.z, lookPos.x, lookPos.y, lookPos.z, upX, upY, upZ);
		return viewMatrix;
	}
	
	/**
	 * Extract values from Matrix4f type into a float array, arranged in column order as OpenGl expects. 
	 * @param mtx Matrix to convert
	 * @return a float array in column order
	 */
	public static float[] matrixToArrayRearrange(final Matrix4f mtx){
		float[] matrixArr = {
				mtx.m00, mtx.m10, mtx.m20, mtx.m30,
				mtx.m01, mtx.m11, mtx.m21, mtx.m31,
				mtx.m02, mtx.m12, mtx.m22, mtx.m32,
				mtx.m03, mtx.m13, mtx.m23, mtx.m33
			};
		return matrixArr;
	}
	
	/**
	 * Extract values from Matrix4f type into a float array
	 * @param mtx
	 * @return
	 */
	public static float[] matrixToArray(final Matrix4f mtx){
		float[] matrixArr = {
				mtx.m00, mtx.m01, mtx.m02, mtx.m03,
				mtx.m10, mtx.m11, mtx.m12, mtx.m13,
				mtx.m20, mtx.m21, mtx.m22, mtx.m23,
				mtx.m30, mtx.m31, mtx.m32, mtx.m33
			};
		return matrixArr;
	}
	
	/**
	 * Extract values from Matrix3f type into a float array
	 * @param mtx
	 * @return
	 */
	public static float[] matrixToArray(final Matrix3f mtx){
		float[] matrixArr = {
				mtx.m00, mtx.m01, mtx.m02,
				mtx.m10, mtx.m11, mtx.m12,
				mtx.m20, mtx.m21, mtx.m22
			};
		return matrixArr;
	}

	/**
	 * Same as for Matrix4f.
	 * @param vec
	 * @return
	 */
	public static float[] vectorToArray(final Vector3f vec){
		float[] matrixArr = { vec.x, vec.y, vec.z };
		return matrixArr;
	}
	
	/**
	 * 
	 * @param vec
	 * @return
	 */
	public static float[] vectorToArray(final Vector4f vec){
		float[] matrixArr = { vec.x, vec.y, vec.z, vec.w };
		return matrixArr;
	}
	
	public static Vector3f mulVec3f(final Vector3f v, final float scalar){
		v.x *= scalar;
		v.y *= scalar;
		v.z *= scalar;
		return v;
	}
	
	public static Vector3f mulQuat4f(final Quat4f q, final Vector3f v){
		// nVidia SDK implementation
		final Vector3f uv = new Vector3f(), uuv = new Vector3f();
		final Vector3f qvec = new Vector3f(q.x, q.y, q.z);
		uv.cross(qvec, v);
		uuv.cross(qvec, uv);
		mulVec3f(uv, 2.0f * q.w);
		mulVec3f(uuv, 2.0f);

		v.add(uv);
		v.add(uuv);
		return v;
	}
	
	/**
	 * Fill buffer with two-point vectors wich are normal directions, (0,0,0) -> (x, y, z)
	 * (Could be one zero point being reused with indices when rendering, but this work and is only for debug)
	 * @param outPairNoramlsBuff if successful it holds pairs of points defining normal directions
	 * @param normals
	 * @return
	 */
	public static boolean createNormalDirectionsBuffer(final FloatBuffer outPairNoramlsBuff, final FloatBuffer normals){
		final float[] normalLinePairs = new float[outPairNoramlsBuff.capacity()];
		
		// Has to have a correct amount of vertices
		if(normalLinePairs.length % VectorArray3f.VEC_SZ != 0)
			return false;
		
		final float NORMAL_LENGTH_SCALE = 10.0f;
		// Every second should be zero, and other should be normal vector 
		Arrays.fill(normalLinePairs, 0.0f);
		int currentVertex = 0;
		for(int i = 0; i < (normalLinePairs.length - VectorArray3f.VEC_SZ); i += VectorArray3f.VEC_SZ){
			if(currentVertex % 2 != 0){
				final int nrOfPreviousZeroVerts = ((currentVertex - 1) / 2);
				final int normalValOffset = VectorArray3f.VEC_SZ * nrOfPreviousZeroVerts;
				normalLinePairs[i] = normals.get(i - normalValOffset) * NORMAL_LENGTH_SCALE;
				normalLinePairs[i + 1] = normals.get(i + 1 - normalValOffset) * NORMAL_LENGTH_SCALE;
				normalLinePairs[i + 2] = normals.get(i + 2 - normalValOffset) * NORMAL_LENGTH_SCALE;
			}
			currentVertex++;
		}
		outPairNoramlsBuff.put(normalLinePairs);
		outPairNoramlsBuff.position(0);
		
		return true;
	}
	
	/**
	 * Get world coords from screen x,y coords at given depth(depth from p.z)
	 * @param modelview
	 * @param projection
	 * @param viewport
	 * @param p
	 * @return vector containing world x,y,z coords mapped to given screen coords
	 */
	public static Vector3f screenToWorld(	final Matrix4f modelview, 
											final Matrix4f projection, 
											final ViewPort viewport,
											final Vector3f p)
	{ 
		final float z = p.z;
		
		// adjust y (0,0 is lowerleft corner in OpenGL)  
		p.y = (viewport.getHeight() - p.y);
		
		// near plane intersection 
		p.z = 0.0f;
		final Vector3f p0 = unproject(modelview, projection, viewport, p);
		
		// far plane intersection 
		p.z = 1.0f;
		final Vector3f p1 = unproject(modelview, projection, viewport, p);
		
		// find (x, y) coordinates for given depth z-value
		final float t = (z - p0.z) / (p1.z - p0.z);
		p.x = (p0.x + t * (p1.x - p0.x));
		p.y = (p0.y + t * (p1.y - p0.y));
		p.z = z;
		
		return p;
	}
	
	public static Vector3f unproject(final Matrix4f modelview, final Matrix4f projection, final ViewPort viewport, final Vector3f pt)
	{
		// find the inverse modelview-projection-matrix
		MatrixArray4f invProjView = new MatrixArray4f();
		Matrix.multiplyMM(	invProjView.getArray(), 0, 
							LinjearAlg.matrixToArray(projection), 0,
							LinjearAlg.matrixToArray(modelview), 0);
		Matrix.invertM(invProjView.getArray(), 0, invProjView.getArray(), 0);
		
		// transform to normalized coordinates in the range [-1, 1] 
		Vector4f in = new Vector4f();
		in.x = (pt.x - viewport.getX()) / viewport.getWidth() * 2.0f - 1.0f;
		in.y = (pt.y - viewport.getY()) / viewport.getHeight() * 2.0f - 1.0f;
		in.z = 2.0f * pt.z - 1.0f;
		in.w = 1.0f;
		
		// find the object's coordinates
		float[] outVectorArr = new VectorArray4f().getArray();
		float[] aArr = invProjView.getArray();
		float[] inArr = LinjearAlg.vectorToArray(in);
		Matrix.multiplyMV(outVectorArr, 0, aArr, 0, inArr, 0);

		Vector4f out = new Vector4f(outVectorArr[X], outVectorArr[Y], outVectorArr[Z], outVectorArr[W]);
		if(out.w != 0.0f) out.w = 1.0f / out.w;
		
		// calculate output
		Vector3f result = new Vector3f();
		result.x = out.x * out.w;
		result.y = out.y * out.w;
		result.z = out.z * out.w;
		 
		return result;
	}

	/**
	 * Sets the rotation and scaling part of the given 4 by 4 matrix to the identity matrix
	 * @param viewMatrix
	 */
	public static void set3x3SubmatrixToIdentity(MatrixArray4f viewMatrix) {
		final float[] mtx = viewMatrix.getArray();
		
		for( int i = 0; i < 3; i++ ){ 
			for( int j = 0; j < 3; j++ ) {
				if ( i == j ){
					mtx[i*4+j] = 1.0f;
				}
				else{
					mtx[i*4+j] = 0.0f;	
				}
			}
		}
	}

	public static Vector3f getTranslationFromMatrix(Matrix4f mtx) {
		return new Vector3f(mtx.m03, mtx.m13, mtx.m23);
	}

}
