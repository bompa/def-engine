package def.engine.logic.math;

public class VectorArray4f {
	public static final int VEC_SZ = 4;
	private float[] value_ = new float[VEC_SZ];
	
	public float[] getArray(){
		return value_; 
	}
	
	public void setArray(float[] value){
		value_ = value;		
	}
}
