package def.engine.logic.math;

public class VectorArray2f {
	public static final int VEC_SZ = 2;
	private float[] value = new float[VEC_SZ];
	
	public float[] getArray(){
		return value; 
	}
}
