package def.engine.logic.math;

public class MatrixArray3f {
	public static final int THREE_BY_THREE_MTX_SZ = 9;
	private final Arrayxf value = new Arrayxf(THREE_BY_THREE_MTX_SZ);
	
	public float[] getArray(){
		return value.getArray(); 
	}
}
