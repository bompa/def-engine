package def.engine.logic.math;

public class MatrixArray4f {
	public static final int FOUR_BY_FOUR_MTX_SZ = 16;
	private float[] value_ = new float[FOUR_BY_FOUR_MTX_SZ];
	
	public MatrixArray4f(){}
	
	public MatrixArray4f(float[] value){
		if(value.length != FOUR_BY_FOUR_MTX_SZ)
			throw new IllegalArgumentException("MatrixArray4f must be constructed with array of size: " + FOUR_BY_FOUR_MTX_SZ);
		
		this.value_ = value;
	}
	
	public float[] getArray(){
		return value_; 
	}
}
