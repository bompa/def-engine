package def.engine.control;

import def.util.FloatPair;
import def.util.LogHandler;
import def.util.TimeStampData;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class OnTouchController implements ConfigureableTouchListener{

	private final LogHandler l = LogHandler.createInstanceShared();

	private final TimeStampData<FloatPair> last1stTouch = TimeStampData.createTimedFloatPairInfinfy();
	private final TimeStampData<FloatPair> last2ndTouch = TimeStampData.createTimedFloatPairInfinfy();
	
	private final AHandleMultiTouch multiTouchHandler;
	private final AHandleSingleTouch singleTouchHandler;
	
	private boolean dispatchPureEventOnSingle = false;
	private boolean dispatchPureEventOnMulti = false;
	
	public static OnTouchController createAndConfigure(final AHandleSingleTouch singleTh, final AHandleMultiTouch multiTh){
		OnTouchController ctrl = new OnTouchController(singleTh, multiTh);
		
		ctrl.setDispatchConfigOpt(	DispatchConfigType.CONF_ON_TOUCH_SINGLE_DISPATCH_EV, 
									singleTh.getDispatchConfigOpt(DispatchConfigType.CONF_ON_TOUCH_SINGLE_DISPATCH_EV));
		ctrl.setDispatchConfigOpt(	DispatchConfigType.CONF_ON_TOUCH_MULTI_DISPATCH_EV, 
									multiTh.getDispatchConfigOpt(DispatchConfigType.CONF_ON_TOUCH_MULTI_DISPATCH_EV));
		return ctrl;
	}
	
	public static OnTouchController createWithDefaultHandlers(final AHandleSingleTouch singleTh, final AHandleMultiTouch multiTh){
		return null;//return new OnTouchController(singleTh, multiTh);
	}
	
	private OnTouchController(final AHandleSingleTouch singleTh, final AHandleMultiTouch multiTh){
		singleTouchHandler = singleTh;
		multiTouchHandler = multiTh;
	}
	
	private static final int EventLogFrequency = 16;
	
	public boolean onTouch(final View v, final MotionEvent event) {
		l.logAtFreq("Motion event detect", Log.DEBUG, EventLogFrequency);
		
		if(dispatchPureEventOnMulti)
		{
			multiTouchHandler.multiTouch(event);
		}
		if(dispatchPureEventOnSingle)
		{
			singleTouchHandler.singleTouch(event);
		}
		
		if(event.getAction() == MotionEvent.ACTION_MOVE){
			TimeStampData<FloatPair> current1stTouchPos = CtrlUtil.createTimeStampData(event);
			determineObjectAction(current1stTouchPos, event);
			last1stTouch.set(current1stTouchPos.get());
		}
		else if(event.getAction() == MotionEvent.ACTION_UP){
			singleTouchHandler.up();
			multiTouchHandler.up();
			last1stTouch.set(TimeStampData.createTimedFloatPairInfinfy().get());
			last1stTouch.resetTimeStamp();
			last2ndTouch.set(TimeStampData.createTimedFloatPairInfinfy().get());
			last2ndTouch.resetTimeStamp();
		}
		return true;
	}
	
	private long timeDiffFromTimeStamps(final TimeStampData<FloatPair> currentTouch, 
			final TimeStampData<FloatPair> lastTouch){
		return currentTouch.timeMs() - lastTouch.timeMs();
	}
	
	/**
	 * Dispatch according to if multi or single touch is detected, check config to just store data or dispatch
	 * the data as well.
	 */
	private void determineObjectAction(final TimeStampData<FloatPair> current1stTouchPos, final MotionEvent event){
		if(CtrlUtil.isMultiTouchEv(event)){
			TimeStampData<FloatPair> current2ndTouchPos = CtrlUtil.createTimeStampData2ndPointer(event);
			final long timeDiff = Math.abs(timeDiffFromTimeStamps(current2ndTouchPos, last2ndTouch));

			if(!dispatchPureEventOnMulti)
			{
				multiTouchHandler.multiTouch(current1stTouchPos.get(), last1stTouch.get(),
						current2ndTouchPos.get(), last2ndTouch.get(), timeDiff);
			}
			last2ndTouch.set(current2ndTouchPos.get());
		}
		else if(!dispatchPureEventOnSingle)
		{
			final long timeDiff = Math.abs(timeDiffFromTimeStamps(current1stTouchPos, last1stTouch));
			singleTouchHandler.singleTouch(current1stTouchPos.get(), last1stTouch.get(), timeDiff);
		}
	}

	public void setDispatchConfigOpt(DispatchConfigType type, boolean configValue) {
		switch(type){
			case CONF_ON_TOUCH_SINGLE_DISPATCH_EV:
				this.dispatchPureEventOnSingle = configValue;
			default:
				this.dispatchPureEventOnMulti = configValue;
		}
		
	}
}
