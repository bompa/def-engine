package def.engine.control;

import android.view.View.OnTouchListener;

public interface ConfigureableTouchListener extends OnTouchListener {
	
	public enum DispatchConfigType{CONF_ON_TOUCH_SINGLE_DISPATCH_EV, CONF_ON_TOUCH_MULTI_DISPATCH_EV};
	public void setDispatchConfigOpt(DispatchConfigType type, boolean configValue);
}
