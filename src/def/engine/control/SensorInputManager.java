package def.engine.control;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import def.engine.logic.math.MatrixArray4f;
import def.engine.logic.math.VectorArray3f;
import def.util.LogHandler;

public class SensorInputManager implements ISensorInput, SensorEventListener {
	private final LogHandler l = LogHandler.createInstanceShared();
	
	private final SensorManager sensorManager;
	private final Sensor sensorMagnetic;
	private final Sensor sensorAccel;

	final VectorArray3f gravity = new VectorArray3f(); 
	final VectorArray3f  geomagnetic = new VectorArray3f();
	
	public static final int X = 0;
	public static final int Y = 1;
	public static final int Z = 2;
	
	public static SensorInputManager create(final Activity activity){
		return new SensorInputManager(activity);
	}
	
	private SensorInputManager(final Activity activity){
		sensorManager = (SensorManager) activity.getSystemService(Context.SENSOR_SERVICE);
		sensorMagnetic = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		sensorAccel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		
		boolean regStatusGyro, regStatusMag;
		regStatusGyro = sensorManager.registerListener(this, sensorAccel, SensorManager.SENSOR_DELAY_GAME);
		regStatusMag = sensorManager.registerListener(this, sensorMagnetic, SensorManager.SENSOR_DELAY_GAME);
		
		if(!regStatusGyro || !regStatusMag){
			l.log("Failed to register a sesnor listener, some senor input will not work!", Log.ERROR);
		}
	}
	
	public boolean getDeviceRotationMtx(final MatrixArray4f r){
		final MatrixArray4f i = new MatrixArray4f();
		if(!SensorManager.getRotationMatrix(r.getArray(), i.getArray(), gravity.getArray(), geomagnetic.getArray())){
			l.log("Failed to get rotation matrix from sensor manager, can not get device orientation.");
			return false;
		}
		return true;		
	}

	public boolean getDeviceOrientation(final VectorArray3f outAxisAngles) {
		final MatrixArray4f r = new MatrixArray4f();
		if(!getDeviceRotationMtx(r)){
			return false;
		}
		
		SensorManager.getOrientation(r.getArray(), outAxisAngles.getArray());
		return true;
	}

	public void onAccuracyChanged(Sensor sensor, int accuracy) { }

	public void onSensorChanged(SensorEvent event) {
		switch(event.sensor.getType()){
			case Sensor.TYPE_ACCELEROMETER:
				handleAccelerometerEvent(event);
				break;
			case Sensor.TYPE_MAGNETIC_FIELD:
				handleMagneticEvent(event);
				break;
			default:
				return;	
		}
	}

	private void handleMagneticEvent(SensorEvent event) {
		copyEventValuesToArrayXYZ(event, geomagnetic.getArray());	
	}

	private void handleAccelerometerEvent(SensorEvent event) {
		copyEventValuesToArrayXYZ(event, gravity.getArray());
	}
	
	private void copyEventValuesToArrayXYZ(SensorEvent event, float[] array) {
		// TODO: Use sytem.arraycopy
		array[X] = event.values[X];
		array[Y] = event.values[Y];
		array[Z] = event.values[Z];
	}
}
