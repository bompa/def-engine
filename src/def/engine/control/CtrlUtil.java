package def.engine.control;

import def.util.FloatPair;
import def.util.TimeStampData;
import android.view.MotionEvent;

public class CtrlUtil 
{	
	/**
	 * Detect i a multi-touch movement is in progress. 
	 */
	static boolean isMultiTouchEv(final MotionEvent event){
		return event.getPointerCount() > 1;
	}
	static float multiTouchGetSecondX(final MotionEvent event){
		return event.getX(1);
	}
	static float multiTouchGetSecondY(final MotionEvent event){
		return event.getY(1);
	}

	/**
	 *
	 * @param event
	 * @return FloatPair(X-value, Y-Value)
	 */
	static FloatPair getDeltaPrevPosition(MotionEvent event){
		if(event.getHistorySize() > 0){
			float dY = event.getY() - event.getHistoricalY(event.getHistorySize() - 1);
			float dX = event.getX() - event.getHistoricalX(event.getHistorySize() - 1);
			return new FloatPair(dX, dY);
		}
		return new FloatPair(Float.NaN, Float.NaN);
	}
	
	public static FloatPair getEventXyAtPointerIndex(final MotionEvent event, int pointerIndex) {
		return new FloatPair(event.getX(pointerIndex), event.getY(pointerIndex));
	}
	
	public static TimeStampData<FloatPair> createTimeStampData(final MotionEvent event, int pointerIndex) {
		return new TimeStampData<FloatPair>(getEventXyAtPointerIndex(event, pointerIndex));
	}
	
	public static TimeStampData<FloatPair> createTimeStampData(final MotionEvent event) {
		return createTimeStampData(event, 0);
	}
	public static TimeStampData<FloatPair> createTimeStampData2ndPointer(final MotionEvent event) {
		return createTimeStampData(event, 1);
	}
}
