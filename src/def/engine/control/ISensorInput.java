package def.engine.control;

import def.engine.logic.math.MatrixArray4f;
import def.engine.logic.math.VectorArray3f;

public interface ISensorInput {
	public boolean getDeviceRotationMtx(final MatrixArray4f r);
	public boolean getDeviceOrientation(VectorArray3f outAxisAngles);
}
