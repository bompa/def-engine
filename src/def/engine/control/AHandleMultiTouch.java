package def.engine.control;

import android.view.MotionEvent;
import def.engine.control.ConfigureableTouchListener.DispatchConfigType;
import def.util.FloatPair;

/**
 * Abstract class to handle a multi-touch only, by removing single touch override ability to subclasses
 * @author jonas
 *
 */
public abstract class AHandleMultiTouch implements IHandleTouch{
	
	final public void singleTouch(final FloatPair xy, final FloatPair previousXy, long timeSinceLastSingleTouch){}
	final public void singleTouch(final MotionEvent e){}
	public void multiTouch(MotionEvent e){ }
	public boolean getDispatchConfigOpt(DispatchConfigType type) { return false; }
}
