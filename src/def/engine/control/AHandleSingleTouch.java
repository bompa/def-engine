package def.engine.control;

import android.view.MotionEvent;
import def.engine.control.ConfigureableTouchListener.DispatchConfigType;
import def.util.FloatPair;

public abstract class AHandleSingleTouch implements IHandleTouch{
	
	final public void multiTouch(	final FloatPair xyOne, final FloatPair previousXyOne, 
									final FloatPair xyTwo, final FloatPair previousXyTwo,
									long timeSinceLastSingleTouch){ }
	
	final public void multiTouch(MotionEvent e){ }
	
	public boolean getDispatchConfigOpt(DispatchConfigType type) { return false; }
}
