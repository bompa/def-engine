package def.engine.control;

import javax.vecmath.Vector3f;

import android.opengl.Matrix;

import def.engine.graphics.IViewFrustum;
import def.engine.graphics.ViewFrustum;
import def.engine.graphics.ViewPort;
import def.engine.graphics.Wrappers;
import def.engine.logic.StateUpdateTracker;
import def.engine.logic.math.LinjearAlg;
import def.engine.logic.math.MatrixArray4f;
import def.shared.math.Radian;

public class FlyCamera extends StateUpdateTracker<MatrixArray4f> implements ICamera{
	private final IViewFrustum viewFrustum = ViewFrustum.create(0, 0, 0, 0);
	private ViewPort vp = ViewPort.create(0, 0, 0, 0);
	private MatrixArray4f cachedViewMtx = new MatrixArray4f();
	private MatrixArray4f cachedProjMtx = new MatrixArray4f();
	
	private Radian rotateXVal = new Radian(0.0f);
	private Radian rotateYVal = new Radian(0.0f);
	private Vector3f position = new Vector3f(0, 0, 0);
	private final Vector3f lookAt = new Vector3f(0, 0, -0.5f);
	
	public FlyCamera(){
		Matrix.setIdentityM(cachedViewMtx.getArray(), 0);
		Matrix.setIdentityM(cachedProjMtx.getArray(), 0);
		stateUpdated();
	}
	
	public Vector3f getLookDirection(){
		final Vector3f tmp = new Vector3f(lookAt); 
		tmp.sub(position);
		
		return tmp;
	}

	public void moveRel(Vector3f v) {
		position.add(v);
		stateUpdated();
	}

	public void moveTo(Vector3f v) {
		position.set(v);
		stateUpdated();
	}

	public void pitch(Radian r) {
		rotateXVal = r;
		stateUpdated();
	}

	public void roll(Radian r) {
	}

	public void yaw(Radian r) {
		rotateYVal = r;
		stateUpdated();
	}

	public void reset() {
		rotateXVal = new Radian(0.0f);
		rotateYVal = new Radian(0.0f);
		position.set(0, 0, 0);
		stateUpdated();
	}

	public Radian getPitch() {
		return rotateXVal;
	}

	public Radian getRoll() {
		// TODO Auto-generated method stub
		return null;
	}

	public Radian getYaw() {
		return rotateYVal;
	}

	
	public Vector3f getPosition() {
		return position;
	}

	public IViewFrustum getViewFrustum() {
		return viewFrustum;
	}

	public IViewFrustum getReadOnlyViewFrustum() {
		return Wrappers.unmodifiableViewFrustum(viewFrustum);
	}

	public MatrixArray4f getViewMatrix() {
		return super.updateCheckCall();
	}

	@Override
	public MatrixArray4f callIfNotUpdated() {
		return cachedViewMtx;
	}

	@Override
	public MatrixArray4f callIfUpdated() {
		cachedViewMtx = LinjearAlg.calculateViewMatrix(position, lookAt);
		return cachedViewMtx;
	}

	public void setViewFrustum(IViewFrustum vf) {
		this.viewFrustum.set(vf.getFovy(), vf.getAspectRatio(), vf.getzNear(), vf.getzFar());
	}

	public void setViewPort(ViewPort vp) {
		this.vp = ViewPort.create(vp.getX(), vp.getY(), vp.getHeight(), vp.getWidth());
	}
	
	public MatrixArray4f getProjectionMatrix() {
		return cachedProjMtx;
	}

	public MatrixArray4f updateAndGetProjectionMatrix() {
		cachedProjMtx = LinjearAlg.calcualteProjectionMatrix(viewFrustum.getFovy(), viewFrustum.getAspectRatio(), viewFrustum.getzNear(), viewFrustum.getzFar());
		return getProjectionMatrix();
	}

	public ViewPort getViewPort() {
		return vp;
	}
}
