package def.engine.control;

import android.view.View.OnTouchListener;

public interface ITouchControllerManager {
	public void updateOnTouchController(OnTouchListener listener);
}
