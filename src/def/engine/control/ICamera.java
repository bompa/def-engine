package def.engine.control;


import javax.vecmath.Vector3f;

import def.engine.graphics.IViewOperations;
import def.shared.math.Radian;

public interface ICamera extends IViewOperations{
	/**
	 * Rotate around Y-axis
	 * @param r
	 */
	public void yaw(Radian r);
	
	/**
	 * Rotate around Z-axis
	 * @param r
	 */
	public void roll(Radian r);
	
	/**
	 * Rotate around X-axis
	 * @param r
	 */
	public void pitch(Radian r);
	
	/**
	 * 
	 * @return direction vector(un-normalized) which camera looks at 
	 */
	public Vector3f getLookDirection();
	
	public Radian getYaw();
	public Radian getRoll();
	public Radian getPitch();
	
	public Vector3f getPosition();
	
	public void moveRel(Vector3f v);
	public void moveTo(Vector3f v);
	public void reset();
}
