package def.engine.control;

import android.view.MotionEvent;
import def.engine.control.ConfigureableTouchListener.DispatchConfigType;
import def.util.FloatPair;

public interface IHandleTouch {
	public void up();
	
	/**
	 *  Called when two index pointers (fingers is registed)
	 * 
	 * @param xyOne	XY-coords for first of the two registred touch
	 * @param previousXyOne XY-coords for previous recorded(recoreded at currentTime - timeSinceLastSingleTouch) touch for first touch 
	 * @param xyTwo XY-coords for second of the two registred touches
	 * @param previousXyTwo XY-coords previous recorded(recoreded at currentTime - timeSinceLastSingleTouch) touch for second of the two registred touches
	 * @param timeSinceLastSingleTouch Time since previous recored touch event
	 */
	public void multiTouch(	final FloatPair xyOne, final FloatPair previousXyOne, 
							final FloatPair xyTwo, final FloatPair previousXyTwo,
							long timeSinceLastSingleTouch);
	
	public void multiTouch(MotionEvent e);
	
	/**
	 * Called when one index pointer (finger is registed)
	 * @param xy
	 * @param previousXy
	 * @param timeSinceLastSingleTouch
	 */
	public void singleTouch(final FloatPair xy, final FloatPair previousXy, long timeSinceLastSingleTouch);
	
	public void singleTouch(final MotionEvent e);
	
	/**
	 * Implemented to return appropriate configuration booleans, which depends on the DispatchConfigType argument
	 * 
	 * @param type is the config type to return
	 * @return true/false for a configuration option
	 */
	public boolean getDispatchConfigOpt(DispatchConfigType type);
}
