package def.engine;

import android.content.Context;
import android.util.Log;
import def.engine.execution.ITaskExecutor;
import def.engine.execution.MultiCoreTaskExecutor;
import def.engine.graphics.components.AndroidResourceManager;
import def.engine.graphics.shader.ShaderProgramFactory;
import def.ui.R;
import def.util.DefUnCaughtExceptionHandler;
import def.util.LogHandler;

public class Engine {
	private static final Engine instance = new Engine();
	private final ITaskExecutor engineTaskExecutor = new MultiCoreTaskExecutor();
	private boolean initStatus = false;
	
	/**
	 * Strings for names of supported physics libs. Constants PHYS_LIB_X are indices in arrays to get correct string
	 */
	public final static String[] PHYSICS_SUPPORTED_ENGINES = {"Bullet", "JBullet"};
	public final static int PHYS_LIB_BULLET = 0;
	public final static int PHYS_LIB_JBULLET = 1;
	
	public static String [] getSupportedPhysicEngines(){
		return Engine.PHYSICS_SUPPORTED_ENGINES;
	}
	
	public static int getPhyicsEngineIndex(String selectedPhysics) {
		for(int i = 0; i < PHYSICS_SUPPORTED_ENGINES.length; i++){
			if(PHYSICS_SUPPORTED_ENGINES[i].equals(selectedPhysics)){
				return i;
			}
		}
		return 0;
	}
	
	/**
	 * Strings for names of supported renders. Constants RENDER_X are indices in arrays to get correct string
	 */
	public final static String[] GRAPHICS_SUPPORTED_RENDERERS = {"OpenGL2.0", "OpenGL ES1.x"};
	public final static int RENDER_OPENGLES20 = 0;
	public final static int RENDER_OPENGLES1X = 1;
	
	public static int getRenderIndex(String selectedRender) {
		for(int i = 0; i < GRAPHICS_SUPPORTED_RENDERERS.length; i++){
			if(GRAPHICS_SUPPORTED_RENDERERS[i].equals(selectedRender)){
				return i;
			}
		}
		return 0;
	}
	
	public static String [] getSupportedRenders(){
		return Engine.GRAPHICS_SUPPORTED_RENDERERS;
	}
	
	public final static String[] LEVELS = {"Cube, Skybox, RTT", "Cube, Skybox", "Cube Simple"};
	public final static int LEVEL_CUBE_RTT_SKYBOX = 0;
	public final static int LEVEL_CUBE_SKYBOX = 1;
	public final static int LEVEL_CUBE_SIMPLE = 2;
	
	public static String [] getLevels(){
		return Engine.LEVELS;
	}
	
	private Engine(){}
	
	public static Engine instance() {
		return instance;
	}
	
	private void initShaderPrograms(Context ctx){
        // TODO: load directory
        AndroidResourceManager arm = AndroidResourceManager.instance();
        arm.init(ctx);
        
        ShaderProgramFactory.instance().setTangetSpaceVSShader(
        		arm.readTextFileFromDefaultPathUpdateIfMissing("lightdir_tangent_space_vs.glsl", R.raw.lightdir_tangent_space_vs));
        ShaderProgramFactory.instance().setPerFragLightNormalMapFShader(
        		arm.readTextFileFromDefaultPathUpdateIfMissing("per_frag_light_nrml_map_fs.glsl", R.raw.per_frag_light_nrml_map_fs));
        ShaderProgramFactory.instance().setbasicTexuredFShader(
        		arm.readTextFileFromDefaultPathUpdateIfMissing("basic_textured_fs.glsl", R.raw.basic_textured_fs));
        ShaderProgramFactory.instance().setPerFragmentLightningFShader(
        		arm.readTextFileFromDefaultPathUpdateIfMissing("per_fragment_lightning_fs.glsl", R.raw.per_fragment_lightning_fs));
        ShaderProgramFactory.instance().setPerFragmentLightningVShader(
        		arm.readTextFileFromDefaultPathUpdateIfMissing("per_fragment_lightning_vs.glsl", R.raw.per_fragment_lightning_vs));
        ShaderProgramFactory.instance().setBasicFShader(
        		arm.readTextFileFromDefaultPathUpdateIfMissing("basic_fs.glsl", R.raw.basic_fs));
        ShaderProgramFactory.instance().setBasicVShader(
        		arm.readTextFileFromDefaultPathUpdateIfMissing("basic_vs.glsl", R.raw.basic_vs));		
	}

	/**
	 * Synchronized initialization
	 * @param ctx
	 * @return
	 */
	public synchronized boolean init(Context ctx){
		if(ctx == null)
			return false;
		if( initStatus )
			return true;
		
        LogHandler.init(Log.VERBOSE);
        LogHandler.createInstanceShared().log("Logger initialized");
        Thread.setDefaultUncaughtExceptionHandler(new DefUnCaughtExceptionHandler());
        
        initShaderPrograms(ctx);
        
        initStatus = true;
		return true;
	}
	
	public ITaskExecutor getTaskExecutor(){
		return engineTaskExecutor;
	}
}
