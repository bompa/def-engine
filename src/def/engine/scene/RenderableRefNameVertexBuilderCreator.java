package def.engine.scene;

import def.engine.graphics.IRenderable;
import def.engine.graphics.components.IBuildAction;

public class RenderableRefNameVertexBuilderCreator implements IRenderableCreator  {
	
	final IBuildAction action;
	
	public static RenderableRefNameVertexBuilderCreator create(final IBuildAction action){
		return new RenderableRefNameVertexBuilderCreator(action);
	}
	
	private RenderableRefNameVertexBuilderCreator(final IBuildAction action){
		if(action == null)
			throw new IllegalArgumentException("IBuildAction or SceneHandlingFacade can not be null in ARenderableVertexBuilderCreator");
				
		this.action = action;
	}

	public IRenderable create(final SceneFacade sceneHandlerFacade, final String modelName) {
		return sceneHandlerFacade.addRenderableShape(modelName, action);
	}

	public IRenderable create(SceneFacade sceneHandlerFacade, String refName, String modelName) {
		return sceneHandlerFacade.addRenderableShape(refName, modelName, action);
	}

}
