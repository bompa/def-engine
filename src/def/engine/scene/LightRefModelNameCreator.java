package def.engine.scene;

import def.engine.graphics.IRenderable;

public class LightRefModelNameCreator implements IRenderableCreator {
	
	public static LightRefModelNameCreator create(){
		return new LightRefModelNameCreator();
	}
	
	private LightRefModelNameCreator(){ }

	public IRenderable create(final SceneFacade sceneHandlerFacade, final String modelName) {
		return sceneHandlerFacade.addRenderableShape(modelName, modelName);
	}

	public IRenderable create(SceneFacade sceneHandlerFacade, String refName, String modelName) {
		return sceneHandlerFacade.addRenderableShape(refName, modelName);
	}
}
