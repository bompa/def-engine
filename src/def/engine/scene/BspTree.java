package def.engine.scene;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import def.engine.graphics.IRenderable;
import def.shared.util.ITree;

public class BspTree<T> implements ITree<T>{

	private final T head;
	private ArrayList<BspTree<T>> leafs = new ArrayList<BspTree<T>>();
	private ITree<T> parent = null;
	private HashMap<T, BspTree<T>> locate = new HashMap<T, BspTree<T>>();
	
	private final List<IRenderable> polygons = new ArrayList<IRenderable>();

	public BspTree(final T head) {
		this.head = head;
		locate.put(head, this);
	}

	/* (non-Javadoc)
	 * @see def.engine.scene.ITree#addLeaf(T, T)
	 */
	public void addLeaf(T root, T leaf) {
		if (locate.containsKey(root)) {
			locate.get(root).addLeaf(leaf);
		} else {
			addLeaf(root).addLeaf(leaf);
		}
	}

	/* (non-Javadoc)
	 * @see def.engine.scene.ITree#addLeaf(T)
	 */
	public ITree<T> addLeaf(T leaf) {
		BspTree<T> t = new BspTree<T>(leaf);
		leafs.add(t);
		t.parent = this;
		t.locate = this.locate;
		locate.put(leaf, t);
		return t;
	}

	/* (non-Javadoc)
	 * @see def.engine.scene.ITree#setAsParent(T)
	 */
	public ITree<T> setAsParent(T parentRoot) {
		BspTree<T> t = new BspTree<T>(parentRoot);
		t.leafs.add(this);
		this.parent = t;
		t.locate = this.locate;
		t.locate.put(head, this);
		t.locate.put(parentRoot, t);
		return t;
	}

	/* (non-Javadoc)
	 * @see def.engine.scene.ITree#getHead()
	 */
	public T getTreeNode() {
		return head;
	}

	/* (non-Javadoc)
	 * @see def.engine.scene.ITree#getTree(T)
	 */
	public ITree<T> getTree(T element) {
		return locate.get(element);
	}

	/* (non-Javadoc)
	 * @see def.engine.scene.ITree#getParent()
	 */
	public ITree<T> getParent() {
		return parent;
	}

	/* (non-Javadoc)
	 * @see def.engine.scene.ITree#getSuccessors(T)
	 */
	public Collection<T> getSuccessors(T root) {
		Collection<T> successors = new ArrayList<T>();
		BspTree<T> tree = (BspTree<T>) getTree(root);
		if (null != tree) {
			for (BspTree<T> leaf : tree.leafs) {
				successors.add(leaf.head);
			}
		}
		return successors;
	}

	/* (non-Javadoc)
	 * @see def.engine.scene.ITree#getSubTrees()
	 */
	public Collection<BspTree<T>> getSubTrees() {
		return leafs;
	}

	public static <T> Collection<T> getSuccessors(T of, Collection<BspTree<T>> in) {
		for (BspTree<T> tree : in) {
			if (tree.locate.containsKey(of)) {
				return tree.getSuccessors(of);
			}
		}
		return new ArrayList<T>();
	}

	@Override
	public String toString() {
		return printTree(0);
	}

	private static final int indent = 2;

	private String printTree(int increment) {
		String s = "";
		String inc = "";
		for (int i = 0; i < increment; ++i) {
			inc = inc + " ";
		}
		s = inc + head;
		for (BspTree<T> child : leafs) {
			s += "\n" + child.printTree(increment + indent);
		}
		return s;
	}

	public ITree<T> addLeaf(ITree<T> tree) {
		// TODO Auto-generated method stub
		return null;
	}

	public void traverseDepthFirst(def.shared.util.ITree.NodeVisitor<T> visitor) {
		// TODO Auto-generated method stub
		
	}
}