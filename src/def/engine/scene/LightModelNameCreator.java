package def.engine.scene;

import def.engine.graphics.IRenderable;

public class LightModelNameCreator extends ARenderableModelNameCreator {
	
	public static LightModelNameCreator create(){
		return new LightModelNameCreator();
	}
	
	private LightModelNameCreator(){}

	public IRenderable create(final SceneFacade sceneHandlerFacade, final String modelName) {
		return sceneHandlerFacade.addRenderableLight(modelName);
	}

}
