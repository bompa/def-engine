package def.engine.scene;

import javax.vecmath.Vector3f;

import android.util.Log;

import def.engine.graphics.IRenderable;
import def.engine.physics.AddRigidPhysicalBodyEvent;
import def.engine.physics.DefaultPhysicsObjectData;
import def.engine.physics.IPhysicsSimModifiedCallback;
import def.engine.physics.ShapeInfo;
import def.shared.interfaces.IPhysicsObjectData;
import def.util.LogHandler;

public class SceneEvents {

	/**
	 * Add static renderable event
	 * @author jonas
	 *
	 */
	public static class AddARenderableStaticShapeEvent implements IRenderableSceneModiyEvent{
		
		private final String modelName;
		private final Vector3f pos;
		private IRenderableCreator rendCreator = null;
		
		private volatile IRenderable renderable;
		
		AddARenderableStaticShapeEvent(final String modelName, final Vector3f pos){
			if(modelName == null)
				throw new IllegalArgumentException("modelName or sceneFacade null when creating AddARenderableStaticShapeEvent");
	
			this.modelName = modelName;
			if(pos == null)
				this.pos = new Vector3f();
			else
				this.pos = pos;
			
		}
	
		/**
		 * Executed concurrently with other tasks
		 */
		public void modifyScene(final SceneFacade sceneHandler) {
			rendCreator = SceneEvents.getRenderCreator(rendCreator, sceneHandler);
			renderable = SceneEvents.createRenderableUsingCreator(sceneHandler, rendCreator, modelName, modelName);
			renderable.getIPositionable().move(pos);
		}
		
		/**
		 * Executed sequentially when modification is done
		 */
		public void initSceneModification(SceneFacade sceneHandler) {
			if(renderable != null && renderable.requiresPreparationOfMaterialForRendering()){
				sceneHandler.prepareMaterial(renderable);
			}
			if(renderable != null && renderable.requiresPreparationOfVertexBuffersForRendering()){
				sceneHandler.prepareBuffers(renderable);
			}
		}
		
		public void setRenderableCreator(IRenderableCreator rendCreator) { this.rendCreator = rendCreator; }
		
		public IRenderable getRenderable() { return renderable;	}
	
		public void setPhysicsModifyDoneCallback(IPhysicsSimModifiedCallback ignore) { }
		public IRenderableSceneModiyEvent setShapeInfo(ShapeInfo ignore) { return null; } 
		public void updateModification(Object ignore) { }
	}

	/**
	 * Event handled by the scene. Used to indicate the creation of a renderable shape, to be
	 * added to the physical simulation.
	 * @author jonas
	 */
	public static class AddARenderablePhysicalShapeEvent implements IRenderableSceneModiyEvent{
		private final LogHandler l = LogHandler.createInstanceShared();
		
		private final String name;
		private final String modelName;
		private final Vector3f pos;
		private IPhysicsSimModifiedCallback physicsModifyDoneCallback = null;
		private IRenderableCreator rendCreator = null;
		private ShapeInfo shapeInfo = ShapeInfo.createCubeUnitSize();
		private IPhysicsObjectData data = null;
				
		/**
		 * Renderable which can be created and intialized in different threads, need to be volatile.
		 */
		private volatile IRenderable renderable;
		
		AddARenderablePhysicalShapeEvent(final String mn, final Vector3f p){
			if(mn == null || p == null)
				throw new IllegalArgumentException("AddARenderablePhysicalShapeEvent");
	
			name = "";
			modelName = mn;
			pos = p;
		}
		
		AddARenderablePhysicalShapeEvent(final String n, final String mn, final Vector3f p){
			if(mn == null || p == null)
				throw new IllegalArgumentException("AddARenderablePhysicalShapeEvent");
			name = n;
			modelName = mn;
			pos = p;
		}
		
		public IRenderable getRenderable(){
			return renderable;
		}
	
		/**
		 * Executed concurrently with other tasks
		 */
		public void modifyScene(SceneFacade sceneHandler) {
			rendCreator = SceneEvents.getRenderCreator(rendCreator, sceneHandler);
			renderable = SceneEvents.createRenderableUsingCreator(sceneHandler, rendCreator, name, modelName);
			
			data = DefaultPhysicsObjectData.create(renderable.getIPositionable());
			renderable.getIPositionable().move(pos);
			sceneHandler.getPhysicsSimulation().addModifyEvent(
					AddRigidPhysicalBodyEvent.create(
							data, physicsModifyDoneCallback
					).setShapeInfo(shapeInfo)
			);
		}
	
		/**
		 * Executed sequentially (in the rendering thread)
		 */
		public void initSceneModification(SceneFacade sceneHandler) {
			if(renderable != null && renderable.requiresPreparationOfMaterialForRendering()){
				sceneHandler.prepareMaterial(renderable);
			}
			if(renderable != null && renderable.requiresPreparationOfVertexBuffersForRendering()){
				sceneHandler.prepareBuffers(renderable);
			}
		}
		
		public AddARenderablePhysicalShapeEvent setPhysicsModifyDoneCallbackChainCall(IPhysicsSimModifiedCallback physicsModifyDoneCallback) {
			setPhysicsModifyDoneCallback(physicsModifyDoneCallback);
			return this;
		}
	
		public void setPhysicsModifyDoneCallback(IPhysicsSimModifiedCallback physicsModifyDoneCallback) {
			this.physicsModifyDoneCallback = physicsModifyDoneCallback;
		}
		
		public void setRenderableCreator(IRenderableCreator rendCreator) {
			this.rendCreator = rendCreator;
		}
		
		public AddARenderablePhysicalShapeEvent setShapeInfo(ShapeInfo shapeInfo){
			this.shapeInfo = shapeInfo;
			return this;
		}
	
		public void updateModification(final Object optionalData) {
			if(data != null){
				data.setOptional(optionalData);
			}
			else{
				if(l.isLevelLoggable(Log.WARN))
					l.log("Could not set optional argument in AddARenderablePhysicalShapeEvent!", Log.WARN);
			}	
		}
	}

	/**
	 * Default impl. of IRenderableCreator - creates renderable using SceneHandlingFacade
	 * @author jonas
	 */
	public static class DefautRenderableCreator implements IRenderableCreator{
		private final SceneFacade sceneFacade;
		
		public static IRenderableCreator createWithSceneFacade(final SceneFacade sf){
			if(sf == null)
				throw new IllegalArgumentException("Can not create DefautRenderableCreator with null arg");
			return new DefautRenderableCreator(sf);
		}
		
		private DefautRenderableCreator(final SceneFacade sf){
			sceneFacade = sf;
		}
	
		public IRenderable create(SceneFacade sceneHandelr, String refName, String modelName) {
			return sceneFacade.addRenderableShape(refName, modelName);
		}
	
		public IRenderable create(SceneFacade sceneHandelr, String modelName) {
			return sceneFacade.addRenderableShape(modelName);
		}
	}

	/**
	 * Evalate String name argument and call rendCreator accordingly.
	 * 
	 * @param sceneHandler the facade that manages creation of renderable
	 * @param rendCreator customizes how the sceneHandler creates the renderable
	 * @param name name of renderable, can be empty string
	 * @param modelName model name of renderable.
	 * @return created renderable
	 */
	public static IRenderable createRenderableUsingCreator(		final SceneFacade sceneHandler,
																final IRenderableCreator rendCreator, 
																final String name, 
																final String modelName){
		if(name != ""){
			return rendCreator.create(sceneHandler, name, modelName);
		}
		else{
			return rendCreator.create(sceneHandler, modelName);
		}
	}

	/**
	 * Evaluate rendCreator argument, reutrn default if null, self otherwise
	 * 
	 * @param rendCreator
	 * @param sceneHandler
	 * @return
	 */
	public static IRenderableCreator getRenderCreator(	final IRenderableCreator rendCreator, 
														final SceneFacade sceneHandler) {
		if(rendCreator == null){ 
			return new SceneEvents.DefautRenderableCreator(sceneHandler);
		}
		return rendCreator;
	}

}
