package def.engine.scene;

import android.util.Log;
import android.util.Pair;
import def.Const;
import def.engine.graphics.IRenderable;
import def.engine.graphics.RenderTargetTexture;
import def.engine.graphics.components.ANonCachableBuildAction;
import def.engine.graphics.components.IMeshComposer;
import def.engine.graphics.components.Square;
import def.engine.graphics.driver.FrameBufferData;
import def.engine.graphics.driver.FrameBufferObject;
import def.engine.graphics.material.IMaterial;
import def.engine.graphics.material.IMaterial.Properties;
import def.engine.graphics.material.ITexture;
import def.engine.graphics.material.ITexture.Settings;
import def.engine.graphics.material.Materials;
import def.engine.graphics.material.Texture;
import def.engine.graphics.resources.ResourceManager;
import def.engine.logic.math.LinjearAlg;
import def.gamedemo.CubeSkyboxRttDemoLevel;
import def.shared.interfaces.ITransform;
import def.shared.math.Radian;
import def.ui.R;
import def.ui.R.drawable;
import def.util.IntPair;
import def.util.LogHandler;

public class ContentConstruction {

	public static IRenderable createSquareRTT(final SceneFacade scene, final ResourceManager matMgr, IntPair renderWinsSz) {
		
		final Pair<Integer, ITexture> t = matMgr.createTextureCustom(renderWinsSz.first, renderWinsSz.second, ITexture.RenderType.TEX_RTT);
		
		final IMaterial material = Materials.createBasicTexturingNoLightning(t.second);
		
		final float scaledW = (float)renderWinsSz.first * 2.0f / 1000.0f; 
		final float scaledH = (float)renderWinsSz.second * 2.0f / 1000.0f;
		IRenderable r = scene.addRenderableShape(Const.SHAPE_SQUARE, new ANonCachableBuildAction(){
				public void build(IMeshComposer buildComposer) {
					Square.createMesh(buildComposer, material, scaledW, scaledH);
				}
			}, material); 
	
		r.setName("RTT-square");
		
		final RenderTargetTexture renderTarget = RenderTargetTexture.create("CustomRTT", t.second);
		final Settings texureSettings = t.second.getSettings();
		
		final FrameBufferObject fbo = renderTarget.getFrameBufferObject();
		fbo.addTargetBuffer(FrameBufferData.createTextureFrameBufferData(t.second));
		fbo.addTargetBuffer(FrameBufferData.createDepthFrameBufferData(texureSettings.getWidth(), texureSettings.getHeight()));
		
		scene.addRenderTarget(renderTarget);
		
		return r;
	}

	public static IRenderable createBillboardAnimation(final SceneFacade sceneFacade) {
		final ResourceManager matMgr = sceneFacade.getResourceManager();
		Pair<Long, ITexture> idTexPair = matMgr.createAnimatedTexture(R.drawable.explosion0, 8);
		
		IRenderable explo = sceneFacade.addRenderableShape(Const.SHAPE_BB, "bilboard-explosion", idTexPair.second);
		
		final IMaterial material = explo.getMaterial();
		final Properties p = material.getProperties();
		p.setAlpha(0.8f);
		
		return explo;
	}

	public static IRenderable createSkybox(final SceneFacade sceneFacade) {
		final IRenderable skybox = sceneFacade.addRenderableShape(Const.SHAPE_SKYBOX, Texture.create(R.drawable.sb_space_fr));
		final IMaterial skyboxMaterial = skybox.getMaterial();
		skyboxMaterial.addTexture(Texture.create(R.drawable.sb_space_bk));
		skyboxMaterial.addTexture(Texture.create(R.drawable.sb_space_dn));
		skyboxMaterial.addTexture(Texture.create(R.drawable.sb_space_lf));
		skyboxMaterial.addTexture(Texture.create(R.drawable.sb_space_rt));
		skyboxMaterial.addTexture(Texture.create(R.drawable.sb_space_up));
		
		return skybox;
	}

	public static IRenderable createGroundShape(SceneFacade sceneFacade) {
		LogHandler l = LogHandler.createInstanceShared(ContentConstruction.class);
		
		final IMaterial groundMat = Materials.createWithPerFragLightNormalMapShaders(Texture.create(R.drawable.grass_texture));
		groundMat.addTexture(Texture.create(R.drawable.grass_texture_nm));
		//groundMat.setTexturesScale(new FloatPair(2.0f, 2.0f));
		
		final IRenderable ground = sceneFacade.addRenderableShape(Const.SHAPE_SQUARE, new ANonCachableBuildAction(){
			public void build(IMeshComposer buildComposer) {
				Square.createDefaultMesh(buildComposer, groundMat);
			}
		}, groundMat);
	
		if(ground == null){
			if(l.isLevelLoggable(Log.WARN))
				l.log("Could not create ground renderable objedt", Log.WARN);
			return null;
		}
		
		final ITransform grounPos = ground.getIPositionable();
		grounPos.move(CubeSkyboxRttDemoLevel.GROUND_INIT_POS);
		grounPos.rotate(LinjearAlg.X_AXIS, new Radian(-1.57f));
		grounPos.setScale(100.0f);
		
		return ground;
	}

}
