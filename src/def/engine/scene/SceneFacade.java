package def.engine.scene;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

import javax.vecmath.Vector3f;

import android.util.Log;
import android.util.Pair;

import def.Const;
import def.engine.Engine;
import def.engine.control.ICamera;
import def.engine.execution.ARenderEventCoordinator;
import def.engine.execution.IRenderPrepWork;
import def.engine.execution.ITaskExecutor;
import def.engine.graphics.ARenderable;
import def.engine.graphics.ARenderableFactory;
import def.engine.graphics.IRenderSequenceEvent;
import def.engine.graphics.IRenderable;
import def.engine.graphics.RenderTargetTexture;
import def.engine.graphics.RenderTargetWindow;
import def.engine.graphics.components.ANonCachableBuildAction;
import def.engine.graphics.components.IBuildAction;
import def.engine.graphics.components.IMeshComposer;
import def.engine.graphics.components.Light;
import def.engine.graphics.components.LightFactory;
import def.engine.graphics.components.RenderableShapeFactory;
import def.engine.graphics.components.Square;
import def.engine.graphics.driver.FrameBufferData;
import def.engine.graphics.driver.FrameBufferObject;
import def.engine.graphics.driver.IRenderTarget;
import def.engine.graphics.driver.Graphics.RenderSequenceAction;
import def.engine.graphics.material.IMaterial;
import def.engine.graphics.material.ITexture;
import def.engine.graphics.material.ITexture.Settings;
import def.engine.graphics.material.Materials;
import def.engine.graphics.material.Texture;
import def.engine.graphics.resources.ResourceManager;
import def.engine.physics.DefaultPhysicsObjectData;
import def.engine.physics.IPhysicsSimModifiedCallback;
import def.engine.physics.IPhysicsSimulation;
import def.engine.physics.ShapeInfo;
import def.engine.physics.driver.BulletPhyics;
import def.engine.physics.driver.JBulletPhyics;
import def.engine.physics.driver.PhysicsSimulationWork;
import def.gamedemo.CubeDemoLevel;
import def.shared.interfaces.ITransform;
import def.ui.R;
import def.util.IntPair;
import def.util.LogHandler;

public class SceneFacade extends ARenderSequenceListeningScene {
	private final LogHandler l = LogHandler.createInstanceShared();
	private final IScene scene = new Scene();
	private final ResourceManager resourceMgr;
	private final ARenderableFactory<ARenderable> absRenderablefactory;
	private final ARenderableFactory<Light> lightFactory;
	private final IPhysicsSimulation physics;
	private final IRenderPrepWork<?, ?> physicsSimulation;
	private final static Vector3f defaultGroundPosition = new Vector3f(0.f, -55.f, 0.f);
	
	private final LinkedBlockingQueue<ISceneModifyEvent> modifyEvents = new LinkedBlockingQueue<ISceneModifyEvent>();
	
	public static final int INIT_DATA_FOR_DYN_CREATE_OBJS = 50;
	
	/**
	 * Create a Scene-Facade with an instance which listens for the scenes render requests.
	 * @param listensOnRenderEvents
	 */
	public static SceneFacade createDefault(final ARenderEventCoordinator renderEventListener, final ResourceManager matmgr){
		return createWithBulletPhysics(renderEventListener, matmgr);
	}
	
	public static SceneFacade createWithBulletPhysics(final ARenderEventCoordinator renderEventListener, final ResourceManager matmgr){
		return new SceneFacade(renderEventListener, matmgr, BulletPhyics.createPhysicsWithGround(defaultGroundPosition));
	}
	
	public static SceneFacade createWithJBulletPhysics(final ARenderEventCoordinator renderEventListener, final ResourceManager matmgr){
		return new SceneFacade(renderEventListener, matmgr, JBulletPhyics.createPhysicsWithGround(defaultGroundPosition));
	}
	
	public static SceneFacade create(final ARenderEventCoordinator renderEventListener, final ResourceManager matmgr, final IPhysicsSimulation ps){
		return new SceneFacade(renderEventListener, matmgr, ps);
	}
	
	private SceneFacade(final ARenderEventCoordinator renderEventListener, final ResourceManager matmgr, final IPhysicsSimulation physics){
		this.resourceMgr = matmgr;
		this.absRenderablefactory = RenderableShapeFactory.create(resourceMgr);
		this.lightFactory = LightFactory.create(resourceMgr);
		this.absRenderablefactory.setDefualtRenderEvListener(renderEventListener);
		this.lightFactory.setDefualtRenderEvListener(renderEventListener);

		// Set default physics
		this.physics = physics;
		this.physicsSimulation = PhysicsSimulationWork.create(physics);
		physics.init();		
	}
	
	public void addRenderTarget( final IRenderTarget rt ){
		absRenderablefactory.getDefualtRenderEvListener().addRenderTarget(rt);
	}
	
	public boolean removeRenderTarget( final IRenderTarget rt ){
		return absRenderablefactory.getDefualtRenderEvListener().removeRenderTarget(rt);
	}

	
	public ResourceManager getResourceManager(){
		return this.resourceMgr;
	}
	
	/**
	 * Initalize created light by creating a visual representation wihch share the positoin information 
	 * with the actual light source.
	 * @param lights
	 */
	private void initCreatedLights(final List<Light> lights){
		for(Light l : lights){
			initCreatedLight(l);
		}
	}
	
	private void initCreatedLight(final Light l){
		final String visualReprName = Light.makeVisualReprName(l.getName());
		IRenderable r = absRenderablefactory.createRenderable(	visualReprName, 
																Const.SHAPE_EMITTING_CUBE, 
																l.getIPositionable()
		);
		if(verifyAndFinishRenderableCreation(r, Const.SHAPE_EMITTING_CUBE)){
			r.getIPositionable().setScale(Const.LIGHT_REPR_SCALE);
			l.setRepresentation(r);
		}
		
		verifyAndFinishLightCreation(l, l.getName());
	}
	
	/**
	 * Create and init light(add representation by default)
	 * @param name
	 * @param modelName
	 * @return
	 */
	public IRenderable addRenderableLight(final String name, final String modelName){
		Light l = lightFactory.createRenderable(name, modelName);
		initCreatedLight(l);
		return l;
	}
	
	/**
	 * Create and inti light witout suppling refernece name of object
	 * @param modelName
	 * @return
	 */
	public IRenderable addRenderableLight(final String modelName){
		final Light l = lightFactory.createRenderable(modelName);
		initCreatedLight(l);
		return l;
	}
	
	/**
	 * Create a normal renderable shape without initalizing the material
	 * @param name
	 * @param modelName
	 * @return
	 */
	public IRenderable addRenderableShape(final String name, final String modelName){
		ARenderable r = absRenderablefactory.createRenderable(name, modelName);
		verifyAndFinishRenderableCreation(r, modelName);
		return r;
	}
		
	/**
	 * Create a normal renderable shape without initalizing the material
	 * @param modelName
	 * @return created renderable if sucessfull, null otherwise
	 */
	public IRenderable addRenderableShape(final String modelName){
		IRenderable r = absRenderablefactory.createRenderable(modelName);
		verifyAndFinishRenderableCreation(r, modelName);
		return r;		
	}
	
	/**
	 * Create Create a normal renderable shape and suppling a custom texture.
	 * 
	 * @param modelName
	 * @param texture
	 * @return
	 */
	public IRenderable addRenderableShape(final String modelName, final String refName, final ITexture texture){
			IRenderable r = absRenderablefactory.createRenderable(refName, modelName, texture);
			verifyAndFinishRenderableCreation(r, modelName);
			return r;
	}
	
	/**
	 * Create a normal renderable shape by suppling a custom build action.
	 * @param modelName
	 * @param buildAction
	 * @return created renderable if sucessfull, null otherwise
	 */
	public IRenderable addRenderableShape(final String modelName, final IBuildAction buildAction){
		IRenderable r = absRenderablefactory.createRenderable( modelName, buildAction);
		verifyAndFinishRenderableCreation(r, modelName);
		return r;
	}
	
	/**
	 * Create a normal renderable shape using custom buildAction and suppling a custom texture.
	 * 
	 * @param modelName
	 * @param texture
	 * @param buildAction
	 * @return
	 */
	public IRenderable addRenderableShape(final String modelName, final IBuildAction buildAction, final ITexture texture){
			IRenderable r = absRenderablefactory.createRenderable(modelName, buildAction, texture);
			verifyAndFinishRenderableCreation(r, modelName);
			return r;
	}
	
	/**
	 * Create a renderable using its modelName and supplying a texture
	 * 
	 * @param modelName
	 * @param texture
	 * @return
	 */
	public IRenderable addRenderableShape(final String modelName, final ITexture texture){
		IRenderable r = absRenderablefactory.createRenderable(modelName, texture);
		verifyAndFinishRenderableCreation(r, modelName);
		return r;
	}
	
	/**
	 * Create renderable with custom material
	 * @param modelName
	 * @param material
	 * @return
	 */
	public IRenderable addRenderableShape(final String modelName, final IMaterial material){
		IRenderable r = absRenderablefactory.createRenderable(modelName, material);
		verifyAndFinishRenderableCreation(r, modelName);
		return r;
	}
	
	/**
	 * Create renderable with custom buildaction and material
	 * @param modelName
	 * @param buildAction
	 * @param material
	 * @return
	 */
	public IRenderable addRenderableShape(final String modelName, final IBuildAction buildAction, final IMaterial material){
		IRenderable r = absRenderablefactory.createRenderable(modelName, buildAction, material);
		verifyAndFinishRenderableCreation(r, modelName);
		return r;
	}
	
	public IRenderable addRenderableShape(final String modelName, final String refName, final IBuildAction buildAction){
		IRenderable r = absRenderablefactory.createRenderable(refName, modelName, buildAction);
		verifyAndFinishRenderableCreation(r, modelName);
		return r;
	}
		
	/**
	 * Remove renderable r from scene
	 * @param r renderable to remove
	 */
	public void removeRenderableFromScene(final IRenderable r){
		boolean res = scene.removeSceneRenderable(r);
		if(!res && l.isLevelLoggable(Log.WARN)){
			l.log("Failed attempt to remove renderable from scene", Log.WARN);
		}
	}
	
	/**
	 * Add custom event to modify the scene asynchronously
	 * @param event
	 */
	public void modifySceneAsync(final ISceneModifyEvent event){
		modifyEvents.add(event);
	}
	
	/**
	 * Create new renderable and add it to physics simulation - done asynchronously. Async event Handled from main render thread
	 * when render sends a pre-render event, so it is done before the coming scene rendering.
	 * @param modelName
	 * @param pos
	 */
	public void createRenderableShapeAsync(final String modelName, final Vector3f pos){
		modifySceneAsync(createAddPhysicalRendableWithGeneratedNameEvent(modelName, pos));
	}
	
	/**
	 * @see createRenderableShapeAsync(final String modelName, final Vector3f pos)
	 * @param name
	 * @param modelName
	 * @param pos
	 */
	public void createRenderableShapeAsync(final String name, final String modelName, final Vector3f pos){
		modifySceneAsync(createAddPhysicalRendableEvent(name, modelName, pos));
	}
	
	/**
	 * Add a renderable to the physical simulation of the world
	 * @param r renderable which positionable will be connected to physics simulations
	 */
	public void addToPhysicsSimulation(final IRenderable r){
		if(r != null)
			addToPhysicsSimulation(r.getIPositionable());
	}
	
	public void removeFromPhysicsSimulationAsync(Object bodyRefID){
		if(bodyRefID != null)
			physics.removeRigidBodyAsync(bodyRefID);
	}
	
	/**
	 * Add a renderable to the physical simulation of the world - asynchronously.
	 * @param r renderable which positionable will be connected to physics simulations
	 */
	public void addToPhysicsSimulationAsync(final IRenderable r, final IPhysicsSimModifiedCallback physicsModifyDoneCallback){
		if(r != null && r.getIPositionable() != null)
			getPhysicsSimulation().addRigidPhysicalBodyAsync(DefaultPhysicsObjectData.create(r.getIPositionable()), physicsModifyDoneCallback);
	}
	
	/**
	 * Add a positionable to be part of the physical simulation of the world
	 * @param p positionable to be postioned by physics simulation
	 */
	public void addToPhysicsSimulation(final ITransform p){
		if(p != null){
			getPhysicsSimulation().addRigidPhysicalBody(DefaultPhysicsObjectData.create(p), ShapeInfo.createCubeUnitSize()); //Physics.getColisionShapeBoxUnitSize());
		}
	}
	
	private <E extends ARenderable> void produceAllFromNames(List<E> producedList, String[] modelnames, 
			ARenderableFactory<E> factory)
	{
		for(String model : modelnames){
			E r = factory.createRenderable(model, Texture.create(R.drawable.tile));
			producedList.add(r);
			verifyAndFinishRenderableCreation(r, model);
		}
	}
		
	/**
	 * Provides scene with intial scene content, such as as static geometry.
	 * @param provider of inital scene content
	 */
	public void produceInitialSceneContent(SceneContentProvider provider){
		addSceneContent(provider.getProducedContent(), provider.getShapes(), provider.getLights());
	}

	private void addSceneContent(List<ARenderable> producedList, String[] shapes, String[] lights){
		List<Light> producedLightList = new ArrayList<Light>(lights.length);
		produceAllFromNames(producedLightList, lights, lightFactory);
		initCreatedLights(producedLightList);
		
		producedList.addAll(producedLightList);
		addSceneContent(producedList, shapes);
	}
	public  void addSceneContent(List<ARenderable> producedList, String[] shapes){
		produceAllFromNames(producedList, shapes, absRenderablefactory);
	}
		
	/**
	 * Verify creation of renderable and add it to the scene
	 * @param rend created renderable object
	 * @param modelName 
	 * @return
	 */
	private boolean verifyAndFinishRenderableCreation(IRenderable rend, String modelName){
		if(rend == null){
			l.log("Failed to create renderable " + modelName  + "!");
			return false;
		}
		else{
			l.log("Created renderable " + rend.getName() + " using model " + modelName + "...");
			scene.addSceneRenderable(rend);
			return true;
		}
	}
	
	/**
	 * Verify creation of light and add it to the scene
	 * @param l2
	 * @param name
	 * @return
	 */
	private boolean verifyAndFinishLightCreation(Light light, String modelName) {
		if(light == null){
			l.log("Failed to create renderable Light " + modelName  + "!");
			return false;
		}
		else{
			l.log("Created renderable Light " + light.getName() + " using model " + modelName + "...");
			scene.addSceneLight(light);
			return true;
		}		
	}

	public ICamera getCamera() {
		return scene.getCurrentCamera();
	}
	
	public boolean prepareBuffers(IRenderable r){
		if(r != null){
			resourceMgr.prepareBuffersForRendering(r);
			return true;
		}
		else{
			if(l.isLevelLoggable(Log.WARN))
				l.log("Could not get renderinfo to prepare buffers for rendering " + r, Log.WARN);
			return false;
		}
	}
	
	public boolean prepareMaterial(IRenderable r){
		IMaterial material = r.getMaterial();
		if(material != null){
			resourceMgr.prepareMaterialForRendering(material);
			return true;
		}
		else{
			if(l.isLevelLoggable(Log.WARN))
				l.log("Could not get material to prepare for rendering for renderable " + r, Log.WARN);
			return false;
		}
	}
	
	private boolean prepareFrameBuffers(Collection<IRenderTarget> allRenderTargets) {
		
		for (final Iterator<IRenderTarget> iterator = allRenderTargets.iterator(); iterator.hasNext(); ) {
			
			final IRenderTarget t = iterator.next();
			
			if(!prepareRenderTargetBuffer(t)){
				if(l.isLevelLoggable(Log.WARN))
					l.log("Rendertarget " + t + " not supported by current render, or other error. Removing..");
				
				return false;
			}
		}
		return true;
	}

	
	private boolean prepareRenderTargetBuffer(IRenderTarget t) {
		if(t.requiresPrepartion()){
			if(resourceMgr.prepareRenderTargetForRendering(t)){
				return absRenderablefactory.getDefualtRenderEvListener().enableRenderTarget(t);
			}
			else{
				return false;
			}
				
		}

		return true;
	}

	/**
	 * Get a generic renderable object(could be a light source) which is in the scene
	 * @param refName the reference name of the object
	 * @return
	 */
	public IRenderable getRenderalbeInScene(final String refName){
		return scene.getRenderalbeInScene(refName);
	}
	
	/**
	 * Get a renderable light which is in the scene
	 * @param refName the reference name of the object
	 * @return
	 */
	public Light getLightInScene(final String refName){
		return scene.getLightInScene(refName);
	}
	
	/**
	 * Handles major scene events through updates from the state of the render sequence
	 */
	@Override
	public void renderSequenceUpdate(IRenderSequenceEvent rse) {
		if(rse.getAction() == RenderSequenceAction.REND_ACTION_SURFACE_CREATED){
			final Collection<IRenderable> renderableList = scene.getRenderablesInScene();
			resourceMgr.generateTextures(renderableList.size() + INIT_DATA_FOR_DYN_CREATE_OBJS);
			
			for(IRenderable r : renderableList){
				// Materials must be prepared before frame buffers
				if(r.requiresPreparationOfMaterialForRendering()){
					prepareMaterial(r);
				}
				
				if(r.requiresPreparationOfVertexBuffersForRendering()){
					prepareBuffers(r);
				}
				r.requestRendering();
			}
			prepareFrameBuffers(absRenderablefactory.getDefualtRenderEvListener().getAllRenderTargets());
			
			absRenderablefactory.setRequestRenderingOnCreation(true);
			lightFactory.setRequestRenderingOnCreation(true);
		}
		else if(rse.getAction() == RenderSequenceAction.REND_ACTION_PRE_WORK_SYNCHED_DRAWING){
			final ITaskExecutor taskExec = Engine.instance().getTaskExecutor();
			if(modifyEvents.size() > 0){
				taskExec.submitWork(SceneModificationWork.create(modifyEvents, this));
			}
			taskExec.submitWork(physicsSimulation);
		}
	}
	
	public IPhysicsSimulation getPhysicsSimulation(){
		return physics;
	}

	/**
	 * Create event instance.
	 * @param p
	 * @param mn
	 * @return
	 */
	public static IRenderableSceneModiyEvent createAddPhysicalRendableWithGeneratedNameEvent(final String mn, final Vector3f p){
		return new SceneEvents.AddARenderablePhysicalShapeEvent(mn, p);
	}
	
	public static IRenderableSceneModiyEvent createAddPhysicalRendableEvent(final String n, final String mn, final Vector3f p){
		return new SceneEvents.AddARenderablePhysicalShapeEvent(n, mn, p);
	}
	
	public static IRenderableSceneModiyEvent createAddPhysicalModifyDoneCallbackRendable(final IPhysicsSimModifiedCallback physicsModifyDoneCallbackfinal, String mn, final Vector3f p){
		return new SceneEvents.AddARenderablePhysicalShapeEvent(mn, p);
	}
	
	
	public static SceneEvents.AddARenderableStaticShapeEvent createAddStaticShapeEventDefaultPos(final String modelName, final IRenderableCreator creator){
		final SceneEvents.AddARenderableStaticShapeEvent ev = new SceneEvents.AddARenderableStaticShapeEvent(modelName, null);
		ev.setRenderableCreator(creator);
		return ev;
	}
	
	public static IRenderableSceneModiyEvent createAddStaticShapeEvent(final String modelName, final Vector3f pos, final IRenderableCreator creator){
		final SceneEvents.AddARenderableStaticShapeEvent ev = new SceneEvents.AddARenderableStaticShapeEvent(modelName, pos);
		ev.setRenderableCreator(creator);
		return ev;
	}
	
	public static IRenderableSceneModiyEvent createAddStaticShapeEvent(final String modelName, final Vector3f pos){
		return new SceneEvents.AddARenderableStaticShapeEvent(modelName, pos);
	}
	
	/**
	 * Create instance of Scene content provider.
	 * @return
	 */
	public static SceneContentProvider creatSceneContentProvider(){
		return new SceneContentProvider();
	}
	/**
	 * Class wich can be supplied with info about content to create in the scene,
	 * as well as information about created data when used with the 
	 * SceneMainFacade::produceInitalSceneContent method.
	 * @author jonas
	 */
	public static class SceneContentProvider{
		private static final String[] NO_DATA = {};
		private String[] lights_ = NO_DATA;
		private String[] shapes_ = NO_DATA;
		private final List<ARenderable> producedContent_ = new ArrayList<ARenderable>();
		
		private SceneContentProvider(){};
		
		private String[] getLights(){
			return lights_;
		}
		private String[] getShapes(){
			return shapes_;
		}
		private List<ARenderable> getProducedContent(){
			return producedContent_;
		}

		public List<ARenderable> getContentProducedFromProvider(){
			if(producedContent_.size() < 1)
				return new ArrayList<ARenderable>();
			List<ARenderable> retCopy = new ArrayList<ARenderable>(producedContent_.size());
			retCopy.addAll(producedContent_);
			return retCopy; 
		}
		public SceneContentProvider addLights(String... lights){
			lights_ = lights;
			return this;
		}
		public SceneContentProvider addShapes(String... shapes){
			shapes_ = shapes;
			return this;
		}
	}
}
