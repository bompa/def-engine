package def.engine.scene;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import android.util.Log;

import def.engine.control.FlyCamera;
import def.engine.control.ICamera;
import def.engine.graphics.IRenderable;
import def.engine.graphics.components.Light;
import def.shared.util.ITree;
import def.util.LogHandler;

/**
 * The scene state
 * 
 * @author jonas
 *
 */
public class Scene implements IScene{
	private final LogHandler l = LogHandler.createInstanceShared();
	
	private final ICamera sceneCam = new FlyCamera();
	private final ConcurrentHashMap<String, IRenderable> renderablesMap = new ConcurrentHashMap<String, IRenderable>();
	private final ConcurrentHashMap<String, Light> lightsMap = new ConcurrentHashMap<String, Light>();

	public ICamera getCurrentCamera() {
		return sceneCam;
	}

	public Collection<IRenderable> getRenderablesInScene() {
		return renderablesMap.values();
	}
	
	public Collection<Light> getLightsInScene() {
		return lightsMap.values();
	}

	public void addSceneRenderable(final IRenderable r) {
		IRenderable prev = renderablesMap.putIfAbsent(r.getName(), r);
		logNameClash(prev);
	}

	private void logNameClash(IRenderable prev) {
		if(prev != null){
			if(l.isLevelLoggable(Log.ERROR))
				l.log("Waring - name clash in renderable names. Renderable not added to scene.", Log.ERROR);
		}
	}
	
	public void addSceneLight(Light l) {
		Light prev = lightsMap.putIfAbsent(l.getName(), l);
		logNameClash(prev);
		
	}
	
	public boolean removeSceneRenderable(final IRenderable r){
		if(renderablesMap.remove(r.getName()) == null){
			return (lightsMap.remove(r.getName()) != null);
		}
			
		return true;
	}
	
	public IRenderable getRenderalbeInScene(final String refName){
		IRenderable rend = renderablesMap.get(refName);
		if(rend == null)
			rend = lightsMap.get(refName);
		
		return rend;
	}
	
	public Light getLightInScene(final String refName){
		return lightsMap.get(refName);
	}

	public long nObjectsInSceneToRender() {
		return renderablesMap.size() + lightsMap.size();
	}
	
	public ITree<IRenderable> createSceneGraph(){
		return null;
	}
	
	static void buildBSPTree(BspTree<IRenderable> tree, List<IRenderable> polygons)
	{/*
	   polygon   *root = polygons.Get_From_List ();
	   tree->partition = root->Get_Plane ();
	   tree->polygons.Add_To_List (root);
	   List<IRenderable> front_list, back_list;
	   polygon   *poly;
	   while ((poly = polygons.Get_From_List ()) != 0)
	   {
	      int   result = tree->partition.Classify_Polygon (poly);
	      switch (result)
	      {
	         case COINCIDENT:
	            tree->polygons.Add_To_List (poly);
	            break;
	         case IN_BACK_OF:
	            back_list.Add_To_List (poly);
	            break;
	         case IN_FRONT_OF:
	            front_list.Add_To_List (poly);
	            break;
	         case SPANNING:
	            polygon   *front_piece, *back_piece;
	            Split_Polygon (poly, tree->partition, front_piece, back_piece);
	            back_list.Add_To_List (back_piece);
	            front_list.Add_To_List (front_piece);
	            break;
	      }
	   }
	   if ( ! front_list.Is_Empty_List ())
	   {
	      tree->front = new BSP_tree;
	      Build_BSP_Tree (tree->front, front_list);
	   }
	   if ( ! back_list.Is_Empty_List ())
	   {
	      tree->back = new BSP_tree;
	      Build_BSP_Tree (tree->back, back_list);
	   }*/
	}
}
