package def.engine.scene;

import def.engine.graphics.IRenderable;

public class RenderableRefModelNameCreator implements IRenderableCreator {
	
	public static RenderableRefModelNameCreator create(){
		return new RenderableRefModelNameCreator();
	}
	
	private RenderableRefModelNameCreator(){ }

	public IRenderable create(final SceneFacade sceneHandlerFacade, final String modelName) {
		return sceneHandlerFacade.addRenderableShape(modelName, modelName);
	}

	public IRenderable create(SceneFacade sceneHandlerFacade, String refName, String modelName) {
		return sceneHandlerFacade.addRenderableShape(refName, modelName);
	}
}
