package def.engine.scene;

public interface ISceneModifyEvent {
	public void modifyScene(SceneFacade sceneHandler);
	public void initSceneModification(SceneFacade sceneHandler);
}
