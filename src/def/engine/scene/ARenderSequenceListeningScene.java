package def.engine.scene;

import def.engine.graphics.IRenderSequenceEvent;
import def.engine.graphics.RenderSequenceListener;

public abstract class ARenderSequenceListeningScene  extends RenderSequenceListener{

	public abstract void renderSequenceUpdate(IRenderSequenceEvent rsa);
}
