package def.engine.scene;

import java.util.Collection;

import def.engine.control.ICamera;
import def.engine.graphics.IRenderable;
import def.engine.graphics.components.Light;

public interface IScene {
	
	public ICamera getCurrentCamera();
	
	/**
	 * Get a reference to the collection of renderables in the scene. Should not be modified.
	 * @return
	 */
	public Collection<IRenderable> getRenderablesInScene();
	
	/**
	 * Get a reference to the collection of subset of renderables in the scene being light sources. Should not be modified.
	 * @return
	 */
	public Collection<Light> getLightsInScene();
	
	public void addSceneRenderable(IRenderable r);
	public void addSceneLight(Light r);
	
	/**
	 * Get a renderable object in the scene, could be any object including a light source
	 */
	public IRenderable getRenderalbeInScene(final String refName);
	
	/**
	 * Get the total number of renderables, including light sources, in the scene
	 * @return
	 */
	public long nObjectsInSceneToRender();
	
	/**
	 * Remove a single renderable object form the scene, could be any object including a light source
	 * @param r renderable to remove
	 * @return true on success, false if failed to remove renderable.
	 */
	public boolean removeSceneRenderable(final IRenderable r);
	
	/**
	 * Get a light from the scene, only looking in the subset of renderables being light sources
	 * @param refName
	 * @return
	 */
	public Light getLightInScene(final String refName);
}
