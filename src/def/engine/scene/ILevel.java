package def.engine.scene;

import android.os.Bundle;
import def.engine.graphics.ACameraRenderer;
import def.gamedemo.GameStateManager;
import def.util.IntPair;

public interface ILevel {

	/**
	 * Get the inital objects (shapes, lights, ...) the scene is created with.
	 */
	public String[] getInitialSceneContent();

	public void createScene(	final SceneFacade sceneFacade,
								final ACameraRenderer renderer, 
								final GameStateManager gameManager,
								final IntPair sceneWindowSize,
								final Bundle extras);
}