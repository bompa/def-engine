package def.engine.scene;

import def.engine.graphics.IRenderable;
import def.engine.graphics.components.IBuildAction;
import def.engine.graphics.material.ITexture;

public class RenderableVertexBuilderCreator extends ARenderableModelNameCreator {
	
	private final IBuildAction action;
	/**
	 * Option texture which can be sent when creating renderable.
	 */
	private ITexture texture = null;
	
	public static RenderableVertexBuilderCreator create(final IBuildAction action){
		return new RenderableVertexBuilderCreator(action);
	}
	
	private RenderableVertexBuilderCreator(final IBuildAction action){
		if(action == null)
			throw new IllegalArgumentException("IBuildAction or SceneHandlingFacade can not be null in ARenderableVertexBuilderCreator");
				
		this.action = action;
	}

	public IRenderable create(final SceneFacade sceneHandlerFacade, final String modelName) {
		if(texture == null)
			return sceneHandlerFacade.addRenderableShape(modelName, action);
		
		return sceneHandlerFacade.addRenderableShape(modelName, action, texture);
	}

	public ITexture getTexture() {
		return texture;
	}

	public RenderableVertexBuilderCreator setTexture(ITexture texture) {
		this.texture = texture;
		return this;
	}

}
