package def.engine.scene;

import def.engine.graphics.IRenderable;

/**
 * Interface used to create a renderable
 * @author jonas
 *
 */
public interface IRenderableCreator {
	IRenderable create(final SceneFacade sceneHandler, final String refName, final String modelName);
	IRenderable create(final SceneFacade sceneHandler, final String modelName);
}
