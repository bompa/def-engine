package def.engine.scene;

import java.util.concurrent.LinkedBlockingQueue;

import def.engine.execution.IRenderPrepResult;
import def.engine.execution.IRenderPrepWork;

public class SceneModificationWork implements IRenderPrepWork<Void, Void>{
	
	private final LinkedBlockingQueue<ISceneModifyEvent> modifyEvents;
	private final SceneFacade sceneFacade;
	private final IRenderPrepResult<Void> result;
	
	public static SceneModificationWork create(final LinkedBlockingQueue<ISceneModifyEvent> modifyEvents, 
			final SceneFacade sceneFacade){
		return new SceneModificationWork(modifyEvents, sceneFacade);
	}
	
	private SceneModificationWork (final LinkedBlockingQueue<ISceneModifyEvent> modifyEvents, 
			final SceneFacade sceneFacade){
		if(modifyEvents == null || sceneFacade == null)
			throw new IllegalArgumentException();
		
		this.modifyEvents = modifyEvents;
		this.sceneFacade = sceneFacade;
		this.result = SceneModificationResult.create(modifyEvents, sceneFacade);
	}

	public IRenderPrepResult<Void> work(Void arg) {
		for(ISceneModifyEvent event : modifyEvents){
			event.modifyScene(sceneFacade);
		}
		return result;
	}
}
