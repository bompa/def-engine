package def.engine.scene;

import def.engine.graphics.IRenderable;
import def.engine.physics.IPhysicsSimModifiedCallback;
import def.engine.physics.ShapeInfo;


public interface IRenderableSceneModiyEvent extends ISceneModifyEvent {
	public IRenderable getRenderable();
	public void setRenderableCreator(IRenderableCreator rendCreator);
	
	/**
	 * Implementation must support null values of this callback.
	 * 
	 * Impl. can allow for a callback to be registered which is called if renderable creation
	 * triggers a physics simulation modification.
	 * 
	 * @param physicsModifyDoneCallback
	 */
	public void setPhysicsModifyDoneCallback(IPhysicsSimModifiedCallback physicsModifyDoneCallback);
	
	public IRenderableSceneModiyEvent setShapeInfo(ShapeInfo shapeInfo);
	
	/**
	 * Update a modification with user data
	 * @param optionalData
	 */
	public void updateModification(final Object optionalData);
}
