package def.engine.scene;

import java.util.concurrent.LinkedBlockingQueue;

import def.engine.execution.IRenderPrepResult;

public class SceneModificationResult implements IRenderPrepResult<Void>{
	private final LinkedBlockingQueue<ISceneModifyEvent> modifyEvents;
	private final SceneFacade sceneFacade;
	
	public static SceneModificationResult create(final LinkedBlockingQueue<ISceneModifyEvent> modifyEvents, 
			final SceneFacade sceneFacade){
		return new SceneModificationResult(modifyEvents, sceneFacade);
	}
	
	private SceneModificationResult (final LinkedBlockingQueue<ISceneModifyEvent> modifyEvents, 
			final SceneFacade sceneFacade){
		if(modifyEvents == null || sceneFacade == null)
			throw new IllegalArgumentException();
		
		this.modifyEvents = modifyEvents;
		this.sceneFacade = sceneFacade;
	}

	public Void finishWork() {
		for(ISceneModifyEvent event : modifyEvents){
			event.initSceneModification(sceneFacade);
		}
		modifyEvents.clear();
		return null;
	}
}
