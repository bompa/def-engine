package def.engine.scene;

import def.engine.graphics.IRenderable;

public abstract class ARenderableModelNameCreator implements IRenderableCreator{
	public IRenderable create(final SceneFacade sceneHandler, final String refName, final String modelName){
		return create(sceneHandler, modelName);
	}
}
