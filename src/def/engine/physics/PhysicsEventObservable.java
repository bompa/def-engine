package def.engine.physics;

import javax.vecmath.Vector3f;

import def.shared.util.ConcurrentObservable;

public class PhysicsEventObservable extends ConcurrentObservable<IPhysicsEventEvent<Vector3f>> {
	public static PhysicsEventObservable create(){
		return new PhysicsEventObservable();
	}
	
	private PhysicsEventObservable(){}
	
	public PhysicsEventObservable(PhysicsEventListener listener){
		registerObserver(listener);
	}
}
