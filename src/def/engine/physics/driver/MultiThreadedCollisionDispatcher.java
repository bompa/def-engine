package def.engine.physics.driver;

import android.util.Log;

import com.bulletphysics.collision.broadphase.BroadphasePair;
import com.bulletphysics.collision.broadphase.CollisionAlgorithm;
import com.bulletphysics.collision.broadphase.Dispatcher;
import com.bulletphysics.collision.broadphase.DispatcherInfo;
import com.bulletphysics.collision.broadphase.OverlapCallback;
import com.bulletphysics.collision.broadphase.OverlappingPairCache;
import com.bulletphysics.collision.dispatch.CollisionConfiguration;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.DefaultNearCallback;
import com.bulletphysics.collision.dispatch.NearCallback;
import com.bulletphysics.collision.narrowphase.PersistentManifold;
import com.bulletphysics.util.ObjectArrayList;

import def.engine.execution.ILoopComputation;
import def.engine.execution.ParallelForTaskBuilder;
import def.engine.Engine;
import def.util.LogHandler;

public class MultiThreadedCollisionDispatcher extends Dispatcher{
	private final LogHandler l = LogHandler.createInstanceShared();
	private final CollisionDispatcher internalDispatcher;
	
	private final static int NROF_COMPUATIONS_SEQ_TRESHOLD= 50;	
	
	public static MultiThreadedCollisionDispatcher create(CollisionConfiguration collisionConfiguration) {
		return new MultiThreadedCollisionDispatcher(collisionConfiguration);
	}
		
	/**
	 * Used as callback to determine action when a pair of objects collide
	 * @author jonas
	 *
	 */
	private static class CollisionPairCallback extends OverlapCallback {
		private final ParallelForTaskBuilder taskBuilder;		
		private DispatcherInfo dispatchInfo;
		private MultiThreadedCollisionDispatcher dispatcher;
		private boolean sequential;
		
		public CollisionPairCallback(ParallelForTaskBuilder taskBuilder){
			this.taskBuilder = taskBuilder;
		}

		public void init(DispatcherInfo dispatchInfo, MultiThreadedCollisionDispatcher dispatcher, int nrOfOverlaps) {
			this.dispatchInfo = dispatchInfo;
			this.dispatcher = dispatcher;
			if(nrOfOverlaps < NROF_COMPUATIONS_SEQ_TRESHOLD){
				sequential = true;
				return;
			}
			sequential = false;
			final int recommendedTaskCount = Engine.instance().getTaskExecutor().getRecommendedTaskCount();
			taskBuilder.prepareComputationTaskDistribution(recommendedTaskCount, nrOfOverlaps);
		}
		
		public boolean processOverlap(BroadphasePair pair) {
			if(sequential){
				(new ProcessOverLapComputation(dispatchInfo, dispatcher, pair)).compute(0);
				return false;
			}
			//Runnable run = taskBuilder.addLoopComputation(new ProcessOverLapComputation(dispatchInfo, dispatcher, pair));
			//if(run != null){
				//Engine.instance().getTaskExecutor().submitWork(run);
			//}
			return false;
		}
		
		public void finshAndWaitForProcessedResult() throws InterruptedException{
			if(sequential){
				return;
			}
			taskBuilder.awaitComputations();
		}
	}
	private final CollisionPairCallback collisionPairCallback = new CollisionPairCallback(ParallelForTaskBuilder.create());
	
	/**
	 * A single computation of overlapp of objects
	 * @author jonas
	 *
	 */
	private static class ProcessOverLapComputation implements ILoopComputation{
		private final BroadphasePair pair;
		private final DispatcherInfo dispatchInfo;
		private final MultiThreadedCollisionDispatcher dispatcher;
		
		public ProcessOverLapComputation(DispatcherInfo dispatchInfo, MultiThreadedCollisionDispatcher dispatcher, 
				BroadphasePair pair){
			this.pair = pair;
			this.dispatchInfo = dispatchInfo;
			this.dispatcher = dispatcher;
		}

		public void compute(int loopIndex) {
			(new DefaultNearCallback()).handleCollision(pair, dispatcher, dispatchInfo);
			//dispatcher.getNearCallback().handleCollision(pair, dispatcher, dispatchInfo);
		}

		public void finishComputation(int loopIndex) {}
	}

	/**
	 * Dispatch all collision pairs to be computed in parallel
	 * @param pairCache
	 * @param dispatchInfo
	 * @param dispatcher
	 */
	@Override
	public void dispatchAllCollisionPairs(OverlappingPairCache pairCache, DispatcherInfo dispatchInfo, Dispatcher dispatcher) {
		collisionPairCallback.init(dispatchInfo, this, pairCache.getNumOverlappingPairs());
		pairCache.processAllOverlappingPairs(collisionPairCallback, dispatcher);
		try {
			collisionPairCallback.finshAndWaitForProcessedResult();
		} catch (InterruptedException e) {
			if(l.isLevelLoggable(Log.ERROR))
				l.log("A collisoin computation was interrupted", Log.ERROR);
		}
	}
	
	/**
	 * Forward to CollisionDispatcher
	 * @param collisionConfiguration
	 */
	private MultiThreadedCollisionDispatcher(CollisionConfiguration collisionConfiguration) {
		internalDispatcher = new CollisionDispatcher(collisionConfiguration);
	}

	/**
	 * Forward to CollisionDispatcher
	 * @param arg0
	 */
	@Override
	public void clearManifold(PersistentManifold arg0) {
		internalDispatcher.clearManifold(arg0);
	}


	/**
	 * Forward to CollisionDispatcher
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @return
	 */
	@Override
	public CollisionAlgorithm findAlgorithm(CollisionObject arg0,
			CollisionObject arg1, PersistentManifold arg2) {
		return internalDispatcher.findAlgorithm(arg0, arg1, arg2);
	}

	/**
	 * Forward to CollisionDispatcher
	 * @param arg0
	 */
	@Override
	public void freeCollisionAlgorithm(CollisionAlgorithm arg0) {
		internalDispatcher.freeCollisionAlgorithm(arg0);		
	}

	/**
	 * Forward to CollisionDispatcher
	 * @return
	 */
	@Override
	public ObjectArrayList<PersistentManifold> getInternalManifoldPointer() {
		return internalDispatcher.getInternalManifoldPointer();
	}

	/**
	 * Forward to CollisionDispatcher
	 * @param arg0
	 * @return
	 */
	@Override
	public PersistentManifold getManifoldByIndexInternal(int arg0) {
		return internalDispatcher.getManifoldByIndexInternal(arg0);
	}

	/**
	 * Forward to CollisionDispatcher
	 * @param arg0
	 * @param arg1
	 * @return
	 */
	@Override
	public PersistentManifold getNewManifold(Object arg0, Object arg1) {
		return internalDispatcher.getNewManifold(arg0, arg1);
	}

	/**
	 * Forward to CollisionDispatcher
	 * @return
	 */
	@Override
	public int getNumManifolds() {
		return internalDispatcher.getNumManifolds();
	}

	/**
	 * Forward to CollisionDispatcher
	 * @param arg0
	 * @param arg1
	 * @return
	 */
	@Override
	public boolean needsCollision(CollisionObject arg0, CollisionObject arg1) {
		return internalDispatcher.needsCollision(arg0, arg1);
	}

	/**
	 * Forward to CollisionDispatcher
	 * @param arg0
	 * @param arg1
	 * @return
	 */
	@Override
	public boolean needsResponse(CollisionObject arg0, CollisionObject arg1) {
		return internalDispatcher.needsResponse(arg0, arg1);
	}

	/**
	 * Forward to CollisionDispatcher
	 * @param arg0
	 */
	@Override
	public void releaseManifold(PersistentManifold arg0) {
		internalDispatcher.releaseManifold(arg0);
	}
	
	/**
	 * Forward to CollisionDispatcher
	 * @return
	 */
	public NearCallback getNearCallback() {
		return internalDispatcher.getNearCallback(); 
	}
}
