package def.engine.physics.driver;

import def.engine.physics.IPhysicsSimModifyEvent;
import def.engine.physics.IPhysicsSimulation;
import def.engine.physics.Physics;
import def.engine.physics.PhysicsEventObservable;

public class RayHitNotifyEvent implements IPhysicsSimModifyEvent {
	
	private final Physics.PhysicsEvent event;
	private final PhysicsEventObservable rayHitListener;
	
	public static RayHitNotifyEvent create(final  Physics.PhysicsEvent ev, final PhysicsEventObservable l){
		return new RayHitNotifyEvent(ev, l);
	}
	
	private RayHitNotifyEvent(final  Physics.PhysicsEvent ev, final PhysicsEventObservable l){{
		this.event = ev;
		this.rayHitListener = l;
	}
	}
	
	public void modifySimulation(IPhysicsSimulation p) {
		rayHitListener.notifyObservers(event);
	}


	public void initSimulationModification(IPhysicsSimulation physis) {	}
}
