package def.engine.physics.driver;


import java.util.concurrent.LinkedBlockingQueue;

import android.util.Log;

import def.engine.physics.AddRigidPhysicalBodyEvent;
import def.engine.physics.IPhysicsSimModifiedCallback;
import def.engine.physics.IPhysicsSimModifyEvent;
import def.engine.physics.IPhysicsSimulation;
import def.engine.physics.RemoveRigidPhysicalBodyEvent;
import def.shared.interfaces.IPhysicsObjectData;
import def.util.LogHandler;

/**
 * Abstract physics simulation which handles async calls with internal queue
 * @author jonas
 *
 */
public abstract class AQueuedAsyncPhysicsSimulation implements IPhysicsSimulation{
	
	private final LogHandler l = LogHandler.createInstanceShared();
	
	private final LinkedBlockingQueue<IPhysicsSimModifyEvent> modifyEvents = new LinkedBlockingQueue<IPhysicsSimModifyEvent>();
	
	public void init(){}
	
	public void addRigidPhysicalBodyAsync(	final IPhysicsObjectData data, 
												final float mass, 
												final IPhysicsSimModifiedCallback physicsModifyDoneCallback)
	{
		addModifyEvent(AddRigidPhysicalBodyEvent.create(data, physicsModifyDoneCallback).setMass(mass));
	}
	
	public void addRigidPhysicalBodyAsync(	final IPhysicsObjectData data, 
												final IPhysicsSimModifiedCallback physicsModifyDoneCallback)
	{
		addModifyEvent(AddRigidPhysicalBodyEvent.create(data, physicsModifyDoneCallback));
	}
	
	public void addModifyEvent(IPhysicsSimModifyEvent event){
		final boolean result = modifyEvents.add(event);
		if(!result && l.isLevelLoggable(Log.ERROR))
			l.log("Failed to add AddRigidPhysicalBodyEvent to simualtion queue", Log.ERROR);

	}	
	
	public void removeRigidBodyAsync(final Object bodyRefID){
		addModifyEvent(RemoveRigidPhysicalBodyEvent.create(bodyRefID));
	}
	
	public final void simulate(final long timeStep){
		//Handle async simulation events - this queue may be updated in parallel
		IPhysicsSimModifyEvent event = modifyEvents.poll();
		while(event != null ){
			event.modifySimulation(this);
			event.initSimulationModification(this);
			event = modifyEvents.poll();
		}
		// Call actual simulation impl.
		postEventSyncedSimulate(timeStep);
	}
	
	/**
	 * Implement by subclass to handle simulation after events has been processed
	 * @param timeStep
	 */
	public abstract void postEventSyncedSimulate(final long timeStep);
}
