package def.engine.physics.driver;


import java.util.List;

import javax.vecmath.Vector3f;

import android.util.Log;
import bullet.BulletLib;
import bullet.adapter.CollisionShapeConstuctionInfo;

import def.engine.logic.math.LinjearAlg;
import def.engine.logic.math.VectorArray3f;
import def.engine.physics.PhysicsEventListener;
import def.engine.physics.IPhysicsSimulation;
import def.engine.physics.Physics;
import def.engine.physics.PhysicsEventObservable;
import def.engine.physics.ShapeInfo;
import def.shared.interfaces.ICollisonReporter;
import def.shared.interfaces.IPhysicsObjectData;
import def.shared.interfaces.IRayHitReporter;
import def.util.LogHandler;
import def.util.Util;

public class BulletPhyics extends AQueuedAsyncPhysicsSimulation{
	
	private final LogHandler l = LogHandler.createInstanceShared();
	
	private final PhysicsEventObservable physEvObeservable = PhysicsEventObservable.create();
	private final Physics.PhysicsEvent physicEvent = new Physics.PhysicsEvent();
	private final CollisionReporter collisionReporter;
	private final RayHitReporter rayHitReporter;
	private Vector3f groundPos = null;

	public static IPhysicsSimulation createPhysics(){
		return new BulletPhyics(null);
	}
	
	public static IPhysicsSimulation createPhysicsWithGround(final Vector3f groundPos){
		return new BulletPhyics(groundPos);
	}
	
	public void addPhysicsEventListener(PhysicsEventListener listener){
		physEvObeservable.registerObserver(listener);
	}
	
	public IPhysicsObjectData addRigidPhysicalBody(IPhysicsObjectData data, ShapeInfo colShape){
		return addRigidPhysicalBody(data, colShape, 1.0f);
	}
	
	private static CollisionShapeConstuctionInfo createFromShapeInfo(ShapeInfo shapeInfo){
		switch (shapeInfo.getType()) {
		case SH_CUBE:
			return CollisionShapeConstuctionInfo.createCube(shapeInfo.getDimensions());
		case SH_SPHERE:
			return CollisionShapeConstuctionInfo.createSphere(shapeInfo.getDimensions()[0]);
		}
		
		return null;
	}
	
	public IPhysicsObjectData addRigidPhysicalBody(IPhysicsObjectData data, ShapeInfo colShape, float mass){
		
		if(data == null || colShape == null || mass <  0)
			throw new IllegalArgumentException("Failed to execute addRigidPhysicalBody due to invalid arguments");
		if(data.getIPostionable() == null)
			throw new IllegalArgumentException("Missing(null value) IPhysicsObjectData::IPositionable when executing addRigidPhysicalBody");

		CollisionShapeConstuctionInfo shapeInfo = createFromShapeInfo(colShape);
		shapeInfo.physicsObjData = data;

		// Call bullet lib to add body to simulation
		final Vector3f p = data.getIPostionable().getPosition();
		long id = BulletLib.addRigidBody(new float[]{p.x, p.y, p.z}, shapeInfo, 1.0f);
		
		data.setPhysicsObjectRefID(Long.valueOf(id));
		
		return data;
	}
	
	public void removeRigidBody(final Object bodyRefID){
		Long id = null;
		try{
			id = (Long)bodyRefID;
		}catch(ClassCastException e){
			if(l.isLevelLoggable(Log.ERROR)){
				l.log("Failed to convert rigid body id - should be on Long type", Log.ERROR);
			}
			return;
		}
		// Call bullet lib to remove body from simulation
		boolean res = BulletLib.removeRigidBody(id.longValue());
		
		if(!res){
			if(l.isLevelLoggable(Log.WARN)){
				l.log("Failed to remove rigid body from simulation: ID = " + id,  Log.WARN);
			}
		}
	}
	
	public void shootRay(final Vector3f from, final Vector3f to, final List<Vector3f> outResult){
		
		BulletLib.shootRay(
				LinjearAlg.vectorToArray(from), 
				LinjearAlg.vectorToArray(to),
				rayHitReporter);
	}
	
	private BulletPhyics(final Vector3f groundPos){
		this.collisionReporter = CollisionReporter.create(physicEvent, physEvObeservable);
		this.rayHitReporter = new RayHitReporter(physicEvent, physEvObeservable);
		this.groundPos = groundPos;
	}
	
	@Override
	public void init(){
		if(groundPos == null)
		{
			BulletLib.initSimulationWithoutGround();
		}
		else
		{
			BulletLib.initSimulation((long)groundPos.y);	
		}
	}
	
	public Vector3f getGravity(){
		final Vector3f out = new Vector3f(BulletLib.getGravity());
		return out;
	}
	
	public void setGravity(final Vector3f gravity) {
		BulletLib.setGravity(LinjearAlg.vectorToArray(gravity));
	}

	public void postEventSyncedSimulate(final long timeStep){
		BulletLib.simulate(timeStep);
	}

	public void updateSimulationResult(){
		// Set transforms for all graphical representations and update collsions using collisionReporter
		BulletLib.syncVisuals(collisionReporter);
		
	}
	
	/**
	 * Used by native lib to report ray hitting object
	 * @author jonas
	 *
	 */
	public class RayHitReporter implements IRayHitReporter{
		private final Physics.PhysicsEvent event;
		private final PhysicsEventObservable rayHitListener;
		
		private RayHitReporter(final Physics.PhysicsEvent event, PhysicsEventObservable listener){
			
			this.event = event;
			this.rayHitListener = listener;
		}

		public float[] rayHit(float[] hitDir, IPhysicsObjectData hitObj) {
			if(hitDir.length != VectorArray3f.VEC_SZ)
				Util.buildIllegalArgException(RayHitReporter.class, "rayDir" , " " + hitDir.length);
			
			Vector3f impulse = new Vector3f(hitDir);
			event.update( impulse, hitObj);
			// Perform later and calc impulse here, due to dalvik suspend bug
			impulse.normalize();
			event.setResult(impulse);
			BulletPhyics.this.addModifyEvent(RayHitNotifyEvent.create(event, physEvObeservable));
			//rayHitListener.notifyObservers(event);
			
			final Vector3f result = event.getResult();
			return new float[]{result.x, result.y, result.z};
		}
		
	}

	/**
	 * Used by native lib to report collisions
	 * @author jonas
	 *
	 */
	private static class CollisionReporter implements ICollisonReporter{
		private final Physics.PhysicsEvent event;
		private final PhysicsEventObservable collisionsObeservable;
		
		public static CollisionReporter create( Physics.PhysicsEvent event, PhysicsEventObservable listener ){
			return new CollisionReporter(event, listener);
		}
		
		private CollisionReporter(final Physics.PhysicsEvent event, PhysicsEventObservable listener){
			this.event = event;
			this.collisionsObeservable = listener;
		}

		public void collision(IPhysicsObjectData a, IPhysicsObjectData b) {
			event.update(a, b);
			collisionsObeservable.notifyObservers(event);
		}
		
	}
	
}
