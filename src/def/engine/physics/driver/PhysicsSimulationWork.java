package def.engine.physics.driver;

import def.engine.execution.IRenderPrepResult;
import def.engine.execution.IRenderPrepWork;
import def.engine.physics.IPhysicsSimulation;

public class PhysicsSimulationWork implements IRenderPrepWork<Void, Long>{
	
	private final IPhysicsSimulation physics;
	private final IRenderPrepResult<Void> result;
	
	public static PhysicsSimulationWork create(IPhysicsSimulation physics){
		return new PhysicsSimulationWork(physics);
	}
	
	private PhysicsSimulationWork (IPhysicsSimulation physics){
		if(physics == null)
			throw new IllegalArgumentException();
		
		this.physics = physics;
		this.result = PhysicsSimulationResult.create(physics);
	}

	public IRenderPrepResult<Void> work(Long arg) {
		long argval = 0;
		if(arg != null)
			argval = arg.longValue();
		physics.simulate(argval);
		return result;
	}
}
