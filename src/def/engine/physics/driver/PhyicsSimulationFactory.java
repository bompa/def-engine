package def.engine.physics.driver;

import def.engine.physics.IPhysicsSimulation;

public class PhyicsSimulationFactory {
	public static IPhysicsSimulation createBullet(){
		return BulletPhyics.createPhysics();
	}
	
	public static IPhysicsSimulation createJBullet(){
		return JBulletPhyics.createPhysics();
	}
}
