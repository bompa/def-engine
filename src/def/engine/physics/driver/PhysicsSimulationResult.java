package def.engine.physics.driver;

import def.engine.execution.IRenderPrepResult;
import def.engine.physics.IPhysicsSimulation;

public class PhysicsSimulationResult implements IRenderPrepResult<Void>{
	
	private final IPhysicsSimulation physics;
	
	public static PhysicsSimulationResult create(IPhysicsSimulation physics){
		return new PhysicsSimulationResult(physics);
	}
	
	private PhysicsSimulationResult (IPhysicsSimulation physics){
		if(physics == null)
			throw new IllegalArgumentException();
		
		this.physics = physics;
	}

	public Void finishWork() {
		physics.updateSimulationResult();
		return null;
	}
}
