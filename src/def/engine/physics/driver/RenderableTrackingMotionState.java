package def.engine.physics.driver;

import com.bulletphysics.linearmath.DefaultMotionState;
import com.bulletphysics.linearmath.MotionState;
import com.bulletphysics.linearmath.Transform;


public class RenderableTrackingMotionState extends MotionState {
	
	private final MotionState defaultMotionState;

	/**
	 * Creates a new DefaultMotionState with all transforms set to identity.
	 */
	public RenderableTrackingMotionState() {
		defaultMotionState = new DefaultMotionState();
	}

	/**
	 * Creates a new DefaultMotionState with initial world transform and center
	 * of mass offset transform set to identity.
	 */
	public RenderableTrackingMotionState(Transform startTrans) {
		defaultMotionState = new DefaultMotionState(startTrans);
	}
	
	/**
	 * Creates a new DefaultMotionState with initial world transform and center
	 * of mass offset transform.
	 */
	public RenderableTrackingMotionState(Transform startTrans, Transform centerOfMassOffset) {
		defaultMotionState = new DefaultMotionState(startTrans, centerOfMassOffset);
	}
	
	public Transform getWorldTransform(Transform out) {
		return defaultMotionState.getWorldTransform(out);
	}

	public void setWorldTransform(Transform centerOfMassWorldTrans) {
		defaultMotionState.setWorldTransform(centerOfMassWorldTrans);
	}

}
