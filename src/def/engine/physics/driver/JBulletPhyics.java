package def.engine.physics.driver;


import java.util.List;

import javax.vecmath.Vector3f;

import android.util.Log;

import com.bulletphysics.collision.broadphase.AxisSweep3;
import com.bulletphysics.collision.broadphase.Dispatcher;
import com.bulletphysics.collision.dispatch.CollisionConfiguration;
import com.bulletphysics.collision.dispatch.CollisionDispatcher;
import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.dispatch.CollisionWorld;
import com.bulletphysics.collision.dispatch.DefaultCollisionConfiguration;
import com.bulletphysics.collision.narrowphase.PersistentManifold;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;
import com.bulletphysics.collision.shapes.SphereShape;
import com.bulletphysics.dynamics.DiscreteDynamicsWorld;
import com.bulletphysics.dynamics.RigidBody;
import com.bulletphysics.dynamics.RigidBodyConstructionInfo;
import com.bulletphysics.dynamics.constraintsolver.HingeConstraint;
import com.bulletphysics.dynamics.constraintsolver.SequentialImpulseConstraintSolver;
import com.bulletphysics.dynamics.constraintsolver.SliderConstraint;
import com.bulletphysics.linearmath.MotionState;
import com.bulletphysics.linearmath.Transform;
import com.bulletphysics.util.ObjectArrayList;

import def.engine.logic.math.LinjearAlg;
import def.engine.physics.PhysicsEventListener;
import def.engine.physics.IPhysicsSimulation;
import def.engine.physics.Physics;
import def.engine.physics.PhysicsEventObservable;
import def.engine.physics.ShapeInfo;
import def.shared.interfaces.IPhysicsObjectData;
import def.shared.interfaces.ITransform;
import def.util.LogHandler;

public class JBulletPhyics extends AQueuedAsyncPhysicsSimulation{ //implements IPhysicsSimulation{
	
	private final LogHandler l = LogHandler.createInstanceShared();
	
	private final PhysicsEventObservable physEvObeservable = PhysicsEventObservable.create();
	private final Physics.PhysicsEvent physicsEvent = new Physics.PhysicsEvent();

	private final DiscreteDynamicsWorld dynamicsWorld;
	
	public static IPhysicsSimulation createPhysicsWithGround(final Vector3f groundPos){
		return new JBulletPhyics(groundPos);
	}
	
	public static IPhysicsSimulation createPhysics(){
		return new JBulletPhyics(null);
	}
	
	public void addPhysicsEventListener(PhysicsEventListener listener){
		physEvObeservable.registerObserver(listener);
	}
	
	public IPhysicsObjectData addRigidPhysicalBody(IPhysicsObjectData data, ShapeInfo colShape){
		return addRigidPhysicalBody(data, colShape, 1.0f);
	}
	
	static CollisionShape constuctCollisionShape(final ShapeInfo info){
		switch(info.getType()){
			case SH_CUBE:
				return new BoxShape(new Vector3f(info.getDimensions()));
			case SH_SPHERE:
				return new SphereShape(info.getDimensions()[0]);
		}
		return null;
	}
	
	public IPhysicsObjectData addRigidPhysicalBody(IPhysicsObjectData data, ShapeInfo shapeInfo, float mass){
		
		if(data == null || shapeInfo == null || mass <  0)
			throw new IllegalArgumentException("Failed to execute addRigidPhysicalBody due to invalid arguments");
		if(data.getIPostionable() == null)
			throw new IllegalArgumentException("Missing(null value) IPhysicsObjectData::IPositionable when executing addRigidPhysicalBody");

		// Create Dynamic Objects
		Transform startTransform = new Transform();
		startTransform.setIdentity();

		// rigidbody is dynamic if and only if mass is non zero,
		// otherwise static
		boolean isDynamic = (mass != 0f);
		
		CollisionShape colShape = constuctCollisionShape(shapeInfo);
		Vector3f localInertia = new Vector3f(0, 0, 0);
		if (isDynamic) {
			colShape.calculateLocalInertia(mass, localInertia);
		}

		final ITransform ipos = data.getIPostionable();
		startTransform.origin.set(ipos.getPosition());

		// using motionstate is recommended, it provides
		// interpolation capabilities, and only synchronizes
		// 'active' objects
		MotionState myMotionState = new RenderableTrackingMotionState(startTransform);

		RigidBodyConstructionInfo rbInfo = new RigidBodyConstructionInfo(
				mass, myMotionState, colShape, localInertia);
		RigidBody body = new RigidBody(rbInfo);

		data.setPhysicsObjectRefID(body);
		body.setUserPointer(data);
		dynamicsWorld.addRigidBody(body);
		return data;
	}
	
	public void removeRigidBody(final Object bodyRefID){
		dynamicsWorld.removeRigidBody((RigidBody)bodyRefID);
	}
	
	public void shootRay(final Vector3f from, final Vector3f to, final List<Vector3f> outResult){
		final CollisionWorld.ClosestRayResultCallback result = new CollisionWorld.ClosestRayResultCallback(from, to);
		dynamicsWorld.rayTest(from, to, result);
		
		if(result.hasHit()){
			final Vector3f hit = result.hitPointWorld;

			RigidBody b = RigidBody.upcast(result.collisionObject);
			b.activate(true);
			final Vector3f dir = new Vector3f(to);
			dir.sub(from);
			dir.normalize();
			
			physicsEvent.update( dir, Physics.getObjectData(b));
			physEvObeservable.notifyObservers(physicsEvent);
			
			final Vector3f resultImpulse = physicsEvent.getResult();
			
 			//Typ b.getConstraintRef(0)
			//int nc = b.getNumConstraintRefs();
			//if(nc > 0)
			//{
				//for(int i = 0; )
				//}
			//else
				//{
				//final Vector3f pivotA = new Vector3f(0.0f, 2.0f, 0.0f ); // right over
				//final Vector3f axis = new Vector3f( 1.0f, 0.0f, 0.0f ); // X-axis
				//HingeConstraint hc = new HingeConstraint(b, pivotA, axis );
				//	this.dynamicsWorld.addConstraint(hc);	
			//}
			
			b.applyImpulse(resultImpulse, new Vector3f(0, 0, 0));
			
			outResult.add(hit);
		}
		else{
			l.log("Miss!", Log.ERROR);
		}
	}
	
	private JBulletPhyics(final Vector3f groundPos){
		// collision configuration contains default setup for memory, collision
		// setup. Advanced users can create their own configuration.
		CollisionConfiguration collisionConfiguration = new DefaultCollisionConfiguration();

		// use the default collision dispatcher. For parallel processing you
		// can use a diffent dispatcher (see Extras/BulletMultiThreaded)
		Dispatcher dispatcher = new CollisionDispatcher(collisionConfiguration); 
			//MultiThreadedCollisionDispatcher.create(collisionConfiguration);
		
		// the maximum size of the collision world. Make sure objects stay
		// within these boundaries
		// Don't make the world AABB size too large, it will harm simulation
		// quality and performance
		Vector3f worldAabbMin = new Vector3f(-10000, -10000, -10000);
		Vector3f worldAabbMax = new Vector3f(10000, 10000, 10000);
		int maxProxies = 1024;
		AxisSweep3 overlappingPairCache =
				new AxisSweep3(worldAabbMin, worldAabbMax, maxProxies);
		//BroadphaseInterface overlappingPairCache = new SimpleBroadphase(
		//		maxProxies);

		// the default constraint solver. For parallel processing you can use a
		// different solver (see Extras/BulletMultiThreaded)
		SequentialImpulseConstraintSolver solver = new SequentialImpulseConstraintSolver();

		dynamicsWorld = new DiscreteDynamicsWorld(
				dispatcher, overlappingPairCache, solver,
				collisionConfiguration);

		setGravity(new Vector3f(0, -10, 0));

		if(groundPos != null)
		{
			CollisionShape groundShape = new BoxShape(new Vector3f(150.f, 50.f, 150.f));

			// keep track of the shapes, we release memory at exit.
			// make sure to re-use collision shapes among rigid bodies whenever
			// possible!
			//TODO: Remove and reuse
			ObjectArrayList<CollisionShape> collisionShapes = new ObjectArrayList<CollisionShape>();
			collisionShapes.add(groundShape);

			Transform groundTransform = new Transform();
			groundTransform.setIdentity();
			groundTransform.origin.set(groundPos);

			{
				float mass = 0f;
				Vector3f localInertia = new Vector3f(0, 0, 0);

				// using motionstate is recommended, it provides interpolation
				// capabilities, and only synchronizes 'active' objects
				// TODO: change to PositionableMotionState
				MotionState myMotionState = new RenderableTrackingMotionState(groundTransform);
				RigidBodyConstructionInfo rbInfo = new RigidBodyConstructionInfo(
						mass, myMotionState, groundShape, localInertia);
				RigidBody body = new RigidBody(rbInfo);

				// add the body to the dynamics world
				dynamicsWorld.addRigidBody(body);
			}
		}
	}
	
	public Vector3f getGravity(){
		final Vector3f out = new Vector3f();
		dynamicsWorld.getGravity(out);
		return out;
	}
	
	public void setGravity(final Vector3f gravity) {
		dynamicsWorld.setGravity(gravity);
	}

	public void postEventSyncedSimulate(final long timeStep){
		dynamicsWorld.stepSimulation(0.05f, 10);
	}

	public void updateSimulationResult(){
		// Set transforms for all graphical representations
		updateGraphics();
		
		// Check collisions
		reportCollisions();
	}
	
	private void reportCollisions() {
		int numManifolds = dynamicsWorld.getDispatcher().getNumManifolds();
		for (int i=0;i<numManifolds;i++)
		{
			PersistentManifold contactManifold =  dynamicsWorld.getDispatcher().getManifoldByIndexInternal(i);
			RigidBody obA = (RigidBody) contactManifold.getBody0();
			RigidBody obB = (RigidBody) contactManifold.getBody1();
			
			if(contactManifold.getNumContacts() > 0){
				physicsEvent.update(Physics.getObjectData(obA), Physics.getObjectData(obB));
				physEvObeservable.notifyObservers(physicsEvent);
			}
		}
	}

	private void updateGraphics() {
		for(int j = dynamicsWorld.getNumCollisionObjects() -1; j >= 0; j--)
		{
			final CollisionObject obj = dynamicsWorld.getCollisionObjectArray().getQuick(j);
			final RigidBody body = RigidBody.upcast(obj);
			if (body != null && body.getMotionState() != null && !body.isStaticObject()) {
				updateVisualBodies(body, obj);
			}
		}
	}
	
	private void updateVisualBodies(final RigidBody body, final CollisionObject obj){
		final Transform trans = new Transform();
		body.getMotionState().getWorldTransform(trans);
		
		
		IPhysicsObjectData visualData = Physics.getObjectData(obj);
		final ITransform connectedVisualPos = visualData.getIPostionable();

		connectedVisualPos.setPosition(LinjearAlg.vectorToArray(trans.origin));
		connectedVisualPos.setRotation(LinjearAlg.matrixToArray(trans.basis));
	}
}
