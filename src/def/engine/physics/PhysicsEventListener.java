package def.engine.physics;

import javax.vecmath.Vector3f;

import def.shared.util.IObserver;

public abstract class PhysicsEventListener implements IObserver<IPhysicsEventEvent<Vector3f>>{

	public final void update(IPhysicsEventEvent<Vector3f> ev){
		physicsEvent(ev);
	}
	
	/**
	 * Pyhics event callback. Called when physics simulations are done in between render calls.
	 * @param ev
	 */
	public abstract void physicsEvent(IPhysicsEventEvent<Vector3f> ev);
}
