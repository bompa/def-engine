package def.engine.physics;

import javax.vecmath.Vector3f;

public class ShapeInfo {
	public enum Type{ SH_CUBE, SH_SPHERE };
	
	public static ShapeInfo createCubeUnitSize(){
		return new ShapeInfo(Type.SH_CUBE, new float[]{1, 1, 1});
	}
	
	public static ShapeInfo createCube(final Vector3f d){
		return new ShapeInfo(Type.SH_CUBE, new float[]{d.x, d.y, d.z});
	}
	
	public static ShapeInfo create(Type t, float[] dim){
		return new ShapeInfo(t, dim);
	}
	
	private ShapeInfo(Type t, float[] dim){
		if(t == null || dim == null)
			throw new IllegalArgumentException("Null argument");
		
		if(getDimensionsSize(t) != dim.length){
			String msg = "";
			if(getDimensionsSize(t) == -1){
				msg = "Invalid type!";
			}
			else{
				msg = "dim arg must have array lenght equal to: " + getDimensionsSize(t);
			}
			
			throw new IllegalArgumentException(msg);
		}
		
		this.type = t;
		this.dimensions = dim;
	}
	
	private Type type;
	private float[] dimensions;
	
	public static int getDimensionsSize(Type t){
		switch(t){
			case SH_CUBE:
				return 3;
			case SH_SPHERE:
				return 1;
		}
		return -1;
	}

	public Type getType() {
		return type;
	}

	public float[] getDimensions() {
		return dimensions;
	}
}
