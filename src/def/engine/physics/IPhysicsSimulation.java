package def.engine.physics;

import java.util.List;

import javax.vecmath.Vector3f;

import def.shared.interfaces.IPhysicsObjectData;

public interface IPhysicsSimulation {

	public void init();

	/**
	 * Add rigid body to physics simulation, connecting it to a positionable type - done asynchronously.
	 * @param ipos
	 */
	public void addRigidPhysicalBodyAsync(	final IPhysicsObjectData data, final IPhysicsSimModifiedCallback modifyDone);
	
	public void addPhysicsEventListener(PhysicsEventListener listener);

	public IPhysicsObjectData addRigidPhysicalBody(	final IPhysicsObjectData data, final ShapeInfo colShape);

	/**
	 * Add rigid body to physics simulation, connecting it to a positionable type and the given colision shape
	 * @param ipos
	 */
	public IPhysicsObjectData addRigidPhysicalBody(IPhysicsObjectData data, ShapeInfo colShape, float mass);

	public void removeRigidBody(final Object bodyRefID);
	
	public void removeRigidBodyAsync(final Object bodyRefID);

	public void simulate(final long timeStep);

	public void updateSimulationResult();
	
	public Vector3f getGravity();
	
	public void setGravity(Vector3f gravity);
	
	public void shootRay(Vector3f from, Vector3f to, final List<Vector3f> outResult);
	
	public void addModifyEvent(IPhysicsSimModifyEvent event);
}