package def.engine.physics;


import javax.vecmath.Vector3f;

import def.shared.interfaces.IPhysicsObjectData;

import android.util.Pair;

public interface IPhysicsEventEvent<Result> {
	enum EventType{EV_TYPE_COLLISION, EV_TYPE_RAYHIT};
	
	EventType getType();

	public PairRes getCollisionPair();
	
	public class PairRes extends Pair<IPhysicsObjectData, IPhysicsObjectData>{
		public PairRes(IPhysicsObjectData first, IPhysicsObjectData second) {super(first, second);}
	}
	
	public class PairRayRes extends Pair<Vector3f, IPhysicsObjectData>{
		public PairRayRes(Vector3f first, IPhysicsObjectData second) {super(first, second);}
	}
	
	public PairRayRes getRayHitResult();
	
	public Result getResult();

	public void setResult(Result result);
}
