package def.engine.physics;

import javax.vecmath.Vector3f;

import def.shared.interfaces.IPhysicsObjectData;

public class AddRigidPhysicalBodyEvent implements IPhysicsSimModifyEvent {
	
	//private final IPositionable p;
	private final IPhysicsObjectData data;
	private final IPhysicsSimModifiedCallback modifyDoneCallback;
	private float mass = 1.0f;
	private Object bodyRefID;
	private float scale = 0.1f;
	private ShapeInfo shapeInfo;
	
	public static AddRigidPhysicalBodyEvent create(final IPhysicsObjectData data, final IPhysicsSimModifiedCallback physicsModifyDoneCallback){
		return new AddRigidPhysicalBodyEvent(data, physicsModifyDoneCallback);
	}
	
	public AddRigidPhysicalBodyEvent(final IPhysicsObjectData data, final IPhysicsSimModifiedCallback physicsModifyDoneCallback){
		this.data = data;
		this.modifyDoneCallback = physicsModifyDoneCallback;
		final Vector3f dimensions = new Vector3f(1f, 1f, 1f);
		dimensions.scale(scale);
		shapeInfo = ShapeInfo.createCube(dimensions);	
	}
	
	public void modifySimulation(IPhysicsSimulation physis) {
		bodyRefID = physis.addRigidPhysicalBody(data, shapeInfo, mass).getPhysicsObjectRefID();
	}


	public void initSimulationModification(IPhysicsSimulation physis) {
		simModifiedCallback();
	}

	private void simModifiedCallback() {
		if(modifyDoneCallback != null)
			modifyDoneCallback.modificationDone(bodyRefID);
	}

	public AddRigidPhysicalBodyEvent setMass(float mass) {
		this.mass = mass;
		return this;
	}

	public float getMass() {
		return mass;
	}
	
	public AddRigidPhysicalBodyEvent setShapeInfo(ShapeInfo si){
		if(si == null)
			throw new IllegalArgumentException("AddRigidPhysicalBodyEvent setShapeInfo NULL arg");
		this.shapeInfo = si;
		return this;
	}

	public AddRigidPhysicalBodyEvent setScale(float scale) {
		this.scale = scale;
		return this;
	}

	public float getScale() {
		return scale;
	}


}
