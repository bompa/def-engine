package def.engine.physics;

public class RemoveRigidPhysicalBodyEvent implements IPhysicsSimModifyEvent {
	
	private final IPhysicsSimModifiedCallback modifyDoneCallback;
	private Object body;
	
	public static RemoveRigidPhysicalBodyEvent create(final Object body){
		return new RemoveRigidPhysicalBodyEvent(body, null);
	}
	
	public static RemoveRigidPhysicalBodyEvent create(final Object body, final IPhysicsSimModifiedCallback physicsModifyDoneCallback){
		return new RemoveRigidPhysicalBodyEvent(body, physicsModifyDoneCallback);
	}
	
	private RemoveRigidPhysicalBodyEvent(final Object body, final IPhysicsSimModifiedCallback physicsModifyDoneCallback){
		this.body = body;
		this.modifyDoneCallback = physicsModifyDoneCallback;
	}
	
	public void modifySimulation(IPhysicsSimulation physis) {
		physis.removeRigidBody(body);
	}


	public void initSimulationModification(IPhysicsSimulation physis) {
		simModifiedCallback();
	}

	private void simModifiedCallback() {
		if(modifyDoneCallback != null)
			modifyDoneCallback.modificationDone(body);
	}


}
