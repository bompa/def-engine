package def.engine.physics;

import javax.vecmath.Vector3f;

import com.bulletphysics.collision.dispatch.CollisionObject;
import com.bulletphysics.collision.shapes.BoxShape;
import com.bulletphysics.collision.shapes.CollisionShape;

import def.shared.interfaces.IPhysicsObjectData;
import def.util.Util;

public class Physics {
	public static class PhysicsEvent implements IPhysicsEventEvent<Vector3f>{
		private PairRes collisionPair = new PairRes(null, null);
		private PairRayRes rayHitPair = new PairRayRes(null, null);
		private final Vector3f result = new Vector3f();
		private EventType type;
		
		public PhysicsEvent(){}
		
		public void update(IPhysicsObjectData a, IPhysicsObjectData b){
			collisionPair = new PairRes(a, b);
			this.type = EventType.EV_TYPE_COLLISION;
		}
		
		public void update(Vector3f a, IPhysicsObjectData b){
			this.rayHitPair = new PairRayRes(a, b);
			this.type = EventType.EV_TYPE_RAYHIT;
		}
	
		public PairRes getCollisionPair() {
			return collisionPair;
		}

		public PairRayRes getRayHitResult() {
			return rayHitPair;
		}
		
		public void setType(EventType t) {
			type = t;
		}

		public EventType getType() {
			return type;
		}

		public Vector3f getResult() {
			return result;
		}

		public void setResult(Vector3f result) {
			if(result == null)
				throw Util.buildIllegalNullArgException(PhysicsEvent.class, "result");
			
			this.result.x = result.x;
			this.result.y = result.y;
			this.result.z = result.z;
		}
	}

	public static final CollisionShape getColisionShapeBoxUnitSize(){ return new BoxShape(new Vector3f(1.0f, 1.0f, 1.0f)); };
	
	public static IPhysicsObjectData getObjectData(CollisionObject cs){
		if(cs.getUserPointer() != null)
			return (IPhysicsObjectData) cs.getUserPointer();
		
		return null;
	}
}
