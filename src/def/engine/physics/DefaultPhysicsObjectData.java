package def.engine.physics;

import def.shared.interfaces.IPhysicsObjectData;
import def.shared.interfaces.ITransform;

public class DefaultPhysicsObjectData implements IPhysicsObjectData {
	private final ITransform p;
	private Object co;
	private Object optional = null;
	
	public static DefaultPhysicsObjectData createVoid(final ITransform p, final Object co){
		return new DefaultPhysicsObjectData(p, co);
	}
	
	public static DefaultPhysicsObjectData create(final ITransform p){
		return new DefaultPhysicsObjectData(p);
	}
	
	private DefaultPhysicsObjectData(final ITransform p, final Object co){
		if(p == null || co == null)
			throw new IllegalArgumentException("DefaultVisualData created with IPositionable == null || final Object == null");
		
		this.p = p;
		this.co = co;
	}
	
	public DefaultPhysicsObjectData(final ITransform p){
		if(p == null )
			throw new IllegalArgumentException("DefaultVisualData created with IPositionable == null");
		
		this.p = p;
	}

	public ITransform getIPostionable() {
		return p;
	}

	public Object getPhysicsObjectRefID() {
		return co;
	}

	public Object getOptional() { return optional; }
	public void setOptional(final Object opt) { optional = opt; }

	public void setPhysicsObjectRefID(Object obj) {	
		co = obj;
	}
}
