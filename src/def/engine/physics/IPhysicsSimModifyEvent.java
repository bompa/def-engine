package def.engine.physics;


public interface IPhysicsSimModifyEvent {
	public void modifySimulation(IPhysicsSimulation physis);
	public void initSimulationModification(IPhysicsSimulation physis);
}