package def.engine.physics;

public interface IPhysicsSimModifiedCallback {
	public void modificationDone(final Object modifiedbodyRefID);
}
