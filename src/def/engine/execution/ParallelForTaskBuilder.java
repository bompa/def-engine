package def.engine.execution;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

import android.util.Log;

import def.shared.util.Exceptions;
import def.util.LogHandler;

/**
 * Creates tasks of given size which can be submitted to ITaskExecutor.submitWork to be computed in parallel, as a parallel-for loop.
 * Each tasks performs a number of computations, which is a subset of a loop, that implements the ILoopComputation interface
 * @author jonas
 *
 */
public class ParallelForTaskBuilder {
	
	private final LogHandler l = LogHandler.createInstanceShared();
	
	private ArrayList<ILoopComputation> computations = null;
	private int nrOfComputationTasks = 0;
	private int nrOfCreatedComputationTasks = 0;
	private int baseTaskSize = 0;
	private int remainderFromBaseSize = 0;
	/**
	 * Latch shared between tasks and counted down when each of them complete.
	 */
	private volatile CountDownLatch latch; 
	
	public static ParallelForTaskBuilder create(){
		return new ParallelForTaskBuilder();
	}
	
	private ParallelForTaskBuilder(){}
	
	public void prepareComputationTaskDistribution(int nrOfTasks, int nrOfComputations){
		final int scaledNrOfTasks = Math.min(nrOfTasks, nrOfComputations);
		baseTaskSize = Math.max(1, nrOfComputations / nrOfTasks);
		remainderFromBaseSize =  nrOfTasks > nrOfComputations ? 0 : nrOfComputations % scaledNrOfTasks;
		latch = new CountDownLatch(scaledNrOfTasks);
		nrOfComputationTasks = scaledNrOfTasks;
		nrOfCreatedComputationTasks = 0;
		
		if(l.isLevelLoggable(Log.ERROR))
			l.log("Created " + nrOfComputationTasks + "(" + nrOfComputations + ")task. With " + baseTaskSize + " " + remainderFromBaseSize + " comps.", Log.ERROR);
	}
	
	public IRenderPrepWork<?, ?> addLoopComputation(ILoopComputation comp){
		if(computations == null)
			computations = new ArrayList<ILoopComputation>();
		
		computations.add(comp);
		
		if(computations.size() == (baseTaskSize + remainderFromBaseSize)){
			remainderFromBaseSize = 0;
			nrOfCreatedComputationTasks++;
			return createComputationTask();
		}
		
		return null;
	}
	
	public int getNrOfPendingTasks(){
		if(computations == null)
			return 0;
		return computations.size();
	}
	
	public IRenderPrepWork<?, ?> createComputationTask() {
		if(getNrOfPendingTasks() == 0)
			return null;

		// Move compuations list to task
		IRenderPrepWork<?, ?> work = new AComputationCompostionWork<Void, Void>(computations){
			public IRenderPrepResult<Void> work(Void work) {
				final ComputationCompostionResult<Void> result = ComputationCompostionResult.create(computations);
				int i = 0;
				final ArrayList<ILoopComputation> computationList = getComputations();
				try{
					for(final ILoopComputation computation : computationList){
						computation.compute(i++);
					}
				}
				catch(Exception exc){
					logLoopComputationException(exc);
				}
				finally{
					latch.countDown();
					logLoopComputationsIfIncomplete(i, computationList.size());
				}

				return result;
			}
			
		};
		computations = null;	// Moved to work and result tasks
		return work;
	}
	
	private void logLoopComputationException(final Exception exc){
		if(l.isLevelLoggable(Log.ERROR)){
			l.log("Exception(" + exc.getClass().getName() + ") Aborted computation... " + exc.getMessage(), Log.ERROR);
			l.log(Exceptions.stackTraceToString(exc), Log.ERROR);
		}
	}
	
	private void logLoopComputationsIfIncomplete(final int computationsDone, final int totalComputations){
		if(computationsDone < totalComputations && l.isLevelLoggable(Log.ERROR)){
			l.log("Error: Not all computations finished (done/total) - " + computationsDone + "/" + totalComputations  + " done.", Log.ERROR);
		}
		
	}
	
	public void awaitComputations() throws InterruptedException {
		latch.await();
	}
}
