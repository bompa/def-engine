package def.engine.execution;

import java.util.Collection;

import def.engine.graphics.IRenderBucket;
import def.engine.graphics.RenderEvent;
import def.engine.graphics.driver.IRenderTarget;
import def.shared.util.IObserver;
import def.shared.util.ITree;

/**
 * Listens for render events and coordinates received events
 * @author jonas
 *
 */
public abstract class ARenderEventCoordinator implements IObserver<RenderEvent>{
	
	public static final int MAX_RENDER_TARGETS = 32;

	/**
	 * Impl of IObserver update that dispatch the event to renderEventReceived method.
	 */
	public final void update(RenderEvent re){
		renderEventReceived(re);
	}
	
	public abstract void setRenderGraph(final ITree<IRenderBucket> graph);
	
	/**
	 * Add target for rendering, must be enabled before it can be used
	 * 
	 * @param target
	 * @return true if target could be added
	 */
	public abstract boolean addRenderTarget(final IRenderTarget target);
	
	/**
	 * Enable a previously added render-target
	 * @param target
	 * @return
	 */
	public abstract boolean enableRenderTarget(final IRenderTarget target);
	
	/**
	 * Remove rendertarget by using its reference
	 * @param target
	 * @return true if found and removed, else false
	 */
	public abstract boolean removeRenderTarget(final IRenderTarget target);

	/**
	 * 
	 * @return
	 */
	public abstract Collection<IRenderTarget> getAllRenderTargets();
	
	/**
	 * Callback 
	 * @param re
	 */
	public abstract void renderEventReceived(RenderEvent re);
}
