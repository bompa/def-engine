package def.engine.execution;

import java.util.concurrent.Future;


public interface ITaskExecutor {
	
	/**
	 * Work to be done in between render calls.
	 * @param physicsSimulation
	 */
	public <V> Future<IRenderPrepResult<?>> submitWork(final IRenderPrepWork<?, V> preRender);
	
	/**
	 * Complete pre-render work and allow rendering to be done by letting caller fetch a render task.
	 * The render work must be synched with any background tasks and the pre-render work.
	 * @return
	 */
	public IRenderCallTask fetchSynchedRenderWork();

	/**
	 * Get event listener which receives rendering request.
	 * @return
	 */
	public ARenderEventCoordinator getRenderEventCoordinator();
	
	/**
	 * Cleart all current tasks pending for execution
	 */
	public void clearTasks();
	
	/**
	 * 
	 * @return
	 */
	public int getRecommendedTaskCount();
}
