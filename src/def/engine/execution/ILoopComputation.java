package def.engine.execution;

/**
 * Interface for creating a new LoopComputation that can be used with the 
 * LoopTaskFactory.
 */
public interface ILoopComputation {
	/**
	 * Run this function for each index in the loop
	 * @param loopIndex the current loop-index
	 */
	public void compute(int loopIndex);
	
	/**
	 * Called after computations is done.
	 * @param loopIndex the current loop-index 
	 */
	public void finishComputation(int loopIndex);
}
