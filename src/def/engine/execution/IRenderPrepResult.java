package def.engine.execution;

public interface IRenderPrepResult<E> {
	public E finishWork();
}
