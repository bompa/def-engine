package def.engine.execution;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;

import android.util.Log;
import android.util.Pair;

import def.engine.graphics.IRenderBucket;
import def.engine.graphics.IRenderable;
import def.engine.graphics.RenderEvent;
import def.engine.graphics.driver.IRenderTarget;
import def.shared.util.ITree;
import def.shared.util.ITree.NodeVisitor;
import def.util.LogHandler;
import def.util.Util;

public class RenderableEventCoordinator extends ARenderEventCoordinator{
	private final static LogHandler l = LogHandler.createInstanceShared(RenderableEventCoordinator.class);
	private final Map<Integer, IRenderBucket> bucketMap = new ConcurrentHashMap<Integer, IRenderBucket>();
	private final AbstractCollection<Pair<Boolean, IRenderTarget>> renderTargets = new ArrayBlockingQueue<Pair<Boolean, IRenderTarget>>(MAX_RENDER_TARGETS);
	
	final NodeVisitor<IRenderBucket> updateBucketMapNodeVisitor = new NodeVisitor<IRenderBucket>() {
		public void visit(IRenderBucket bucket) {
			bucketMap.put(bucket.getRenderPrio(), bucket);
		}
	};
	
	final NodeVisitor<IRenderBucket> clearBucketMapNodeVisitor = new NodeVisitor<IRenderBucket>() {
		public void visit(IRenderBucket bucket) {
			bucket.emptyBucket();
		}
	};
	
	public static class ToOrderedListBucketMapVisitor implements NodeVisitor<IRenderBucket>{
		 
		private final List<IRenderBucket> bucketsInTraversalOrder = new ArrayList<IRenderBucket>();

		public void visit(IRenderBucket bucket) {
			final IRenderBucket bucketCopy = bucket.clone();
			bucketsInTraversalOrder.add(bucketCopy);
		}

		public List<IRenderBucket> getBucketsInTraversalOrder() {
			return bucketsInTraversalOrder;
		}
		
		public void reset(){
			bucketsInTraversalOrder.clear();
		}
	};
	final ToOrderedListBucketMapVisitor toOrderedListBucketMapNodeVisitor = new ToOrderedListBucketMapVisitor();
	
	private ITree<IRenderBucket> renderBucketsRootNode = null;
	
	public static RenderableEventCoordinator create(){
		return new RenderableEventCoordinator();
	}
	
	private RenderableEventCoordinator(){ }
	
	private RenderableEventCoordinator(final ITree<IRenderBucket> graph){ 
		setRenderGraph(graph); 
	}
	
	/**
	 * Get read-only snap shot copy of objects that have requested rendering and the rendertargets.
	 * @return
	 */
	public Pair< Collection<IRenderBucket>, Collection<IRenderTarget>> getAwaitRenderSnapShot(){
		assert renderBucketsRootNode != null;
		
		checkRenderablesStates();
		
		toOrderedListBucketMapNodeVisitor.reset();
		renderBucketsRootNode.traverseDepthFirst(toOrderedListBucketMapNodeVisitor);
		
		List<IRenderTarget> enabledRenderTargets = new ArrayList<IRenderTarget>();
		for( final Pair<Boolean, IRenderTarget> rt : renderTargets ){
			if(rt.first == true){
				enabledRenderTargets.add(rt.second);
			}
		}
		return Util.makePair(Collections.unmodifiableCollection(toOrderedListBucketMapNodeVisitor.getBucketsInTraversalOrder()), 
				Collections.unmodifiableCollection(enabledRenderTargets)) ;
	}
	
	/**
	 * Check if renderables has updated state which causes them to be
	 * rendered in another bucket or be removed from the queue
	 */
	private void checkRenderablesStates() {
		
		final List<IRenderable> renderablesInWrongRenderBucket = new ArrayList<IRenderable>();
		final Iterator<Entry<Integer, IRenderBucket>> it = bucketMap.entrySet().iterator();
		
		while(it.hasNext()){
			final Entry<Integer, IRenderBucket> entry = it.next();
			
			final Collection<IRenderable> renderables = entry.getValue().getRenderables();
			final Iterator<IRenderable> rendIt = renderables.iterator();
			
			while(rendIt.hasNext()){
				
				final IRenderable rend = rendIt.next();
				boolean inWrongBucket = rend.getRenderPrio() != entry.getKey();
				
				if(!rend.getContiniousRendering()){
					rendIt.remove();
				}
				else if(inWrongBucket){
					rendIt.remove();
					renderablesInWrongRenderBucket.add(rend);
				}
			}
		}
		
		for(IRenderable r : renderablesInWrongRenderBucket){
			addRenderable(r);
		}
	}

	public void renderEventReceived(RenderEvent re) {
		addRenderable(re.getRenderObj());
	}
	
	public void clearQueue(){
		assert renderBucketsRootNode != null;
		
		renderBucketsRootNode.traverseDepthFirst(clearBucketMapNodeVisitor);
	}
	
	private void addRenderable(IRenderable r){
		assert renderBucketsRootNode != null;
		
		final IRenderBucket bucket = bucketMap.get(r.getRenderPrio());
		if(bucket == null){
			if(LogHandler.isLogLevelWithinRange(Log.ERROR))
				l.log("Could not add for rendering since renderPrio for renderable does not exsits as a IRenderBucket", Log.ERROR);
			
			return;
		}
		bucket.add(r);
	}

	@Override
	public void setRenderGraph(final ITree<IRenderBucket> graph) {
		if(graph == null)
			throw new IllegalArgumentException("Can not set graph to null value in RenderableQueue");
			
		renderBucketsRootNode = graph;
		renderBucketsRootNode.traverseDepthFirst(updateBucketMapNodeVisitor);
	}

	@Override
	public boolean addRenderTarget(final IRenderTarget target) {
		boolean res = false;
		try{
		res = renderTargets.add(new Pair<Boolean, IRenderTarget>(false, target));
		}catch(final IllegalStateException e){
			if(l.isLevelLoggable(Log.ERROR))
				l.log("Maximim no. of rendertargets reached", Log.ERROR);
			return false;
		}
		
		return res;
	}

	@Override
	public boolean removeRenderTarget(final IRenderTarget target) {
		return renderTargets.remove(target);
	}

	@Override
	public Collection<IRenderTarget> getAllRenderTargets() {
		final List<IRenderTarget> allRenderTargets = new ArrayList<IRenderTarget>(renderTargets.size());
		for( final Pair<Boolean, IRenderTarget> rt : renderTargets ){
				allRenderTargets.add(rt.second);
		}
		return Collections.unmodifiableCollection(allRenderTargets);
	}

	@Override
	public boolean enableRenderTarget(IRenderTarget target) {
		for( final Pair<Boolean, IRenderTarget> rt : renderTargets )
		{
			if(rt.second == target)
			{
				final IRenderTarget foundRenderTarget = rt.second;
				if(renderTargets.remove(rt))
				{
					return renderTargets.add(new Pair<Boolean, IRenderTarget>(true, foundRenderTarget));
				}
				else
				{
					l.log("Could not enable rendertarget since old state could not be removed", Log.ERROR);
					return false;
				}
			}
		}
		
		return false;
	}
}
