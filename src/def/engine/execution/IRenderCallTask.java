package def.engine.execution;

import def.engine.graphics.driver.IGraphicsDriver;

public interface IRenderCallTask {
	/**
	 * Performs rendering task 
	 * @param graphics is the graphics driver object used to perform rendering
	 * @return true if has more work to perform(should be called again), false if done
	 */
	public boolean render(IGraphicsDriver graphics);
}
