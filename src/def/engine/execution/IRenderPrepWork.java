package def.engine.execution;

public interface IRenderPrepWork<Res, Arg> {
	public IRenderPrepResult<Res> work(Arg work);
}
