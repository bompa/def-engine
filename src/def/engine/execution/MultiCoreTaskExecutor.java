package def.engine.execution;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import android.util.Log;
import android.util.Pair;

import def.engine.graphics.IRenderBucket;
import def.engine.graphics.RenderingTask;
import def.engine.graphics.driver.IRenderTarget;

import def.shared.util.Exceptions;
import def.util.LogHandler;
import def.util.Stats;

/**
 *	Distributes and synchronizes work, including rendering. 
 */
public class MultiCoreTaskExecutor implements ITaskExecutor{
	private static final LogHandler l = LogHandler.createInstanceShared();
	private static final int EXTRA_TASKS = 2;//Runtime.getRuntime().availableProcessors();
	private final ExecutorService workExecutor = Executors.newFixedThreadPool(EXTRA_TASKS);
	private final Stats stats = Stats.instance();
	
	private final RenderableEventCoordinator renderQueue = RenderableEventCoordinator.create();
	private final RenderingTask renderingTask = new RenderingTask();
	private final List<Future<IRenderPrepResult<?>>> renderPrepWorkResults = new ArrayList<Future<IRenderPrepResult<?>>>();

	public Future<?> submitWork(Runnable task) {
		return workExecutor.submit(task);
	}
	
	public <Arg> Future<IRenderPrepResult<?>> submitWork(final IRenderPrepWork<?, Arg> preRender) {
		
		Callable<IRenderPrepResult<?>> call = new Callable<IRenderPrepResult<?>>() {
			public IRenderPrepResult<?> call(){
				return preRender.work(null);
			}
		};
		
		Future<IRenderPrepResult<?>> future = workExecutor.submit(call);
		renderPrepWorkResults.add(future);
		
		return future;
	}
	
	private void awaitAndHandleFutureResult(Future<IRenderPrepResult<?>> futureResult){
		IRenderPrepResult<?> result = null;
		try {
			result = futureResult.get();
		} catch (InterruptedException e) {
			if(l.isLevelLoggable(Log.ERROR))
				l.log("Interrupted while waiting for a render preparation result", Log.ERROR);
			throw new CancellationException();
			
		} catch (StackOverflowError e) {
			if(l.isLevelLoggable(Log.ERROR)){
				l.log("An stack-overflow exception occured when waiting for a render preparation result", Log.ERROR);
				l.log("StackTracr: " + Exceptions.stackTraceToString(e), Log.ERROR);
			}
			throw new CancellationException();
			
		} catch (ExecutionException e) {
			if(l.isLevelLoggable(Log.ERROR)){
				l.log("Execution exception occured when waiting for a render preparation result", Log.ERROR);
				l.log("Exception(" + e.getClass().getName() + ") Aborted computation... " + e.getMessage(), Log.ERROR);
				l.log(Exceptions.stackTraceToString(e), Log.ERROR);
			}
			throw new CancellationException();
		}
		
		if(result != null)
			result.finishWork();
	}
	
	private void synchPreRenderWork(){
		for(Future<IRenderPrepResult<?>> futureResult : renderPrepWorkResults){
			stats.updateComputationTaskWait();
			try{
				awaitAndHandleFutureResult(futureResult);
			} catch(CancellationException e){
				if(l.isLevelLoggable(Log.ERROR))
					l.log("Sync work cancelled, aborting",  Log.ERROR);
				
				renderPrepWorkResults.clear();
				return;
			}
			
			stats.updateComputationTaskWait();
		}
		renderPrepWorkResults.clear();
	}

	public IRenderCallTask fetchSynchedRenderWork() {
		synchPreRenderWork();
		Pair< Collection<IRenderBucket>, Collection<IRenderTarget>> renderSnapShot = renderQueue.getAwaitRenderSnapShot();
		renderingTask.setRenderTargets(renderSnapShot.second);
		renderingTask.setToRender(renderSnapShot.first);
		return renderingTask;
	}

	public ARenderEventCoordinator getRenderEventCoordinator() {
		return renderQueue;
	}

	public void clearTasks() {
		renderQueue.clearQueue();
		renderingTask.setToRender(null);
	}

	public int getRecommendedTaskCount() {
		return EXTRA_TASKS;
	}
}
