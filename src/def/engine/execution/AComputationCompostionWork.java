package def.engine.execution;

import java.util.ArrayList;

public abstract class AComputationCompostionWork<E, V> implements IRenderPrepWork<E, V>{
	private final ArrayList<ILoopComputation> computations;
	
	public AComputationCompostionWork(ArrayList<ILoopComputation> computations){
		this.computations = computations;
	}

	protected ArrayList<ILoopComputation> getComputations() {
		return computations;
	}
}
