package def.engine.execution;

import java.util.ArrayList;

import def.engine.execution.IRenderPrepResult;

public class ComputationCompostionResult<E> implements IRenderPrepResult<E>{
	
	private final ArrayList<ILoopComputation> computations;
	
	public static <E> ComputationCompostionResult<E> create(ArrayList<ILoopComputation> computations){
		return new ComputationCompostionResult<E>(computations);
	}
	
	private ComputationCompostionResult (ArrayList<ILoopComputation> computations){
		if(computations == null)
			throw new IllegalArgumentException();
		
		this.computations = computations;
	}
	
	protected ArrayList<ILoopComputation> getComputations() {
		return computations;
	}

	public E finishWork() {
		int i = 0;
		for(final ILoopComputation computation : computations){
			computation.finishComputation(i);
		}
		return null;
	}
}
