package def.engine.graphics;

import java.nio.Buffer;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

import javax.vecmath.Matrix4f;


import def.Const;
import def.engine.graphics.driver.Graphics;
import def.engine.graphics.driver.IRenderableData;
import def.engine.graphics.driver.VertexBufferData;
import def.engine.graphics.driver.VertexData.BufferDataType;
import def.engine.graphics.shader.FragmentShader;
import def.engine.graphics.shader.VertexShader;
import def.engine.logic.math.MatrixArray4f;
import def.shared.util.OutArg;
import def.util.IntPair;
import def.util.Util;

/**
 * Renderable data with references to drawing buffers owned by a mesh instance, as
 * well as refences to shader instances
 * @author jonas
 *
 */
public class RenderableData implements IRenderableData {
	//private static LogHandler l = LogHandler.createInstanceShared(RenderableData.class);
	
	public static IRenderableData createWithStaticSharedData(final IRenderableData sharedRd){
		final VertexBufferData vdata = sharedRd.getVertexBufferData();
		// Ignore renderable data without buffer info
		if(vdata == null){
			return sharedRd;
		}
		
		// Create shared or owned buffers.
		OutArg<Integer> newVertexBuffId = Util.makeOutArg(Graphics.BufferId.InvalidID.value());
		OutArg<Integer> newIndexBuffId = Util.makeOutArg(Graphics.BufferId.InvalidID.value());
		final FloatBuffer vertexBuff = getSharedOrCopiedFloatBuffer(sharedRd.getVertexBuffer(), vdata.getPositionsBufferType(),  vdata.getPositionsID(), newVertexBuffId);
		final short[] indexBuff = getSharedOrCopiedIndices(sharedRd, vdata, newIndexBuffId);
		
		final IRenderableData newRendData = create(sharedRd.getRenderOperation(), sharedRd.getRenderDirection(), vertexBuff, 
				indexBuff, new ArrayList<IntPair>(sharedRd.getComponentIndices()));
		
		final VertexBufferData newVertexBuffData = newRendData.getVertexBufferData();
		newVertexBuffData.setPositionsID(new Graphics.BufferId(newVertexBuffId.value));
		newVertexBuffData.setIndicesID(new Graphics.BufferId(newIndexBuffId.value));
		
		OutArg<Integer> newBuffId = Util.makeOutArg(Graphics.BufferId.InvalidID.value());
		final FloatBuffer colorBuffer = getSharedOrCopiedFloatBuffer(sharedRd.getColorBuffer(), vdata.getColorsBufferType(), vdata.getColorsID(), newBuffId);
		newVertexBuffData.setColorsID(new Graphics.BufferId(newBuffId.value));
		
		newBuffId = Util.makeOutArg(Graphics.BufferId.InvalidID.value());
		final FloatBuffer normalsBuffer = getSharedOrCopiedFloatBuffer(sharedRd.getNormalsBuffer(), vdata.getNormalsBufferType(), vdata.getNoramlsID(), newBuffId);
		newVertexBuffData.setNoramlsID(new Graphics.BufferId(newBuffId.value));
		
		newBuffId = Util.makeOutArg(Graphics.BufferId.InvalidID.value());
		final FloatBuffer texCoordsBuffer = getSharedOrCopiedFloatBuffer(sharedRd.getTextureCoordsBuffer(), vdata.getTexturesBufferType(), vdata.getTexturesID(), newBuffId);
		newVertexBuffData.setTexturesID(new Graphics.BufferId(newBuffId.value));
		
		newBuffId = Util.makeOutArg(Graphics.BufferId.InvalidID.value());
		final FloatBuffer tangetBuffer = getSharedOrCopiedFloatBuffer(sharedRd.getTangetBuffer(), vdata.getTangentsBufferType(), vdata.getTangentsID(), newBuffId);
		newVertexBuffData.setTangentsID(new Graphics.BufferId(newBuffId.value));
		
		newBuffId = Util.makeOutArg(Graphics.BufferId.InvalidID.value());
		final FloatBuffer binormalBuffer = getSharedOrCopiedFloatBuffer(sharedRd.getBinormalBuffer(), vdata.getBinormalsBufferType(), vdata.getBinormalsID(), newBuffId);
		newVertexBuffData.setBinormalsID(new Graphics.BufferId(newBuffId.value));
		
		// Set buffers(shared or copied) for new renderable-data
		newRendData.setTextureCoordsBuffer(texCoordsBuffer);
		newRendData.setNormalsBuffer(normalsBuffer);
		newRendData.setColorBuffer(colorBuffer);
		newRendData.setTangetBuffer(tangetBuffer);
		newRendData.setBinormalBuffer(binormalBuffer);
		
		return newRendData;
	}

	private static FloatBuffer getSharedOrCopiedFloatBuffer(final FloatBuffer floatBufferIn, final BufferDataType bufferType, final Graphics.BufferId buffId, final OutArg<Integer> sharedBuffId) {
		if(floatBufferIn == null){
			return null;
		}
		
		if(bufferType == null){
			throw new IllegalArgumentException("null buffertype not allowed in getSharedOrOwnedFloatBuffer");
		}
		
		final FloatBuffer floatBufferToShareOrCopy = floatBufferIn.asReadOnlyBuffer();
		
		// Only share buffer if static
		FloatBuffer floatBufferReturn;
		if( bufferType == BufferDataType.BUFFER_DATA_STATIC ){
			floatBufferReturn = floatBufferToShareOrCopy.asReadOnlyBuffer();
			sharedBuffId.value = buffId.value(); 
		}
		else{
			floatBufferReturn = createEmptyFloatBuffer(floatBufferToShareOrCopy);
			floatBufferReturn.put(floatBufferToShareOrCopy);
			floatBufferToShareOrCopy.position(0);
		}
		
		floatBufferReturn.position(0);
		return floatBufferReturn;
	}

	private static short[] getSharedOrCopiedIndices(final IRenderableData sharedRd, final VertexBufferData vdata, final OutArg<Integer> buffId) {
		short[] indexBuff;
		if(vdata.getIndicesBufferType() == BufferDataType.BUFFER_DATA_STATIC){
			indexBuff = sharedRd.getIndices();
			buffId.value = vdata.getIndicesID().value();
		}
		else{
			indexBuff = new short[sharedRd.getIndices().length];
			System.arraycopy(sharedRd.getIndices(), 0, indexBuff, 0, indexBuff.length);
		}
		return indexBuff;
	}

	private static FloatBuffer createEmptyFloatBuffer(final Buffer buff) {
		return Util.allocateDirectNativeOrder(buff.capacity() * Const.FLOAT_BYTE_SIZE).asFloatBuffer();
	}
	
	public static IRenderableData create(	RenderOperation rendOp, 
											RenderDirection renderDir,
											FloatBuffer vertexBuff,
											short[] indexBuff,
											ArrayList<IntPair> componetIndices)
	{
		return new RenderableData(rendOp, renderDir, vertexBuff, indexBuff, componetIndices);
	}
	
	private RenderableData(	RenderOperation rendOp, 
							RenderDirection renderDir,
							FloatBuffer vertexBuff,
							short[] indexBuff,
							ArrayList<IntPair> componetIndices)
	{
		
		if(vertexBuff == null || indexBuff == null || componetIndices == null)
			throw new IllegalArgumentException();
		
		this.renderOp = rendOp;
		this.renderDir = renderDir;
		this.indexBuffer = indexBuff;
		this.componetIndices = componetIndices;
		
		vertexBuffer = vertexBuff;
		modelMatrix.setIdentity();
	}
	
	private final short[] indexBuffer;
	/**
	 * List of start-end indexes for vertex(mesh) components
	 */
	private final ArrayList<IntPair> componetIndices;
	private final FloatBuffer vertexBuffer;
	
	private FloatBuffer colorBuffer;
	private FloatBuffer normalsBuffer;
	private FloatBuffer texCoordsBuffer;
	
	// Calculated buffers
	private FloatBuffer tangetBuffer;
	private FloatBuffer binormalBuffer;
	
	private FragmentShader fs;
	private VertexShader vs;
	
	private Matrix4f modelMatrix = new Matrix4f();
	private final RenderOperation renderOp;
	private final RenderDirection renderDir;
	
	private float scale = 1.0f;
	
	private final VertexBufferData vertBufferIDs = VertexBufferData.createWithStaitcBufferTypes();
	
	/**
	 * Index in
	 */
	private int activeComponentIndex = 0;
	

	public RenderOperation getRenderOperation() {
		return renderOp;
	}

	public RenderDirection getRenderDirection() {
		return renderDir;
	}

	public void setModelMatrix(Matrix4f modelMatrix) {
		this.modelMatrix = modelMatrix;
	}
	public Matrix4f getModelMatrix() {
		return modelMatrix;
	}
	
	public int getNrOfComponents() {
		return componetIndices.size();
	}
	
	public ArrayList<IntPair> getComponentIndices(){
		return componetIndices;
	}

	public void setActiveComponent(int componentIndex) {
		if(componentIndex >= componetIndices.size() || componentIndex < 0)
			throw new IllegalArgumentException("Invalid mesh componet index in RenderableData");
		
		activeComponentIndex = componentIndex;
	}
	
	public IntPair getIndexBufferRange(){
		final IntPair startEnd = componetIndices.get(activeComponentIndex);
		return new IntPair(startEnd.first, startEnd.second);
	}

	public ShortBuffer getActiveIndexBuffer() {
		if(activeComponentIndex < componetIndices.size()){
			final IntPair startEnd = componetIndices.get(activeComponentIndex);
			return ShortBuffer.wrap(indexBuffer, startEnd.first, ( startEnd.second + 1 ) - startEnd.first);
		}
		else if(indexBuffer.length > 0){
			return ShortBuffer.wrap(indexBuffer, 0, indexBuffer.length );
		}
		
		return ShortBuffer.allocate(0);
	}
	
	public short[] getIndices(){
		return indexBuffer;
	}

	public FloatBuffer getVertexBuffer() {
		return vertexBuffer;
	}

	public void setColorBuffer(FloatBuffer colorBuffer_) {
		this.colorBuffer = colorBuffer_;
	}

	public FloatBuffer getColorBuffer() {
		return colorBuffer;
	}

	public void setNormalsBuffer(FloatBuffer normalsBuffer_) {
		this.normalsBuffer = normalsBuffer_;
	}

	public FloatBuffer getNormalsBuffer() {
		return normalsBuffer;
	}

	public FloatBuffer getTextureCoordsBuffer() {
		return texCoordsBuffer;
	}

	public void setTextureCoordsBuffer(FloatBuffer tcBuffer) {
		this.texCoordsBuffer = tcBuffer;
	}

	public void setFragmentShader(FragmentShader fs_) {
		this.fs = fs_;
	}

	public FragmentShader getFragmentShader() {
		return fs;
	}

	public void setVertexShader(VertexShader vs_) {
		this.vs = vs_;
	}

	public VertexShader getVertexShader() {
		return vs;
	}

	public void adaptViewMatrix(MatrixArray4f viewMatrix) { }

	public float getScale() {
		return scale;
	}

	public void setScale(float scale) {
		this.scale = scale; 
	}

	public FloatBuffer getTangetBuffer() {
		return tangetBuffer;
	}

	public void setTangetBuffer(FloatBuffer tangetBuffer) {
		this.tangetBuffer = tangetBuffer;
	}

	public FloatBuffer getBinormalBuffer() {
		return binormalBuffer;
	}

	public void setBinormalBuffer(FloatBuffer binormalBuffer) {
		this.binormalBuffer = binormalBuffer;
	}

	public VertexBufferData getVertexBufferData() {
		return vertBufferIDs;
	}
}
