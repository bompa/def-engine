package def.engine.graphics.resources;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import def.shared.util.OutArg;

/**
 * Generic repository.
 * 
 * Thread-safe.
 * 
 * @author jonas
 *
 * @param <K> key
 * @param <V> value
 */
public class ResourceRepository<K, V> {
	
	private final Map<K, V> repo = new ConcurrentHashMap<K, V>();
	
	public void updateRepository(final K key, final V val){
		repo.put(key, val);
	}
	
	public boolean checkRepository(final K key, final OutArg<V> val){
		val.value = repo.get(key);
		return val.value != null;
	}
}
