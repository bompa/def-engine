package def.engine.graphics.resources;

import java.io.IOException;
import java.io.InputStream;
import java.nio.Buffer;
import java.nio.ShortBuffer;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import def.Const;
import def.engine.graphics.IRenderSequenceEvent;
import def.engine.graphics.IRenderable;
import def.engine.graphics.RenderSequenceListener;
import def.engine.graphics.ViewFrustum;
import def.engine.graphics.driver.FrameBufferData;
import def.engine.graphics.driver.Graphics;
import def.engine.graphics.driver.Graphics.BufferId;
import def.engine.graphics.driver.IGraphicsResources;
import def.engine.graphics.driver.IRenderTarget;
import def.engine.graphics.driver.IRenderTarget.BufferAttachmentType;
import def.engine.graphics.driver.IRenderableData;
import def.engine.graphics.driver.VertexBufferData;
import def.engine.graphics.driver.Graphics.RenderSequenceAction;
import def.engine.graphics.driver.VertexData.BufferDataType;
import def.engine.graphics.driver.VertexData.BufferTarget;
import def.engine.graphics.material.AnimatedTexture;
import def.engine.graphics.material.IMaterial;
import def.engine.graphics.material.ITexture;
import def.engine.graphics.material.ITexture.RenderId;
import def.engine.graphics.material.ITexture.RenderType;
import def.engine.graphics.material.ITexture.Settings;
import def.engine.graphics.material.Materials;
import def.engine.graphics.material.Texture;
import def.engine.graphics.shader.FragmentShader;
import def.engine.graphics.shader.IShaderProgram;
import def.engine.graphics.shader.Shaders;
import def.engine.graphics.shader.VertexShader;
import def.engine.logic.IAnimation;
import def.shared.util.OutArg;
import def.util.LogHandler;
import android.util.Pair;
import def.util.Util;

/**
 * Resource manager, initalizes and creates resources such as textures, and allocates hardware graphics resources
 * @author jonas
 *
 */
public class ResourceManager extends RenderSequenceListener{
	private final LogHandler l = LogHandler.createInstanceShared();
	
	private final IGraphicsResources irend;
	private final Context ctx;
	private final Map<String, IShaderProgram.Id> shaderRepository = new ConcurrentHashMap<String, IShaderProgram.Id>();
	private final ResourceRepository<Integer, ITexture.RenderId> texRepro = new ResourceRepository<Integer, ITexture.RenderId>();
	private final ResourceRepository<Integer, ITexture.RenderId> customTexRepro = new ResourceRepository<Integer, ITexture.RenderId>();
	private final ResourceRepository<Integer, IRenderableData> vboRepro = new ResourceRepository<Integer, IRenderableData>();
	
	private final ResouceIdList texturesList;
	private final ResouceIdList bufferIDsList;
	private final ResouceIdList frameBufferIDsList;
	private final ResouceIdList renderBufferIDsList;
	
	private final AtomicLong animatedTexureIdCount = new AtomicLong(0);
	private final AtomicInteger customTexureIdCount = new AtomicInteger(0);
	
	private final Map<Integer, ITexture> customTextures = new ConcurrentHashMap<Integer, ITexture>();
	private final Map<Long, IAnimation> animations = new ConcurrentHashMap<Long, IAnimation>();
	
	public static ResourceManager create(IGraphicsResources irend, Context ctx){
		return new ResourceManager(irend, ctx);
	}
	
	private ResourceManager(IGraphicsResources igd, Context ctx){
		if(igd == null || ctx == null)
			throw new IllegalArgumentException();
		
		this.irend = igd;
		this.ctx = ctx;
		
		final IResouceIDsGenerator texIdgenerator = new IResouceIDsGenerator() {
			public int[] generateIds(int nIds) {
				return irend.generateTextureIds(nIds);
			}
		};
		this.texturesList = ResouceIdList.create(texIdgenerator);
		
		final IResouceIDsGenerator bufferIdgenerator = new IResouceIDsGenerator() {			
			public int[] generateIds(int nIds) {
				return irend.generateVBOs(nIds);
			}
		};
		this.bufferIDsList = ResouceIdList.create(bufferIdgenerator);
		
		final IResouceIDsGenerator frameBufferIdgenerator = new IResouceIDsGenerator() {
			
			public int[] generateIds(int nIds) {
				return irend.generateFrameBufferObjects(nIds);
			}
		};
		this.frameBufferIDsList = ResouceIdList.create(frameBufferIdgenerator);
		
		final IResouceIDsGenerator renderBufferIdgenerator = new IResouceIDsGenerator() {
			
			public int[] generateIds(int nIds) {
				return irend.generateRenderBufferIDs(nIds);
			}
		};
		this.renderBufferIDsList = ResouceIdList.create(renderBufferIdgenerator);
		
	}
	
	public Pair<Integer, ITexture> createTextureCustom(int width, int height, ITexture.RenderType type){
		final ITexture tex = Texture.createWithoutResourceID();
		
		if( type == RenderType.TEX_RTT )
		{
			tex.setSettings(Settings.createRttCommon(height, width, ViewFrustum.create(45.0f, (float)width / (float)height, 0.1f, 350.0f)));
		}
			
		tex.setRenderType(type);
		
		final Integer id = customTexureIdCount.incrementAndGet();
		tex.setCustomId(id);
		customTextures.put(id, tex);
		
		return Util.makePair(id, tex);
	}
	
	public boolean deleteCustomTexure(final Long id){
		ITexture t = customTextures.remove(id);
		return t != null;
	}
	
	/**
	 * Create animated texture from resource id, which is animated an infinite nr. of times.
	 * @param resourceId
	 * @param imageSize size of one row or columns(must be equal) of the animated texture
	 * @return
	 */
	public Pair<Long, ITexture> createAnimatedTexture(int resourceId, int imageSize){
		return createAnimatedTexture(resourceId, imageSize, AnimatedTexture.SEQUENCE_COUNT_INFINITE);
	}
	
	/**
	 * Create animated texture from resource id, which is animated nrOfAnimationSquences times.
	 * @param resourceId
	 * @param imageSize
	 * @param nrOfAnimationSquences
	 * @return
	 */
	public Pair<Long, ITexture> createAnimatedTexture(int resourceId, int imageSize, int nrOfAnimationSquences){
		final AnimatedTexture tex = AnimatedTexture.create(resourceId);
		tex.setImageRowCount(imageSize);
		tex.setNrOfSequences(nrOfAnimationSquences);
		final long id = animatedTexureIdCount.incrementAndGet();
		animations.put(id, tex);
		
		return new Pair<Long, ITexture>(id, tex);
	}

	
	public IAnimation removeAnimation(Long id){
		return animations.remove(id);
	}
	
	/**
	 * Return bitmap from resource if texure has such an associated resource 
	 * @param t
	 * @return resource as bitmap if available otherwise, null
	 */
	private Bitmap loadBitmapFromResourceIfApplicable(final ITexture t){
		
		if(!Materials.isTextureUsingAndrouidResourceId(t))
			return null;
		
		Bitmap bitmap = null;		
		final InputStream is = ctx.getResources().openRawResource(t.getAndroidResource()); 
		try {
			bitmap = BitmapFactory.decodeStream(is);
		} finally {
		    try {
		        is.close();
		    } catch(IOException e) {
		    	if(l.isLevelLoggable(Log.ERROR))
		    		l.log("Failed to create bitmap for texture!", Log.ERROR);
		    }
		}
		return bitmap;
	}
	
	public boolean checkIRenderableDataRepo(Integer id, final OutArg<IRenderableData> rd){
		return vboRepro.checkRepository(id, rd);
	}
	
	public void updateIRenderableDataRepo(Integer id, IRenderableData rd){
		vboRepro.updateRepository(id, rd);
	}
	
	/**
	 * Create texture by opening raw resource or reuse already loaded texture 
	 * @param material
	 * @param texture index
	 * @return id of texture loaded by driver
	 */
	private ITexture.RenderId createTextureID(final IMaterial material, int idx){
		if(!Materials.isMaterialUsingTexture(material))
			return new ITexture.RenderId(0);
		
		final ITexture texture = material.getTexture(idx);
		final ITexture.Settings settings = texture.getSettings();
		final ResourceRepository<Integer, ITexture.RenderId> usedTexRepro = getTextureRepoByIdType(texture);
		
		OutArg<ITexture.RenderId> storedTexRenderId = Util.makeOutArg();
		if(usedTexRepro.checkRepository(Materials.getTextureId(texture), storedTexRenderId)){
			return new ITexture.RenderId(storedTexRenderId.value);
		}

		ITexture.RenderId newRenderId = new ITexture.RenderId(0);
		final int texId = texturesList.nextId();
		if(texId == ResouceIdList.INVALID_RES_ID){
			if(l.isLevelLoggable(Log.WARN))
				l.log("Failed to prepare material for rendering. No texture ids available.", Log.WARN);
			return newRenderId;
		}
		try{
			// Get bitmap, may be null
			Bitmap bitmap = loadBitmapFromResourceIfApplicable(texture);
			newRenderId = irend.createTexture(bitmap, texId, settings);
		} catch(Resources.NotFoundException e){
			l.log("Failed to prepare for rendering. Could not find texture resouce for material " + material, Log.ERROR);
		}
		usedTexRepro.updateRepository(Integer.valueOf(texture.getAndroidResource()), newRenderId);
		return newRenderId;
	}

	private ResourceRepository<Integer, RenderId> getTextureRepoByIdType(final ITexture texture) {
		return Materials.isTextureUsingAndrouidResourceId(texture)? texRepro: customTexRepro;
	}
	
	
	private Graphics.BufferId createBufferID(){
		
		final int buffId = bufferIDsList.nextId();
		if(buffId == ResouceIdList.INVALID_RES_ID){
			if(l.isLevelLoggable(Log.WARN))
				l.log("Failed to generate buffer id", Log.ERROR);
			return Graphics.BufferId.InvalidID;
		}
		
		return new Graphics.BufferId(buffId);
	}
	
	private boolean checkForSingleShaderInRepository(final IShaderProgram p, final OutArg<IShaderProgram.Id> id){
		if(p != null){
			id.value = shaderRepository.get(p.getShaderName());
			return true;
		}
		else{
			return false;
		}
	}
	
	/**
	 * Check if shaders are found in repository
	 * @param material
	 * @param fsId
	 * @param vsId
	 * @return true if both shaders are found
	 */
	private boolean checkShaderProgramRepository(IMaterial material, 
			OutArg<IShaderProgram.Id> vsId, OutArg<IShaderProgram.Id> fsId){
		
		if(!checkForSingleShaderInRepository(material.getVertexShader(), vsId)){
			return false;
		}
		
		if(!checkForSingleShaderInRepository(material.getFragmentShader(), fsId)){
			return false;
		}
		
		return (vsId.value != null) && (fsId.value != null);
	}
	
	/**
	 * Update rep with compiled shaders so that they can be linked into to other programs
	 * @param material
	 * @param vsId
	 * @param fsId
	 */
	private void updateShaderProgramRepositoy(VertexShader vs, FragmentShader fs ){
		IShaderProgram.Id vsId = vs.getInstanceProgramId();
		if(vs != null && Shaders.checkComileStatus(vs)){
			shaderRepository.put(vs.getShaderName(), vsId);
		}
		
		IShaderProgram.Id fsId = fs.getInstanceProgramId();
		if(fs != null && Shaders.checkComileStatus(fs)){
			shaderRepository.put(fs.getShaderName(), fsId);
		}
	}
	
	/**
	 * Check if valid and return id
	 * @param linkedProgId
	 * @return
	 */
	private IShaderProgram.Id createShaderProgram(IShaderProgram.Id linkedProgId){
		if(linkedProgId.get() == 0){
			if(l.isLevelLoggable(Log.ERROR))
				l.log("Failed to create shader program for material", Log.ERROR);
		}
		
		return linkedProgId;
	}	
	
	/**
	 * Create shader progeam by linking existing programs or create new frag and vertex shaders
	 * @param material
	 * @return
	 */
	private IShaderProgram.Id createShaderProgram(IMaterial material){
		IShaderProgram.Id linkedProgId = new IShaderProgram.Id(0);
		OutArg<IShaderProgram.Id> vsId = Util.makeOutArg();
		OutArg<IShaderProgram.Id> fsId = Util.makeOutArg();
		try{
			if(checkShaderProgramRepository(material, vsId, fsId)){
				linkedProgId = irend.createProgramLinkCompiledShaders(vsId.value, fsId.value);
				return createShaderProgram(linkedProgId);
			}
			linkedProgId = irend.createProgramCompileShaders(material.getVertexShader(), material.getFragmentShader());
			updateShaderProgramRepositoy(material.getVertexShader(), material.getFragmentShader());
			return createShaderProgram(linkedProgId);
		} catch(IllegalArgumentException e){
			if(l.isLevelLoggable(Log.WARN)){
				l.log("Fragment or Vertex shader is invalid for material: " + material, Log.WARN);
				l.log("Failed to create GPU program for material: " + material, Log.WARN);
			}
		}
		
		return linkedProgId;	
	}
	
	
	/**
	 * Prepare buffers - should be done after current batch of renderables is created to prevent
	 * not resusing any bufferids
	 * @param r
	 */
	public void prepareBuffersForRendering(final IRenderable renderable){
		
		final IRenderableData r = renderable.getRenderInfo();
		final VertexBufferData buffers = r.getVertexBufferData();
		
		if(buffers.getIndicesID().equals(Graphics.BufferId.InvalidID)){
			final ShortBuffer indicesBuff = r.getActiveIndexBuffer();
			final Graphics.BufferId idxid = initBuffer(r, BufferTarget.BUFFER_TARGET_ELEMENT_ARRAY, indicesBuff, buffers.getIndicesBufferType(), Const.SHORT_BYTE_SIZE * Util.calcIndexCount(indicesBuff));
			if(!idxid.equals(Graphics.BufferId.InvalidID)){
				buffers.setIndicesID(idxid);
			}
		}
		
		if(buffers.getPositionsID().equals(Graphics.BufferId.InvalidID)){
			final Graphics.BufferId posid = initBuffer(r, BufferTarget.BUFFER_TARGET_ARRAY, r.getVertexBuffer(), Const.FLOAT_BYTE_SIZE, buffers.getPositionsBufferType());
			if(!posid.equals(Graphics.BufferId.InvalidID)){
				buffers.setPositionsID(posid);
			}
		}
		
		if(buffers.getNoramlsID().equals(Graphics.BufferId.InvalidID)){
			final Graphics.BufferId normalid = initBuffer(r, BufferTarget.BUFFER_TARGET_ARRAY, r.getNormalsBuffer(), Const.FLOAT_BYTE_SIZE, buffers.getNormalsBufferType());
			if(!normalid.equals(Graphics.BufferId.InvalidID)){
				buffers.setNoramlsID(normalid);
			}
		}
		
		if(buffers.getTangentsID().equals(Graphics.BufferId.InvalidID)){
			final Graphics.BufferId texcid = initBuffer(r, BufferTarget.BUFFER_TARGET_ARRAY, r.getTextureCoordsBuffer(), Const.FLOAT_BYTE_SIZE, buffers.getTexturesBufferType());
			if(!texcid.equals(Graphics.BufferId.InvalidID)){
				buffers.setTexturesID(texcid);
			}
		}
		
		if(buffers.getColorsID().equals(Graphics.BufferId.InvalidID)){
			final Graphics.BufferId colorid = initBuffer(r, BufferTarget.BUFFER_TARGET_ARRAY, r.getColorBuffer(), Const.FLOAT_BYTE_SIZE, buffers.getColorsBufferType());
			if(!colorid.equals(Graphics.BufferId.InvalidID)){
				buffers.setColorsID(colorid);
			}
		}
		
		if(buffers.getTangentsID().equals(Graphics.BufferId.InvalidID)){
			final Graphics.BufferId tangid = initBuffer(r, BufferTarget.BUFFER_TARGET_ARRAY, r.getTangetBuffer(), Const.FLOAT_BYTE_SIZE, buffers.getTangentsBufferType());
			if(!tangid.equals(Graphics.BufferId.InvalidID)){
				buffers.setTangentsID(tangid);
			}
		}
		
		if(buffers.getBinormalsID().equals(Graphics.BufferId.InvalidID)){
			final Graphics.BufferId binid = initBuffer(r, BufferTarget.BUFFER_TARGET_ARRAY, r.getBinormalBuffer(), Const.FLOAT_BYTE_SIZE, buffers.getBinormalsBufferType());
			if(!binid.equals(Graphics.BufferId.InvalidID)){
				buffers.setBinormalsID(binid);
			}
		}
	}
	
	private Graphics.BufferId initBuffer(final IRenderableData r, BufferTarget target, final Buffer data, BufferDataType bufferType, int bufferSize) {
		Graphics.BufferId id = Graphics.BufferId.InvalidID;
		if(data != null && Graphics.bufferSupportsVbo(bufferType)){
			id = createBufferID();
			irend.setVboData(id.value(), target, bufferSize, data, bufferType);
		}
		return id;
	}

	private Graphics.BufferId initBuffer(final IRenderableData r, BufferTarget target, final Buffer data, int elemSize, BufferDataType bufferType) {
		Graphics.BufferId id = Graphics.BufferId.InvalidID;
		if(data != null && Graphics.bufferSupportsVbo(bufferType)){
			id = createBufferID();
			irend.setVboData(id.value(), target, data.remaining() * elemSize, data, bufferType);
		}
		return id;
	}
	
	public void prepareMaterialForRendering(final IMaterial material){
		setMaterialTextureIds(material);
		material.setShaderid(createShaderProgram(material));
		material.getFragmentShader().initShaderParams(irend);
		material.getVertexShader().initShaderParams(irend);
	}
	
	public boolean prepareRenderTargetForRendering(final IRenderTarget target){
		target.getFrameBufferObject().setRenderFboId(BufferId.create(frameBufferIDsList.nextId()));
		
		List<FrameBufferData> buffers = target.getFrameBufferObject().getTargetBuffersReadOnly();
		for(FrameBufferData data : buffers){
			if(data.getAttatchmentType() != BufferAttachmentType.BUFF_ATTATCH_COLOR){
				data.setRenderId(BufferId.create(renderBufferIDsList.nextId()));
			}
		}
		
		return irend.initRenderTarget(target);
	}
	
	private void setMaterialTextureIds(final IMaterial material) {
		final int nTextures = material.getNrOfTextures();
		for(int i = 0; i < nTextures; i++){
			try{
				final ITexture.RenderId id = createTextureID(material, i);
				material.getTexture(i).setRenderId(id);
			}
			catch(Materials.TextureNotFoundException e){
				if(l.isLevelLoggable(Log.ERROR))
					l.log("Texture at index " + i + " not found in material.", Log.ERROR);
				continue;
				
			}
		}
		
	}

	public void generateTextures(int nTextures) {
		texturesList.generateIDs(nTextures);
	}
	
	public void freeAllTextures(){
		//TODO: delete generated textrues and shader program
	}
	

	@Override
	public void renderSequenceUpdate(IRenderSequenceEvent rsa) {
		if(rsa.getAction() == RenderSequenceAction.REND_ACTION_FRAME_RENDERED){
			
			Set<Entry<Long, IAnimation>> animationEntries = animations.entrySet();
			
			for(final Entry<Long, IAnimation> e : animationEntries){
				if(e.getValue().updateAnimationState()){
					animations.remove(e.getKey());
				}
			}
		}
	}
}
