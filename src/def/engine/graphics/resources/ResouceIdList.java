package def.engine.graphics.resources;

import def.util.Util;

public class ResouceIdList {
	private int[] resources = null;
	private int nextResourceIdx = -1;
	
	public static final int INVALID_RES_ID = -1;
	public static final int MAX_RESOURCE_LIST_SZ = 250;
	public static final int DEFAULT_LIST_SIZE = 25;
	
	private final IResouceIDsGenerator idGen;
	
	public static ResouceIdList create(final IResouceIDsGenerator idGen){
		return new ResouceIdList(idGen);
	}
	
	private ResouceIdList(final IResouceIDsGenerator idGen){
		if(idGen == null)
			throw Util.buildIllegalNullArgException(getClass(), "idGenerator");
		
		this.idGen = idGen;
	}
	
	public void generateIDs(int nIDs) {
		resources = idGen.generateIds(nIDs);
		if(resources != null)
			nextResourceIdx = 0;
	}
	
	private boolean expandList(){
		if(nextResourceIdx >= MAX_RESOURCE_LIST_SZ)
			return false;
		if(resources == null){
			generateIDs(DEFAULT_LIST_SIZE);
			return true;
		}
		
		final int[] newResources = idGen.generateIds(resources.length * 2);
		final int[] tmp = new int[resources.length + newResources.length];
		System.arraycopy(resources, 0, tmp, 0, resources.length);
		System.arraycopy(newResources, 0, tmp, resources.length, newResources.length);
		
		resources = tmp;
		return true;
	}
	
	public int nextId(){
		if(nextResourceIdx == -1 || nextResourceIdx >= resources.length){
			if(!expandList()){
				return INVALID_RES_ID;
			}
		}
		return resources[nextResourceIdx++];
	}

}
