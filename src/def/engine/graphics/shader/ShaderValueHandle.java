package def.engine.graphics.shader;

public class ShaderValueHandle {
		private final int handle;
		
		public ShaderValueHandle(int value){
			handle = value;
		}

		public int getHandle() {
			return handle;
		}
}
