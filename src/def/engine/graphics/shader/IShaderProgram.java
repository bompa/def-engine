package def.engine.graphics.shader;

import def.engine.graphics.driver.IGraphicsResources;

public interface IShaderProgram {
	
	enum Type{FRAGMENT, VERTEX};
	
	public String getSource();

	/**
	 * Init all params available in shader
	 * @param irend
	 */
	public void initShaderParams(IGraphicsResources irend);
	
	/**
	 * Get unique name of shader.
	 * @return
	 */
	public String getShaderName();
	
	/**
	 * Set id for compiled parent program, which included this program linked with others, i.e vertex or shader programs
	 * @param parentProgramId render-id of parent program
	 */
	public void setParentProgramId(IShaderProgram.Id parentProgramId);
	/**
	 * get id for compiled parent program.
	 * @see setParentProgramId
	 * @param parentProgramId render-id of parent program
	 */
	public IShaderProgram.Id getParentProgramId();
	
	/**
	 * Set id of this shader instance, for example a single fragment shader.
	 * @param parentProgramId render-id of this shader program
	 */
	public void setInstanceProgramId(IShaderProgram.Id parentProgramId);
	/**
	 * 
	 * @return render-id of this shader program
	 */
	public IShaderProgram.Id getInstanceProgramId();
	
	/**
	 * Return the size of the shaders array of lights, or zero it the shader does not have any.
	 * @return
	 */
	public int getSizeOfLightArray();
	public void setSizeOfLightsArray(int sizeOfLightsArray);
	
	
	/**
	 * Render id for compiled loaded shader
	 * @author jonas
	 *
	 */
	public static class Id{
		private final int value_;
		
		public int get(){
			return value_;
		}
		
		public Id(final int id){
			value_ = id;
		}	
	}
}
