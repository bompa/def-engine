package def.engine.graphics.shader;

public abstract class AbstractShader implements IShaderProgram{
	private final String shaderName;
	private final String shaderCode;
	private IShaderProgram.Id parentProgramId;
	private IShaderProgram.Id instanceProgramId;
	private int sizeOfLightsArray;
	
	public AbstractShader(final String sn, final String sc) {
		shaderName = sn;
		shaderCode = sc;
	}

	public String getSource() {
		return shaderCode;
	}

	public void setParentProgramId(IShaderProgram.Id parentProgramId) {
		this.parentProgramId = parentProgramId;
	}

	public IShaderProgram.Id getParentProgramId() {
		return parentProgramId;
	}

	public void setInstanceProgramId(IShaderProgram.Id instanceProgramId_) {
		this.instanceProgramId = instanceProgramId_;
	}

	public IShaderProgram.Id getInstanceProgramId() {
		return instanceProgramId;
	}

	public String getShaderName() {
		return shaderName;
	}

	public void setSizeOfLightsArray(int sizeOfLightsArray) {
		this.sizeOfLightsArray = sizeOfLightsArray;
	}

	public int getSizeOfLightArray(){
		return sizeOfLightsArray;
	}

}
