package def.engine.graphics.shader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import android.util.Log;

import def.util.LogHandler;
import android.util.Pair;
import def.util.Util;

/**
 * Representss a value in a shader program.
 * 
 * Syntax of name:
 * name[[x]]:[array_length],[nr_of_entries]
 * E.g. simple -> ambient_color 
 * E.g. array of 3 elements ->  vectors[x]:3
 * E.g. struct with 4 entries -> light_struct:_,4
 * E.g. array of structs -> lights[x]:3,4
 * 
 * @author jonas
 *
 * @param <E> the type of shader value, such Vec3. Used mainly as type saftey to minimize 
 * 	misstates when creating shader programs.
 */
public class ShaderValue<E> {
	private final LogHandler l = LogHandler.createInstanceShared();	
	
	public final static String SHADER_VAL_ARRAY_TAG_STR = "[x]";
	public final static String SHADER_VAL_PARAMS_DELIM_TAG_STR = ":";
	public final static String SHADER_VAL_PARAM_ENTRY_DELIM_TAG_STR = ",";

	private final int SHADER_VAL_PARAM_INVALID_ENTRY = Integer.MIN_VALUE;
	private final int SHADER_VAL_ARRAY_LEN_IDX = 0;
	private final int SHADER_VAL_NR_ENTRIES_IDX = 1;
	
	public enum ValueScope{
		SHADER_VALUE_SCOPE_INVALID,
		SHADER_VALUE_SCOPE_PER_VERTEX,
		SHADER_VALUE_SCOPE_PER_PRIMITIVE};
	
	private E value;
		
	private int id = 0;
	private HashMap<Integer, HashMap<String, ShaderValue<?>>> nestedValues;
	private ValueScope scope = ValueScope.SHADER_VALUE_SCOPE_INVALID;
	private final String name;
	private final String valueTypeString;
	private final boolean isArray;
	private int arrayLength;
	
	public ShaderValue(final String nameIn, final String valueTypeStringIn){
		this.name = extractName(nameIn);
		this.valueTypeString = valueTypeStringIn;
		this.isArray = isValueNameArray(nameIn);
		
		initPossibleNestedValues(nameIn);
	}
	
	private void initPossibleNestedValues(final String nameIn){
		final String params = extractParams(nameIn);
		if(isArray){
			arrayLength = extractArrayLengthFromParams(params);
			nestedValues = new HashMap<Integer, HashMap<String, ShaderValue<?>>>();
		}
		else
			nestedValues = null;
	}	
	
	/**
	 * Add new inner nested shaderValue, used a entry in a array if the parameter is an array
	 * @param val
	 * @param index 
	 */
	public void addNestedValue(final ShaderValue<?> val, int index){
		if(nestedValues == null){
			if(l.isLevelLoggable(Log.WARN)){
				l.log("Warning, attmpted to add nested value to ShaderValue that does not support it.");
				return;
			}
		}
		
		HashMap<String, ShaderValue<?>> map = nestedValues.get(index);
		if(map == null){
			map = new HashMap<String, ShaderValue<?>>();
			nestedValues.put(index, map);
		}
		map.put(val.getName(), val);
	}
	
	/**
	 * Get map of values nested in array at index
	 * @param index
	 * @return Map of shader values at the given index, map key is the name of the shaderValues at that index
	 */
	private HashMap<String, ShaderValue<?>> getNestedValue(final int index){
		if(index > arrayLength && l.isLevelLoggable(Log.WARN)){
			l.log("Attempt to get nested array value [" + index + " ] in ShaderValue that is out of range", Log.WARN);
			return null;
		}
			
		return nestedValues.get(index);
	}
	
	/**
	 * Get a ShaderValue for a specific index in the nested map of ShaderValues
	 * @param valueName name of ShaderValue to get, for example a struct member
	 * @param index in array that holds the set of ShaderValues.
	 * @return the shader value at the index and with the name valueName.
	 */
	public ShaderValue<?> getShaderValueInArray(final String valueName, final int index){
		final Map<String, ShaderValue<?>> valuesAtIndex = getNestedValue(index);
		if(valuesAtIndex != null){
			return valuesAtIndex.get(valueName);
		}
		return null;
	}
	
	/**
	 * Get iterator that iterates over all values that are nested in this ShaderValue, including
	 * array indices and struct members.
	 * @return
	 */
	public Iterator<Pair<Integer, ShaderValue<?>>> getNestedValuesIterator(){
		return new NestedValuesIterator(nestedValues);
	}
	
	private int extractArrayLengthFromParams(final String params){
		final int[] values = extractParamInts(params);
		if(values.length > 0 && values[SHADER_VAL_ARRAY_LEN_IDX] != SHADER_VAL_PARAM_INVALID_ENTRY){
			return values[SHADER_VAL_ARRAY_LEN_IDX];
		}
		
		return 0;
	}
	
	private int[] extractParamInts(final String params) {
		final String[] paramsArray = params.split(SHADER_VAL_PARAM_ENTRY_DELIM_TAG_STR);
		final ArrayList<Integer> paramsInt = new ArrayList<Integer>();
		for(String p : paramsArray){
			try{
				int val = Integer.valueOf(p);
				paramsInt.add(val);
			}catch(NumberFormatException e){
				paramsInt.add(SHADER_VAL_PARAM_INVALID_ENTRY);
			}
		}
		final int[] paramValues = Util.integerListToIntArray(paramsInt);
		return paramValues;
	}

	public static String extractName(final String name){
		return splitWithParamsDelimiter(name, 0, name);
	}
	
	public static String extractParams(final String name){
		return splitWithParamsDelimiter(name, 1, "");
	}
	
	private static String splitWithParamsDelimiter(final String name, final int reutrnIndex, 
			final String returnIfFail){
		String[] values = name.split(SHADER_VAL_PARAMS_DELIM_TAG_STR);
		if(values.length > reutrnIndex)
			return values[reutrnIndex];
		
		return returnIfFail;
	}
	
	public static boolean isValueNameArray(final String name){		
		return name.contains(SHADER_VAL_ARRAY_TAG_STR);
	}
	
	/**
	 * Extract length from shader name of format 'array_name[x]:length'
	 * @param name
	 * @return
	 */
	public static int extractLengthFromName(final String name){
		final String[] values = name.split(SHADER_VAL_PARAMS_DELIM_TAG_STR);
		if(values.length > 1)
			return Integer.valueOf(values[1]);
		return 0;
	}

	public void setValue(E valueIn) {
		this.value = valueIn;
	}
	public E getValue() {
		return value;
	}
	
	public String getNameInArray(int index){
		if(isArray && index < arrayLength){
			final String nameCopy = new String(name);
			return nameCopy.replace(SHADER_VAL_ARRAY_TAG_STR, "[" + index + "]");
		}
		return "";
	}
	
	public String getNestedName(int index) {
		if(isArray){
			//TODO
		}
		return "";
	}
	public String getName() {
		return name;
	}
	
	public String getValueTypeString() {
		return valueTypeString;
	}
	
	public String getShaderCodeSring(){
		return valueTypeString + " " + name + ";";
	}

	public void setId(int id, int index) {
		this.id = id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * Get id, used in shader progam, of the shader value. Index is ignored if shader value is not an array. 
	 * @param index Used to get id of index in array - if shader value is an array
	 * @return the id of the shader value.
	 */
	public int getNestedId(int index) {
		if(isArray){
			//return array index value
		}
		return -1;
	}
	
	public int getId() {
		return id;
	}

	public ShaderValue<E> setValueScope(ValueScope scope) {
		this.scope = scope;
		return this;
	}
	public ValueScope getValueScope() {
		return scope;
	}

	public boolean isArray() {
		return isArray;
	}
	
	/**
	 * Used to go through all arrays values and each of those values members
	 * @author jonas
	 *
	 */
	public static class NestedValuesIterator implements Iterator<Pair<Integer, ShaderValue<?>>>{
		private final Iterator<Integer> keysIterator;
		private final HashMap<Integer, HashMap<String, ShaderValue<?>>> valueMap;
		private Iterator<ShaderValue<?>> currentMembersIterator;
		private int currentKey;
		
		private NestedValuesIterator(HashMap<Integer, HashMap<String, ShaderValue<?>>> map){
			this.valueMap = map;
			this.keysIterator = this.valueMap.keySet().iterator();
			if(keysIterator.hasNext()){
				updateCurrentMembersIterator();
			}
		}
		
		private void updateCurrentMembersIterator(){
			currentKey = keysIterator.next();
			currentMembersIterator = this.valueMap.get(currentKey).values().iterator();
		}

		public boolean hasNext() {
			if(currentMembersIterator == null || !currentMembersIterator.hasNext()){
				if(keysIterator.hasNext()){
					updateCurrentMembersIterator();
					return currentMembersIterator.hasNext();
				}
				return false;
			}
			return true;
		}

		public Pair<Integer, ShaderValue<?>> next() {
			if((currentMembersIterator == null || !currentMembersIterator.hasNext()) && keysIterator.hasNext()){
				updateCurrentMembersIterator();
			}
			
			if(currentMembersIterator != null && currentMembersIterator.hasNext())
				return new Pair<Integer, ShaderValue<?>>(currentKey, currentMembersIterator.next());
			return null;
		}

		public void remove() {
			// TODO Auto-generated method stub
			
		}
	}
}
