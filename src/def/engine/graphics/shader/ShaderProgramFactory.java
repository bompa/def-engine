package def.engine.graphics.shader;

import java.util.concurrent.ConcurrentHashMap;

import def.Const;
import def.engine.logic.math.MatrixArray4f;
import def.engine.logic.math.VectorArray2f;
import def.engine.logic.math.VectorArray4f;


public class ShaderProgramFactory {
	
	private final static ShaderProgramFactory instance = new ShaderProgramFactory();

	public void setbasicTexuredFShader(String fs){
		basicTexuredFShader = fs;
	}
	
	public void setPerFragLightNormalMapFShader(String perFragLightNormalMapFShader) {
		this.perFragLightNormalMapFShader = perFragLightNormalMapFShader;
	}
	
	public void setPerFragmentLightningVShader(String vs){
		perFragmentLightningVShader = vs;
	}
	public void setPerFragmentLightningFShader(String fs){
		perFragmentLightningFShader = fs;
	}
	
	public void setBasicVShader(String basicVShader) {
		this.basicVShader = basicVShader;
	}
	public void setBasicFShader(String basicFShader) {
		this.basicFShader = basicFShader;
	}
	
	private String basicTexuredFShader = "";
	private String perFragLightNormalMapFShader = "";
	
	private String perFragmentLightningVShader = "";		
	private String perFragmentLightningFShader = "";
	
	private String basicVShader = "";		
	private String basicFShader = "";
	
	private String tangetSpaceVSShader = "";
	
	public void addCustomFShader(String name, String basicFShaderCode) {
		customFragShaderPrograms.put(name, basicFShaderCode);
	}
	
	public void addCustomVShader(String name, String basicVShaderCode) {
		customVertexShaderPrograms.put(name, basicVShaderCode);
	}
	
	private ConcurrentHashMap<String, String> customFragShaderPrograms = new ConcurrentHashMap<String, String>();
	private ConcurrentHashMap<String, String> customVertexShaderPrograms = new ConcurrentHashMap<String, String>();
	
	public IShaderProgram createCustomFragmentShaderProgram(String name){
		String prog = customFragShaderPrograms.get(name);
		return null;
	}
	
	public IShaderProgram createVertexShaderProgram(String name){
		String prog = customVertexShaderPrograms.get(name);
		return null;
	}
	
	private void addLightParams(final ShaderValue<Shaders.ShaderArray> lights, final int nLights){
		for(int i = 0; i < nLights; i++){
			lights.addNestedValue(Shaders.createShaderVec3Uniform(Shaders.LightStruct.POSITION), i);
			lights.addNestedValue(Shaders.createShaderVec3Uniform(Shaders.LightStruct.DIFFUSE), i);
			lights.addNestedValue(Shaders.createShaderVec3Uniform(Shaders.LightStruct.AMBIENT), i);
		}
	}
	
	//* Basic texured, no lightning *
	public IShaderProgram createBasicTexuredFShader(){
		// Create with mandatory params
		final ShaderValue<Sampler2D> s2d = Shaders.createShaderSampler2DUniform("sTexture0");
		final FragmentShader fs = new FragmentShader("BasicTexuredFShader", basicTexuredFShader, s2d); 
	
		return fs;
	}
	
	//* Vertex-shader requiring tangent and binormal imput. Varying lightdir output in tangent space *
	public IShaderProgram createTangentSpaceVertShader(){
		// Create with mandatory params
		ShaderValue<MatrixArray4f> mvp = Shaders.createShaderMtx4Uniform("uMVPMatrix");
		ShaderValue<VectorArray4f> pos = Shaders.createShaderVec4Attribute("aPosition");
		ShaderValue<VectorArray2f> texc = Shaders.createShaderVec2Attribute("aTextureCoord");
		VertexShader vs = new VertexShader("TangetBinormalVSShader", tangetSpaceVSShader, mvp, pos, texc);
		
		// Add optional params
		vs.setTangentParam(Shaders.createShaderVec3Attribute("aTangent"));
		vs.setBinormalParam(Shaders.createShaderVec3Attribute("aBinormal"));
		
		vs.setModelViewParam(	Shaders.createShaderMtx4Uniform("uMVMatrix")	);
		vs.setNormalsParam(		Shaders.createShaderVec3Attribute("aNormal")	);
		return vs;
	}
		
	//* Per-fragment normal mapped lightning *
	public IShaderProgram createPerFragLightNormalMapFragShader(){
		final ShaderValue<Sampler2D> s2d = Shaders.createShaderSampler2DUniform("sTexture");
		final FragmentShader fs = new FragmentShader("PerFragmentLightNormalMapFShader", perFragLightNormalMapFShader, s2d);
		
		final ShaderValue<Sampler2D> sNm = Shaders.createShaderSampler2DUniform("sNormalMap");
		fs.setSampler2dNormalMap(sNm);
		
		// Add optional params
		createPerFragLightParams(fs);
		
		return fs;
	}

	//* Per-fragment ligtning *
	public IShaderProgram createPerFragmentLightningFragShader(){
		// Create with mandatory params
		final ShaderValue<Sampler2D> s2d = Shaders.createShaderSampler2DUniform("sTexture");
		final FragmentShader fs = new FragmentShader("PerFragmentLightningFShader", perFragmentLightningFShader, s2d); 
		
		// Add optional params
		createPerFragLightParams(fs);
		return fs;
	}

	private void createPerFragLightParams(final FragmentShader fs) {
		final ShaderValue<Shaders.ShaderArray> lights = Shaders.createShaderArrayUniform(Shaders.makeArrayString("lightArr", Const.SHADER_DEF_NR_OF_LIGHTS));
		addLightParams(lights, Const.SHADER_DEF_NR_OF_LIGHTS);
		fs.setLightsParam(lights);
		
		fs.setMatEmmisionParam(			Shaders.createShaderVec4Uniform("uMatEmmison")	);
		fs.setMatDiffuseParam(			Shaders.createShaderVec4Uniform("uMatDiffuse")	);
		fs.setMatBrightnessParam(		Shaders.createShaderVec4Uniform("uMatBrightness")	);
		fs.setAlphaParam(				Shaders.createShaderFloatUniform("uAlpha")		);
		fs.setSizeOfLightsArrayParam(	Shaders.createShaderIntUniform("uNrOfLights")	);
	}
	
	public IShaderProgram createPerFragmentLightningVertShader(){
		// Create with mandatory params
		ShaderValue<MatrixArray4f> mvp = Shaders.createShaderMtx4Uniform("uMVPMatrix");
		ShaderValue<VectorArray4f> pos = Shaders.createShaderVec4Attribute("aPosition");
		ShaderValue<VectorArray2f> texc = Shaders.createShaderVec2Attribute("aTextureCoord");
		VertexShader vs = new VertexShader("PerFragmentLightningVShader", perFragmentLightningVShader, mvp, pos, texc);
		
		// Add optional params
		vs.setModelViewParam(	Shaders.createShaderMtx4Uniform("uMVMatrix")	);
		vs.setNormalsParam(		Shaders.createShaderVec3Attribute("aNormal")	);
		return vs;
	}
	
	//* Basic ligtning *
	public IShaderProgram createBasicFragShader(){
		FragmentShader fs = new FragmentShader("BasicFShader", basicFShader);
		
		fs.setAlphaParam( Shaders.createShaderFloatUniform("uAlpha") );
		
		return fs;
	}
	
	public IShaderProgram createPerBasicVertShader(){
		// Create with mandatory params
		ShaderValue<MatrixArray4f> mvp = Shaders.createShaderMtx4Uniform("uMVPMatrix");
		ShaderValue<VectorArray4f> pos = Shaders.createShaderVec4Attribute("aPosition");
		ShaderValue<VectorArray4f> col = Shaders.createShaderVec4Attribute("aColor");

		VertexShader vs = new VertexShader("BasicVShader", basicVShader, mvp, pos);
		vs.setColorParam(col);
		return vs;
	}

	public static ShaderProgramFactory instance() {
		return instance;
	}

	public void setTangetSpaceVSShader(String tangetSpaceVSShader) {
		this.tangetSpaceVSShader = tangetSpaceVSShader;
	}
}
