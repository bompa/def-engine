package def.engine.graphics.shader;

import def.engine.graphics.driver.IGraphicsResources;
import def.engine.graphics.shader.Shaders.ShaderArray;
import def.engine.logic.math.VectorArray4f;

public class FragmentShader extends AbstractShader {
	
	/** Optional shader parameters */
	private ShaderValue<Sampler2D> sampler2d;
	private ShaderValue<Sampler2D> sampler2dNormalMap;
	private ShaderValue<VectorArray4f> matEmm;
	private ShaderValue<VectorArray4f> matDiff;
	private ShaderValue<VectorArray4f> matBright;
	private ShaderValue<Float> alpha;
	private ShaderValue<Integer> nrOfLights;
	private ShaderValue<ShaderArray> lights;
	
	public FragmentShader(final String sn, String sc) {
		super(sn, sc);
	}

	public FragmentShader(final String sn, String sc, ShaderValue<Sampler2D> s2d) {
		super(sn, sc);
		sampler2d = s2d;
	}

	public ShaderValue<Sampler2D> getSampler2d() {
		return sampler2d;
	}

	public void initShaderParams(IGraphicsResources irend) {
		IShaderProgram.Id program = getParentProgramId();
		
		Shaders.initParam(irend, sampler2d, program);
		Shaders.initParam(irend, sampler2dNormalMap, program);
		Shaders.initParam(irend, lights, program);
		Shaders.initParam(irend, matDiff, program);
		Shaders.initParam(irend, matEmm, program);
		Shaders.initParam(irend, matBright, program);
		Shaders.initParam(irend, alpha, program);
		Shaders.initParam(irend, nrOfLights, program);
	}
	
	/*
	 * Set optional shader parameters
	 */
	public void setSampler2d(ShaderValue<Sampler2D> sampler) {
		this.sampler2d = sampler;
	}
	
	public void setSampler2dNormalMap(ShaderValue<Sampler2D> sampler2dNormalMap) {
		this.sampler2dNormalMap = sampler2dNormalMap;
	}
	
	public void setMatDiffuseParam(ShaderValue<VectorArray4f> matDiff) {
		this.matDiff = matDiff;
	}
	public void setMatEmmisionParam(ShaderValue<VectorArray4f> matEmm) {
		this.matEmm = matEmm;
	}
	public void setMatBrightnessParam(ShaderValue<VectorArray4f> matBright) {
		this.matBright = matBright;
	}
	public void setAlphaParam(ShaderValue<Float> alpha) {
		this.alpha = alpha;
	}
	public void setLightsParam(ShaderValue<ShaderArray> lights) {
		this.lights = lights;
	}
	public void setSizeOfLightsArrayParam(ShaderValue<Integer> nofl) {
		this.nrOfLights = nofl;
	}
	
	
	/*
	 * Get Names/IDs for option params
	 */
	
	// Texturesampler
	public String getSampler2dUniformName() {
		return Shaders.getOptionalParamName(sampler2d);
	}
	public int getSampler2dUniformId() {
		return Shaders.getOptionalParam(sampler2d);
	}
	
	// Normal Map sampler
	public String getSampler2dNormalMapUniformName() {
		return Shaders.getOptionalParamName(sampler2dNormalMap);
	}
	public int getSampler2dNormalMapUniformId() {
		return Shaders.getOptionalParam(sampler2dNormalMap);
	}

	// Material emission
	public String getMatEmmisionUniformName() {
		return Shaders.getOptionalParamName(matEmm);
	}
	public int getMatEmmisionUniformId() {
		return Shaders.getOptionalParam(matEmm);
	}
	
	//Material diffuse
	public String getMatDiffuseUniformName() {
		return Shaders.getOptionalParamName(matDiff);
	}
	public int getMatDiffuseUniformId() {
		return Shaders.getOptionalParam(matDiff);
	}
	
	//Material brightness
	public String getMatBrightnessUniformName() {
		return Shaders.getOptionalParamName(matBright);
	}
	public int getMatBrightnessUniformId() {
		return Shaders.getOptionalParam(matBright);
	}
	
	//Alpha
	public String getAlphaUniformName() {
		return Shaders.getOptionalParamName(alpha);
	}
	public int getAlphaUniformId() {
		return Shaders.getOptionalParam(alpha);
	}
	
	//Nr of lights
	public String getNrOfLightsUniformName() {
		return Shaders.getOptionalParamName(nrOfLights);
	}
	public int getNrOfLightsUniformId() {
		return Shaders.getOptionalParam(nrOfLights);
	}

	//Light array
	public int getLightUniformId(final String lightMemberName, final int index) {
		return Shaders.getOptionalStructInArrayParamId(lights, lightMemberName, index);
	}
	public String getLightUniformName(final String lightMemberName, final int index) {
		return Shaders.getOptionalStructInArrayParamName(lights, lightMemberName, index);
	}
}
