package def.engine.graphics.shader;

public abstract class UniformValue<E> extends ShaderValue<E>{
	
	public final static String UNIFORM_SHADER_TYPE_STR = "uniform"; 

	public UniformValue(final String name, final String 	tmp){
		super(name, UNIFORM_SHADER_TYPE_STR);
	}
	
	public UniformValue(final E value, final String name){
		super(name, UNIFORM_SHADER_TYPE_STR);
		setValue(value);
	}
}
