package def.engine.graphics.shader;

import def.engine.graphics.driver.IGraphicsResources;
import def.shared.util.RestrictInterfaceWrapper;

public class ShaderProgramImmutableWrapper extends RestrictInterfaceWrapper<IShaderProgram> implements IShaderProgram {
	
	public ShaderProgramImmutableWrapper(IShaderProgram wc){
		super(wc, "Illegal modify of immutable IShaderProgram");
	}

	public String getSource() {
		return getWrappedContent().getSource();
	}

	public void initShaderParams(IGraphicsResources irend) {
		handleUnsupported();
	}

	public Id getInstanceProgramId() {
		return getWrappedContent().getInstanceProgramId();
	}

	public Id getParentProgramId() {
		return getWrappedContent().getParentProgramId();
	}

	public void setInstanceProgramId(Id parentProgramId) {
		handleUnsupported();
	}

	public void setParentProgramId(Id parentProgramId) {
		handleUnsupported();
	}

	public String getShaderName() {
		return getWrappedContent().getShaderName();
	}

	public int getSizeOfLightArray() {
		return getWrappedContent().getSizeOfLightArray();
	}

	public void setSizeOfLightsArray(int sizeOfLightsArray) {
		handleUnsupported(); 
	}
}
