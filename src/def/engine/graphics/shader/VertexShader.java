package def.engine.graphics.shader;

import def.engine.graphics.driver.IGraphicsResources;
import def.engine.logic.math.MatrixArray4f;
import def.engine.logic.math.VectorArray2f;
import def.engine.logic.math.VectorArray3f;
import def.engine.logic.math.VectorArray4f;

public class VertexShader extends AbstractShader {
	/** Mandatory shader parameters */
	private final ShaderValue<MatrixArray4f> mvpMtx;
	private final ShaderValue<VectorArray4f> pos;
	
	/** Optional shader parameters */
	private ShaderValue<VectorArray2f> texc;
	private ShaderValue<MatrixArray4f> mvMtx;
	private ShaderValue<VectorArray3f> normal;
	private ShaderValue<VectorArray4f> color;
	private ShaderValue<VectorArray3f> tangent;
	private ShaderValue<VectorArray3f> binormal;
	
	public VertexShader(final String sn, final String sc,
			final ShaderValue<MatrixArray4f> mvpMtx,
			final ShaderValue<VectorArray4f> pos) {
		super(sn, sc);
		if(mvpMtx == null || pos == null)
			throw new IllegalArgumentException("null arguments when creating shaders");
		this.mvpMtx = mvpMtx;
		this.pos = pos;
	}


	public VertexShader(final String sn, final String sc,
			final ShaderValue<MatrixArray4f> mvpMtx,
			final ShaderValue<VectorArray4f> pos,
			final ShaderValue<VectorArray2f> texc) {
		super(sn, sc);
		if(mvpMtx == null || pos == null || texc == null)
			throw new IllegalArgumentException("null arguments when creating shaders");
		this.mvpMtx = mvpMtx;
		this.pos = pos;
		this.texc = texc;
	}
	
	public void initShaderParams(IGraphicsResources irend){
		IShaderProgram.Id program = getParentProgramId();
		
		Shaders.initParam(irend, mvpMtx, program);
		Shaders.initParam(irend, pos, program);
		Shaders.initParam(irend, texc, program);
		Shaders.initParam(irend, normal, program);	
		Shaders.initParam(irend, mvMtx, program);
		Shaders.initParam(irend, color, program);
		Shaders.initParam(irend, tangent, program);
		Shaders.initParam(irend, binormal, program);
	}
	
	/*
	 * Set optional shader parameters
	 */
	public void setTexCoordParam( final ShaderValue<VectorArray2f> texc){
		this.texc = texc;	
	}
	public void setModelViewParam( final ShaderValue<MatrixArray4f> mvMtx){
		this.mvMtx = mvMtx;	
	}
	public void setNormalsParam(final ShaderValue<VectorArray3f> normals) {
		this.normal = normals;
	}
	public void setColorParam(final ShaderValue<VectorArray4f> color) {
		this.color = color;
	}
	public void setTangentParam(final ShaderValue<VectorArray3f> t) {
		this.tangent = t;
	}
	public void setBinormalParam(final ShaderValue<VectorArray3f> bin) {
		this.binormal = bin;
	}
	
	/*
	 * Get Names
	 */
	String getMvpMatrixUniformName(){
		return mvpMtx.getName();
	}
	
	public String getPosVecAttrName() {
		return pos.getName();
	}

	public String getTexCoordsVecAttrName() {
		return texc.getName();
	}
	
	/*
	 * Get Names for option params
	 */
	public String getModelViewMtxUniformName() {
		return Shaders.getOptionalParamName(mvMtx);
	}
	
	public String getNormalUniformName() {
		return Shaders.getOptionalParamName(normal);
	}
	
	public String getColorUniformName() {
		return Shaders.getOptionalParamName(color);
	}
	
	public String getTangentUniformName() {
		return Shaders.getOptionalParamName(tangent);
	}
	
	public String getBinormalUniformName() {
		return Shaders.getOptionalParamName(binormal);
	}
	
	/*
	 * Get Ids(handles)
	 */
	public int getMvpMatrixUniformId(){
		return mvpMtx.getId();
	}
	
	public int getPosVecAttrId() {
		return pos.getId();
	}

	public int getTexCoordsVecAttrId() {
		return texc.getId();
	}
	
	/*
	 * Get Ids(handles) for optional params
	 */
	public int getModelViewMtxUniformId() {
		return Shaders.getOptionalParam(mvMtx);
	}
	
	public int getNormalAttrId() {
		return Shaders.getOptionalParam(normal);
	}
	
	public int getColorAttrId() {
		return Shaders.getOptionalParam(color);
	}
	
	public int getTangentAttrId() {
		return Shaders.getOptionalParam(tangent);
	}
	
	public int getBinormalAttrId() {
		return Shaders.getOptionalParam(binormal);
	}

}
