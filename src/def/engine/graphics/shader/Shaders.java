package def.engine.graphics.shader;

import java.util.Iterator;

import android.util.Log;
import def.engine.graphics.driver.IGraphicsResources;
import def.engine.graphics.shader.ShaderValue.ValueScope;
import def.engine.logic.math.MatrixArray4f;
import def.engine.logic.math.VectorArray2f;
import def.engine.logic.math.VectorArray3f;
import def.engine.logic.math.VectorArray4f;
import def.util.LogHandler;
import android.util.Pair;

public class Shaders {
	public final static String UNIFORM_SHADER_TYPE_STR = "uniform";
	public final static String ATTRIBUTE_SHADER_TYPE_STR = "attribute";

	public final static String SHADER_TYPE_MTX4_STR = "mat4";
	public final static String SHADER_TYPE_VEC4_STR = "vec4";
	public final static String SHADER_TYPE_VEC3_STR = "vec3";
	public final static String SHADER_TYPE_VEC2_STR = "vec2";
	public final static String SHADER_TYPE_FLOAT_STR = "float";
	public final static String SHADER_TYPE_INT_STR = "int";
	public final static String SHADER_TYPE_SAMPLER2D_STR = "sampler2D";
	
	public final class LightStruct{
		public final static String POSITION = "uLightPos";
		public final static String AMBIENT = "ambient";
		public final static String DIFFUSE = "diffuse";
	}
	
	public static final LogHandler l = LogHandler.createInstanceShared();
	private static final int INVALID_SHADER_ID = -1;
	
	public static String getArrayString(final String arrName){
		return arrName + ShaderValue.SHADER_VAL_ARRAY_TAG_STR;
	}
	
	public static String makeArrayString(final String arrName, final int size){
		return getArrayString(arrName) + 
			ShaderValue.SHADER_VAL_PARAMS_DELIM_TAG_STR +
			size +
			ShaderValue.SHADER_VAL_PARAM_ENTRY_DELIM_TAG_STR;
	}
	
	/**
	 * Create a immutable shader program wrapper.
	 * @param isp the shader program reference to wrap and make immutable
	 * @return Immutable shader program
	 */
	public static IShaderProgram unmodifiableShaderProgram(IShaderProgram isp){
		return new ShaderProgramImmutableWrapper(isp);
	}
	
	/**
	 * Check compile status of unlinked shader program.
	 * @param p shader program to check
	 * @return true if successfully compiled, false otherwise.
	 */
	public static boolean checkComileStatus(IShaderProgram p){
		if(p == null || p.getInstanceProgramId() == null)
			return false;
		
		return p.getInstanceProgramId().get() != 0;
	}
	
	/**
	 * Get Shader value in array if supplied shaderValue is not null
	 */
	private static <E> ShaderValue<?> getOptionalShaderValueInArray(final ShaderValue<E> sv, final String structMemberName, final int index){
		if(sv == null)
			return null;
		return sv.getShaderValueInArray(structMemberName, index);
	}
			
	
	/**
	 * Get ids for optional params, wich are not arrays(using array index 0)
	 */
	public static <E> int getOptionalParam(ShaderValue<E> sv){
		if(sv == null)
			return -1;
		return sv.getId();	
	}
	
	/**
	 * Get ids for optional params held in the array of the current shader value
	 */
	public static <E> int getOptionalArrayParam(final ShaderValue<E> sv, final int index){
		if(sv == null)
			return -1;
		return sv.getNestedId(index);	
	}
	
	/**
	 * Get member id of shaderValue in array with given name
	 * @param <E> type of shader value
	 * @param sv Shader value holding array of nested shader values
	 * @param structMemberName member name to find at given index in parameter sv
	 * @param index
	 * @return found name of shaderValue with given name and index
	 */
	public static <E> int getOptionalStructInArrayParamId(final ShaderValue<E> sv, final String structMemberName, final int index){
		final ShaderValue<?> innerSv = getOptionalShaderValueInArray(sv, structMemberName, index);
		if(innerSv != null)
			return innerSv.getId();
		
		return -1;
	}
	
	/**
	 * Get name for optional params
	 */
	public static <E> String getOptionalParamName(final ShaderValue<E> sv){
		if(sv == null)
			return "";
		return sv.getName();
	}
	
	/**
	 * Get name for optional params, held in the array of the current shader value
	 */
	public static <E> String getOptionalArrayParamName(final ShaderValue<E> sv, final int index){
		if(sv == null)
			return "";
		return sv.getNestedName(index);
	}
	
	/**
	 * Get member in arrays with given name
	 * @param <E> type of shader value
	 * @param sv Shader value holding array of nested shader values
	 * @param structMemberName member name to find at given index in parameter sv
	 * @param index
	 * @return found name of shaderValue with given name and index
	 */
	public static <E> String getOptionalStructInArrayParamName(final ShaderValue<E> sv, final String structMemberName, final int index){
		final ShaderValue<?> innerSv = getOptionalShaderValueInArray(sv, structMemberName, index);
		if(innerSv != null)
			return sv.getNameInArray(index) + "." + innerSv.getName();
		
		return "";
	}
	

	/**
	 * Init array shader param
	 * @param <E>
	 * @param irend the engine render, used to get render driver info about a parameter
	 * @param sv ShaderValue to initalize
	 * @param parentId id of program which is linked to the shaderValue
	 */
	private static <E> void initArrayParam(final IGraphicsResources irend, final ShaderValue<E> sv, final IShaderProgram.Id parentId){
		Iterator<Pair<Integer, ShaderValue<?>>> it = sv.getNestedValuesIterator();
		while(it.hasNext()){
			Pair<Integer, ShaderValue<?>> innerKeySvPair = it.next();
			final int index = innerKeySvPair.first;
			final ShaderValue<?> innerSv = innerKeySvPair.second;
			final int id = irend.getShaderParamId(parentId, sv.getNameInArray(index) + "." + innerSv.getName(), sv.getValueScope());
			setShaderValueId(innerSv, id);
		}
	}
	
	private static <E> void initNormalParam(final IGraphicsResources irend, final ShaderValue<E> sv, final IShaderProgram.Id parentId){
		final int id = irend.getShaderParamId(parentId, sv.getName(), sv.getValueScope());
		setShaderValueId(sv, id);	
	}
	
	private static void setShaderValueId(final ShaderValue<?> sv, final int id){
		if(id == -1){
			if(l.isLevelLoggable(Log.WARN))
				l.log("Failed to init shader parameter \'" + sv.getName() + "\'." , Log.WARN);
		}
		sv.setId(id);
	}
	
	/**
	 * Init a shader parameter
	 */
	public static <E> void initParam(final IGraphicsResources irend, final ShaderValue<E> sv, final IShaderProgram.Id parentId){
		if(sv == null)
			return;
		if(sv.isArray()){
			initArrayParam(irend, sv, parentId);
		}
		else{
			initNormalParam(irend, sv, parentId);
		}
	}
	
	/**
	 * Dummy for Shader Values 
	 * @author jonas
	 */
	public final class ShaderArray{}
	
	/***************************************************
	 * factory methods for different shader value types
	 ***************************************************/
	/**
	 * Create shadervalue array
	 */
	public static ShaderValue<ShaderArray> createShaderArrayAttribute(final String name){
		return createShaderArray(name, ATTRIBUTE_SHADER_TYPE_STR).setValueScope(ValueScope.SHADER_VALUE_SCOPE_PER_VERTEX);
	}
	
	public static ShaderValue<ShaderArray> createShaderArrayUniform(final String name){
		return createShaderArray(name, UNIFORM_SHADER_TYPE_STR).setValueScope(ValueScope.SHADER_VALUE_SCOPE_PER_PRIMITIVE);
	}
	
	private static ShaderValue<ShaderArray> createShaderArray(final String name, String memType){
		return new ShaderValue<ShaderArray>(name, memType);
	}
	
	/**
	 * Create shadervalue of float type
	 */
	public static ShaderValue<Float> createShaderFloatAttribute(final String name){
		return createShaderFloat(name, ATTRIBUTE_SHADER_TYPE_STR).setValueScope(ValueScope.SHADER_VALUE_SCOPE_PER_VERTEX);
	}
	
	public static ShaderValue<Float> createShaderFloatUniform(final String name){
		return createShaderFloat(name, UNIFORM_SHADER_TYPE_STR).setValueScope(ValueScope.SHADER_VALUE_SCOPE_PER_PRIMITIVE);
	}
	
	private static ShaderValue<Float> createShaderFloat(final String name, String memType){
		memType = memType + " " + SHADER_TYPE_FLOAT_STR;
		return new ShaderValue<Float>(name, memType);
	}
	
	/**
	 * Create shadervalue of int type
	 */
	public static ShaderValue<Integer> createShaderIntAttribute(final String name){
		return createShaderInt(name, ATTRIBUTE_SHADER_TYPE_STR).setValueScope(ValueScope.SHADER_VALUE_SCOPE_PER_VERTEX);
	}
	
	public static ShaderValue<Integer> createShaderIntUniform(final String name){
		return createShaderInt(name, UNIFORM_SHADER_TYPE_STR).setValueScope(ValueScope.SHADER_VALUE_SCOPE_PER_PRIMITIVE);
	}
	
	private static ShaderValue<Integer> createShaderInt(final String name, String memType){
		memType = memType + " " + SHADER_TYPE_INT_STR;
		return new ShaderValue<Integer>(name, memType);
	}

	
	/**
	 * Create shadervalue of matrix4f type
	 */
	public static ShaderValue<MatrixArray4f> createShaderMtx4Attribute(final String name){
		return createShaderMtx4(name, ATTRIBUTE_SHADER_TYPE_STR).setValueScope(ValueScope.SHADER_VALUE_SCOPE_PER_VERTEX);
	}
	
	public static ShaderValue<MatrixArray4f> createShaderMtx4Uniform(final String name){
		return createShaderMtx4(name, UNIFORM_SHADER_TYPE_STR).setValueScope(ValueScope.SHADER_VALUE_SCOPE_PER_PRIMITIVE);
	}
	
	private static ShaderValue<MatrixArray4f> createShaderMtx4(final String name, String memType){
		memType = memType + " " + SHADER_TYPE_MTX4_STR;
		return new ShaderValue<MatrixArray4f>(name, memType);
	}
	
	/**
	 * Create shadervalue of vec4f type
	 */
	public static ShaderValue<VectorArray4f> createShaderVec4Attribute(final String name){
		return createShaderVec4(name, ATTRIBUTE_SHADER_TYPE_STR).setValueScope(ValueScope.SHADER_VALUE_SCOPE_PER_VERTEX);
	}
	
	public static ShaderValue<VectorArray4f> createShaderVec4Uniform(final String name){
		return createShaderVec4(name, UNIFORM_SHADER_TYPE_STR).setValueScope(ValueScope.SHADER_VALUE_SCOPE_PER_PRIMITIVE);
	}
	
	private static ShaderValue<VectorArray4f> createShaderVec4(final String name, String memType){
		memType = memType + " " + SHADER_TYPE_VEC4_STR;
		return new ShaderValue<VectorArray4f>(name, memType);
	}
	
	/**
	 * Create shadervalue of vec3f type
	 */
	public static ShaderValue<VectorArray3f> createShaderVec3Attribute(final String name){
		return createShaderVec3(name, ATTRIBUTE_SHADER_TYPE_STR).setValueScope(ValueScope.SHADER_VALUE_SCOPE_PER_VERTEX);
	}
	
	public static ShaderValue<VectorArray3f> createShaderVec3Uniform(final String name){
		return createShaderVec3(name, UNIFORM_SHADER_TYPE_STR).setValueScope(ValueScope.SHADER_VALUE_SCOPE_PER_PRIMITIVE);
	}
	
	private static ShaderValue<VectorArray3f> createShaderVec3(final String name, String memType){
		memType = memType + " " + SHADER_TYPE_VEC3_STR;
		return new ShaderValue<VectorArray3f>(name, memType);
	}
	
	/**
	 * Create shadervalue of vec2f type
	 */
	public static ShaderValue<VectorArray2f> createShaderVec2Attribute(final String name){
		return createShaderVec2(name, ATTRIBUTE_SHADER_TYPE_STR).setValueScope(ValueScope.SHADER_VALUE_SCOPE_PER_VERTEX);
	}
	
	public static ShaderValue<VectorArray2f> createShaderVec2Uniform(final String name){
		return createShaderVec2(name, UNIFORM_SHADER_TYPE_STR).setValueScope(ValueScope.SHADER_VALUE_SCOPE_PER_PRIMITIVE);
	}
	
	private static ShaderValue<VectorArray2f> createShaderVec2(final String name, String memType){
		memType = memType + " " + SHADER_TYPE_VEC4_STR;
		return new ShaderValue<VectorArray2f>(name, memType);
	}
	
	/**
	 * Create shadervalue of sampler2d type
	 */
	public static ShaderValue<Sampler2D> createShaderSampler2DAttribute(final String name){
		return createShaderSampler2D(name, ATTRIBUTE_SHADER_TYPE_STR).setValueScope(ValueScope.SHADER_VALUE_SCOPE_PER_VERTEX);
	}
	
	public static ShaderValue<Sampler2D> createShaderSampler2DUniform(final String name){
		return createShaderSampler2D(name, UNIFORM_SHADER_TYPE_STR).setValueScope(ValueScope.SHADER_VALUE_SCOPE_PER_PRIMITIVE);
	}
	
	private static ShaderValue<Sampler2D> createShaderSampler2D(final String name, String memType){
		memType = memType + " " + SHADER_TYPE_SAMPLER2D_STR;
		return new ShaderValue<Sampler2D>(name, memType);
	}

	public static boolean isShaderIdValid(int id) {
		return id != INVALID_SHADER_ID;
	}
}
