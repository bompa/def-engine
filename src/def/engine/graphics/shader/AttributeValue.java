package def.engine.graphics.shader;

public class AttributeValue<E> {
	private final E value;
	private final String name;
	
	public AttributeValue(final E value, final String name){
		this.value = value;
		this.name = name;
	}

	public E getUniformValue() {
		return value;
	}

	public String geAttributetName() {
		return name;
	}
}
