package def.engine.graphics;

import def.engine.graphics.driver.Graphics.RenderSequenceAction;

public interface IRenderSequenceEvent {

	public abstract RenderSequenceAction getAction();

	public abstract void setRenderStep(long renderStep_);

	public abstract long getRenderStep();

}