package def.engine.graphics;

import def.engine.logic.math.MatrixArray4f;

public class ViewFrustumImmutableWrapper implements IViewFrustum{
	
	private final IViewFrustum wrappedContent_; 

	public ViewFrustumImmutableWrapper(IViewFrustum wrappedContent){
		wrappedContent_ = wrappedContent;
	}
	
	private static final String ILLEGAL_MODIFY_MSG = "Modification of immutable ViewPort not allowed!";
	
	private void handleUnsupported(){
		throw new UnsupportedOperationException(ILLEGAL_MODIFY_MSG);
	}
	
	public void setAspectRatio(float aspectRatio_) {
		handleUnsupported();
	}
	public void setFovy(float fovy_) {
		handleUnsupported();
	}
	public void setzNear(float zNear_) {
		handleUnsupported();
	}
	public void setzFar(float zFar_) {
		handleUnsupported();
	}

	public float getAspectRatio() {
		return wrappedContent_.getAspectRatio();
	}

	public float getFovy() {
		return wrappedContent_.getFovy();
	}

	public float getzFar() {
		return wrappedContent_.getzFar();
	}

	public float getzNear() {
		return wrappedContent_.getzNear();
	}

	public MatrixArray4f getProjectionMatrix() {
		return wrappedContent_.getProjectionMatrix();
	}

	public MatrixArray4f calcualteProjectionMatrix(float fovy, float aspectRatio, float zNear, float zFar) {
		return wrappedContent_.calcualteProjectionMatrix(fovy, aspectRatio, zNear, zFar);
	}

	public void set(float fovy, float aspectRatio, float zNear, float zFar) {
		wrappedContent_.set(fovy, aspectRatio, zNear, zFar);
	}
}
