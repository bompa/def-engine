package def.engine.graphics.material;

import def.util.FloatPair;
import android.util.Pair;


public class Texture implements ITexture {
	private final int androidResId;
	private int customId = ITexture.NotUsingCustomID;
	private ITexture.RenderId renderId = ITexture.RenderId.getInvalidID();
	private ITexture.Settings settings = ITexture.Settings.getCommonsettings();
	
	private RenderType renderType = RenderType.TEX_BASIC; 
	
	public static Texture createWithoutResourceID(){
		return create(ITexture.NotUsingResourceID);
	}
	
	public static Texture create(int rid){
		return new Texture(rid);
	}
	
	public Texture(int resId){
		androidResId = resId;
	}
	
	public int getAndroidResource(){
		return androidResId;
	}
	
	public void setRenderId(ITexture.RenderId renderId) {
		this.renderId = renderId;
	}

	public ITexture.RenderId getRenderId() {
		return renderId;
	}

	public int getTextureRatio() {
		return 1;
	}

	public Pair<FloatPair, FloatPair> getTextureCoordinates() {
		return Materials.createDefaultTextureCoordinates();
	}

	public Settings getSettings() {
		return settings;
	}

	public void setSettings(final Settings settings) {
		this.settings = settings;
	}

	public int getCustomId() {
		return customId;
	}

	public void setCustomId(int id) {
		customId = id;
	}

	public RenderType getRenderType() {
		return renderType;
	}

	public void setRenderType(RenderType renderType) {
		this.renderType = renderType;
	}

}

