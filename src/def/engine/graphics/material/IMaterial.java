package def.engine.graphics.material;

import javax.vecmath.Vector4f;

import def.engine.graphics.components.Light;
import def.engine.graphics.shader.FragmentShader;
import def.engine.graphics.shader.IShaderProgram;
import def.engine.graphics.shader.VertexShader;
import def.util.FloatPair;

public interface IMaterial {
	enum MaterialMode{NONE, FIXED_COLOR, FIXED_TEXTURE, SHADER_PROGRAMS_COLOR, SHADER_PROGRAMS_TEXTURE};
	static final int TEX_UNIT_ALL = -1;
	static final int TEX_UNIT_0 = 0;
	static final int TEX_UNIT_MAX = 32;// TODO Read from drivrer

	public abstract void setShaders(FragmentShader fs, VertexShader vs);
	public abstract FragmentShader getFragmentShader();
	public abstract VertexShader getVertexShader();
	
	public abstract boolean hasProgram();
	
	/**
	 * Set id of shader program compiled by graphics driver
	 * @param id
	 */
	public abstract void setShaderid(IShaderProgram.Id id);
	
	/**
	 * Get the first (deafult)texture
	 * @return
	 */
	public abstract ITexture getTexture();
	
	/**
	 * Get the i:th texture if multitexturing is used
	 * @param i
	 * @return
	 */
	public abstract ITexture getTexture(int i);
	
	/**
	 * Add another texture to the material
	 * @param texture the texture to add
	 * @return the index of the added texture
	 */
	public abstract int addTexture(final ITexture texture);
	
	/**
	 * Get the number of textures used with this material
	 * @return
	 */
	public abstract int getNrOfTextures();
	
	/**
	 * Get the color if not texture is used
	 * @return
	 */
	public abstract Vector4f getColor();
	
	/**
	 * If the material is compiler with the driver, get the id of the compiled shader
	 * @return the id of the compiled shader, 0 if not compiled
	 */
	public abstract IShaderProgram.Id getShaderid();
	
	public abstract MaterialMode getMode();
	
	/**
	 * Get scale of textures used with material.
	 * @return IntPair with first=u-scalue and seconde=v-scale
	 */
	public abstract FloatPair getTexturesScale();
	
	/**
	 * @param uv
	 * @see getTexturesScale
	 */
	public abstract void setTexturesScale(FloatPair uv);
	
	/**
	 * This material needs the vertex tangent value to be
	 * calcualted to be rendered coorectly;
	 * @return
	 */
	public boolean requiresTangent();
	
	/**
	 * @see requiresTangent 
	 * @param requiresTangentValue
	 */
	public void setRequiresTangent(boolean requiresTangentValue);
	
	public abstract Properties getProperties();
	public abstract void setProperties(Properties p);
	
	/**
	 * Internal class that defines light and material properties
	 * @author jonas
	 *
	 */
	public static class Properties{
		private Light.Properties lightProps;
		private Vector4f emission = new Vector4f();
		private Vector4f brightness = new Vector4f();
		private float alpha;
		
		public static Properties createFullAlpha(final Light.Properties lp){
			final Properties p = new Properties(lp);
			p.setAlpha(1.0f);
			return p;
		}
		
		public static Properties createSelfEmitting(final Light.Properties lp){
			final Properties p = new Properties(lp);
			final float[] emission = {1.0f, 1.0f, 1.0f, 1.0f};  
			p.setEmission(new Vector4f(emission));
			p.setAlpha(1.0f);
			return p;
		}
		
		public static Properties createBright(final Light.Properties lp){
			final Properties p = new Properties(lp);
			final float[] brightness = {1.0f, 1.0f, 1.0f, 0.0f};  
			p.setBrightness(new Vector4f(brightness));
			return p;
		}
		
		private Properties(final Light.Properties lp){
			lightProps = lp;
		}
		
		public void setAmbient(Vector4f ambient) {
			lightProps.setAmbient(ambient);
		}
		public Vector4f getAmbient() {
			return lightProps.getAmbient();
		}

		public void setDiffuse(Vector4f diffuse) {
			lightProps.setDiffuse(diffuse);
		}
		public Vector4f getDiffuse() {
			return lightProps.getDiffuse();
		}

		public void setSpecular(Vector4f specular) {
			lightProps.setSpecular(specular);
		}
		public Vector4f getSpecular() {
			return lightProps.getSpecular();
		}
		
		public void setEmission(Vector4f emission) {
			this.emission.set(emission);
		}
		public Vector4f getEmission() {
			return new Vector4f(emission);
		}

		public void setAlpha(float alpha) {
			this.alpha = alpha;
		}

		public float getAlpha() {
			return alpha;
		}

		public void setBrightness(Vector4f brightness) {
			this.brightness.set(brightness);
		}

		public Vector4f getBrightness() {
			return new Vector4f(brightness);
		}		
	}
}