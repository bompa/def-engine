package def.engine.graphics.material;

import javax.vecmath.Vector4f;

import def.engine.graphics.material.ITexture.RenderId;
import def.engine.graphics.shader.FragmentShader;
import def.engine.graphics.shader.VertexShader;
import def.engine.graphics.shader.IShaderProgram.Id;
import def.shared.util.RestrictInterfaceWrapper;
import def.util.FloatPair;

public class MaterialImmutableWrapper extends RestrictInterfaceWrapper<IMaterial> implements IMaterial {

	public MaterialImmutableWrapper(IMaterial wc) {
		super(wc, "Illegal modify error!");
	}

	public FragmentShader getFragmentShader() {
		return getWrappedContent().getFragmentShader();
	}

	public ITexture getTexture() {
		return getWrappedContent().getTexture();
	}

	public void setFragmentShader(FragmentShader fs) {
		handleUnsupported();
	}

	public VertexShader getVertexShader() {
		return getWrappedContent().getVertexShader();
	}

	public void setVertexShader(VertexShader vs) {
		handleUnsupported();
	}

	public boolean hasProgram() {
		return getWrappedContent().hasProgram();
	}

	public Vector4f getColor() {
		return getWrappedContent().getColor();
	}

	public MaterialMode getMode() {
		return getWrappedContent().getMode();
	}

	public void setShaders(FragmentShader fs, VertexShader vs) {
		handleUnsupported();
	}

	public void setTextureRenderid(ITexture.RenderId id, int i) {
		handleUnsupported();
	}

	public Id getShaderid() {
		return getWrappedContent().getShaderid();
	}

	public void setShaderid(Id id) {
		handleUnsupported();
	}

	public Properties getProperties() {
		return getWrappedContent().getProperties();
	}

	public void setProperties(Properties p) {
		handleUnsupported();
	}

	public ITexture getTexture(int i) {
		return getWrappedContent().getTexture(i);
	}

	public int addTexture(ITexture texture) {
		handleUnsupported();
		return 0;
	}

	public int getNrOfTextures() {
		return getWrappedContent().getNrOfTextures();
	}

	public FloatPair getTexturesScale() {
		return getWrappedContent().getTexturesScale();
	}

	public void setTexturesScale(FloatPair uv) {
		handleUnsupported();
		
	}

	public boolean requiresTangent() {
		return getWrappedContent().requiresTangent();
	}

	public void setRequiresTangent(boolean requiresTangentValue) {
		handleUnsupported();
	}

}
