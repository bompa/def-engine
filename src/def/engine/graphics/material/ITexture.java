package def.engine.graphics.material;

import def.engine.graphics.IViewFrustum;
import def.util.FloatPair;
import android.util.Pair;

public interface ITexture {
	
	public enum RenderType{TEX_BASIC, TEX_RTT};

	public static final int NotUsingResourceID = -1;
	public static final int NotUsingCustomID = -1;

	/**
	 * Get associated android resource id
	 * @return
	 */
	public int getAndroidResource();
	
	/**
	 * Get custom id used if android resource id is not set
	 * @return
	 */
	public int getCustomId();
	
	public RenderType getRenderType();
	
	public void setRenderType(RenderType rt);
	
	public void setCustomId(int id);

	/**
	 * Set id used for rendering object with the texture
	 * @param renderId
	 */
	public void setRenderId(ITexture.RenderId renderId_);

	/**
	 * Get id used for rendering object with the texture
	 * @return
	 */
	public ITexture.RenderId getRenderId();
	
	/**
	 * Get settings for texture 
	 * @return
	 */
	public Settings getSettings();
	
	/**
	 * Set settings to use when rendering object with texture
	 */
	public void setSettings(Settings settings);
	
	/**
	 * Get ratio of texture. A ratio of 1 means a one-one mapping of the texture.
	 * @return
	 */
	public int getTextureRatio();
	
	/**
	 * Pair.first -> -----
	 * 				 |	 |
	 * 				 ----- <- Pair.second
	 * @return tex coords for top left and bottom right
	 */
	public Pair<FloatPair, FloatPair> getTextureCoordinates();
	
	/**
	 * Immutable ITexture renderid class
	 * @author jonas
	 *
	 */
	public static class RenderId {
		private final int id;
		
		public final static RenderId InvalidID = new RenderId(-1);
		
		public static RenderId getInvalidID() {
			return InvalidID;
		}
		
		public static RenderId create(int id){
			return new RenderId(id);
		}
		
		public RenderId(int id){
			this.id = id;
		}
		
		public RenderId(RenderId other){
			this.id = other.id;
		}
		
		public int value(){
			return id;
		}
	}
	
	
	enum Filter{FILTER_DEFAULT, FILTER_NEAREST, FILTER_LINEAR, FILTER_NEAREST_MIPMAP_NEAREST, FILTER_LINEAR_MIPMAP_NEAREST, FILTER_NEAREST_MIPMAP_LINEAR};
	
	enum WrapMode{WRAP_DEFAULT, WRAP_REPEAT, WRAP_CLAMP_EDGE};
	
	enum Format{FORMAT_DEFAULT, FORMAT_RGB, FORMAT_RGBA};
	
	/**
	 * Immutable Texture settings
	 * @author jonas
	 *
	 */
	public static class Settings{
		/**
		 * static create
		 * @param magFilter
		 * @param minFilter
		 * @param wraps
		 * @param wrapt
		 * @return
		 */
		public static Settings createInitialized(	final Filter magFilter,
													final Filter minFilter,
													final WrapMode wraps,
													final WrapMode wrapt){
			
			final Settings s = new Settings();
			s.magFilter = magFilter;
			s.minFilter = minFilter;
			s.wrapS = wraps;
			s.wrapT = wrapt;
			
			return s;
		}
		
		/**
		 * Static create common settings
		 * @return
		 */
		public static Settings createCommon(){
			return createCommon(SizeValueFromBitmap, SizeValueFromBitmap);
		}
		
		public static Settings createCommon(int w, int h){
			final Settings s = new Settings();
			
			s.magFilter = Filter.FILTER_LINEAR;
			s.minFilter = Filter.FILTER_NEAREST;
			s.wrapS = WrapMode.WRAP_REPEAT;
			s.wrapT = WrapMode.WRAP_REPEAT;
			
			return s;
		}
		
		/**
		 * Static create with common settings for render to texture
		 * 
		 * @param height texture height
		 * @param width texture height
		 * @param frustums defines the area used being renderer to the texture. Specific like projection must be done
		 * in the shader 
		 * @return
		 */
		public static Settings createRttCommon(int height, int width, final IViewFrustum frustum){
			final Settings s = new Settings(height, width, frustum);
			
			s.magFilter = Filter.FILTER_LINEAR;
			s.minFilter = Filter.FILTER_NEAREST;
			s.wrapS = WrapMode.WRAP_CLAMP_EDGE;
			s.wrapT = WrapMode.WRAP_CLAMP_EDGE;
			s.format = Format.FORMAT_RGBA;
			
			return s;
		}
		
		public static Settings create(){
			return new Settings();
		}
		
		private Settings(){ 
			height = SizeValueFromBitmap;
			width = SizeValueFromBitmap;
			this.rttFrustum = null;
		}
		
		private Settings(int height, int width, IViewFrustum f){
			this.height = height;
			this.width = width;
			this.rttFrustum = f;
		}
		
		public static final int SizeValueFromBitmap = -1;
		
		private Filter minFilter = Filter.FILTER_DEFAULT;
		private Filter magFilter= Filter.FILTER_DEFAULT;
		
		private WrapMode wrapS = WrapMode.WRAP_DEFAULT;
		private WrapMode wrapT = WrapMode.WRAP_DEFAULT;
		
		/**
		 * Frustum used in case of rendering is done to the texture using these settings-
		 * Can be null.
		 */
		private final IViewFrustum rttFrustum;
		
		/**
		 * Format used if not deduced from bitmap
		 */
		private Format format = Format.FORMAT_DEFAULT;
		
		final int height;
		final int width;
		
		private static final Settings DefaultSettings = Settings.create();
		
		public static Settings getDefaultSettings(){
			return DefaultSettings;
		}

		private static final Settings CommonSettings = Settings.createCommon();
		
		public static Settings getCommonsettings() {
			return CommonSettings;
		}
		
		public Filter getMinFilter() {
			return minFilter;
		}

		public Settings setMinFilter(Filter minFilter) {
			return Settings.createInitialized(minFilter, this.magFilter, this.wrapS, this.wrapT);
		}

		public Filter getMagFilter() {
			return magFilter;
		}

		public Settings setMagFilter(Filter magFilter) {
			return Settings.createInitialized(this.minFilter, magFilter, this.wrapS, this.wrapT);
		}

		public WrapMode getWrapS() {
			return wrapS;
		}

		public Settings setWrapS(WrapMode wrapS) {
			return Settings.createInitialized(this.minFilter, this.magFilter, wrapS, this.wrapT);
		}

		public WrapMode getWrapT() {
			return wrapT;
		}

		public Settings setWrapT(WrapMode wrapT) {
			return Settings.createInitialized(this.minFilter, this.magFilter, wrapS, wrapT);
		}

		public Format getFormat() {
			return format;
		}
		
		public int getHeight(){
			return this.height;
		}
		
		public int getWidth(){
			return this.width;
		}
		
		public IViewFrustum getRttFrustum(){
			return this.rttFrustum;
		}
	}

}