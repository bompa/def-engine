package def.engine.graphics.material;

import java.util.concurrent.atomic.AtomicLong;

import def.engine.logic.IAnimation;
import def.util.FloatPair;
import android.util.Pair;

public class AnimatedTexture implements ITexture, IAnimation {
	//private final LogHandler l = LogHandler.instance();
	private final ITexture texture;
	
	/**
	 * The number of images in row or column used to ceated the animated texture
	 * Total nr. of images is imageRowCount^2
	 */
	private int imageRowCount = 1;
	private int totImages = imageRowCount * imageRowCount;
	
	public final static int SEQUENCE_COUNT_INFINITE = -1;
	private int nrOfSequences = SEQUENCE_COUNT_INFINITE;
	private int currentSequence = 0;
	
	/**
	 * current image index to use as texture
	 */
	private int imageIndex = 0;
	
	public static AnimatedTexture create(int resId){
		return new AnimatedTexture(resId);
	}
	
	private AnimatedTexture(int resId){
		texture = new Texture(resId);
	}

	public int getAndroidResource() {
		return texture.getAndroidResource();
	}

	public ITexture.RenderId getRenderId() {
		return texture.getRenderId();
	}

	public void setRenderId(ITexture.RenderId renderId) {
		texture.setRenderId(renderId);
	}

	/**
	 * Set count of a row in the animated image whcih consits of an qual amounts of rows and columns
	 * @param imageCount
	 */
	public void setImageRowCount(int imageCount) {
		assert imageCount > 1;
		
		this.imageRowCount = imageCount;
		this.totImages = imageRowCount * imageRowCount;
	}

	public int getImageCount() {
		return imageRowCount;
	}
	
	public Pair<FloatPair, FloatPair> getTextureCoordinates(){
		float uTopLeft = imageIndex % (imageRowCount);
		float vTopLeft = imageIndex / (imageRowCount);

		float imageSize = 1.0f / imageRowCount;
		// normalize 0 -> 1 and add bottom right coords
		uTopLeft /= (float)imageRowCount;
		vTopLeft /= (float)imageRowCount;
		float uBottomRight = uTopLeft + imageSize;
		float vBottomRight = vTopLeft + imageSize;
						
		return new Pair<FloatPair, FloatPair>(new FloatPair(uTopLeft, vTopLeft), new FloatPair(uBottomRight, vBottomRight));
	}

	private static long AnimationSpeed = 2;
	private AtomicLong counter = new AtomicLong();

	private int customId;
	public boolean updateAnimationState() {
		if(currentSequence == nrOfSequences)
			return true;
		
		if( (counter.incrementAndGet() % AnimationSpeed) == 0){
			imageIndex++;
			imageIndex %= totImages;

			if(imageIndex == 0){
				currentSequence++;
			}
		}
		
		return false;
	}

	public int getTextureRatio() {
		return imageRowCount;
	}

	/**
	 * Set the number of sequences this animation should run before considerd complete.
	 * @param nrOfSequences
	 */
	public void setNrOfSequences(int nrOfSequences) {
		this.nrOfSequences = nrOfSequences;
	}

	/**
	 * Get the number of sequences this animation should run before considered complete.
	 * @return
	 */
	public int getNrOfSequences() {
		return nrOfSequences;
	}

	public Settings getSettings() {
		return texture.getSettings();
	}

	public void setSettings(Settings settings) {
		texture.setSettings(settings);
	}
	
	public int getCustomId() {
		return customId;
	}

	public void setCustomId(int id) {
		customId = id;
	}

	public RenderType getRenderType() {
		// TODO Auto-generated method stub
		return texture.getRenderType();
	}

	public void setRenderType(RenderType rt) {
		texture.setRenderType(rt);		
	}

}
