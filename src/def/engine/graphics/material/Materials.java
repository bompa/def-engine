package def.engine.graphics.material;

import javax.vecmath.Vector4f;


import def.engine.graphics.shader.FragmentShader;
import def.engine.graphics.shader.ShaderProgramFactory;
import def.engine.graphics.shader.VertexShader;
import def.util.FloatPair;
import android.util.Pair;

public class Materials {
	public static final float MATERIAL_FULL_APLHA = 1.0f;
	
	public static IMaterial unmodifiableMaterial(IMaterial imat){
		return new MaterialImmutableWrapper(imat);
	}
	
	public static IMaterial createBasicTexturingNoLightning(final ITexture texture){
		final FragmentShader fs = (FragmentShader) ShaderProgramFactory.instance().createBasicTexuredFShader();
		final VertexShader vs = (VertexShader) ShaderProgramFactory.instance().createPerFragmentLightningVertShader();
		return createWithCustomShaders(texture, vs, fs);
	}

	public static IMaterial createWithPerFragmentLightningShaders(int textureResId){
		return createWithPerFragmentLightningShaders(new Texture(textureResId));
	}
	
	public static boolean isTextureUsingAndrouidResourceId(final ITexture tex){
		return ITexture.NotUsingResourceID != Integer.valueOf(tex.getAndroidResource());
	}
	
	public static int getTextureId(final ITexture tex){
		if(ITexture.NotUsingResourceID != Integer.valueOf(tex.getAndroidResource())){
			return tex.getAndroidResource();
		}
		else if(ITexture.NotUsingCustomID != tex.getCustomId()){
			return tex.getCustomId();
		}
		else{
			throw new IllegalStateException("No valid id found for texture: " + tex);
		}
		
	}
	
	public static IMaterial createWithPerFragmentLightningShaders(final ITexture texture){
		final FragmentShader fs = (FragmentShader) ShaderProgramFactory.instance().createPerFragmentLightningFragShader();
		final VertexShader vs = (VertexShader) ShaderProgramFactory.instance().createPerFragmentLightningVertShader();
		return createWithCustomShaders(texture, vs, fs);
	}
	
	public static IMaterial createWithPerFragLightNormalMapShaders(final ITexture texture){
		final FragmentShader fs = (FragmentShader) ShaderProgramFactory.instance().createPerFragLightNormalMapFragShader();
		final VertexShader vs = (VertexShader) ShaderProgramFactory.instance().createTangentSpaceVertShader();
		final IMaterial m = createWithCustomShaders(texture, vs, fs);
		m.setRequiresTangent(true);
		return m;
	}

	
	public static IMaterial createWithBasicColorShaders(final Vector4f c){
		FragmentShader fs = (FragmentShader) ShaderProgramFactory.instance().createBasicFragShader();
		VertexShader vs = (VertexShader) ShaderProgramFactory.instance().createPerBasicVertShader();
		return createWithCustomShaders(c, vs, fs);
	}
	
	public static IMaterial createWithCustomShaders(final Vector4f c, VertexShader vs, FragmentShader fs){
		IMaterial material = new Material(c);
		material.setShaders(fs, vs);
		return material;
	}
	
	public static IMaterial createWithCustomShaders(int textureResId, VertexShader vs, FragmentShader fs){
		return createWithCustomShaders(new Texture(textureResId), vs, fs);
	}
	
	public static IMaterial createWithCustomShaders(ITexture texture, VertexShader vs, FragmentShader fs){
		IMaterial material = new Material(texture);
		material.setShaders(fs, vs);
		return material;
	}

	public static boolean isMaterialUsingTexture(final IMaterial material) {
		final IMaterial.MaterialMode mode = material.getMode();
		return mode == IMaterial.MaterialMode.FIXED_TEXTURE || mode == IMaterial.MaterialMode.SHADER_PROGRAMS_TEXTURE;
	}

	public static Pair<FloatPair, FloatPair> createDefaultTextureCoordinates(){
		return new Pair<FloatPair, FloatPair>( new FloatPair(0f, 0f), new FloatPair(1f, 1f));
	}
	
	public static class TextureNotFoundException extends RuntimeException{

		private static final long serialVersionUID = 1L;

		public TextureNotFoundException(String msg){
			super(msg);
		}
	}
}
