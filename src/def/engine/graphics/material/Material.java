package def.engine.graphics.material;

import java.util.ArrayList;

import javax.vecmath.Vector4f;

import def.engine.graphics.components.Light;
import def.engine.graphics.shader.FragmentShader;
import def.engine.graphics.shader.IShaderProgram;
import def.engine.graphics.shader.VertexShader;
import def.util.FloatPair;
import def.util.Util;

/**
 * Defines a material with a set of textures and properties.
 * Modifying (adding/removing) of textures in non-threadsafe and must be synchronized
 * @author jonas
 *
 */
public class Material implements IMaterial{
	private final ArrayList<ITexture> textures = new ArrayList<ITexture>();
	private final Vector4f color;
	
	private MaterialMode mode;
	
	private FragmentShader fs;
	private VertexShader vs;
	private IShaderProgram.Id shaderProgramId = new IShaderProgram.Id(0);
	private FloatPair texUVScale = new FloatPair(1.0f, 1.0f);
	private boolean requiresTangentValue = false;
	
	private IMaterial.Properties properties = IMaterial.Properties.createFullAlpha((Light.Properties.create())); 
	
	public Material(ITexture tx){
		if(tx == null)
			throw new IllegalArgumentException("Material created with texture paramter = null");
		textures.add(tx);
		color = new Vector4f();
		mode = MaterialMode.FIXED_TEXTURE;
	}
	
	public Material(Vector4f c){
		color = c;
		mode = MaterialMode.FIXED_COLOR;
	}

	public FragmentShader getFragmentShader () {
		return fs;
	}

	public ITexture getTexture() {
		return getTexture(0);
	}

	public VertexShader getVertexShader() {
		return vs;
	}

	public boolean hasProgram() {
		return true;
	}

	public Vector4f getColor() {
		return color;
	}

	public void setShaders(FragmentShader fs, VertexShader vs) {
		this.fs = fs;
		this.vs = vs;
		
		if(textures.size() == 0)
			mode = MaterialMode.SHADER_PROGRAMS_COLOR;
		else
			mode = MaterialMode.SHADER_PROGRAMS_TEXTURE;
	}

	public MaterialMode getMode() {
		return mode;
	}
	
	public String toString(){
		switch(mode){
		case FIXED_COLOR:
			return "Material with color" + color;
		case FIXED_TEXTURE:
			return "Material with " + textures.size() + " nr. of textures";
		case SHADER_PROGRAMS_COLOR:
			return "Material with shaders - " + fs + ", " + vs + " and color " + color;
		case SHADER_PROGRAMS_TEXTURE:
			return "Material with shaders - " + fs + ", " + vs + " and default texture " + (textures.size() == 0? "none" : textures.get(0));
		default:
			return "Invalid material";	
		}
	}

	public void setShaderid(IShaderProgram.Id shaderProgramId) {
		this.shaderProgramId = shaderProgramId;
		fs.setParentProgramId(shaderProgramId);
		vs.setParentProgramId(shaderProgramId);
	}

	public IShaderProgram.Id getShaderid() {
		return shaderProgramId;
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties p) {
		properties = p;
	}

	public ITexture getTexture(int i) {
		if(i >= textures.size())
			throw new Materials.TextureNotFoundException("Mising texture wuth idx " + i);
		
		return textures.get(i);
	}

	public int addTexture(ITexture texture) {
		textures.add(texture);
		return textures.size() - 1;
	}

	public int getNrOfTextures() {
		return textures.size();
	}

	public FloatPair getTexturesScale() {
		return texUVScale;
	}

	public void setTexturesScale(FloatPair uv) {
		if(uv == null)
			throw Util.buildIllegalArgException(Material.class, "uv");
		
		this.texUVScale = uv;		
	}

	public boolean requiresTangent() {
		return requiresTangentValue;
	}

	public void setRequiresTangent(boolean requiresTangentValue) {
		this.requiresTangentValue = requiresTangentValue;
	}
}