package def.engine.graphics;

import def.engine.graphics.driver.Graphics.RenderSequenceAction;

public class RenderSequenceEvent  implements IRenderSequenceEvent{
	private final RenderSequenceAction action_;
	
	private long renderStep_ = 0;
	
	public RenderSequenceEvent(final RenderSequenceAction action){
		action_ = action;
	}
	
	public RenderSequenceAction getAction() {
		return action_;
	}

	public void setRenderStep(long renderStep_) {
		this.renderStep_ = renderStep_;
	}

	public long getRenderStep() {
		return renderStep_;
	}
}

