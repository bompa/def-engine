package def.engine.graphics;

import def.engine.graphics.driver.Graphics.RenderSequenceAction;
import def.shared.util.RestrictInterfaceWrapper;

public class RenderSequenceEventImmutableWrapper extends RestrictInterfaceWrapper<IRenderSequenceEvent> 
	implements IRenderSequenceEvent{

	public RenderSequenceEventImmutableWrapper(IRenderSequenceEvent wc) {
		super(wc, "Illegal update of immutable object");
	}

	public RenderSequenceAction getAction() {
		return getWrappedContent().getAction();
	}

	public long getRenderStep() {
		return getWrappedContent().getRenderStep();
	}

	public void setRenderStep(long renderStep) {
		handleUnsupported();
	}
}
