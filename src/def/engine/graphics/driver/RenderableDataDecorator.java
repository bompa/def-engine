package def.engine.graphics.driver;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

import javax.vecmath.Matrix4f;

import def.engine.graphics.shader.FragmentShader;
import def.engine.graphics.shader.VertexShader;
import def.engine.logic.math.MatrixArray4f;
import def.util.IntPair;

public class RenderableDataDecorator implements IRenderableData{
	protected final IRenderableData decoratedRenderableData;
	
	public RenderableDataDecorator(final IRenderableData parent){
		decoratedRenderableData = parent; 
	}

	public void adaptViewMatrix(MatrixArray4f viewMatrix) {	decoratedRenderableData.adaptViewMatrix(viewMatrix); }
	public FloatBuffer getColorBuffer() { return decoratedRenderableData.getColorBuffer();	}
	public FragmentShader getFragmentShader() { return decoratedRenderableData.getFragmentShader(); }
	public ShortBuffer getActiveIndexBuffer() { return decoratedRenderableData.getActiveIndexBuffer();	}
	public Matrix4f getModelMatrix() { return decoratedRenderableData.getModelMatrix(); }
	public FloatBuffer getNormalsBuffer() { return decoratedRenderableData.getNormalsBuffer();	}
	public RenderDirection getRenderDirection() { return decoratedRenderableData.getRenderDirection();	}
	public RenderOperation getRenderOperation() { return decoratedRenderableData.getRenderOperation(); }
	public FloatBuffer getTextureCoordsBuffer() { return decoratedRenderableData.getTextureCoordsBuffer();	}
	public FloatBuffer getVertexBuffer() { return decoratedRenderableData.getVertexBuffer(); }
	public VertexShader getVertexShader() { return decoratedRenderableData.getVertexShader(); }
	public void setColorBuffer(FloatBuffer colorBuffer) { decoratedRenderableData.setColorBuffer(colorBuffer); }
	public void setFragmentShader(FragmentShader fs) { decoratedRenderableData.setFragmentShader(fs); }
	public void setModelMatrix(Matrix4f modelMatrix) { decoratedRenderableData.setModelMatrix(modelMatrix); }
	public void setNormalsBuffer(FloatBuffer normalsBuffer) { decoratedRenderableData.setNormalsBuffer(normalsBuffer);	}
	public void setTextureCoordsBuffer(FloatBuffer tcBuffer) { decoratedRenderableData.setTextureCoordsBuffer(tcBuffer); }
	public void setVertexShader(VertexShader vs) { decoratedRenderableData.setVertexShader(vs); }
	public float getScale() { return decoratedRenderableData.getScale(); }
	public void setScale(float scale) { decoratedRenderableData.setScale(scale); }
	public int getNrOfComponents() { return decoratedRenderableData.getNrOfComponents(); }
	public void setActiveComponent(int componentIndex) { decoratedRenderableData.setActiveComponent(componentIndex); }
	public FloatBuffer getTangetBuffer() { return decoratedRenderableData.getTangetBuffer(); }
	public void setTangetBuffer(FloatBuffer tangetBuffer) { decoratedRenderableData.setTangetBuffer(tangetBuffer); }
	public FloatBuffer getBinormalBuffer() { return decoratedRenderableData.getBinormalBuffer(); }
	public void setBinormalBuffer(FloatBuffer binormalBuffer) { decoratedRenderableData.setBinormalBuffer(binormalBuffer); }
	public VertexBufferData getVertexBufferData() {return decoratedRenderableData.getVertexBufferData();}
	public IntPair getIndexBufferRange() { return decoratedRenderableData.getIndexBufferRange(); }
	public ArrayList<IntPair> getComponentIndices() { return decoratedRenderableData.getComponentIndices(); }
	public short[] getIndices() {return decoratedRenderableData.getIndices();}
}
