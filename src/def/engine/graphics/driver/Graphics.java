package def.engine.graphics.driver;

import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLES20;
import android.util.Log;

import def.engine.graphics.driver.VertexData.BufferDataType;
import def.engine.graphics.material.ITexture;
import def.util.LogHandler;

public class Graphics {
	private static final LogHandler l = LogHandler.createInstanceShared();
	
	public enum RenderSequenceAction{REND_ACTION_FRAME_RENDERED, REND_ACTION_PRE_WORK_SYNCHED_DRAWING, REND_ACTION_SURFACE_CREATED};
	
	/**
	 * Immutable wrapper for integer id of vertex buffer
	 * @author jonas
	 *
	 */
	public static class BufferId {
		public final static BufferId InvalidID = new BufferId(-1);
		private final int id;
		
		public static boolean isBufferIdValid(BufferId id){
			return id.value() != InvalidID.value();
		}
		
		public static BufferId create(int id){
			return new BufferId(id);
		}
		
		public BufferId(int id){
			this.id = id;
		}
		
		public BufferId(BufferId other){
			this.id = other.id;
		}
		
		public int value(){
			return this.id;
		}
		
		public boolean equals(Object rhs){
		    if(rhs == null){
		        return false;
		    }
	
		    if(!(rhs instanceof BufferId)){
		        return false;
		    }
	
		    BufferId other = (BufferId)rhs;
	
		    if(this.value() == other.value()){
		        return true;
		    }
		    
		    return false;
		}
	}

	public static boolean hasNormalsBuffer(IRenderableData ri){
		return ri.getNormalsBuffer() != null;
	}
	
	public static boolean hasColorBuffer(IRenderableData ri){
		return ri.getColorBuffer() != null;
	}
	
	public static boolean hasTexCoordBuffer(IRenderableData ri){
		return ri.getTextureCoordsBuffer() != null;
	}
	
	public static boolean hasTangentsBuffer(IRenderableData ri){
		return ri.getTangetBuffer() != null;
	}

	public static boolean hasBinormalsBuffer(IRenderableData ri){
		return ri.getBinormalBuffer() != null;
	}

	public static boolean bufferSupportsVbo(VertexData.BufferDataType t){
		return ( t == BufferDataType.BUFFER_DATA_STATIC ) || ( t == BufferDataType.BUFFER_DATA_DYNAMIC ); 
	}
	
	public static int toOpenGlEsType(IRenderableData.RenderOperation rop){
		switch(rop){
		case RO_TRIANGLES:
			return GL10.GL_TRIANGLES;
		case RO_LINES:
			return GL10.GL_LINES;
		default:
			l.log("Invalid renderoperation when converting to OpenGl type, defaulting to GL_TRIANGLES", Log.WARN);
			return GL10.GL_TRIANGLES;
		}
	}
	
	public static int toOpenGlEsType(IRenderableData.RenderDirection rd){
		switch(rd){
		case RD_CCW:
			return GL10.GL_CCW;
		case RD_CW:
			return GL10.GL_CW;
		default:
			l.log("Invalid render direction when converting to OpenGl type, defaulting to GL_CCW", Log.WARN);
			return GL10.GL_CCW;				
		}
	}
	
	public static int toOpenGlEsType(VertexData.BufferDataType bd){
		switch(bd){
		case BUFFER_DATA_STATIC:
			return GLES20.GL_STATIC_DRAW;
		case BUFFER_DATA_DYNAMIC:
			return GLES20.GL_DYNAMIC_DRAW;
		default:
			break;
		}
		
		return GLES20.GL_STATIC_DRAW;
	}
	
	public static int toOpenGlEsType(VertexData.BufferTarget t){
		switch(t){
		case BUFFER_TARGET_ARRAY:
			return GLES20.GL_ARRAY_BUFFER;
		case BUFFER_TARGET_ELEMENT_ARRAY:
			return GLES20.GL_ELEMENT_ARRAY_BUFFER;
		}
		
		return GLES20.GL_ARRAY_BUFFER;
	}
	
	public static int toOpenGlEsType( ITexture.Format format ){
		switch (format) {
		case FORMAT_RGB:
			return GLES20.GL_RGB;
		case FORMAT_RGBA:
			return GLES20.GL_RGBA;
		default:
			break;

		}
		
		return GLES20.GL_RGBA;
	}
	
	public static int toOpenGlEsType( ITexture.Filter filter ){
		switch (filter) {
		case FILTER_NEAREST:
			return GLES20.GL_NEAREST;
		case FILTER_LINEAR:
			return GLES20.GL_LINEAR;
		case FILTER_LINEAR_MIPMAP_NEAREST:
			return GLES20.GL_LINEAR_MIPMAP_NEAREST;
		case FILTER_NEAREST_MIPMAP_LINEAR:
			return GLES20.GL_NEAREST_MIPMAP_LINEAR;
		case FILTER_NEAREST_MIPMAP_NEAREST:
			return GLES20.GL_NEAREST_MIPMAP_NEAREST;
		case FILTER_DEFAULT:
		default:
			break;
		}
		
		return GLES20.GL_NEAREST;
	}
	
	public static int toOpenGlEsType( ITexture.WrapMode wrap ){
		switch (wrap) {
		case WRAP_CLAMP_EDGE:
			return GLES20.GL_CLAMP_TO_EDGE;
		case WRAP_REPEAT:
			return GLES20.GL_REPEAT;
		case WRAP_DEFAULT:
		default:
			break;
		}
		
		return GLES20.GL_CLAMP_TO_EDGE;
	}
	
	public static int toOpenGlEsType( IRenderTarget.BufferAttachmentType type){
		switch (type) {
		case BUFF_ATTATCH_COLOR:
			return GLES20.GL_COLOR_ATTACHMENT0;
		case BUFF_ATTATCH_DEPTH:
			return GLES20.GL_DEPTH_ATTACHMENT;
		case BUFF_ATTATCH_STENCIL:
			return GLES20.GL_STENCIL_ATTACHMENT;
		default:
			break;
		}
		
		return GLES20.GL_DEPTH_ATTACHMENT;
	}
	
	public static int toOpenGlEsType( IRenderTarget.BufferStorageType type){
		switch (type) {
		case BUFF_STORAGE_DEPTH_COMPONENT16:
			return GLES20.GL_DEPTH_COMPONENT16;
		case BUFF_STORAGE_RGB565:
			return GLES20.GL_RGB565;
		case BUFF_STORAGE_RGB5_A1:
			return GLES20.GL_RGB5_A1;
		case BUFF_STORAGE_RGBA4:
			return GLES20.GL_RGBA4;
		case BUFF_STORAGE_STENCIL_INDEX:
			return GLES20.GL_STENCIL_INDEX;
		default:
			break;
		}
		
		return GLES20.GL_DEPTH_COMPONENT;
	}

	public static final BufferId DefaultFrameBufferID = new BufferId(0);
}
