package def.engine.graphics.driver;

public class VertexBufferData {
	
	public static VertexBufferData createWithStaitcBufferTypes(){
		return new VertexBufferData();
	}
	
	private VertexBufferData(){}
	
	private Graphics.BufferId indicesID = 						Graphics.BufferId.InvalidID;
	private Graphics.BufferId positionsID = 					Graphics.BufferId.InvalidID;
	private Graphics.BufferId normalsID = 						Graphics.BufferId.InvalidID;
	private Graphics.BufferId colorsID = 						Graphics.BufferId.InvalidID;
	private Graphics.BufferId texturesID = 						Graphics.BufferId.InvalidID;
	private Graphics.BufferId tangentsID = 						Graphics.BufferId.InvalidID;
	private Graphics.BufferId binormalsID = 					Graphics.BufferId.InvalidID; 
	
	private VertexData.BufferDataType indicesBufferType = 		VertexData.BufferDataType.BUFFER_DATA_STATIC;
	private VertexData.BufferDataType positionsBufferType  =	VertexData.BufferDataType.BUFFER_DATA_STATIC;
	private VertexData.BufferDataType normalsBufferType  = 		VertexData.BufferDataType.BUFFER_DATA_STATIC;
	private VertexData.BufferDataType colorsBufferType  = 		VertexData.BufferDataType.BUFFER_DATA_STATIC;
	private VertexData.BufferDataType texturesBufferType  = 	VertexData.BufferDataType.BUFFER_DATA_STATIC;
	private VertexData.BufferDataType tangentsBufferType  = 	VertexData.BufferDataType.BUFFER_DATA_STATIC;
	private VertexData.BufferDataType binormalsBufferType  =	VertexData.BufferDataType.BUFFER_DATA_STATIC;
	
	//
	// Getter / Setters
	//
	public Graphics.BufferId getIndicesID() {
		return indicesID;
	}

	public void setIndicesID(Graphics.BufferId indicesID) {
		this.indicesID = indicesID;
	}

	public Graphics.BufferId getPositionsID() {
		return positionsID;
	}

	public void setPositionsID(Graphics.BufferId positionsID) {
		this.positionsID = positionsID;
	}

	public Graphics.BufferId getNoramlsID() {
		return normalsID;
	}

	public void setNoramlsID(Graphics.BufferId noramlsID) {
		this.normalsID = noramlsID;
	}

	public Graphics.BufferId getTexturesID() {
		return texturesID;
	}

	public void setTexturesID(Graphics.BufferId texturesID) {
		this.texturesID = texturesID;
	}

	public Graphics.BufferId getTangentsID() {
		return tangentsID;
	}

	public void setTangentsID(Graphics.BufferId tangentsID) {
		this.tangentsID = tangentsID;
	}

	public Graphics.BufferId getBinormalsID() {
		return binormalsID;
	}

	public void setBinormalsID(Graphics.BufferId binormalsID) {
		this.binormalsID = binormalsID;
	}

	public VertexData.BufferDataType getIndicesBufferType() {
		return indicesBufferType;
	}

	public void setIndicesBufferType(VertexData.BufferDataType indicesBufferType) {
		this.indicesBufferType = indicesBufferType;
	}

	public VertexData.BufferDataType getPositionsBufferType() {
		return positionsBufferType;
	}

	public void setPositionsBufferType(VertexData.BufferDataType positionsBufferType) {
		this.positionsBufferType = positionsBufferType;
	}

	public VertexData.BufferDataType getNormalsBufferType() {
		return normalsBufferType;
	}

	public void setNoramlsBufferType(VertexData.BufferDataType noramlsBufferType) {
		this.normalsBufferType = noramlsBufferType;
	}

	public VertexData.BufferDataType getTexturesBufferType() {
		return texturesBufferType;
	}

	public void setTexturesBufferType(VertexData.BufferDataType texturesBufferType) {
		this.texturesBufferType = texturesBufferType;
	}

	public VertexData.BufferDataType getTangentsBufferType() {
		return tangentsBufferType;
	}

	public void setTangentsBufferType(VertexData.BufferDataType tangentsBufferType) {
		this.tangentsBufferType = tangentsBufferType;
	}

	public VertexData.BufferDataType getBinormalsBufferType() {
		return binormalsBufferType;
	}

	public void setBinormalsBufferType(VertexData.BufferDataType binormalsBufferType) {
		this.binormalsBufferType = binormalsBufferType;
	}

	public Graphics.BufferId getColorsID() {
		return colorsID;
	}

	public void setColorsID(Graphics.BufferId colorsID) {
		this.colorsID = colorsID;
	}

	public VertexData.BufferDataType getColorsBufferType() {
		return colorsBufferType;
	}

	public void setColorsBufferType(VertexData.BufferDataType colorsBufferType) {
		this.colorsBufferType = colorsBufferType;
	}
}
