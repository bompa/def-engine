package def.engine.graphics.driver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import def.engine.graphics.driver.Graphics.BufferId;
import def.util.IntPair;
import def.util.Util;

/**
 * Defines a framebuffer object with attachments such as target textures and depth buffers.
 * 
 * @author jonas
 *
 */
public class FrameBufferObject {
	public static int BufferSzInvalid = -1;
	
	private BufferId renderFboId = BufferId.InvalidID;
	private final IntPair size = new IntPair(BufferSzInvalid, BufferSzInvalid);
	private final List<FrameBufferData> targetBuffers = new ArrayList<FrameBufferData>();
	
	public static FrameBufferObject create(){
		return new FrameBufferObject();
	}
	
	public static FrameBufferObject createDefault(){
		final FrameBufferObject fbo = new FrameBufferObject();
		fbo.setRenderFboId(Graphics.DefaultFrameBufferID);
		return fbo;
	}
	
	private FrameBufferObject(){
		
	}
	
	public List<FrameBufferData> getTargetBuffersReadOnly(){
		return Collections.unmodifiableList(this.targetBuffers);
	}
	
	/**
	 * @return first=width and second=height
	 */
	public IntPair getFrameBufferSize(){
		return new IntPair(size.first, size.second);
	}

	/**
	 * Add buffer attached to this frame-buffer object
	 * @param bufferData FrameBufferData to be attached
	 */
	public void addTargetBuffer(final FrameBufferData bufferData){
		if(bufferData == null){
			throw Util.buildIllegalNullArgException(FrameBufferData.class, "bufferData");
		}
		
		targetBuffers.add(bufferData);
	}

	public BufferId getRenderFboId() {
		return renderFboId;
	}

	public void setRenderFboId(BufferId renderFboId) {
		if(renderFboId == null)
			throw Util.buildIllegalNullArgException(FrameBufferObject.class, "renderFboId");
			
		this.renderFboId = renderFboId;
	}
}
