package def.engine.graphics.driver;

import java.nio.Buffer;

import android.graphics.Bitmap;

import def.engine.graphics.material.ITexture;
import def.engine.graphics.shader.FragmentShader;
import def.engine.graphics.shader.IShaderProgram;
import def.engine.graphics.shader.VertexShader;
import def.engine.graphics.shader.ShaderValue.ValueScope;
import def.util.IntPair;

/**
 * Interface for handling/creating hw-related graphics resources
 * 
 * @author jonas
 *
 */
public interface IGraphicsResources {
	
	/**
	 * Gener nBuffer framebuffer objects
	 * @param nBuffers
	 * @return list of nBuffer number of ids
	 */
	public int[] generateFrameBufferObjects(int nBuffers);
	
	/**
	 * Gener nBuffer renderbuffer objects
	 * @param nBuffers
	 * @return list of nBuffer number of ids
	 */
	public int[] generateRenderBufferIDs(int nBuffers);
	
	/**
	 * 
	 * @param nTextures nr of texture ids for unique textures to render
	 * @return list containing texture ids.
	 */
	public int[] generateTextureIds(int nTextures);
	
	/**
	 * Generate ids for vbos
	 * @param nVbos nr of ids to generate
	 * @return array of generated ids
	 */
	public int[] generateVBOs(int nVbos);
	
	/**
	 * Delete previously generated vbos
	 * @param nVbos number of ids in vbos param
	 * @param vbos the ids to delete
	 */
	public void deleteVbos(int nVbos, int[] vbos);
	
	/**
	 * Initalize buffers of rendertarget and connect attachmetnts to renderbuffer etc
	 * @param renderTarget
	 * @return
	 */
	public boolean initRenderTarget(IRenderTarget renderTarget);
	
	/**
	 * 
	 * @param vboID id buffer which assign data to
	 * @param target type of buffer
	 * @param size size of data in bytes
	 * @param data buffer of buffer data such as vertex positions
	 * @param usage static/dynamic
	 */
	public void setVboData(int vboID, VertexData.BufferTarget target, int size, Buffer data, VertexData.BufferDataType usage);

	/**
	 * Create a texture using by calling that can be used with the current render system.
	 * @param is
	 * @return
	 */
	public abstract ITexture.RenderId createTexture(Bitmap bitmap, int textureId, ITexture.Settings settings);

	/**
	 * Create a shader program consisting of a fragment and vertex shader using the underlying
	 * render system. The vertex and fragment shader are loaded, compiled and linked into a full program
	 * @param vs the vertex shader program
	 * @param fs the fragment shader program
	 * @return a generated id used to identify the shader program by the render-system.
	 */
	public abstract IShaderProgram.Id createProgramCompileShaders(VertexShader vs, FragmentShader fs);
	
	/**
	 * Link together already compiled shaders into a new full program.
	 * @param vsId id of compiled vertex shader.
	 * @param fsId id of compiled fragment shader.
	 * @return
	 */
	public IShaderProgram.Id createProgramLinkCompiledShaders(IShaderProgram.Id vsId, IShaderProgram.Id fsId);
	
	/**
	 * Get the id/location/address of a shader parameter.
	 * @param program the shader program to collect the parameter id from.
	 * @param name of the parameter.
	 * @param vs is the scope of the variable, such as a whole primitive or per vertex.
	 * @return the id to identify the parameter to the render.
	 */
	public abstract int getShaderParamId(IShaderProgram.Id program, String name, ValueScope vs);
	
	/**
	 * Get dimensions of main window used for rendering
	 * @return first=width and second=height of window
	 */
	public abstract IntPair getRenderWindowSize();
	
	/**
	 * The version of the engine, ie 2 for opengl 2.0
	 * @return
	 */
	public abstract int getSupporteRenderVersion();

}