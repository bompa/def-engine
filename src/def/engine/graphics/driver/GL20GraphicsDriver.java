package def.engine.graphics.driver;

import java.nio.Buffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.microedition.khronos.opengles.GL11;
import javax.vecmath.Vector4f;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.opengl.GLES20;
import android.opengl.GLU;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.util.Log;
import android.util.Pair;

import def.Const;
import def.engine.graphics.ViewPort;
import def.engine.graphics.components.Light;
import def.engine.graphics.driver.Graphics.BufferId;
import def.engine.graphics.driver.IRenderTarget.BufferAttachmentType;
import def.engine.graphics.driver.VertexData.BufferDataType;
import def.engine.graphics.driver.VertexData.BufferTarget;
import def.engine.graphics.material.IMaterial;
import def.engine.graphics.material.ITexture;
import def.engine.graphics.material.ITexture.Settings;
import def.engine.graphics.material.Materials;
import def.engine.graphics.shader.FragmentShader;
import def.engine.graphics.shader.IShaderProgram;
import def.engine.graphics.shader.Shaders;
import def.engine.graphics.shader.VertexShader;
import def.engine.graphics.shader.ShaderValue.ValueScope;
import def.engine.logic.math.LinjearAlg;
import def.engine.logic.math.MatrixArray4f;
import def.engine.logic.math.VectorArray4f;
import def.util.LogHandler;
import def.util.Options;
import def.util.Util;

@TargetApi(value = 10)
public class GL20GraphicsDriver extends AGraphicsDriver {
	private final LogHandler l = LogHandler.createInstanceShared(GL20GraphicsDriver.class);
	
	static final int RENDER_VER = 2;
	static final int MAX_LIGHTS = Const.SHADER_DEF_NR_OF_LIGHTS;
	
	private RenderConfig conf;
	private boolean useLightning = true;
	private boolean useVBOs = true;
	
	private MatrixArray4f modelViewProjMtx = new MatrixArray4f();
	private MatrixArray4f modelViewMtx = new MatrixArray4f();
		
	public void init(GL11 ignored, RenderConfig conf) {
		this.conf = conf;
		Matrix.setIdentityM(modelViewProjMtx.getArray(), 0);
		
		// Enables depth testing.
		setDepthTest(conf.useDepthTest());
		
		setBlending(conf.useBlending());
		
		this.useLightning = conf.useLightning();
		
		setFaceCulling(this.conf.performFaceCulling());
	}
	
	public void configure(Options.Val[] options) {
		
		if(options[RenderConfig.REND_ATTR_LIGHTNING] != Options.Val.UNDEF){
			this.useLightning = Options.getAttrBool(options[RenderConfig.REND_ATTR_LIGHTNING]);
		}
		
		if(options[RenderConfig.REND_ATTR_BLENDING] != Options.Val.UNDEF){
			setBlending(Options.getAttrBool(options[RenderConfig.REND_ATTR_BLENDING]));
		}
		
		if(options[RenderConfig.REND_ATTR_DEPTH_TEST] != Options.Val.UNDEF){
			setDepthTest(Options.getAttrBool(options[RenderConfig.REND_ATTR_DEPTH_TEST]));
		}
		
		if(options[RenderConfig.REND_ATTR_FACE_CULL] != Options.Val.UNDEF){
			setFaceCulling(Options.getAttrBool(options[RenderConfig.REND_ATTR_FACE_CULL]));
		}
	}
	
	private void setFaceCulling(boolean on){
		if(on){
			GLES20.glEnable(GLES20.GL_CULL_FACE);
			GLES20.glCullFace(GLES20.GL_BACK);			
		}
		else{
			GLES20.glDisable(GLES20.GL_CULL_FACE);
		}
	}
	
	private void setBlending(boolean on) {
		if(on){
			GLES20.glEnable(GLES20.GL_BLEND);
			GLES20.glBlendFunc(GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA);	
		}
		else{
			GLES20.glDisable(GLES20.GL_BLEND);
		}
		
	}

	private void setDepthTest(boolean on) {
		if(on){
			GLES20.glEnable(GLES20.GL_DEPTH_TEST);
			// The type of depth testing to do.
			GLES20.glDepthFunc(GLES20.GL_LEQUAL);	
		}
		else{
			GLES20.glDisable(GLES20.GL_DEPTH_TEST);
		}
		
	}

	public RenderConfig getRenderConfig() {
		return this.conf;
	}
	
	public GL11 getGlInterface() {
		return null;
	}

	public void setViewPort(int x, int y, int width, int height) {
		GLES20.glViewport(x, y, width, height);
	}
	
	@Override
	protected void prepeareRendering() {
		
		activeRenderTarget(getRenderTarget());
		
		// Clears the screen and depth buffer.
		final Vector4f backg = conf.getBackGroundColor();
		GLES20.glClearColor(backg.x, backg.y, backg.z, backg.w);
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
	}
	
	private void activeRenderTarget(IRenderTarget renderTarget) {
		if((null != renderTarget)){
			ViewPort vp = renderTarget.getViewPort();
			if(null == vp) {
				vp = ViewPort.create(0, 0, conf.getRenderWinSize().first, conf.getRenderWinSize().first);
				if(l.isLevelLoggable(Log.WARN)){
					l.log("No viewport is set for current RenderTarget, using default.", Log.WARN);
				}
			}
			setViewPort((int)vp.getX(), (int)vp.getY(), (int)vp.getWidth(), (int)vp.getHeight());
			GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, renderTarget.getFrameBufferObject().getRenderFboId().value());
			if(GLES20.GL_FRAMEBUFFER_COMPLETE != GLES20.glCheckFramebufferStatus(GLES20.GL_FRAMEBUFFER))
			{
				if(l.isLevelLoggable(Log.WARN)){
					l.log("Incomplete gramebuffer, using default.", Log.WARN);
				}
				GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, Graphics.DefaultFrameBufferID.value());
			}
			checkError("glBindFramebuffer");
		}
		else{
			if(l.isLevelLoggable(Log.ERROR)){
				l.log("No rendertarget is set!", Log.ERROR);
			}
		}
		
	}

	@Override
	public void enableLight(Light l){
		super.enableLight(l);
	}
	
	@Override
	protected void updatePojection() {}
	
	/**
	 * Set vertex shader light data
	 */
	private void setLights(IRenderableData rd){
		if(!useLightning){
			return;
		}
		
		final FragmentShader fs = rd.getFragmentShader();
		
		Collection<Light> lList = getLights();
		GLES20.glUniform1i(fs.getNrOfLightsUniformId(), lList.size());
		
		final Iterator<Light> lightsIt = lList.iterator();
		for(int idx = 0; lightsIt.hasNext() && idx < MAX_LIGHTS; idx++){
			Light l1 = lightsIt.next();
			final float[] lightPosModelSpace = new float[] {0.0f, 0.0f, 0.0f, 1.0f};
			final float[] lightModelMtx = LinjearAlg.matrixToArrayRearrange(l1.getWorldTransforms());
			final VectorArray4f lightWorldSpace = new VectorArray4f();
			final VectorArray4f lightEyeSpace = new VectorArray4f();
	
			// Transform light into eye-space
			Matrix.multiplyMV(lightWorldSpace.getArray(), 0, lightModelMtx, 0, lightPosModelSpace, 0);
			Matrix.multiplyMV(lightEyeSpace.getArray(), 0, getViewMatrix().getArray(), 0, lightWorldSpace.getArray(), 0); 
		    
			float lightPos[] = lightEyeSpace.getArray();
			float diffuse[] = LinjearAlg.vectorToArray(l1.getProperties().getDiffuse());
			float ambient[] = LinjearAlg.vectorToArray(l1.getProperties().getAmbient());
			GLES20.glUniform3f(fs.getLightUniformId(Shaders.LightStruct.POSITION, idx), lightPos[0], lightPos[1], lightPos[2]);
			GLES20.glUniform4f(fs.getLightUniformId(Shaders.LightStruct.DIFFUSE, idx), diffuse[0], diffuse[1], diffuse[2], diffuse[3]);
			GLES20.glUniform4f(fs.getLightUniformId(Shaders.LightStruct.AMBIENT, idx), ambient[0], ambient[1], ambient[2], ambient[3]);
		}
	}

	/**
	 * Activate float-buffer with vbo id or by binding data directly
	 * 
	 * @param attibId id of shader param
	 * @param bufferId buffer, valid in case of vbo usage
	 * @param target type buffer used for binding vbo buffer
	 * @param data buffer data, can be null if vbo is used through valid bufferId
	 * @param dataFloatCnt number of floats per element in the buffer
	 */
	private void activateFloatBuffer(int attibId, final Graphics.BufferId bufferId, final BufferDataType type,
			final BufferTarget target, Buffer data, int dataFloatCnt)
	{

		if(useVBOs && Graphics.BufferId.isBufferIdValid(bufferId) && (type == BufferDataType.BUFFER_DATA_DYNAMIC)){
			setGlBufferData(bufferId.value(), target, data.remaining() * Const.FLOAT_BYTE_SIZE, data, type);
		}
		
		if( useVBOs && Graphics.BufferId.isBufferIdValid(bufferId)){
			GLES20.glBindBuffer(Graphics.toOpenGlEsType(target), bufferId.value());
			GLES20.glEnableVertexAttribArray(attibId);
			GLES20.glVertexAttribPointer(attibId, dataFloatCnt, GLES20.GL_FLOAT, false, 0, 0);
		}
		else{
			GLES20.glEnableVertexAttribArray(attibId);
			GLES20.glVertexAttribPointer(attibId, dataFloatCnt, GLES20.GL_FLOAT, false, 0, data);
		}
	}
	
	private int setVertexPointer(VertexShader vs, IRenderableData renderableData){
		final int posId = vs.getPosVecAttrId();
		final VertexBufferData vdata = renderableData.getVertexBufferData();
		
		activateFloatBuffer(posId, vdata.getPositionsID(), vdata.getPositionsBufferType(), BufferTarget.BUFFER_TARGET_ARRAY, 
				renderableData.getVertexBuffer(), VertexData.VERTX_POS_SIZE);
		return posId;
	}
	
	private int setTextureCoordPointer(VertexShader vs, IRenderableData renderableData){
		final int texcId = vs.getTexCoordsVecAttrId();
		final VertexBufferData vdata = renderableData.getVertexBufferData();
		
		activateFloatBuffer(texcId, vdata.getTexturesID(), vdata.getTexturesBufferType(), BufferTarget.BUFFER_TARGET_ARRAY, 
				renderableData.getTextureCoordsBuffer(), VertexData.VERTEX_TEX_COORD_SIZE);
		return texcId;
	}

	private int setNormalsPointer(VertexShader vs, IRenderableData renderableData){
		final int normId = vs.getNormalAttrId();
		final VertexBufferData vdata = renderableData.getVertexBufferData();
		
		activateFloatBuffer(normId, vdata.getNoramlsID(), vdata.getNormalsBufferType(), BufferTarget.BUFFER_TARGET_ARRAY, 
				renderableData.getNormalsBuffer(), VertexData.VERTX_NOR_SIZE);
		return normId;
	}
	
	private int setColorPointer(VertexShader vs, IRenderableData renderableData){
		final int colId = vs.getColorAttrId();
		final VertexBufferData vdata = renderableData.getVertexBufferData();
		
		activateFloatBuffer(colId, vdata.getColorsID(), vdata.getColorsBufferType(), BufferTarget.BUFFER_TARGET_ARRAY, 
				renderableData.getColorBuffer(), VertexData.VERTX_COL_SIZE);
		return colId;
	}

	private int setTangentsPointer(VertexShader vs, IRenderableData renderableData){
		final int tangentId = vs.getTangentAttrId();
		final VertexBufferData vdata = renderableData.getVertexBufferData();
		
		activateFloatBuffer(tangentId, vdata.getTangentsID(), vdata.getTangentsBufferType(), BufferTarget.BUFFER_TARGET_ARRAY, renderableData.getTangetBuffer(), 
				VertexData.VERTX_TANG_SIZE);
		return tangentId;
	}
	
	private int setBinormalsPointer(VertexShader vs, IRenderableData renderableData){
		final int binorId = vs.getBinormalAttrId();
		final VertexBufferData vdata = renderableData.getVertexBufferData();
		
		activateFloatBuffer(binorId, vdata.getBinormalsID(), vdata.getBinormalsBufferType(), BufferTarget.BUFFER_TARGET_ARRAY, 
				renderableData.getBinormalBuffer(), VertexData.VERTX_BINOR_SIZE);
		return binorId;			
	}

	private void computeAlphaBlending(final IMaterial m){
		final FragmentShader fs = m.getFragmentShader();
		if(fs != null)
			GLES20.glUniform1f(fs.getAlphaUniformId(), m.getProperties().getAlpha());
	}
	
	public void draw(IRenderableData renderableData) {
		GLES20.glFrontFace(Graphics.toOpenGlEsType(renderableData.getRenderDirection()));
		final VertexShader vs = renderableData.getVertexShader();
		setLights(renderableData);
		
		// Set render data pointers, position is mandatory
		setVertexPointer(vs, renderableData);
		
		// Set other optional pointers and collect enabled vertex attributes
		final List<Integer> attributeIDs = new ArrayList<Integer>();
		
		if(Graphics.hasTexCoordBuffer(renderableData))
			attributeIDs.add(setTextureCoordPointer(vs, renderableData));
		if(Graphics.hasNormalsBuffer(renderableData))
			attributeIDs.add(setNormalsPointer(vs, renderableData));
		if(Graphics.hasColorBuffer(renderableData))
			attributeIDs.add(setColorPointer(vs, renderableData));
		if(Graphics.hasTangentsBuffer(renderableData))
			attributeIDs.add(setTangentsPointer(vs, renderableData));
		if(Graphics.hasBinormalsBuffer(renderableData))
			attributeIDs.add(setBinormalsPointer(vs, renderableData));
		
		final float[] modelMatrix = LinjearAlg.matrixToArrayRearrange(renderableData.getModelMatrix());
		final float[] mVPMatrix = modelViewProjMtx.getArray();
		final float[] mVMatrix = modelViewMtx.getArray(); 
        
        // Set the model-view matrix of the shader, allow the renderableData impl to make adjustments to the view matrix
		float viewMtx[] = getViewMatrix().getArray();
		float localViewMtx[] = new float[viewMtx.length];
		System.arraycopy(viewMtx, 0, localViewMtx, 0, localViewMtx.length);
		renderableData.adaptViewMatrix(new MatrixArray4f(localViewMtx));
        Matrix.multiplyMM(mVMatrix, 0, localViewMtx, 0, modelMatrix, 0);
        GLES20.glUniformMatrix4fv(vs.getModelViewMtxUniformId(), 1, false, mVMatrix, 0);
        
        // Set the model-view-projection-matrix of the shader
        Matrix.multiplyMM(mVPMatrix, 0, getProjectionMatrix().getArray(), 0, mVMatrix, 0);
        GLES20.glUniformMatrix4fv(vs.getMvpMatrixUniformId(), 1, false, mVPMatrix, 0);
        
        drawElementsCall(renderableData);
        
        // Unbind all bound buffers
        if(useVBOs){
        	GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0);
        	GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, 0);
        }
        // Disable enabled attributes
        for(int id : attributeIDs){
        	GLES20.glDisableVertexAttribArray(id);
        }
	}

	private void drawElementsCall(IRenderableData renderableData) {
		final IRenderableData.RenderOperation op = renderableData.getRenderOperation();
        final ShortBuffer indices = renderableData.getActiveIndexBuffer();
        final int count = Util.calcIndexCount(indices);
        
        final VertexBufferData vdata = renderableData.getVertexBufferData();
        if(useVBOs && Graphics.BufferId.isBufferIdValid(vdata.getIndicesID())){
        	GLES20.glBindBuffer(GLES20.GL_ELEMENT_ARRAY_BUFFER, vdata.getIndicesID().value());
         	GLES20.glDrawElements(
            		Graphics.toOpenGlEsType(op), 
            		count, 
            		GLES20.GL_UNSIGNED_SHORT,
            		0);
        	
        }
        else{
        	GLES20.glDrawElements(
            		Graphics.toOpenGlEsType(op), 
            		count, 
            		GLES20.GL_UNSIGNED_SHORT, 
            		indices);	
        }
	}

	public void checkError(String op) {
		int error;
		while((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
			if(l.isLevelLoggable(Log.ERROR)){
				l.log(op + ": glError " + GLU.gluErrorString(error) + " (" + error + ")", Log.ERROR);
			}
		    throw new RuntimeException(op + ": glError " + error);
		}
	}
	
	private int loadShader(int shaderType, String source, String name) {
	    int shader = GLES20.glCreateShader(shaderType);
	    if (shader != 0) {
	        GLES20.glShaderSource(shader, source);
	        GLES20.glCompileShader(shader);
	        checkError("compile");
	        int[] compiled = new int[1];
	        GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled, 0);
	        if (compiled[0] == 0) {
	        	if(l.isLevelLoggable(Log.WARN))
	        		l.log("Could not compile shader " + shaderType + ": " + name, Log.WARN);
	            String str = GLES20.glGetShaderInfoLog(shader);
	            if(l.isLevelLoggable(Log.WARN))
	            	l.log(str, Log.WARN);
	            GLES20.glDeleteShader(shader);
	            shader = 0;
	        }
	    }
	    return shader;
	}
	
	public IShaderProgram.Id createProgramLinkCompiledShaders(final IShaderProgram.Id compiledVertexShaderId, final IShaderProgram.Id compiledFragShaderId) {
		int program = GLES20.glCreateProgram();
		if (program != 0) {
			GLES20.glAttachShader(program, compiledVertexShaderId.get());
			checkError("glAttachShader");
			GLES20.glAttachShader(program, compiledFragShaderId.get());
			checkError("glAttachShader");
			GLES20.glLinkProgram(program);
			int[] linkStatus = new int[1];
			GLES20.glGetProgramiv(program, GLES20.GL_LINK_STATUS, linkStatus, 0);
			if (linkStatus[0] != GLES20.GL_TRUE) {
			    l.log("Could not link program: ");
			    l.log(GLES20.glGetProgramInfoLog(program));
			    GLES20.glDeleteProgram(program);
			    program = 0;
			}
		}
		return new IShaderProgram.Id(program);
	}

	public IShaderProgram.Id createProgramCompileShaders(final VertexShader vs, final FragmentShader fs) {
		if(vs == null || fs == null)
			throw new IllegalArgumentException("Cannot create shader when vertex-shader or fragemnt shader is null.");
	
		int vsId = loadShader(getGlType(vs), vs.getSource(), vs.getShaderName());
		int fsId = loadShader(getGlType(fs), fs.getSource(), fs.getShaderName());
		vs.setInstanceProgramId(new IShaderProgram.Id(vsId));
		fs.setInstanceProgramId(new IShaderProgram.Id(fsId));
		
		return createProgramLinkCompiledShaders(new IShaderProgram.Id(vsId), new IShaderProgram.Id(fsId));
	}

	public boolean supportsProgramablePipeline() {
		return true;
	}
	
	public static int getGlType(FragmentShader ignored){
		return GLES20.GL_FRAGMENT_SHADER;
	}
	public static int getGlType(VertexShader ignored){
		return GLES20.GL_VERTEX_SHADER;
	}

	public void useProgram(IShaderProgram.Id program) {
		GLES20.glUseProgram(program.get());
	}

	public int getShaderParamId(IShaderProgram.Id program, String name, ValueScope vs) {
		int handle = 0;
		if(vs == ValueScope.SHADER_VALUE_SCOPE_PER_VERTEX)
			handle = GLES20.glGetAttribLocation(program.get(), name);
		else if(vs == ValueScope.SHADER_VALUE_SCOPE_PER_PRIMITIVE)
			handle = GLES20.glGetUniformLocation(program.get(), name);
		
		checkError("glGetAttribLocation " + name);
		
		return handle;
	}
	
	public int getSupporteRenderVersion() {
		return RENDER_VER;
	}

	
	public int getMaxLightCount() {
		return MAX_LIGHTS;
	}
	
	private void setUniformColor(int id, Vector4f color){
		if(Shaders.isShaderIdValid(id)){
			float[] colorArr = LinjearAlg.vectorToArray(color);
			GLES20.glUniform4f(id, colorArr[0], colorArr[1], colorArr[2], colorArr[3]);
			checkError("glUniform4f");
		}
	}

	public void initMaterialForDrawing(final IMaterial material, int textureUnit) {
		computeAlphaBlending(material);
		if(textureUnit >= material.getNrOfTextures()){
			l.log("Invalid texture unit recived in driver", Log.ERROR);
			throw new IllegalArgumentException("textureUnit >= material.getNrOfTextures");
		}
		
		final boolean useTexturing = Materials.isMaterialUsingTexture(material);
		if( useTexturing && textureUnit == IMaterial.TEX_UNIT_ALL){
			activateTexureUnits( material);			 
		}
		else if(useTexturing && textureUnit > 0){
			GLES20.glActiveTexture(GLES20.GL_TEXTURE0 );
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, material.getTexture(textureUnit).getRenderId().value());
		}
		else{
			GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
		}
		
		FragmentShader fs = material.getFragmentShader();
		if(fs != null){
			setUniformColor(fs.getMatEmmisionUniformId(), material.getProperties().getEmission());
			setUniformColor(fs.getMatBrightnessUniformId(), material.getProperties().getBrightness());
			//setUniformColor(fs.getMatDiffuseUniformId(), material.getProperties().getDiffuse());
		}
	}

	private void activateTexureUnits(final IMaterial material) {
		final int nrTextures = material.getNrOfTextures();
		final FragmentShader fs = material.getFragmentShader();
		
		final int[] samplers = new int[]{
				fs.getSampler2dUniformId(),
				fs.getSampler2dNormalMapUniformId()
		};
		if( samplers.length < nrTextures && l.isLevelLoggable(Log.WARN)){
			l.log("Driver does not support required nr of textures in material! Ignoring some textures.", Log.WARN);
		}
			
		for(int i = 0; i < nrTextures && i < samplers.length; i++){
			GLES20.glActiveTexture(GLES20.GL_TEXTURE0 + i);
			GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, material.getTexture(i).getRenderId().value());
			GLES20.glUniform1i(samplers[i], i);
		}
	}

	public int[] generateTextureIds(int nTextures) {
		int[] textures = new int[nTextures];
		GLES20.glGenTextures(textures.length, textures, 0);
		return textures;
	}
	
	
	public void setVboData(int vboID, VertexData.BufferTarget target, int size, Buffer data, VertexData.BufferDataType usage){
		setGlBufferData(vboID, target, size, data, usage);
		GLES20.glBindBuffer(Graphics.toOpenGlEsType(target), 0);
	}

	private void setGlBufferData(int vboID, VertexData.BufferTarget target, int size, Buffer data, VertexData.BufferDataType usage) {
		GLES20.glBindBuffer(Graphics.toOpenGlEsType(target), vboID);
		GLES20.glBufferData(Graphics.toOpenGlEsType(target), size, data, Graphics.toOpenGlEsType(usage));
	}
	
	public int[] generateFrameBufferObjects(int nBuffers){
		final int[] buffers = new int[nBuffers];
		GLES20.glGenFramebuffers(nBuffers, buffers, 0);
		
		return buffers;
	}

	public int[] generateRenderBufferIDs(int nBuffers) {
		final int[] buffers = new int[nBuffers];
		GLES20.glGenRenderbuffers(nBuffers, buffers, 0);
		
		return buffers;
	}
	
	public int[] generateVBOs(int nVbos) {
		int[] vbos = new int[nVbos];
		GLES20.glGenBuffers(vbos.length, vbos, 0);
		return vbos;
	}
	public void deleteVbos(int nVbos, int[] vbos) {
		GLES20.glDeleteBuffers(nVbos, vbos, 0);
	}
	
	public void deleteTextures(int nTextures, int[] textures) {
		GLES20.glDeleteTextures(nTextures, IntBuffer.wrap(textures));
	}

	public ITexture.RenderId createTexture(final Bitmap bitmap, final int textureID, ITexture.Settings settings) {
		if(settings == null){
			throw Util.buildIllegalNullArgException(getClass(), "settings");
		}
		
		if((bitmap == null) && ((settings.getWidth() == Settings.SizeValueFromBitmap) || (settings.getHeight() == Settings.SizeValueFromBitmap))){
			throw new IllegalArgumentException("bitmap == null requires initalized size values in settings instance");
		}

		GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureID);
		
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER,
				Graphics.toOpenGlEsType(settings.getMinFilter()));
		GLES20.glTexParameterf(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER,
				Graphics.toOpenGlEsType(settings.getMagFilter()));
		
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_S,
				Graphics.toOpenGlEsType(settings.getWrapS()));
		GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_WRAP_T,
				Graphics.toOpenGlEsType(settings.getWrapT()));
		
		if(bitmap != null)
		{
			GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
			bitmap.recycle();
		}
		else
		{
			final int format = Graphics.toOpenGlEsType(settings.getFormat());
			GLES20.glTexImage2D(GLES20.GL_TEXTURE_2D, 0, format, settings.getWidth(), settings.getHeight(), 0, format, GLES20.GL_UNSIGNED_BYTE, null);
		}
		
		return new ITexture.RenderId(textureID);
	}

	public boolean initRenderTarget(IRenderTarget renderTarget) {
		
		ITexture.RenderId textureId = ITexture.RenderId.InvalidID; 
		final List<FrameBufferData> buffers = renderTarget.getFrameBufferObject().getTargetBuffersReadOnly();
		final List<Pair<BufferId, BufferAttachmentType>> renderBuffers = new ArrayList<Pair<BufferId, BufferAttachmentType>>();
		
		for(FrameBufferData data : buffers){
			
			if(data.getAttatchmentType() == BufferAttachmentType.BUFF_ATTATCH_COLOR){
				textureId = data.getTargetTexture().getRenderId();
			}
			else{
				
				final BufferId depthId = data.getRenderId();
				final int storageType = Graphics.toOpenGlEsType(data.getBufferStorageType());
				GLES20.glBindRenderbuffer(GLES20.GL_RENDERBUFFER, depthId.value() );
				GLES20.glRenderbufferStorage(GLES20.GL_RENDERBUFFER, storageType, 
						data.getBufferWidth(), data.getBufferHeight());
				
				GLES20.glBindRenderbuffer(GLES20.GL_RENDERBUFFER, 0);
				renderBuffers.add(Util.makePair(depthId, data.getAttatchmentType()));
			}
		}
		
		if(textureId == ITexture.RenderId.InvalidID || renderBuffers.size() <= 0){
			if(l.isLevelLoggable(Log.WARN))
				l.log("Invalid render-target" + renderTarget.getName() + ":Texure or depth buffer not initalized", Log.WARN);
			return true;
		}
			
		
		final BufferId frameBufferId = renderTarget.getFrameBufferObject().getRenderFboId();
		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, frameBufferId.value());
		// Set texture color buffer
		GLES20.glFramebufferTexture2D(GLES20.GL_FRAMEBUFFER, GLES20.GL_COLOR_ATTACHMENT0, GLES20.GL_TEXTURE_2D, textureId.value(), 0);
		
		// Set other render buffers, eg depth buffer
		for(Pair<BufferId, BufferAttachmentType> bufferIdAndType : renderBuffers){
			GLES20.glFramebufferRenderbuffer(GLES20.GL_FRAMEBUFFER,  Graphics.toOpenGlEsType(bufferIdAndType.second), GLES20.GL_RENDERBUFFER, bufferIdAndType.first.value());
		}
		
		GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
		checkError("glBindFramebuffer");
		
		return true;
	}
}
