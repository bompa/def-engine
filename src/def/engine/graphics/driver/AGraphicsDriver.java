package def.engine.graphics.driver;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.vecmath.Matrix4f;

import def.engine.control.ICamera;
import def.engine.graphics.components.Light;
import def.engine.graphics.material.ITexture;
import def.engine.graphics.material.IMaterial.MaterialMode;
import def.engine.logic.math.MatrixArray4f;
import def.util.IntPair;

/**
 * Non-thread safe abstact render. State is configured and the then executed with the
 * draw method which is implemented by a concrete subclass. 
 * @author jonas
 *
 */
public abstract class AGraphicsDriver implements IGraphicsDriver{
	
	private final ConcurrentHashMap<Integer, Light> lights = new ConcurrentHashMap<Integer, Light>();
	private ICamera renderCamera;
	
	private boolean projectionMtxUpdated = true;
	
	private Matrix4f modelMatrix = new Matrix4f();	// TODO: Change to MatrixArray4f
	private MatrixArray4f viewMatrix = new MatrixArray4f();
	private MatrixArray4f projectionMatrix = new MatrixArray4f();
	
	private MaterialMode matMode = MaterialMode.NONE;
	private ITexture.RenderId textureRenderId = new ITexture.RenderId(0);
	private IRenderTarget renderTarget = null;
	
	public IRenderTarget getRenderTarget(){
		return renderTarget;
	}
	
	public void setRenderTarget(IRenderTarget target) {
		this.renderTarget = target;	
	}
	
	public IntPair getRenderWindowSize() {
		return getRenderConfig().getRenderWinSize();
	}

	public void setModelMatrix(Matrix4f mtx) {
		setModelMatrix_(mtx);
	}
	
	public void setRenderCamera(ICamera c) {
		renderCamera = c;
		setViewMatrix(c.getViewMatrix());
	}
	
	public ICamera getRenderCamera(){
		return renderCamera;
	}
	
	public void setProjectionMatrix(MatrixArray4f mtx) {
		setProjectionMatrix_(mtx);
		projectionMtxUpdated = true;
	}
	
	public void setMaterialModeState(MaterialMode mode) {
		setMatMode(mode);
	}
	
	public void setTextureRenderId(ITexture.RenderId texId) {
		setTextureRenderId_(texId);
	}
	
	public void preDraw() {
		prepeareRendering();

		if(projectionMtxUpdated){
			updatePojection();
			projectionMtxUpdated = false;
		}
	}
	
	public void enableLight(Light l) {
		lights.putIfAbsent(l.getId(), l);
	}
	
	/**
	 * Implement by concrete driver to define how the projection is updated, called only
	 * when the projection matrix has been updated. 
	 */
	protected abstract void updatePojection();
	
	/**
	 * Implemented by concrete driver to define how rendering are prepared, like setting background color.
	 */
	protected abstract void prepeareRendering();

	
	/***************************************************
	 * Internal Render-state get/set operations. Setter only used internally(private), getter by subclasses(protected).
	 ***************************************************/
	
	/** Model-Matrix **/
	private void setModelMatrix_(Matrix4f modelMatrix_) {
		this.modelMatrix = modelMatrix_;
	}
	protected Matrix4f getModelMatrix_() {
		return modelMatrix;
	}
	
	/** View-Matrix**/
	private void setViewMatrix(MatrixArray4f viewMatrix_) {
		this.viewMatrix = viewMatrix_;
	}
	protected MatrixArray4f getViewMatrix() {
		return viewMatrix;
	}

	/** Projection-Matrix **/
	private void setProjectionMatrix_(MatrixArray4f projectionMatrix_) {
		this.projectionMatrix = projectionMatrix_;
	}
	protected MatrixArray4f getProjectionMatrix() {
		return projectionMatrix;
	}
	
	/** Material-Mode **/
	private void setMatMode(MaterialMode mode) {
		matMode = mode;
	}
	protected MaterialMode getMaterialMode(){
		return matMode;
	}
	
	/** Texture Render-Id**/
	private void setTextureRenderId_(ITexture.RenderId texId) {
		textureRenderId = texId;
	}
	protected ITexture.RenderId getTextureRenderId() {
		return textureRenderId;
	}
	
	/** Light list **/
	protected Collection<Light> getLights(){
		List<Light> list = new ArrayList<Light>(lights.values());
		return list;
	}
	
	public void drawFrameDone() {
		lights.clear();
	}
}
