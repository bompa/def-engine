package def.engine.graphics.driver;

import java.util.Vector;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;

import def.engine.graphics.components.Vertex;


/**
 * Temporary data and indices to create a Vertex, and static helper functions for validating vertex-data
 * @author jonas
 * @see Vertex
 *
 */
public class VertexData {
	/*
	 * Static data
	 */
	public static enum UsedBuffersType{VERT_POSITION, VERT_POSITION_COLOR, VERT_POSITION_NORMAL, VERT_POSITION_TEXCOORD, VERT_POSITION_NORMAL_TEXCOORDS}
	public static enum BufferDataType{ BUFFER_DATA_STATIC, BUFFER_DATA_DYNAMIC, BUFFER_DATA_DYNAMIC_ARRAY }
	public static enum BufferTarget{BUFFER_TARGET_ARRAY, BUFFER_TARGET_ELEMENT_ARRAY};

	public static final int VERTX_POS_SIZE = 3;
	public static final int VERTX_NOR_SIZE = 3;
	public static final int VERTX_TANG_SIZE = 3;
	public static final int VERTX_BINOR_SIZE = 3;
	public static final int VERTX_COL_SIZE = 4;
	public static final int VERTEX_TEX_COORD_SIZE = 2;
	public static final int VERTEX_MAX_SIZE = VERTX_POS_SIZE + VERTX_NOR_SIZE + VERTEX_TEX_COORD_SIZE + VERTX_COL_SIZE + VERTX_TANG_SIZE + VERTX_BINOR_SIZE;
		
	public static void validateNormalArray(float[] vertexNormal) {
		if(vertexNormal == null || vertexNormal.length != VERTX_NOR_SIZE)
			throw new IllegalArgumentException();
	}
	public static void validateTexCoordArray(float[] vertexTexCoord) {
		if(vertexTexCoord  == null || vertexTexCoord.length != VERTEX_TEX_COORD_SIZE)
			throw new IllegalArgumentException();
	}
	public static void validateColorArray(float[] color) {
		if(color == null || color.length != VERTX_COL_SIZE)
			throw new IllegalArgumentException();
	}
	public static void validateVertexPosArray(float[] vertexPos) {
		if(vertexPos == null || vertexPos.length != VERTX_POS_SIZE)
			throw new IllegalArgumentException();
	}
	
	/**
	 * Pos, texcoord and normal indices 
	 * @author jonas
	 *
	 */
	public static class Indices{
		public static final int Invalid = -1;
		
		public final int v;
		public final int t;
		public final int n;
		
		private int cachedHash = Invalid;
		
		public static Indices create(int v, int t, int n){
			return new Indices(v, t, n);
		}
		
		public static Indices createInvalid(){
			return new Indices();
		}
		
		private Indices(int v, int t, int n){
			this.v = v;
			this.t = t;
			this.n = n;
		}
		
		private Indices(){
			v = Invalid;
			t = Invalid;
			n = Invalid;
		}
		
		@Override
		public boolean equals(Object obj){
			if (obj == null)
				return false;
			if (obj == this)
				return true;
			
			if (obj.getClass() != getClass())
				return false;
			
			final Indices rhs = (Indices)obj; 
			if(this.v == rhs.v && this.t == rhs.t && this.n == rhs.n)
				return true;

			return false;
		}
		
		@Override
		public int hashCode(){
			if(cachedHash == Invalid){
				final String hashString = String.valueOf(v) + "/" + String.valueOf(t) + "/" + String.valueOf(n);
				cachedHash = hashString.hashCode();
			}
			
			return cachedHash;
		}
	}

	
	/**
	 * Defines a face triangle using pos-texcoord-normal indices for each point-
	 * @author jonas
	 *
	 */
	public static class FaceTriangle{
		public final Indices first;
		public final Indices second;
		public final Indices third;
		
		public static FaceTriangle createDefault(){
			return new FaceTriangle();
		}
		
		public static FaceTriangle create(final Indices first, final Indices second, final Indices third){
			return new FaceTriangle(first, second, third);
		}
		
		private FaceTriangle(final Indices first, final Indices second, final Indices third){
			this.first = first;
			this.second = second;
			this.third = third;
		}
		
		private FaceTriangle(){
			first = new Indices();
			second = new Indices();
			third = new Indices();
		}
	}
	
	/**
	 * Create new instance
	 * @return
	 */
	public static VertexData create(){
		return new VertexData();
	}
	
	private VertexData(){
		positions = new Vector<Vector3f>();
		normals= new Vector<Vector3f>();
		textureCoords= new Vector<Vector2f>();
		faces = new Vector<VertexData.FaceTriangle>();
	}
	
	public boolean hasNormals(){
		return normals.size() > 0;
	}
	
	public boolean hasTexcoords(){
		return textureCoords.size() > 0;
	}
	
	public final Vector<Vector3f> positions;
	public final Vector<Vector3f> normals;
	public final Vector<Vector2f> textureCoords;
	public final Vector<FaceTriangle> faces;
}
