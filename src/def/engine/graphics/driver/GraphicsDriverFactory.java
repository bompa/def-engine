package def.engine.graphics.driver;


public class GraphicsDriverFactory {
	public static IGraphicsDriver createOpenGl10Driver(){
		return new GL10GraphicsDriver();
	}
	
	public static IGraphicsDriver createOpenGl20Driver(){
		return new GL20GraphicsDriver();
	}
}
