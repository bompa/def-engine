package def.engine.graphics.driver;

import javax.microedition.khronos.opengles.GL11;
import javax.vecmath.Matrix4f;

import def.engine.control.ICamera;
import def.engine.graphics.components.Light;
import def.engine.graphics.material.IMaterial;
import def.engine.graphics.material.IMaterial.MaterialMode;
import def.engine.graphics.shader.IShaderProgram;
import def.engine.logic.math.MatrixArray4f;
import def.util.Options;

public interface IGraphicsDriver extends IGraphicsResources {	//TODO: Change to composition
	
	/**
	 * Initalize driver with oprngl interface and config
	 * @param glInterface
	 * @param conf
	 */
	public void init(GL11 glInterface, RenderConfig conf);
	
	/**
	 * Configure driver state using OptVal[] array of the RenderConfig
	 */
	public void configure(Options.Val[] conf);
	
	public RenderConfig getRenderConfig();
	
	public void setViewPort(int x, int y, int width, int height);
	public void setModelMatrix(Matrix4f mtx);
	public void setProjectionMatrix(MatrixArray4f mtx);
	public void setMaterialModeState(MaterialMode mode);
	
	/**
	 * Prepare a single or all of the materials texture units
	 * @param material
	 * @param textureUnit
	 */
	public void initMaterialForDrawing(IMaterial material, int textureUnit);
	
	public GL11 getGlInterface();
	
	/**
	 * Set camera used to render from, updates view-matrix
	 * @param c
	 */
	public void setRenderCamera(ICamera c);
	
	/**
	 * Get camera that used to render in next draw call
	 * @return
	 */
	public ICamera getRenderCamera();
	
	/**
	 * Called before drawing current snapshot
	 */
	public void preDraw();

	/**
	 * Set the target which will be used when rendering
	 * @param target
	 */
	public void setRenderTarget(IRenderTarget target );
	
	public IRenderTarget getRenderTarget();
	
	/**
	 * Main draw function, draws vertex-data defined and transformed as described by the
	 * renderable-data
	 * @param irendInfo
	 */
	public void draw(IRenderableData irendInfo);
	public void drawFrameDone();
	
	public boolean supportsProgramablePipeline();
	public void useProgram(IShaderProgram.Id program);
	
	/**
	 * Enable a light source
	 */
	public void enableLight(Light l);
	
	/**
	 * Get the maximum nr of lights the driver can render.
	 */
	public int getMaxLightCount();
	
	public void checkError(String op);
}
