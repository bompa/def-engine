package def.engine.graphics.driver;

import javax.vecmath.Vector4f;

import def.util.Configuration;
import def.util.IntPair;
import def.util.Options;

public class RenderConfig {
	// Static data/functions
	private static final CharSequence[] OPTIONS =
		{"Use Ligtning", "Draw Normals", "Perform Face Culling"};
	private static final boolean[] OPTION_DEFAULTS =
		{true, false, true};
	
	private static final int OPT_USE_LIGTNING = 0;
	private static final int OPT_DRAW_NORMALS = 1;
	private static final int PERFORM_FACE_CULLING = 2;
	private static final int MAX_OPTION_VALUE = OPTIONS.length;
	
	private static final Vector4f backgColor = new Vector4f(1.0f, 0.0f, 0.0f, 1.0f);
	
	public static CharSequence[] getAvailableOptionNames(){
		return OPTIONS.clone();
	}
	public static boolean[] getAvailableOptionValues(){
		return OPTION_DEFAULTS.clone();
	}
	
	private static CharSequence getOptionName(int option){
		if(option > MAX_OPTION_VALUE)
			return "";
		return OPTIONS[option];
	}
	
	public static boolean getOptionBool(final Configuration conf, int id){
		return conf.getRenderConfigOptionBool(
				Configuration.convertStringToConfigStd(getOptionName(id).toString()));
	}
	
	public static RenderConfig createFromConfiguration(final Configuration conf){
		RenderConfig rc = new RenderConfig();
		boolean useLigthing = getOptionBool(conf, OPT_USE_LIGTNING);
		rc.setUseLigthing(useLigthing);
		boolean drawNormals = getOptionBool(conf, OPT_DRAW_NORMALS);
		rc.setDrawNormals_(drawNormals);
		boolean faceCulling = getOptionBool(conf, PERFORM_FACE_CULLING);
		rc.setFaceCulling_(faceCulling);
		return rc;
	}
	
	public final static int REND_ATTR_LIGHTNING = 0;
	public final static int REND_ATTR_NORMALS = 1;
	public final static int REND_ATTR_FACE_CULL = 2;
	public final static int REND_ATTR_BLENDING = 3;
	public final static int REND_ATTR_DEPTH_TEST = 4;
	private static Options.Val[] renderAttributes = {Options.Val.ON, Options.Val.ON, Options.Val.ON, Options.Val.ON, Options.Val.ON};
	
	private IntPair winSize = new IntPair(0, 0);
	
	/**
	 * Get template list of attributes with correct size, equal to nr off option attributes
	 * @see updateRenderConfigOptions
	 * @return
	 */
	public static Options.Val[] getUndefinedAttributesTemplate(){
		Options.Val[] attributes = Options.getUndefValuesOptValArray(renderAttributes.length);
		return attributes;
	}
	public Options.Val[] getOptionAttributes(){
		return renderAttributes;
	}
	
	public void updateRenderConfigOptions(Options.Val[] renderAttributesNew){
		if(renderAttributesNew.length != renderAttributes.length)
			throw new IllegalArgumentException("Wring length or attibute list, should be" + renderAttributes.length);
		
		updateAttributeIfValid(renderAttributesNew, REND_ATTR_LIGHTNING);
		updateAttributeIfValid(renderAttributesNew, REND_ATTR_NORMALS);
		updateAttributeIfValid(renderAttributesNew, REND_ATTR_FACE_CULL);
		updateAttributeIfValid(renderAttributesNew, REND_ATTR_BLENDING);
		updateAttributeIfValid(renderAttributesNew, REND_ATTR_DEPTH_TEST);
	}
	
	/**
	 * Update attribute to new value if valid, i.e not equal to Val.UNDEF
	 * @param renderAttributesIn
	 * @param pos
	 */
	private void updateAttributeIfValid(Options.Val[] renderAttributesIn, int pos) {
		Options.updateAttributeIfValid( renderAttributes, renderAttributesIn, pos);
	}
	
	public void setUseLigthing(boolean useLigthing) {
		renderAttributes[REND_ATTR_LIGHTNING] = Options.toOptVal(useLigthing);
	}
	public void setDrawNormals_(boolean drawNormals) {
		renderAttributes[REND_ATTR_NORMALS] = Options.toOptVal(drawNormals);
	}
	public void setFaceCulling_(boolean faceCulling) {
		renderAttributes[REND_ATTR_FACE_CULL] = Options.toOptVal(faceCulling);
	}
	
	public void setUseDepthTest(boolean useDepthTest) {
		renderAttributes[REND_ATTR_DEPTH_TEST] = Options.toOptVal(useDepthTest);
	}
	
	public boolean drawNormals(){
		return Options.getAttrBool(renderAttributes[REND_ATTR_NORMALS]);
	}
	
	public boolean performFaceCulling(){
		return Options.getAttrBool(renderAttributes[REND_ATTR_FACE_CULL]);
	}
	
	public Vector4f getBackGroundColor(){
		return backgColor;
	}

	public boolean useLightning(){
		return Options.getAttrBool(renderAttributes[REND_ATTR_LIGHTNING]);
	}
	
	public boolean useBlending() {
		return Options.getAttrBool(renderAttributes[REND_ATTR_BLENDING]);
	}
	
	public boolean useDepthTest() {
		return Options.getAttrBool(renderAttributes[REND_ATTR_DEPTH_TEST]);
	}
	
	/**
	 * Set win size, first=width, second=height
	 * @param intPair
	 */
	public void setRenderWinSize(IntPair intPair) {
		if(intPair == null)
			throw new IllegalArgumentException("setRenderWinSize");
		
		winSize = intPair;
	}
	
	/**
	 * Get win size, IntPair.first=width, IntPair.second=height
	 * @return IntPair of window width/height
	 */
	public IntPair getRenderWinSize() {
		return winSize;
	}
}
