package def.engine.graphics.driver;

import def.engine.control.ICamera;
import def.engine.graphics.IRenderable;
import def.engine.graphics.ViewPort;
import def.engine.logic.math.MatrixArray4f;

public interface IRenderTarget {
	
	enum BufferStorageType{BUFF_STORAGE_INVALID, BUFF_STORAGE_RGBA4, BUFF_STORAGE_RGB565, BUFF_STORAGE_RGB5_A1, BUFF_STORAGE_DEPTH_COMPONENT16, BUFF_STORAGE_STENCIL_INDEX, BUFF_STORAGE_COLOR_TEXTURE2D};
	enum BufferAttachmentType{BUFF_ATTATCH_COLOR, BUFF_ATTATCH_DEPTH, BUFF_ATTATCH_STENCIL};

	/**
	 * Camera mode for target CAM_MODE_MAIN_CAMERA=use default scene camera, requires
	 * scene to be added before surface is updated (REND_ACTION_SURFACE_CREATED event). 
	 * @author jonas
	 *
	 */
	enum CameraMode{CAM_MODE_CUSTOM, CAM_MODE_CUSTOM_USE_SCREEN_SIZE, CAM_MODE_MAIN_CAMERA};
	
	/**
	 * Get the name of the render-target, if any
	 * @return
	 */
	public String getName();
	
	/**
	 * Get the the frame-buffer, includes it buffer-id used to by driver to activate this
	 * render target for rendering
	 * @return
	 */
	public FrameBufferObject getFrameBufferObject();
	
	/**
	 * Get viewport associated with target camera, requires camera to be set.
	 * 
	 * @see ViewPort
	 * @return
	 */
	public ViewPort getViewPort();
	
	/**
	 * Set the camera used with this target
	 * @param camera
	 */
	public void setTargetCamera(ICamera camera);
	
	/**
	 * Get camera used with this target 
	 * 
	 * @return
	 */
	public ICamera getTargetCamera();
	
	/**
	 * The rendertarget can override the default projection mtx, if so
	 * a non-null value is returned.
	 * 
	 * @return null if projection mtx is not set, if set to override default,
	 *  the matrix is returned.
	 */
	public MatrixArray4f getProjectionMatrix();

	/**
	 * Check if renderable is supported and can/should be rendered by this target.
	 * @param renderable
	 * @return
	 */
	public boolean checkIfSupportedByTarget(IRenderable renderable);
	
	/**
	 * @see CameraMode desc.
	 * @return camera mode enum
	 */
	public CameraMode getCameraMode();
	
	/**
	 * Check if target has buffers wich requires preparation before use
	 * @return true if target needs to be prepared
	 */
	public boolean requiresPrepartion();
}
