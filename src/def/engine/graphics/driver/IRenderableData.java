package def.engine.graphics.driver;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

import javax.vecmath.Matrix4f;

import def.engine.graphics.shader.FragmentShader;
import def.engine.graphics.shader.VertexShader;
import def.engine.logic.math.MatrixArray4f;
import def.util.IntPair;

public interface IRenderableData {
	public enum RenderOperation{RO_NONE, RO_TRIANGLES, RO_LINES}
	public enum RenderCullFace{RD_NONE, RD_BACK, RD_FRONT}
	public enum RenderDirection{RD_NONE, RD_CW, RD_CCW}
	
	/**
	 * Get datastructure containg all possible buffer ids
	 * @return
	 */
	public VertexBufferData getVertexBufferData();

	/**
	 * @return the operation used to render the object
	 */
	public RenderOperation getRenderOperation();
	
	/**
	 * Get the direction the vertices are rendered, clockwise or counter clockwise 
	 * @return
	 */
	public RenderDirection getRenderDirection();
	
	/**
	 * Get nr of components which can be render using the buffers of this render data
	 */
	int getNrOfComponents();
	
	/**
	 * Set which component should be rendered from the buffers
	 * 
	 * @param componentIndex
	 * 
	 * @see getNrOfComponents
	 */
	void setActiveComponent(int componentIndex);
	
	/**
	 * get ref to list of all component index ranges
	 * @return
	 */
	public ArrayList<IntPair> getComponentIndices();
	
	/**
	 * Make specific local changes to the view matrix used to render the object
	 * @param viewMatrix
	 */
	public void adaptViewMatrix(MatrixArray4f viewMatrix);
	
	/**
	 * Get the sclale used to render object
	 * @return the scale of the object
	 */
	public float getScale();
	
	/**
	 * Set the scale used to render object
	 * @return
	 */
	public void setScale(float scale);
	
	/**
	 * Set model matrix
	 * @param modelMatrix_
	 */
	public void setModelMatrix(Matrix4f modelMatrix_);
	
	/**
	 * Get model matrix
	 * @return
	 */
	public Matrix4f getModelMatrix();
	
	/**
	 * Get range of indices for renderable drawing
	 * @return
	 * @see getIndexBuffer
	 */
	public IntPair getIndexBufferRange();
	
	/**
	 * Get all indices available in the renderableData
	 * @return
	 */
	public short[] getIndices();
	
	/*
	 * Shader get/set operations
	 */
	public void setFragmentShader(FragmentShader fs_);
	public FragmentShader getFragmentShader();
	
	public void setVertexShader(VertexShader vs_);
	public VertexShader getVertexShader();
	
	/*
	 * Buffer get/set operations
	 */
	public void setColorBuffer(FloatBuffer colorBuffer_);
	public FloatBuffer getColorBuffer();

	public void setNormalsBuffer(FloatBuffer normalsBuffer);
	public FloatBuffer getNormalsBuffer();
	
	public void setTextureCoordsBuffer(FloatBuffer tcBuffer);
	public FloatBuffer getTextureCoordsBuffer();
	
	public ShortBuffer getActiveIndexBuffer();
	public FloatBuffer getVertexBuffer();
	
	public FloatBuffer getTangetBuffer();
	public void setTangetBuffer(FloatBuffer tangetBuffer);

	public FloatBuffer getBinormalBuffer();
	public void setBinormalBuffer(FloatBuffer binormalBuffer);
}
