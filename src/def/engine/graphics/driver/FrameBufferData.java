package def.engine.graphics.driver;

import def.engine.graphics.driver.Graphics.BufferId;
import def.engine.graphics.driver.IRenderTarget.BufferAttachmentType;
import def.engine.graphics.driver.IRenderTarget.BufferStorageType;
import def.engine.graphics.material.ITexture;
import def.util.Util;

public class FrameBufferData {
	private final BufferStorageType bufferStorageType;
	private BufferAttachmentType attatchmentType;
	private BufferId renderId = BufferId.InvalidID;
	
	private ITexture targetTexture = null;
	private int bufferWidth = 0;
	private int bufferHeight = 0;
	
	public static FrameBufferData createTextureFrameBufferData(final ITexture tex){
		final FrameBufferData buff = new FrameBufferData(BufferStorageType.BUFF_STORAGE_COLOR_TEXTURE2D);
		buff.setTargetTexture(tex);
		
		return buff;
	}
	
	public static FrameBufferData createDepthFrameBufferData(int width, int height){
		final FrameBufferData buff = new FrameBufferData(BufferStorageType.BUFF_STORAGE_DEPTH_COMPONENT16);
		buff.attatchmentType = BufferAttachmentType.BUFF_ATTATCH_DEPTH;
		buff.setBufferSize( width, height);
		
		return buff;
	}
	
	public static FrameBufferData create(final BufferStorageType type){
		return new FrameBufferData(type);
	}
	
	private FrameBufferData(final BufferStorageType type){
		this.bufferStorageType = type;
	}
	
	public BufferStorageType getBufferStorageType() {
		return bufferStorageType;
	}
	public BufferId getRenderId() {
		return renderId;
	}
	public void setRenderId(BufferId renderId) {
		if(renderId == null)
			throw Util.buildIllegalNullArgException(FrameBufferData.class, "renderFboId");
		
		this.renderId = renderId;
	}

	public BufferAttachmentType getAttatchmentType() {
		return attatchmentType;
	}

	public void setAttatchmentType(BufferAttachmentType attatchmentType) {
		this.attatchmentType = attatchmentType;
	}

	public ITexture getTargetTexture() {
		return targetTexture;
	}

	public void setTargetTexture(ITexture targetTexture) {
		if(targetTexture == null)
			throw Util.buildIllegalNullArgException(FrameBufferData.class, "func:setTargetTexture", "targetTexture");
		if((targetTexture.getSettings() == null))
			throw new IllegalArgumentException("Texture setting is null in FrameBufferData::setTargetTexture");
		
		final ITexture.Settings settings = targetTexture.getSettings();
		
		setBufferSize( settings.getWidth(), settings.getHeight());
		this.attatchmentType = BufferAttachmentType.BUFF_ATTATCH_COLOR;
		this.targetTexture = targetTexture;
	}

	public int getBufferWidth() {
		return bufferWidth;
	}

	public void setBufferSize(int width, int height) {
		this.bufferWidth = width;
		this.bufferHeight = height;
	}

	public int getBufferHeight() {
		return bufferHeight;
	}

}
