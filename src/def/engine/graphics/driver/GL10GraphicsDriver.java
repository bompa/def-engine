package def.engine.graphics.driver;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL11;
import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

import android.graphics.Bitmap;
import android.opengl.GLUtils;
import android.util.Log;

import def.Const;
import def.engine.graphics.components.Light;
import def.engine.graphics.material.IMaterial;
import def.engine.graphics.material.ITexture;
import def.engine.graphics.material.Materials;
import def.engine.graphics.material.ITexture.RenderId;
import def.engine.graphics.shader.FragmentShader;
import def.engine.graphics.shader.IShaderProgram;
import def.engine.graphics.shader.VertexShader;
import def.engine.graphics.shader.IShaderProgram.Id;
import def.engine.graphics.shader.ShaderValue.ValueScope;
import def.engine.logic.math.LinjearAlg;
import def.engine.logic.math.MatrixArray4f;
import def.util.LogHandler;
import def.util.Options;
import def.util.Util;
import def.util.Options.Val;

public class GL10GraphicsDriver extends AGraphicsDriver {
	private final LogHandler l = LogHandler.createInstanceShared();
	
	private static final int RENDER_VER = 1;
	private static final int MAX_LIGHTS = 9;
	private static final int[] glLights = {GL11.GL_LIGHT0, GL11.GL_LIGHT1, GL11.GL_LIGHT2, GL11.GL_LIGHT3,
		GL11.GL_LIGHT4, GL11.GL_LIGHT5, GL11.GL_LIGHT6, GL11.GL_LIGHT7};
	
	private RenderConfig renderConf;
	private GL11 gl = null;
		
	public void init(GL11 glInterface, RenderConfig conf) {
		if(glInterface == null)
			throw new IllegalArgumentException("The Gl10 graphics driver recieved an illegal null value as glInterface reference!");
		
		gl = glInterface;
		renderConf = conf;
		
		final Vector4f backg = renderConf.getBackGroundColor();
		gl.glClearColor(backg.x, backg.y, backg.z, backg.w);
		// Enable Smooth Shading, default not really needed.
		gl.glShadeModel(GL11.GL_SMOOTH);// OpenGL docs.
		// Depth buffer setup.
		gl.glClearDepthf(1.0f);// OpenGL docs.
		
		setDepthTest(conf.useDepthTest());
		
		// The type of depth testing to do.
		gl.glDepthFunc(GL11.GL_LEQUAL);// OpenGL docs.
		
		gl.glEnable(GL11.GL_TEXTURE_2D);
		gl.glEnable(GL11.GL_COLOR_MATERIAL);	// Color used for blending
		
		setBlending(conf.useBlending());
		
		setLightning(conf.useLightning());
		
		if(conf.performFaceCulling()){
			gl.glEnable(GL11.GL_CULL_FACE);
			// What faces to remove with the face culling.
			gl.glCullFace(GL11.GL_BACK);	
		}
		else{
			gl.glDisable(GL11.GL_CULL_FACE);
		}

		gl.glHint(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST);		
	}
	
	public void configure(Options.Val[] options) {
		
		if(options[RenderConfig.REND_ATTR_LIGHTNING] != Options.Val.UNDEF){
			setLightning(Options.getAttrBool(options[RenderConfig.REND_ATTR_LIGHTNING]));
		}
		
		if(options[RenderConfig.REND_ATTR_BLENDING] != Options.Val.UNDEF){
			setBlending(Options.getAttrBool(options[RenderConfig.REND_ATTR_BLENDING]));
		}
		
		if(options[RenderConfig.REND_ATTR_DEPTH_TEST] != Options.Val.UNDEF){
			setDepthTest(Options.getAttrBool(options[RenderConfig.REND_ATTR_DEPTH_TEST]));
		}
		
		if(options[RenderConfig.REND_ATTR_FACE_CULL] != Options.Val.UNDEF){
			setFaceCulling(Options.getAttrBool(options[RenderConfig.REND_ATTR_FACE_CULL]));
		}
	}
	
	private void setLightning(boolean on) {
		if(on){
			gl.glEnable(GL11.GL_LIGHTING);
			gl.glEnable(GL11.GL_LIGHT0);
		}
		else
		{
			gl.glDisable(GL11.GL_LIGHTING);
			gl.glDisable(GL11.GL_LIGHT0);
		}
	}
	
	private void setFaceCulling(boolean on){
		if(on){
			gl.glEnable(GL11.GL_CULL_FACE);
			gl.glCullFace(GL11.GL_BACK);			
		}
		else{
			gl.glDisable(GL11.GL_CULL_FACE);
		}
	}
	
	private void setDepthTest(boolean on) {
		if(on)
			gl.glEnable(GL11.GL_DEPTH_TEST);
		else
			gl.glDisable(GL11.GL_DEPTH_TEST);
	}

	private void setBlending(boolean on) {
		if(on){
			gl.glEnable(GL11.GL_BLEND);
			gl.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);			
		}
		else{
			gl.glDisable(GL11.GL_BLEND);
		}
	}

	public RenderConfig getRenderConfig() {
		return this.renderConf;
	}
	
	public GL11 getGlInterface() {
		return gl;
	}

	public void setViewPort(int x, int y, int width, int height) {
		gl.glViewport(x, y, width, height);
	}
	
	private void setGlLight(final int glLightId, Light.Properties props, final Vector3f posVec){
		float[] amb = LinjearAlg.vectorToArray(props.getAmbient());
		float[] diff = LinjearAlg.vectorToArray(props.getDiffuse());
		float[] spec = LinjearAlg.vectorToArray(props.getSpecular());
		float[] pos = LinjearAlg.vectorToArray(posVec);
		//float[] spotDirection = { 0.0f, 0.0f, 1.0f };

		gl.glLightf(glLightId, GL11.GL_QUADRATIC_ATTENUATION, 0.5f);
		gl.glLightf(glLightId, GL11.GL_CONSTANT_ATTENUATION, 2.0f);
		gl.glLightf(glLightId, GL11.GL_LINEAR_ATTENUATION, 1.0f);
		//gl_.glLightfv(glLightId, GL11.GL_SPOT_DIRECTION, spotDirection, 0);
		gl.glLightfv(glLightId, GL11.GL_POSITION, pos, 0);
		gl.glLightfv(glLightId, GL11.GL_AMBIENT, amb, 0);
		gl.glLightfv(glLightId, GL11.GL_DIFFUSE, diff, 0);
		gl.glLightfv(glLightId, GL11.GL_SPECULAR, spec, 0);
	}
	
	@Override
	public void enableLight(Light l){
		super.enableLight(l);
		setGlLight(GL11.GL_LIGHT0, l.getProperties(), l.getIPositionable().getPosition());
	}
	
	@Override
	protected void prepeareRendering() {
		// Clears the screen and depth buffer.
		gl.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
		
		gl.glLoadIdentity();
	}
	
	@Override
	protected void updatePojection() {
		gl.glMatrixMode(GL11.GL_PROJECTION);
		gl.glMultMatrixf(getProjectionMatrix().getArray(), 0);
		
		// Reset the modelview matrix
		gl.glMatrixMode(GL11.GL_MODELVIEW);
		gl.glLoadIdentity();		
	}


	private void setGlRenderAttributes(IRenderableData ri){
		gl.glFrontFace(Graphics.toOpenGlEsType(ri.getRenderDirection()));
	}
	
	public void draw(IRenderableData renderableData) {
		float[] modelMtx = LinjearAlg.matrixToArrayRearrange(renderableData.getModelMatrix());
		float viewMtx[] = getViewMatrix().getArray();

		float localViewMtx[] = new float[viewMtx.length];
		System.arraycopy(viewMtx, 0, localViewMtx, 0, localViewMtx.length);
		renderableData.adaptViewMatrix(new MatrixArray4f(localViewMtx));

		gl.glPushMatrix();
		
		gl.glMultMatrixf(localViewMtx, 0);
		gl.glMultMatrixf(modelMtx, 0);
		
		setGlRenderAttributes(renderableData);
		
		final boolean hasTexCoords = Graphics.hasTexCoordBuffer(renderableData);
		final boolean hasNormals = Graphics.hasNormalsBuffer(renderableData);
		
		// Enable correct ogengl draw state
		gl.glEnableClientState(GL11.GL_VERTEX_ARRAY);
		if(hasTexCoords)
			gl.glEnableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		if(hasNormals)
			gl.glEnableClientState(GL11.GL_NORMAL_ARRAY);
		
		// Draw stuff
		gl.glVertexPointer(3, GL11.GL_FLOAT, 0, renderableData.getVertexBuffer());
		if(hasNormals)
			gl.glNormalPointer(GL11.GL_FLOAT, 0, renderableData.getNormalsBuffer());
		if(hasTexCoords)
			gl.glTexCoordPointer(2, GL11.GL_FLOAT, 0, renderableData.getTextureCoordsBuffer());

		IRenderableData.RenderOperation op = renderableData.getRenderOperation();
		final ShortBuffer indices = renderableData.getActiveIndexBuffer();
        final int count = Util.calcIndexCount(indices);
        
		gl.glDrawElements(
				Graphics.toOpenGlEsType(op),
				count,
				GL11.GL_UNSIGNED_SHORT, 
				indices);
		
		if(renderConf.drawNormals() && hasNormals)
			drawNormals(renderableData.getNormalsBuffer());

		// Disable correct ogengl draw state		
		gl.glDisableClientState(GL11.GL_VERTEX_ARRAY);
		if(hasTexCoords)
			gl.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		
		unsetGlRenderAttributes();
		gl.glPopMatrix();
	}
	
	private void drawNormals(final FloatBuffer normals){
		// Disable unneeded states
		gl.glDisableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
		gl.glDisableClientState(GL11.GL_NORMAL_ARRAY);
		
		gl.glEnableClientState(GL11.GL_VERTEX_ARRAY);

		final ByteBuffer bb = ByteBuffer.allocateDirect(normals.capacity() * Const.FLOAT_BYTE_SIZE * 2);
		bb.order(ByteOrder.nativeOrder());
		final FloatBuffer pairNoramlsBuff = bb.asFloatBuffer();
		if(!LinjearAlg.createNormalDirectionsBuffer(pairNoramlsBuff, normals)){
			if(l.isLevelLoggable(Log.WARN))
				l.log("Failed to draw normals, due to invalid vertex data.", Log.WARN);
			return;
		}	
		gl.glVertexPointer(3, GL11.GL_FLOAT, 0, pairNoramlsBuff);
		gl.glDrawArrays(GL11.GL_LINES, 0, pairNoramlsBuff.capacity() / VertexData.VERTX_POS_SIZE);
	}
	
	private void unsetGlRenderAttributes(){
		gl.glDisable(GL11.GL_CULL_FACE); 
	}	

	public void checkError(String op) {
	}

	public IShaderProgram.Id createProgramLinkCompiledShaders(IShaderProgram.Id vs, IShaderProgram.Id fs) {
		throw new UnsupportedOperationException();
	}

	public IShaderProgram.Id createProgramCompileShaders(VertexShader vs, FragmentShader fs) {
		throw new UnsupportedOperationException();
	}

	public boolean supportsProgramablePipeline() {
		return false;
	}

	public void useProgram(Id program) {
		throw new UnsupportedOperationException();
	}

	public int getShaderParamId(IShaderProgram.Id program, String name) {
		throw new UnsupportedOperationException();
	}

	public int getSupporteRenderVersion() {
		return RENDER_VER;
	}

	public int getShaderParamId(Id program, String name, ValueScope vs) {
		throw new UnsupportedOperationException();	
	}

	public int getMaxLightCount() {
		return MAX_LIGHTS;
	}

	public void initMaterialForDrawing(IMaterial material, int textureUnit) {
		final float aplha = material.getProperties().getAlpha();
		gl.glColor4f(aplha, aplha, aplha, aplha);
		
		//
		final boolean useTexturing = Materials.isMaterialUsingTexture(material);
		if( useTexturing && textureUnit == IMaterial.TEX_UNIT_ALL){
			gl.glBindTexture(GL11.GL_TEXTURE_2D, material.getTexture().getRenderId().value());			 
		}
		else if(useTexturing && textureUnit > 0){
			gl.glBindTexture(GL11.GL_TEXTURE_2D, material.getTexture(textureUnit).getRenderId().value());
		}
		
		float[] amb = LinjearAlg.vectorToArray(material.getProperties().getAmbient());
		float[] diff = LinjearAlg.vectorToArray(material.getProperties().getDiffuse());
		float[] spec = LinjearAlg.vectorToArray(material.getProperties().getSpecular());
		float[] emm = LinjearAlg.vectorToArray(material.getProperties().getEmission());
		
		gl.glMaterialfv(GL11.GL_FRONT_AND_BACK, GL11.GL_AMBIENT, new float[]{0.4f, 0.4f, 0.4f,1.0f}, 0);
		gl.glMaterialfv(GL11.GL_FRONT_AND_BACK, GL11.GL_DIFFUSE, new float[]{0.7f,0.7f,0.7f,1.0f}, 0);
		gl.glMaterialfv(GL11.GL_FRONT_AND_BACK, GL11.GL_SPECULAR, new float[]{1.0f,1.0f,1.0f,1.0f}, 0);
		gl.glMaterialfv(GL11.GL_FRONT_AND_BACK, GL11.GL_EMISSION, emm, 0);			
	}

	public int[] generateTextureIds(int nTextures) {
		int[] textures = new int[nTextures];
		gl.glGenTextures(textures.length, textures, 0);
		return textures;
	}
	
	public void deleteTextures(int nTextures, int[] textures) {
		gl.glDeleteTextures(nTextures, IntBuffer.wrap(textures));
	}
	
	public int[] generateVBOs(int nVbos) {
		int[] vbos = new int[nVbos];
		gl.glGenBuffers(vbos.length, vbos, 0);
		return vbos;
	}
	

	public int[] generateFrameBufferObjects(int nBuffers) {
		return new int[]{1};
	}

	public int[] generateRenderBufferIDs(int nBuffers) {
		return new int[]{1};
	}
	
	public void deleteVbos(int nVbos, int[] vbos) {
		gl.glDeleteBuffers(nVbos, vbos, 0);
	}
	
	public void setVboData(int vboID, VertexData.BufferTarget target, int size, Buffer data, VertexData.BufferDataType usage)
	{
		gl.glBindBuffer(Graphics.toOpenGlEsType(target), vboID);
		gl.glBufferData(Graphics.toOpenGlEsType(target), size, data, Graphics.toOpenGlEsType(usage));
		gl.glBindBuffer(Graphics.toOpenGlEsType(target), 0);
	}

	public ITexture.RenderId createTexture(final Bitmap bitmap, final int textureID, ITexture.Settings settings) {

		if(bitmap == null){
			l.log("Null bitmap parameter to createTexture ignoring value", Log.WARN);
			return new ITexture.RenderId(0);
		}
		
		gl.glBindTexture(GL11.GL_TEXTURE_2D, textureID);
		
		gl.glTexParameterf(GL11.GL_TEXTURE_2D,
				GL11.GL_TEXTURE_MIN_FILTER,
				GL11.GL_NEAREST);
		gl.glTexParameterf(GL11.GL_TEXTURE_2D,
				GL11.GL_TEXTURE_MAG_FILTER,
				GL11.GL_LINEAR);
		
		gl.glTexParameterx(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_S,
				GL11.GL_CLAMP_TO_EDGE);
		gl.glTexParameterx(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_WRAP_T,
				GL11.GL_CLAMP_TO_EDGE);
		
		GLUtils.texImage2D(GL11.GL_TEXTURE_2D, 0, bitmap, 0);
		bitmap.recycle();
		
		return new ITexture.RenderId(textureID);
	}

	public boolean initRenderTarget(IRenderTarget renderTarget) {
		// TODO Auto-generated method stub
		return false;
	}
}