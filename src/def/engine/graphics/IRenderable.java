package def.engine.graphics;

import javax.vecmath.Matrix4f;

import def.engine.graphics.components.IBuildAction;
import def.engine.graphics.driver.IRenderableData;
import def.engine.graphics.driver.VertexData;
import def.engine.graphics.material.IMaterial;
import def.shared.interfaces.ITransform;

public interface IRenderable extends Comparable<IRenderable>{
	/**
	 * Get the matrix representing the model matrix of the renderable
	 * @return
	 */
	public Matrix4f getWorldTransforms();
	
	/**
	 * Get the Z-depth from the current main camera from which this renderable is render
	 * @return the depth value if calculated or 0.0 otherwise
	 */
	public float getCalculatedZDepth();
	
	/**
	 * Set calculated(from camera) zdepth for this renderable
	 * @param zdepth
	 */
	public void setCalculatedZDepth(float zdepth);
	
	/**
	 * Return a reference to the IPositionable implementation
	 * @return
	 */
	public ITransform getIPositionable();
	
	/**
	 * Get the material of this renderable
	 * @return immutable material reference
	 */
	public IMaterial getMaterialReadOnly();
	
	/**
	 * Get read-only data about how the renderable should be rendered.
	 * @return
	 */
	public IRenderableData getRenderInfoReadOnly();
	
	/**
	 * Get data about how the renderable should be rendered.
	 * @return
	 */
	public IRenderableData getRenderInfo();
	
	/**
	 * Returns true if the material requires preparation before rendering.
	 * If this returns true getMaterial/ReadOnly must not return null.
	 * @return
	 */
	public boolean requiresPreparationOfMaterialForRendering();
	
	/**
	 * Returns true if the vertex buffers used by the renderable requires preparation before rendering.
	 * If this returns true getRenderableInfo should not return null.
	 * @return
	 */
	public boolean requiresPreparationOfVertexBuffersForRendering();
	
	/**
	 * Get the material of this renderable
	 * @return material reference
	 */
	public IMaterial getMaterial();
	
	/**
	 * Set the material of this renderable
	 */
	public void setMaterial(IMaterial mat);
	
	/**
	 * Request that rendering should be done.
	 */
	public void requestRendering();
	
	/**
	 * Accept a visit from a renderer, rendering the renderable object.
	 */
	public void acceptRendering(IRenderableVisitor renderVisitor);
	
	/**
	 * Defeine in what priority the renderable should be rendered, i.e in what order.
	 */
	public int getRenderPrio();
	
	/**
	 * Get the name of the renderable 
	 */
	public String getName();

	/**
	 * Setvthe name of the renderable 
	 */
	public void setName(String name);
	
	/**
	 * Get of this renderable should request new rendering automatically every frame 
	 * @return
	 */
	public boolean getContiniousRendering();

	/**
	 * Set of this renderable should request new rendering automatically every frame
	 * @param value
	 */
	public void setContiniousRendering(boolean value);
	
	/**
	 * Get the build action implementation, which in turn declares how th vertices of the renderable are composed
	 */
	public IBuildAction getIBuildAction();
	
	/**
	 * Get the used vertex data buffer type
	 * @return
	 */
	public VertexData.UsedBuffersType getVertexDataType();
}
