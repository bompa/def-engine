package def.engine.graphics;

import def.shared.util.ConcurrentObservable;

public class RenderSequenceObservable extends ConcurrentObservable<IRenderSequenceEvent> {
	
	public RenderSequenceObservable(){}
	
	public RenderSequenceObservable(RenderSequenceListener listener){
		registerObserver(listener);
	}
}
