package def.engine.graphics;

import def.engine.control.ICamera;
import def.engine.graphics.driver.FrameBufferObject;
import def.engine.graphics.driver.IRenderTarget;
import def.engine.logic.math.MatrixArray4f;
import def.util.Util;

public class RenderTargetWindow implements IRenderTarget{
	
	private final String name;
	private final FrameBufferObject fbo = FrameBufferObject.createDefault();
	private ICamera camera;
	private CameraMode cameraMode = CameraMode.CAM_MODE_MAIN_CAMERA;
	
	public static RenderTargetWindow create(final String name){
		return new RenderTargetWindow(name);
	}
	
	private RenderTargetWindow(final String name){
		this.name = name;
	}
	
	private void handleInvalidCameraState() {
		if(null == camera){
			throw new IllegalStateException("RenderTargetTexture has not been initalized. camera==null");
		}
	}

	public ViewPort getViewPort() {
		handleInvalidCameraState();
		
		return camera.getViewPort();
	}

	public void setViewPort(ViewPort viewPort) {
		if(viewPort == null)
			throw Util.buildIllegalNullArgException(RenderTargetWindow.class, "func:setViewPort", "viewPort");
		
		camera.setViewPort(viewPort);
	}

	public String getName() {
		return name;
	}
	
	public FrameBufferObject getFrameBufferObject() {
		return fbo;
	}

	public boolean checkIfSupportedByTarget(IRenderable renderable) {
		return true;
	}

	public void setTargetCamera(ICamera camera) {
		this.camera = camera;
	}
	
	public ICamera getTargetCamera() {
		return camera;
	}
	
	public MatrixArray4f getProjectionMatrix()	{
		return null;
	}

	public CameraMode getCameraMode() {
		return cameraMode;
	}

	public void setCameraMode(CameraMode cameraMode) {
		this.cameraMode = cameraMode;
	}

	public boolean requiresPrepartion() {
		return false;
	}
	
	public String toString(){
		return getName();
	}
}
