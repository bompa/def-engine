package def.engine.graphics;

import java.util.Collection;

import def.engine.graphics.driver.IGraphicsDriver;
import def.shared.util.Modifiable;

/**
 * Defines how a set of renderables is rendered.
 * @author jonas
 *
 */
public interface IRenderBucket extends Modifiable<IRenderBucket>{
	
	void add(final IRenderable r);
	
	void remove(final IRenderable r);
	
	/**
	 * Get renderables list, backed by instance so modifications are reflected
	 * @return
	 */
	public Collection<IRenderable> getRenderables();
	
	public int getRenderPrio();

	public void renderContent(final IGraphicsDriver graphics);
	
	public void emptyBucket();

}
