package def.engine.graphics;

public class ViewPort {
	public static final float InvalidMetric = -1;

	private static final ViewPort InvalidViewPort = new ViewPort(InvalidMetric, InvalidMetric, InvalidMetric, InvalidMetric);
	
	private final float x, y, height, width;
	
	public static ViewPort createInvalid(){
		return InvalidViewPort;
	}
	
	public static ViewPort create(float x, float y, float height, float width){
		return new ViewPort(x, y, height, width);
	}
	
	private ViewPort(float x, float y, float height, float width){
		this.x = x;
		this.y = y;
		this.height = height;
		this.width = width;
	}

	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}

	public float getY() {
		return y;
	}

	public float getX() {
		return x;
	}

}
