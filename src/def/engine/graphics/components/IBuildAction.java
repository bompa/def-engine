package def.engine.graphics.components;

public interface IBuildAction {
	public static final int INVALID_BUILD_ACTION_ID = new String("__INVLALID_BUILDID_HASH__").hashCode();
	public static final int DEFAULT_MODEL_BUILD_ACTION_ID = new String("__DEFAULT_MODEL_BUILDID_HASH__").hashCode();
	
	public int getUniqueID();
	public void setUniqueID(int id);
	public void build(IMeshComposer buildComposer);
}
