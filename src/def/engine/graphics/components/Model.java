package def.engine.graphics.components;

import javax.vecmath.Vector4f;

import android.util.Log;

import def.engine.graphics.ARenderable;
import def.engine.graphics.RenderableData;
import def.engine.graphics.driver.IRenderableData;
import def.engine.graphics.driver.VertexData;
import def.engine.graphics.material.IMaterial;
import def.engine.graphics.material.ITexture;
import def.engine.graphics.material.Materials;
import def.shared.interfaces.ITransform;
import def.util.LogHandler;

public class Model extends ARenderable{
	private final LogHandler l = LogHandler.createInstanceShared(Model.class);
	
	private final RenderableData.RenderDirection renderDir = RenderableData.RenderDirection.RD_CCW;
	private final IMaterial material; 
	
	public Model(final ITexture tex, final ITransform posImpl){
		super(posImpl);
		
		if(tex == null){
			material = handleMissingTexture();
		}
		else{
			material = Materials.createWithPerFragmentLightningShaders(tex); 
		}
	}
	
	public Model(final ITexture texture){
		if(texture == null){
			material = handleMissingTexture();
		}
		else{
			material = Materials.createWithPerFragmentLightningShaders(texture);
		}
	}
	
	public Model(final int textureResource, final ITransform posImpl){
		super(posImpl);
		material = Materials.createWithPerFragmentLightningShaders(textureResource);	
	}
	
	private IMaterial handleMissingTexture() {
		if(l.isLevelLoggable(Log.WARN))
			l.log("Failed to get texture when creating model " + getName(), Log.WARN);
			
		return Materials.createWithBasicColorShaders(new Vector4f());
	}
	
	public VertexData.UsedBuffersType getVertexDataType(){ return VertexData.UsedBuffersType.VERT_POSITION_NORMAL_TEXCOORDS; }
	
	@Override
	protected void initSharedRenderInfo(final IRenderableData rd){
		initRenderableDataShaders(rd);
	}

	@Override
	protected IRenderableData createRenderInfo(Mesh mesh) {
		IRenderableData rendInfo = mesh.createRenderableData(
				RenderableData.RenderOperation.RO_TRIANGLES, renderDir);

		initRenderableDataShaders(rendInfo);
		return rendInfo;
	}

	private void initRenderableDataShaders(IRenderableData rendInfo) {
		rendInfo.setFragmentShader(material.getFragmentShader());
		rendInfo.setVertexShader(material.getVertexShader());
	}

	@Override
	public IMaterial getMaterialReadOnly() {
		return material;
	}

	@Override
	public IMaterial getMaterial() {
		return material;
	}

	public boolean requiresPreparationOfMaterialForRendering() {
		return true;
	}
}
