package def.engine.graphics.components;

import java.nio.FloatBuffer;


import def.engine.graphics.ARenderable;
import def.engine.graphics.IRenderableVisitor;
import def.engine.graphics.driver.IRenderableData;
import def.engine.graphics.driver.RenderableDataDecorator;
import def.engine.graphics.driver.VertexData;
import def.engine.graphics.driver.VertexData.BufferDataType;
import def.engine.graphics.material.IMaterial;
import def.engine.graphics.material.ITexture;
import def.engine.graphics.material.Materials;
import def.engine.logic.math.LinjearAlg;
import def.engine.logic.math.MatrixArray4f;
import def.shared.interfaces.ITransform;
import def.util.FloatPair;
import android.util.Pair;

public class BillBoard extends ARenderable{
	// Square to hold animated texture
	private final Square square;
	
	// Local copy of IRenderinfo ref, to update texture coords.
	IRenderableData renderData = null;

	public BillBoard(final ITexture texture, final ITransform posImpl){
		super(posImpl);	
		square = new Square(Materials.createBasicTexturingNoLightning(texture), posImpl);
	}
	
	public BillBoard(final ITexture texture) {
		square = new Square(Materials.createBasicTexturingNoLightning(texture));
	}
	
	@Override
	public IRenderableData getRenderInfoReadOnly(){
		final IRenderableData data = super.getRenderInfoReadOnly();
		return data;
	}
	
	@Override
	protected void initSharedRenderInfo(final IRenderableData rd){
		square.initSharedRenderInfo(rd);
		renderData = rd;
		renderData.getVertexBufferData().setTexturesBufferType(BufferDataType.BUFFER_DATA_DYNAMIC);
		updateRenderDataTextureCoords();
	}

	@Override
	protected IRenderableData createRenderInfo(Mesh mesh) {
		renderData = BillBoardRenderableData.create(square.createRenderInfo(mesh));
		// Texture coords will be modified, so set textures buffer type to dynamic
		// which means a dynamic vbo buffer will be used
		renderData.getVertexBufferData().setTexturesBufferType(BufferDataType.BUFFER_DATA_DYNAMIC);
		updateRenderDataTextureCoords();
		return renderData;
	}
	
	@Override
	public void acceptRendering(IRenderableVisitor renderVisitor){
		super.acceptRendering(renderVisitor);	
		updateRenderDataTextureCoords();
	}

	private void updateRenderDataTextureCoords() {
		final ITexture texture = square.getMaterial().getTexture();
		if(texture != null && renderData != null){
			Pair<FloatPair, FloatPair> coords = texture.getTextureCoordinates();
			FloatPair uv00 = coords.first;
			FloatPair uv11 = coords.second;
			
			// Add values to texcoord buffer 
			FloatBuffer buffer = renderData.getTextureCoordsBuffer();
			buffer.position(0);
			
			buffer.put(uv00.first).put(uv00.second);
			buffer.put(uv11.first).put(uv00.second);
			buffer.put(uv11.first).put(uv11.second);
			buffer.put(uv00.first).put(uv11.second);
			buffer.position(0);
		}
	}
	
	/**
	 * Adapts renderable data to be rendered as a billboard, always facing the camera.
	 * @author jonas
	 */
	private static class BillBoardRenderableData extends RenderableDataDecorator{
			
		public static BillBoardRenderableData create(final IRenderableData decorated){
			return new BillBoardRenderableData(decorated);
		}
		
		private BillBoardRenderableData(final IRenderableData decorated){
			super(decorated); 
		}

		@Override
		public void adaptViewMatrix(MatrixArray4f viewMatrix) {
			// Create billboard effect by canceling out cameras rotation(and scaling) values(top 3x3 matrix).
			// TODO: Re-apply scale
			LinjearAlg.set3x3SubmatrixToIdentity(viewMatrix);
		}
	}
		
	/*
	 * Methods forwarded to square renderable.
	 */
	
	protected void createMesh(IMeshComposer builder){ square.createMesh(builder); }

	public VertexData.UsedBuffersType getVertexDataType() { return square.getVertexDataType(); }
	
	@Override
	public IMaterial getMaterialReadOnly() { return square.getMaterialReadOnly(); }
	
	@Override
	public void setMaterial(IMaterial mat) { square.setMaterial(mat); }
	
	@Override
	public IMaterial getMaterial() { return square.getMaterial(); }
	
	public boolean requiresPreparationOfMaterialForRendering() { return square.requiresPreparationOfMaterialForRendering();	}
}
