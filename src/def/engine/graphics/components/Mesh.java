package def.engine.graphics.components;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

import def.Const;
import def.engine.graphics.RenderableData;
import def.engine.graphics.driver.IRenderableData;
import def.engine.graphics.driver.VertexData;
import def.util.IntPair;
import def.util.Util;

public class Mesh {
	
	private FloatBuffer vertexBuffer;
	private FloatBuffer normalsBuffer;
	private FloatBuffer colorBuffer;
	private FloatBuffer texCoordBuffer;
	
	// Calculated buffers
	private FloatBuffer tangetBuffer;
	private FloatBuffer binormalBuffer;

	// Indices
	private short[] indexArray;
	private final ArrayList<IntPair> componetIndices = new ArrayList<IntPair>();
	
	private FloatBuffer compressesTriangleVeretxData;
		
	public IRenderableData createRenderableData(RenderableData.RenderOperation ro, RenderableData.RenderDirection rd){
		IRenderableData rendInfo = 
			RenderableData.create(ro, rd, this.getVertexBuffer(), this.getIndexArray(), this.getComponetIndices());

		rendInfo.setTextureCoordsBuffer(this.getTexCoordBuffer());
		rendInfo.setNormalsBuffer(this.getNormalBuffer());
		rendInfo.setColorBuffer(this.getColorBuffer());
		rendInfo.setTangetBuffer(this.getTangetBuffer());
		rendInfo.setBinormalBuffer(this.getBinormalBuffer());
		return rendInfo;
	}
	
	private Mesh(){}

	
	public static Mesh create(	final VertexData.UsedBuffersType vertType, 
								final List<Vertex> vertices, 
								final List<Short> indices, 
								final ArrayList<IntPair> componetIndices)
	{
		Mesh tm = new Mesh();
		
		tm.intitBuffers(vertType, vertices, indices, componetIndices);
		return tm;
	}
	
	public FloatBuffer getVertexBuffer(){
		return vertexBuffer;
	}
	public FloatBuffer getNormalBuffer(){
		return normalsBuffer;
	}
	public FloatBuffer getTexCoordBuffer(){
		return texCoordBuffer;
	}
	public FloatBuffer getColorBuffer(){
		return colorBuffer;
	}
	public short[] getIndexArray(){
		return indexArray;
	}
	
	private void intitBuffers(	VertexData.UsedBuffersType vertType, 
								List<Vertex> vertices, 
								List<Short> indices,
								final ArrayList<IntPair> componetIndicesIn)
	{
		addVertexData(vertType, vertices);

		this.indexArray = new short[indices.size()];
		for(int i = 0; i < indices.size(); i++)
			indexArray[i] = indices.get(i).shortValue();
		this.componetIndices.addAll(componetIndicesIn);
	}
	
	private void addVertexData(Vertex v){
		vertexBuffer.put(v.getVertexPos());
		compressesTriangleVeretxData.put(v.getVertexPos());
		
		float[] normal = v.getVertexNormal();
		if(normalsBuffer != null && normal != null){
			normalsBuffer.put(normal);
			compressesTriangleVeretxData.put(normal);
		}
		
		float[] texCoord = v.getVertexTexCoords();
		if(texCoordBuffer != null && texCoord != null){
			texCoordBuffer.put(texCoord);
			compressesTriangleVeretxData.put(texCoord);
		}
		
		float[] color = v.getVertexColor();
		if(colorBuffer != null && color != null){
			colorBuffer.put(color);
			compressesTriangleVeretxData.put(color);
		}
		
		float[] tangent = v.getVertexTangent();
		float[] binormal = v.getVertexBinormal();
		if(tangetBuffer != null && binormalBuffer != null && tangent != null && binormal != null){
			tangetBuffer.put(tangent);
			compressesTriangleVeretxData.put(tangent);
			binormalBuffer.put(binormal);
			compressesTriangleVeretxData.put(binormal);
		}
	}
	
	/**
	 * add vertex-data from vertices list
	 * @param vertType
	 * @param vertices
	 */
	private void addVertexData(VertexData.UsedBuffersType vertType, List<Vertex> vertices){
		if(vertices.size() < 1)
			return;
		
		int vertSize = vertices.size() * Const.FLOAT_BYTE_SIZE;
		vertexBuffer = Util.allocateDirectNativeOrder(vertSize * VertexData.VERTX_POS_SIZE).asFloatBuffer();
		compressesTriangleVeretxData = Util.allocateDirectNativeOrder(vertSize * VertexData.VERTEX_MAX_SIZE).asFloatBuffer();
		
		final boolean hasNormals = hasNormals(vertType);
		final boolean hasTexCoords = hasTexCoords(vertType);
		final boolean hasColors = hasColors(vertType);
		// Check if tangents and binormals has been calculated
		final boolean hasTangent = vertices.get(0).getVertexTangent() != null;
		final boolean hasBinormals = vertices.get(0).getVertexBinormal() != null;
		
		prepareOptionalBuffers(vertSize, hasNormals, hasTexCoords, hasColors, hasTangent && hasBinormals);
			
		
		for(Vertex v : vertices){
			addVertexData(v);
		}
		
		finishOptionalBuffers(hasNormals, hasTexCoords, hasColors, hasTangent && hasBinormals);
	}

	private void finishOptionalBuffers(final boolean hasNormals, final boolean hasTexCoords, final boolean hasColors, boolean hasTangentAndBinormals) {
		vertexBuffer.position(0);
		if(hasNormals){
			normalsBuffer.position(0);;
		}		
		if(hasTexCoords){
			texCoordBuffer.position(0);
		}
		if(hasColors){
			colorBuffer.position(0);
		}
		if(hasTangentAndBinormals){
			tangetBuffer.position(0);
			binormalBuffer.position(0);
		}
	}

	private void prepareOptionalBuffers(int vertSize, final boolean hasNormals,	final boolean hasTexCoords, 
			final boolean hasColors, boolean hasTangentAndBinormals) 
	{
		if(hasNormals){
			normalsBuffer = Util.allocateDirectNativeOrder(vertSize * VertexData.VERTX_NOR_SIZE).asFloatBuffer();
		}		
		if(hasTexCoords){
			texCoordBuffer = Util.allocateDirectNativeOrder(vertSize * VertexData.VERTEX_TEX_COORD_SIZE).asFloatBuffer();
		}
		if(hasColors){
			colorBuffer = Util.allocateDirectNativeOrder(vertSize * VertexData.VERTX_COL_SIZE).asFloatBuffer();
		}
		if(hasTangentAndBinormals){
			tangetBuffer = Util.allocateDirectNativeOrder(vertSize * VertexData.VERTX_TANG_SIZE).asFloatBuffer();
			binormalBuffer = Util.allocateDirectNativeOrder(vertSize * VertexData.VERTX_BINOR_SIZE).asFloatBuffer();
		}
	}
	
	private boolean hasColors(VertexData.UsedBuffersType vertType) {
		return vertType == VertexData.UsedBuffersType.VERT_POSITION_COLOR; 
	}

	private boolean hasNormals(VertexData.UsedBuffersType vertType) {
		return (	vertType == VertexData.UsedBuffersType.VERT_POSITION_NORMAL || 
					vertType == VertexData.UsedBuffersType.VERT_POSITION_NORMAL_TEXCOORDS);
	}

	private boolean hasTexCoords(VertexData.UsedBuffersType vertType) {
		return (	vertType == VertexData.UsedBuffersType.VERT_POSITION_TEXCOORD ||
					vertType == VertexData.UsedBuffersType.VERT_POSITION_NORMAL_TEXCOORDS);
	}

	public ArrayList<IntPair> getComponetIndices() {
		return componetIndices;
	}

	public FloatBuffer getTangetBuffer() {
		return tangetBuffer;
	}

	public FloatBuffer getBinormalBuffer() {
		return binormalBuffer;
	}

}
