package def.engine.graphics.components;

import def.engine.graphics.ARenderable;

public abstract class ARenderableSupportingConstruction extends ARenderable {
	
	public abstract IMeshComposer getBuilder();

}
