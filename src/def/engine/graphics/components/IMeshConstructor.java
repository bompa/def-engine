package def.engine.graphics.components;

public interface IMeshConstructor {
	/**
	 * Build a mesh 
	 * @return the newly constucted mesh
	 */
	public Mesh buildMesh();
}
