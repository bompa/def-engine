package def.engine.graphics.components;

import def.Const;
import def.engine.graphics.ARenderable;
import def.engine.graphics.IRenderableVisitor;
import def.engine.graphics.RenderableData;
import def.engine.graphics.driver.IRenderableData;
import def.engine.graphics.driver.VertexData;
import def.engine.graphics.driver.VertexData.BufferDataType;
import def.engine.graphics.material.IMaterial;
import def.engine.graphics.material.ITexture;
import def.engine.graphics.material.Materials;
import def.engine.graphics.material.Texture;
import def.ui.R;


public class SkyBox extends ARenderable{
	
	private final RenderableData.RenderDirection renderDir = RenderableData.RenderDirection.RD_CCW;
	private final IMaterial material;
	
	public SkyBox() { 
		material = Materials.createBasicTexturingNoLightning(Texture.create(R.drawable.basic_tex));
	}
	
	public SkyBox(final ITexture tex) {
		material = Materials.createBasicTexturingNoLightning(tex);
	}
	
	public VertexData.UsedBuffersType getVertexDataType(){ return VertexData.UsedBuffersType.VERT_POSITION_NORMAL_TEXCOORDS; }
	
	/**
	 * Creates the mesh and adds all vertices in a counter-clockwise order so that face culling is possible.
	 */
	protected void createMesh(IMeshComposer builder){
		
		Vertex backTopLeft = Vertex.create(-1.0f,  1.0f, -1.0f).normal(0.0f, 0.0f, -1.0f).texCoords(0.0f, 0.0f);
		Vertex backBottomLeft = Vertex.create(-1.0f, -1.0f, -1.0f).normal(0.0f, 0.0f, -1.0f).texCoords(1.0f, 0.0f);
		Vertex backBottomRight = Vertex.create(1.0f, -1.0f, -1.0f).normal(0.0f, 0.0f, -1.0f).texCoords(1.0f, 1.0f);
		Vertex backTopRight = Vertex.create(1.0f,  1.0f, -1.0f).normal(0.0f, 0.0f, -1.0f).texCoords(0.0f, 1.0f);
		builder.addVertex(backTopRight).addVertex(backBottomRight).addVertex(backTopLeft).
			reuseVertex(backBottomRight).addVertex(backBottomLeft).reuseVertex(backTopLeft);
		
		builder.newComponent();
		Vertex frontTopLeft = Vertex.create(-1.0f,  1.0f, 1.0f).normal(0.0f, 0.0f, 1.0f).texCoords(0.0f, 0.0f);
		Vertex frontBottomLeft = Vertex.create(-1.0f, -1.0f, 1.0f).normal(0.0f, 0.0f, 1.0f).texCoords(1.0f, 0.0f);
		Vertex frontBottomRight = Vertex.create(1.0f, -1.0f, 1.0f).normal(0.0f, 0.0f, 1.0f).texCoords(1.0f, 1.0f);
		Vertex frontTopRight = Vertex.create(1.0f,  1.0f, 1.0f).normal(0.0f, 0.0f, 1.0f).texCoords(0.0f, 1.0f);
		builder.addVertex(frontTopLeft).addVertex(frontBottomLeft).addVertex(frontBottomRight).
			reuseVertex(frontTopLeft).reuseVertex(frontBottomRight).addVertex(frontTopRight);
		
		builder.newComponent();
		Vertex bottomTopLeft = Vertex.create(1.0f,  -1.0f, -1.0f).normal(0.0f, -1.0f, 0.0f).texCoords(0.0f, 0.0f);
		Vertex bottomBottomLeft = Vertex.create(-1.0f, -1.0f, -1.0f).normal(0.0f, -1.0f, 0.0f).texCoords(1.0f, 0.0f);
		Vertex bottomBottomRight = Vertex.create(-1.0f, -1.0f, 1.0f).normal(0.0f, -1.0f, 0.0f).texCoords(1.0f, 1.0f);
		Vertex bottomTopRight = Vertex.create(1.0f,  -1.0f, 1.0f).normal(0.0f, 0.0f, -1.0f).texCoords(0.0f, 1.0f);
		builder.addVertex(bottomTopRight).addVertex(bottomBottomRight).addVertex(bottomTopLeft).
			reuseVertex(bottomBottomRight).addVertex(bottomBottomLeft).reuseVertex(bottomTopLeft);
		
		builder.newComponent();
		Vertex leftTopLeft = Vertex.create(-1.0f,  1.0f, -1.0f).normal(-1.0f, 0.0f, 0.0f).texCoords(0.0f, 0.0f);
		Vertex leftBottomLeft = Vertex.create(-1.0f, -1.0f, -1.0f).normal(-1.0f, 0.0f, 0.0f).texCoords(1.0f, 0.0f);
		Vertex leftBottomRight = Vertex.create(-1.0f, -1.0f, 1.0f).normal(-1.0f, 0.0f, 0.0f).texCoords(1.0f, 1.0f);
		Vertex leftTopRight = Vertex.create(-1.0f,  1.0f, 1.0f).normal(-1.0f, 0.0f, 0.0f).texCoords(0.0f, 1.0f);
		builder.addVertex(leftTopLeft).addVertex(leftBottomLeft).addVertex(leftBottomRight).
			reuseVertex(leftTopLeft).reuseVertex(leftBottomRight).addVertex(leftTopRight);
		
		builder.newComponent();
		Vertex rightTopLeft = Vertex.create(1.0f,  1.0f, -1.0f).normal(1.0f, 0.0f, 0.0f).texCoords(0.0f, 0.0f);
		Vertex rightBottomLeft = Vertex.create(1.0f, -1.0f, -1.0f).normal(1.0f, 0.0f, 0.0f).texCoords(1.0f, 0.0f);
		Vertex rightBottomRight = Vertex.create(1.0f, -1.0f, 1.0f).normal(1.0f, 0.0f, 0.0f).texCoords(1.0f, 1.0f);
		Vertex rightTopRight = Vertex.create(1.0f,  1.0f, 1.0f).normal(1.0f, 0.0f, 0.0f).texCoords(0.0f, 1.0f);
		builder.addVertex(rightTopRight).addVertex(rightBottomRight).addVertex(rightTopLeft).
			reuseVertex(rightBottomRight).addVertex(rightBottomLeft).reuseVertex(rightTopLeft);
				
		builder.newComponent();
		Vertex topTopLeft = Vertex.create(1.0f,  1.0f, -1.0f).normal(0.0f, 1.0f, 0.0f).texCoords(0.0f, 0.0f);
		Vertex topBottomLeft = Vertex.create(-1.0f, 1.0f, -1.0f).normal(0.0f, 1.0f, 0.0f).texCoords(1.0f, 0.0f);
		Vertex topBottomRight = Vertex.create(-1.0f, 1.0f, 1.0f).normal(0.0f, 1.0f, 0.0f).texCoords(1.0f, 1.0f);
		Vertex topTopRight = Vertex.create(1.0f,  1.0f, 1.0f).normal(0.0f, 1.0f, 0.0f).texCoords(0.0f, 1.0f);
		builder.addVertex(topTopLeft).addVertex(topBottomLeft).addVertex(topBottomRight).
			reuseVertex(topTopLeft).reuseVertex(topBottomRight).addVertex(topTopRight);
	}
	
	/**
	 * Override of ARenderable's impl. of IRenderable interface definition.
	 */
	@Override
	public void acceptRendering(IRenderableVisitor renderVisitor) {
		renderVisitor.renderVisit(this);
	}
	
	@Override
	protected void initSharedRenderInfo(final IRenderableData rd){
		initRenderableDataShaders(rd);
	}

	@Override
	protected IRenderableData createRenderInfo(Mesh mesh) {
		IRenderableData rendInfo = mesh.createRenderableData(
				RenderableData.RenderOperation.RO_TRIANGLES, renderDir);

		// Set indices using buffer array directly
		rendInfo.getVertexBufferData().setIndicesBufferType(BufferDataType.BUFFER_DATA_DYNAMIC_ARRAY);
		
		initRenderableDataShaders(rendInfo);
		return rendInfo;
	}

	private void initRenderableDataShaders(IRenderableData rendInfo) {
		rendInfo.setFragmentShader(material.getFragmentShader());
		rendInfo.setVertexShader(material.getVertexShader());
	}

	@Override
	public IMaterial getMaterialReadOnly() {
		return Materials.unmodifiableMaterial(material);
	}

	@Override
	public IMaterial getMaterial() {
		return material;
	}
	
	@Override
	public int getRenderPrio(){
		return Const.REND_PRIO_SKYBOX;
	}

	public boolean requiresPreparationOfMaterialForRendering() {
		return true;
	}
}
