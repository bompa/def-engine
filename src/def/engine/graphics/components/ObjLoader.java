package def.engine.graphics.components;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;

import android.util.Log;

import def.engine.graphics.driver.VertexData;
import def.engine.graphics.driver.VertexData.FaceTriangle;
import def.engine.graphics.driver.VertexData.Indices;
import def.util.LogHandler;

/**
 * A loader for Wavefront OBJ files.
 * Made by bburns, modified by jonas
 */
public class ObjLoader implements IModelLoader {
	private static final LogHandler l = LogHandler.createInstanceShared(ObjLoader.class);
	
	public static enum hand { LEFT, RIGHT};

	private static final String[] suffs = new String[] {".jpg", ".png", ".gif"};
	
	public static ObjLoader create() {
		return new ObjLoader();
	}
	
	private ObjLoader() {
		this(hand.RIGHT);
	}

	/**
	 * Constructor
	 * @param h The order in which triangle vertices are specifed.
	 */
	public ObjLoader(hand h) {
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean canLoad(File f) {
		return f.getName().endsWith(".obj");
	}

	/**
	 * {@inheritDoc}
	 */
	public VertexData load(String file) throws IOException
	{
		VertexData vd = load(new FileInputStream(file));
		int ix = file.indexOf(".obj");
		if (ix != -1) {
			String texture = file.substring(0, ix);
			for (int i = 0; i < suffs.length; i++) {
				File f = new File(texture+suffs[i]);
				if (f.exists()) {
					//vd.getFrame(0).getMesh().setTextureFile(texture+suffs[i]);
					break;
				}
			}
		}
		return vd;
	}

	/**
	 * {@inheritDoc}
	 */
	public VertexData load(InputStream in) throws IOException
	{

		final VertexData vertexData = VertexData.create(); 

		LineNumberReader input = new LineNumberReader(new InputStreamReader(in));	    
		String line = null;
		for (line = input.readLine(); line != null;	line = input.readLine()){
			if (line.length() > 0) {
				if (line.startsWith("v ")) {
					float[] vertex = new float[3];
					StringTokenizer tok = new StringTokenizer(line);
					try{
						tok.nextToken();
		
						vertex[0] = Float.parseFloat(tok.nextToken());
						vertex[1] = Float.parseFloat(tok.nextToken());
						vertex[2] = Float.parseFloat(tok.nextToken());
					}
					catch(NoSuchElementException e){
						return parseFailed("Unexcpected end of line beginning with \'v\'");
					}
					catch(NumberFormatException e){
						return parseFailed("Failed to parse vertex position");
					}
					vertexData.positions.add(new Vector3f(vertex));
				}
				else if (line.startsWith("vt ")) 
				{
					final float[] coord = new float[2];
					StringTokenizer tok = new StringTokenizer(line);
					tok.nextToken();
					coord[0] = Float.parseFloat(tok.nextToken());
					coord[1] = Float.parseFloat(tok.nextToken());
					vertexData.textureCoords.add(new Vector2f(coord));
				}
				else if (line.startsWith("f ")) 
				{
					int[] face = new int[3];
					int[] face_n_ix = new int[3];
					int[] face_tx_ix = new int[3];
					int[] val;

					StringTokenizer tok = new StringTokenizer(line);
					tok.nextToken();
					val = parseIntTriple(tok.nextToken());
					face[0] = val[0];
					if (val.length > 1 && val[1] > -1)
						face_tx_ix[0] = val[1];
					if (val.length > 2 && val[2] > -1)
						face_n_ix[0] = val[2];

					val = parseIntTriple(tok.nextToken());
					face[1] = val[0];
					if (val.length > 1 && val[1] > -1)
						face_tx_ix[1] = val[1];
					if (val.length > 2 && val[2] > -1)
						face_n_ix[1] = val[2];

					val = parseIntTriple(tok.nextToken());
					face[2] = val[0];
					if (val.length > 1 && val[1] > -1) {
						face_tx_ix[2] = val[1];
					}
					if (val.length > 2 && val[2] > -1) {
						face_n_ix[2] = val[2];
					}
					vertexData.faces.add(FaceTriangle.create(
							Indices.create(face[0], face_tx_ix[0], face_n_ix[0]), 
							Indices.create(face[1], face_tx_ix[1], face_n_ix[1]), 
							Indices.create(face[2], face_tx_ix[2], face_n_ix[2])));

				}
				else if (line.startsWith("vn ")) {
					float[] norm = new float[3];
					StringTokenizer tok = new StringTokenizer(line);
					tok.nextToken();
					norm[0] = Float.parseFloat(tok.nextToken());
					norm[1] = Float.parseFloat(tok.nextToken());
					norm[2] = Float.parseFloat(tok.nextToken());
					vertexData.normals.add(new Vector3f(norm));
					//fileNormal = true;
				}
			}
		}
		
		return vertexData;
	}
	
	private VertexData parseFailed(String msg) {
		if(l.isLevelLoggable(Log.ERROR))
			l.log(msg, Log.ERROR);
		
		return VertexData.create();
	}

	private static int parseInt(String val) {
		if (val.length() == 0) {
			return -1;
		}
		return Integer.parseInt(val);
	}

	private static int[] parseIntTriple(String face) {
		int ix = face.indexOf("/");
		if (ix == -1) 
		{
			return new int[] { Integer.parseInt(face) - 1 };
		}
		else 
		{
			int ix2 = face.indexOf("/", ix+1);
			if (ix2 == -1) 
			{
				return new int[]{ 	
									Integer.parseInt(face.substring(0,ix)) - 1,
									Integer.parseInt(face.substring(ix+1)) - 1 
								};
			}
			else 
			{
				return new int[]{	
									parseInt(face.substring(0,ix)) - 1,
									parseInt(face.substring(ix+1,ix2)) - 1,
									parseInt(face.substring(ix2+1)) - 1 
								};
			}
		}
	}

	public VertexData load(File f) throws IOException {
		return load(new FileInputStream(f));
	}

	public boolean canLoad(String f) {
		return canLoad(new File(f));
	}
}