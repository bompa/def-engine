package def.engine.graphics.components;

import def.engine.graphics.driver.VertexData;


public class ARenderableBuilder {
	private final RenderableMeshBuilder builder;
	private final IBuildAction buildAction;
	
	private static final class DefaultBuildAction extends ANonCachableBuildAction{

		public void build(IMeshComposer builder) { }
	}
	/**
	 * Create with vertex all buffers - position, normal and texture coords 
	 * @return
	 */
	public static ARenderableBuilder createWithAlllBuffers(){
		return new ARenderableBuilder(VertexData.UsedBuffersType.VERT_POSITION_NORMAL_TEXCOORDS, new DefaultBuildAction());
	}
	
	/**
	 * Create.. 
	 * @return
	 */
	public static ARenderableBuilder create(final VertexData.UsedBuffersType vData, final IBuildAction ba){
		return new ARenderableBuilder(vData, ba);
	}
	
	private ARenderableBuilder(final VertexData.UsedBuffersType data, final IBuildAction buildAction){
		if(buildAction == null)
			throw new IllegalArgumentException("ARenderableBuilder must have non-null IBuildAction");
		
		this.buildAction = buildAction;
		this.builder = new RenderableMeshBuilder(data);
	}
	
	public static Mesh buildAndGetMesh(ARenderableBuilder builder){
		builder.build();
		return builder.finishBuild();
	}
	
	/**
	 * Build using internal build customization supplied at constuction
	 */
	public void build(){
		buildAction.build(builder.getInternalMeshBuilder());
	}
	
	public Mesh finishBuild(){
		return builder.getInternalMeshBuilder().buildMesh();
	}
	
	public RenderableMeshBuilder getBuilderForCustomBuild(){
		return builder;
	}

	
	public static class RenderableMeshBuilder
	{
		private final IMeshBuilder meshBuilder;
		
		public static RenderableMeshBuilder create(VertexData.UsedBuffersType data){
			return new RenderableMeshBuilder(data);
		}
		
		private IMeshBuilder getInternalMeshBuilder() {
			return meshBuilder;
		}
		
		private RenderableMeshBuilder(VertexData.UsedBuffersType data){
			meshBuilder = MeshBuilder.create(data);
		}

		public IMeshComposer addVertex(Vertex v) {
			return meshBuilder.addVertex(v);
		}

		public IMeshComposer reuseVertex(Vertex v) {
			return meshBuilder.reuseVertex(v);
		}

		public boolean validateVertex(Vertex v) {
			return meshBuilder.validateVertex(v);
		}
		
	}

}
