package def.engine.graphics.components;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import def.engine.graphics.driver.VertexData;


public interface IModelLoader {
	
	/**
	 * Load a model from a file specified by a File object.
	 * @param f The File object specifying the file to read from.
	 * @return Raw vertexdata of the loaded model
	 * @throws IOException If an error occurs reading the file.
	 */
	public VertexData load(File f) throws IOException;
	
	/**
	 * Load a model from a stream of bytes.
	 * @param is The InputStream to load the model from.
	 * @return Raw vertexdata of the loaded model
	 * @throws IOException If an error occurs reading the file.
	 */
	public VertexData load(InputStream is) throws IOException;
	
	/**
	 * Determine if this loader can handle a particular file
	 * @param f The file to check.
	 * @return true if this loader can load an object from the file, false otherwise.
	 */
	public boolean canLoad(File f);

	/**
	 * Determine if this loader can handle a particular named file
	 * @param f The name of file to check.
	 * @return true if this loader can load an object from the file, false otherwise.
	 */
	public boolean canLoad(String f);
}
