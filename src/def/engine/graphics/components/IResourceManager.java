package def.engine.graphics.components;

import java.io.InputStream;

public interface IResourceManager<ResourceHandleT, ResourceIdT> {
	
	public boolean init(ResourceHandleT resourceHandle);
	public String readTextFileDefaultPath(final String fname);
	public InputStream openResourceFromID(final ResourceIdT id);
}
