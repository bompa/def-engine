package def.engine.graphics.components;

import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;

import def.shared.interfaces.INode;
import def.shared.math.Radian;
import def.shared.util.ITree;
import def.shared.util.RestrictInterfaceWrapper;

public class NodeImmutableWrapper extends RestrictInterfaceWrapper<INode> implements
		INode {
	public NodeImmutableWrapper(final INode wrapped){
		super(wrapped, "Illegal modify if immutable INode");
	}

	public void addChild(INode child) {
		handleUnsupported();
	}

	public Matrix4f getAllTransforms() {
		return getWrappedContent().getAllTransforms();
	}

	public Matrix4f getAllTransformsCopy() {
		return getWrappedContent().getAllTransformsCopy();
	}

	public String getName() {
		return getWrappedContent().getName();
	}

	public INode getParent() {
		return getWrappedContent().getParent();
	}

	public Vector3f getPosition() {
		return getWrappedContent().getPosition();
	}

	public float getScale() {
		return getWrappedContent().getScale();
	}

	public boolean isInheritOrientation() {
		return getWrappedContent().isInheritOrientation();
	}

	public boolean isInheritScale() {
		return getWrappedContent().isInheritScale();
	}

	public void notifyChildrenOfUpdate() {
		getWrappedContent().notifyChildrenOfUpdate();
	}

	public void setFullMatrixTransformationOverride(Matrix4f mtx) {
		handleUnsupported();	}

	public void setInheritOrientation(boolean inheritOrientation) {
		handleUnsupported();	}

	public void setInheritScale(boolean inheritScale) {
		handleUnsupported();
	}

	public void setParent(INode parent) {
		handleUnsupported();
	}

	public void setParentIsUpdated() {
		handleUnsupported();	}

	public void setPos(Vector3f pos) {
		handleUnsupported();	}

	public void setRotatation(Vector3f axis, Radian angle) {
		handleUnsupported();	
	}
	
	public void setRotatation(Matrix4f mtx) {
		handleUnsupported();
	}

	public void setScale(float scale) {
		handleUnsupported();
	}

	public void updatePos(Vector3f pos) {
		handleUnsupported();
	}

	public void updateRotation(Matrix4f mtx) {
		handleUnsupported();
	}

	public void updateRotation(Vector3f axis, Radian angle) {
		handleUnsupported();
	}

	public ITree<INode> getTree() {
		return getWrappedContent().getTree();
	}

	public void setFullMatrixTransformationOverride(float[] mtx) {
		handleUnsupported();
		
	}

	public void setRotatation(Matrix3f arg0) {
		// TODO Auto-generated method stub
		
	}

	public void updateRotation(Matrix3f arg0) {
		// TODO Auto-generated method stub
		
	}

}
