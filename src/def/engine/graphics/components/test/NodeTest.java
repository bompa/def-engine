/**
 * 
 */
package def.engine.graphics.components.test;

import def.engine.graphics.components.Node;
import def.shared.interfaces.INode;
import junit.framework.TestCase;

/**
 * @author jonas
 *
 */
public class NodeTest extends TestCase {

	private INode n;
	/**
	 * @param name
	 */
	public NodeTest(String name) {
		super(name);
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		n = new Node();
	}

	/* (non-Javadoc)
	 * @see junit.framework.TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	/**
	 * Test method for {@link def.engine.graphics.components.Node#Node()}.
	 */
	public void testNode() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link def.engine.graphics.components.Node#Node(javax.vecmath.Vector3f)}.
	 */
	public void testNodeVector3f() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link def.engine.graphics.components.Node#setParent(def.engine.graphics.components.Node)}.
	 */
	public void testSetParent() {
		INode n1 = new Node();
		n.setParent(n1);
		assertTrue(n1.getName().equals(n.getName()));
	}

	/**
	 * Test method for {@link def.engine.graphics.components.Node#addChild(def.engine.graphics.components.Node)}.
	 */
	public void testAddChild() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link def.engine.graphics.components.Node#updatePos(javax.vecmath.Vector3f)}.
	 */
	public void testUpdatePos() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link def.engine.graphics.components.Node#setPos(javax.vecmath.Vector3f)}.
	 */
	public void testSetPos() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link def.engine.graphics.components.Node#setFullMatrixTransformationOverride(javax.vecmath.Matrix4f)}.
	 */
	public void testSetFullMatrixTransformationOverride() {
		fail("Not yet implemented");
	}

}
