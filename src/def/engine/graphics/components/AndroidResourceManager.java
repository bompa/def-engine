package def.engine.graphics.components;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import def.shared.util.OutArg;
import def.util.LogHandler;
import def.util.Util;

import android.content.Context;
import android.content.res.Resources;
import android.os.Environment;
import android.util.Log;

public class AndroidResourceManager implements IResourceManager<Context, Integer>{
	
	private static final AndroidResourceManager instance = new AndroidResourceManager();
	private final LogHandler l = LogHandler.createInstanceShared();
	private Context context = null;
	
	public boolean init(Context resourceHandle) {
		context = resourceHandle; 
		return true;
	}
	
	private boolean checkInitState(){
		if(context == null){
			if(l.isLevelLoggable(Log.WARN))
				l.log("AndroidResourceManager has not been initalized, no resource can be fetched", Log.WARN);
			return false;
		}
		return true;
	}

	public static boolean checkExternalResourceAvailability(){
		return checkExternalResourceAvailability(Util.makeOutArg(Boolean.valueOf(false)), Util.makeOutArg(Boolean.valueOf(false)));
	}

	public static boolean checkExternalResourceAvailability(final OutArg<Boolean> outExternalStorageAvailable, 
			final OutArg<Boolean> outExternalStorageWriteable){
		boolean externalStorageAvailable = false;
		boolean externalStorageWriteable = false;
		String state = Environment.getExternalStorageState();

		if (Environment.MEDIA_MOUNTED.equals(state)) {
		    // We can read and write the media
		    externalStorageAvailable = externalStorageWriteable = true;
		} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
		    // We can only read the media
		    externalStorageAvailable = true;
		    externalStorageWriteable = false;
		} else {
		    // Something else is wrong. It may be one of many other states, but all we need
		    //  to know is we can neither read nor write
		    externalStorageAvailable = externalStorageWriteable = false;
		}
		outExternalStorageAvailable.value = externalStorageAvailable;
		outExternalStorageWriteable.value = externalStorageWriteable;
		return externalStorageAvailable && externalStorageWriteable;	
	}
	
	public InputStream openResourceFromID(final Integer id){
		if(checkInitState()){
			try{
				return context.getResources().openRawResource(id);
			}
			catch(Resources.NotFoundException e){
				l.log("Resource with id " + id + " not found!", Log.ERROR);
			}
		}
		return null;
	}
	
	public String readFileSearchPath(final String fileName){
		final File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
		final File file = new File(path, fileName);
		InputStreamReader inputStreamReader = null;
		try {
			inputStreamReader = new InputStreamReader(new FileInputStream(file));
		} catch (FileNotFoundException e1) {
			if(l.isLevelLoggable(Log.ERROR))
				l.log("Failed to read text file - File not found.", Log.ERROR);
		}
		final BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

		String nextLine;
		final StringBuilder body = new StringBuilder();

		try
		{
			while ((nextLine = bufferedReader.readLine()) != null)
			{
				body.append(nextLine);
				body.append('\n');
			}
		}
		catch (IOException e)
		{
			return "";
		}
		finally{
			try {
				bufferedReader.close();
			} catch (IOException e) {
				l.log("Failed to close buffered reader for file: " + file, Log.ERROR);
				e.printStackTrace();
			}
		}
		return body.toString();
	}
	
	public void writeFileToPublicPath(final String fileName, final InputStream inputStream){
		if(checkExternalResourceAvailability()){
			final File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
			final File file = new File(path, fileName);				
		
			try {
				file.createNewFile();
			} catch (IOException newfileE) {
				if(l.isLevelLoggable(Log.ERROR))
					l.log("Failed to write text file - Could not ceate file.", Log.ERROR);
			}
			
			OutputStream os = null;
			try {
				 os = new FileOutputStream(file);
			} catch (FileNotFoundException e) {
				if(l.isLevelLoggable(Log.ERROR))
					l.log("Failed to write text file - File not found.", Log.ERROR);
			}
	        
	        try {
	        	byte[] data = new byte[inputStream.available()];
				inputStream.read(data);
		        os.write(data);
		        inputStream.close();
		        os.close();
	        } catch (IOException e) {
	        	if(l.isLevelLoggable(Log.ERROR))
	        		l.log("Failed to write text file - IO error.", Log.ERROR);
			}
	        return;
			
		}
		if(l.isLevelLoggable(Log.WARN))
			l.log("Failed to write text file - external storage not available.", Log.WARN);
	}
	
	public static int InvalidResourceID = -1;
	
	public String readTextFileFromDefaultPathUpdateIfMissing(final String fname, int id){
		if(!checkInitState()){
			return "";
		}
		// Save file in public dir
		if( id != InvalidResourceID )
		{
			writeFileToPublicPath(fname, openResourceFromID(id));
		}

		return readFileSearchPath(fname);
	}
		
	public String readTextFileDefaultPath(final String fname){
		return readTextFileFromDefaultPathUpdateIfMissing(fname, InvalidResourceID);
	}

	public static AndroidResourceManager instance() {
		return instance;
	}
}
