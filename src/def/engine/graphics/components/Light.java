package def.engine.graphics.components;

import javax.vecmath.Vector4f;

import def.Const;
import def.engine.graphics.ARenderable;
import def.engine.graphics.IRenderable;
import def.engine.graphics.IRenderableVisitor;
import def.engine.graphics.LightRenderableData;
import def.engine.graphics.driver.IRenderableData;
import def.engine.graphics.driver.VertexData;
import def.engine.graphics.material.IMaterial;

public class Light extends ARenderable{
	
	enum Type{LIGHT_T_INVALID, LIGT_T_SPOT};
	
	private final int uniqueId;
	
	private IRenderable representation;
	private Properties properties = new Properties();
	
	public static Light create(int id, Properties props){
		Light l = new Light(id);
		l.setProperties(props);
		return l;
	}
	
	private Light(int id){
		uniqueId = id;
	}
	
	public static String makeVisualReprName(final String lightName){
		return lightName + Const.SHAPE_LIGHT_VISUAL_REPRESENTATION_NAME;
	}
	
	/**
	 * Set a renderable that represents the light visually
	 */
	public void setRepresentation(final IRenderable representation){
		this.representation = representation;
	}

	public IRenderable getRepresentation() {
		return representation;
	}
	
	@Override
	public IRenderableData getRenderInfoReadOnly(){
		if(hasRepresentation())
			return representation.getRenderInfoReadOnly();
		
		return super.getRenderInfoReadOnly();
	}
	
	@Override
	public IRenderableData getRenderInfo(){
		if(hasRepresentation())
			return representation.getRenderInfo();
		
		return super.getRenderInfo();
	}

	@Override
	protected IRenderableData createRenderInfo(Mesh ignore) {
		return new LightRenderableData();
	}
	
	@Override
	protected void initSharedRenderInfo(final IRenderableData rd){ }
	
	public int getId(){
		return uniqueId;
	}
	
	/**
	 * Override of ARenderable's impl. of IRenderable interface definition.
	 */
	@Override
	public void acceptRendering(IRenderableVisitor renderVisitor) {
		renderVisitor.renderVisit(this);
	}
	
	@Override
	public int getRenderPrio(){
		return Const.REND_PRIO_LIGHT;
	}
	
	@Override
	public IMaterial getMaterial(){
		if(hasRepresentation())
			return representation.getMaterial();
		return null;
	}

	private boolean hasRepresentation() {
		return representation != null;
	}
	
	@Override
	public void setContiniousRendering(boolean value) {
		if(hasRepresentation())
			getRepresentation().setContiniousRendering(value);
		super.setContiniousRendering(value);
	}
	
	public IBuildAction getIBuildAction() {
		return buildAction;
	}
	
	public boolean requiresPreparationOfVertexBuffersForRendering() {
		return false;
	}

	/**
	 * Only prepare material if light source has visual representation
	 */
	public boolean requiresPreparationOfMaterialForRendering() {
		return hasRepresentation();
	}
	
	public Properties getProperties() {
		return properties;
	}
	public void setProperties(Light.Properties properties) {
		this.properties = properties;
	}
	
	public VertexData.UsedBuffersType getVertexDataType(){ return VertexData.UsedBuffersType.VERT_POSITION_NORMAL_TEXCOORDS; }

	public static class Properties{
		private final Vector4f ambient = new Vector4f();
		private final Vector4f diffuse = new Vector4f();
		private final Vector4f specular = new Vector4f();
		
		public static Properties create(){
			return new Properties();
		}
		
		private Properties(){ }

		public void setAmbient(Vector4f ambient) {
			this.ambient.set(ambient);
		}
		public Vector4f getAmbient() {
			return new Vector4f(ambient);
		}

		public void setDiffuse(Vector4f diffuse) {
			this.diffuse.set(diffuse);
		}
		public Vector4f getDiffuse() {
			return new Vector4f(diffuse);
		}

		public void setSpecular(Vector4f specular_) {
			this.specular.set(specular_);
		}
		public Vector4f getSpecular() {
			return new Vector4f(specular);
		}
	}
}
