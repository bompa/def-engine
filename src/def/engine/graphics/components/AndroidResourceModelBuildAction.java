package def.engine.graphics.components;

import java.io.IOException;

import android.util.Log;

import def.util.LogHandler;
import def.engine.graphics.components.ObjLoader;
import def.engine.graphics.driver.VertexData;


public class AndroidResourceModelBuildAction implements IBuildAction {
	final LogHandler l = LogHandler.createInstanceShared(AndroidResourceModelBuildAction.class);
	final int id;
	final int uniqueID;
	
	public static final String ResourceModelString = "ResourceModelID";
	
	public static AndroidResourceModelBuildAction create(int id){
		return new AndroidResourceModelBuildAction(id);
	}
	
	private AndroidResourceModelBuildAction(int id){
		this.id = id;
		String hashString = ResourceModelString + id;
		uniqueID = hashString.hashCode();
	}

	public void build(IMeshComposer buildComposer) {
		final IModelLoader loader = ObjLoader.create();
		VertexData vd = null;
		try {
			vd = loader.load(AndroidResourceManager.instance().openResourceFromID(id));
		} catch (IOException e) {
			if(l.isLevelLoggable(Log.ERROR))
				l.log("Fail to load model with id " +id, Log.ERROR);
		}
		
		if(vd.faces.size() > 0){
			buildComposer.readVertexData(vd);
			//buildComposer.calculateTBNVectors();
		}
		else{
			if(l.isLevelLoggable(Log.ERROR))
				l.log("Found no faces when loading model, can not build mesh: " + id, Log.ERROR);
		}
	}
	
	public static String modelNameFromAndoridID(int id){
		return String.valueOf(id);
	}

	public int getUniqueID() {
		return uniqueID;
	}

	public void setUniqueID(int ignore) { }
}
