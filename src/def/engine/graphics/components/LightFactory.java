package def.engine.graphics.components;

import java.util.concurrent.atomic.AtomicInteger;

import javax.vecmath.Vector4f;

import def.Const;
import def.engine.graphics.ARenderableFactory;
import def.engine.graphics.material.IMaterial;
import def.engine.graphics.material.ITexture;
import def.engine.graphics.resources.ResourceManager;
import def.shared.interfaces.ITransform;
import def.util.Util.NameGenerator;

public class LightFactory extends ARenderableFactory<Light>{
	
	public static LightFactory create(ResourceManager rm) {
		return new LightFactory(rm);
	}
	
	private LightFactory(ResourceManager rm) {
		super(rm);
	}

	static final AtomicInteger lightIdCount = new AtomicInteger(0);

	private static Light.Properties createDefaultLightProps(){
		Light.Properties lprops = Light.Properties.create();
		lprops.setAmbient(new Vector4f(0.0f, 0.0f, 0.0f, 1.0f));
		lprops.setDiffuse(new Vector4f(0.9f, 0.9f, 0.9f, 1.0f));
		lprops.setSpecular(new Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
		return lprops;
	}

	@Override
	protected Light createConcreteRenderable(	String referenceName, 
												String modelName, 
												ITransform positionableImpl,
												IMaterial m,
												ITexture tex) {
		int id = lightIdCount.incrementAndGet();
		if(modelName == Const.SHAPE_LIGHT1){
			Light light = Light.create(id, createDefaultLightProps());
			light.setName(NameGenerator.instance().generateName(Const.SHAPE_LIGHT1));
			return light;
		}
		return null;
	}
}
