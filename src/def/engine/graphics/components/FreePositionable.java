package def.engine.graphics.components;

import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;

import android.util.Log;

import def.engine.logic.math.MatrixArray3f;
import def.engine.logic.math.VectorArray3f;
import def.shared.interfaces.INode;
import def.shared.interfaces.ITransform;
import def.shared.math.Radian;
import def.util.LogHandler;
import def.util.Util;

public class FreePositionable implements ITransform{
	private final INode node;
	private final LogHandler l = LogHandler.createInstanceShared();

	public FreePositionable(){
		this.node = new Node(); 
	}
	
	FreePositionable(INode node){
		this.node = node;
	}

	public void move(Vector3f transl) {
		this.node.updatePos(transl);
	}
	
	public void setPosition(Vector3f pos) {
		this.node.setPos(pos);
	}
	
	public void setPosition(float[] pos) {
		if(pos !=null && pos.length == VectorArray3f.VEC_SZ){
			this.node.setPos(new Vector3f(pos));
		}
	}

	public void rotate(Vector3f axis, Radian angle) {
		this.node.updateRotation(axis, angle);
	}
	public void rotate(Matrix3f mtx) {
		this.node.updateRotation(mtx);
	}
	
	public void setRotation(float[] mtx){
		if(mtx != null){
			if(mtx.length == MatrixArray3f.THREE_BY_THREE_MTX_SZ ){
				Matrix3f m = new Matrix3f(mtx);
				this.node.setRotatation(m);
			}
			else{
				l.log("FreePositionable::setRotation - rotation value ignored, matrix size invalid(" + mtx.length + ")", Log.WARN);
			}
		}
	}
	
	public void setRotation(Matrix3f mtx) {
		this.node.setRotatation(mtx);
	}
	
	public Matrix4f getModelMatrixCopy() {
		return this.node.getAllTransformsCopy();
	}

	public Matrix4f getModelMatrix() {
		return this.node.getAllTransforms();
	}

	public Vector3f getPosition() {
		return this.node.getPosition();
	}

	public float getScale() {
		return this.node.getScale();
	}

	public void setScale(float scale) {
		this.node.setScale(scale);		
	}

	public void setFullMatrixTransformations(final Matrix4f mtx){
		this.node.setFullMatrixTransformationOverride(mtx);
	}
	
	public void setFullMatrixTransformations(float[] mtx) {
		this.node.setFullMatrixTransformationOverride(mtx);
	}
	
	public INode getNodeReadOnly(){
		return Util.unmodifiableNode(node);
	}
	
	public INode getNode() {
		return node;
	}
	
	public void setSceneParent(final ITransform parent){
		node.setParent(parent.getNode());
	}	
	
	public void addSceneChild(final ITransform child){
		node.addChild(child.getNode());
	}
	
	public String toString(){
		return node.getName() + ": { " + node.getPosition().x + ", " + node.getPosition().y + ", "+ node.getPosition().z + "}";
	}
}
