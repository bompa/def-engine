package def.engine.graphics.components;



import java.util.Collection;

import javax.vecmath.AxisAngle4f;
import javax.vecmath.Matrix3f;
import javax.vecmath.Matrix4f;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

import def.engine.logic.StateUpdateTracker;
import def.engine.logic.math.LinjearAlg;
import def.shared.interfaces.INode;
import def.shared.math.Radian;
import def.shared.util.ITree;
import def.shared.util.Tree;
import def.util.Util.NameGenerator;

public class Node extends StateUpdateTracker<Matrix4f> implements INode{
	private final static String NODE_BASE_NAME = "Node";
	
	/**
	 * Cached result of world matrix with applied scale, rotation and transform, in that order.
	 * This determines how the object is rendered in the world.
	 */
	private final Matrix4f cachedMtxTransformations = new Matrix4f(LinjearAlg.M4_IDENTIY);
	// Local rotation, position and scale
	private final Matrix3f rotationMtx;
	private final Vector3f pos;
	private float scale = 1.0f;
	
	// Inherited rotation(orientation), position and scale
	//private final Quat4f inheritedOrientation = new Quat4f();
	private final Matrix3f inheritedOrientation = new Matrix3f();
	private final Vector3f inheritedPos = new Vector3f();
	private float inheritedScale = 1.0f;
	
	private boolean inheritScale = false;
	private boolean inheritOrientation = false;
	
	private final String name;
	private boolean parentUpdated = false;
	private final ITree<INode> nodeTreeEntry = new Tree<INode>(this);
	/***************************************************
	 * Ctors & init
	 ***************************************************/
	public Node(){
		this.pos = new Vector3f();
		this.rotationMtx =  new Matrix3f(LinjearAlg.M3_IDENTIY);
		this.name = NameGenerator.instance().generateName(NODE_BASE_NAME);
		resetInheritOrientation();
		stateUpdated();
	}
	
	public Node(final Vector3f pos){
		if(pos == null)
			throw new IllegalArgumentException("Null position not allowed in Node");
		this.pos = pos;
		this.rotationMtx =  new Matrix3f(LinjearAlg.M3_IDENTIY);
		this.name = NameGenerator.instance().generateName(NODE_BASE_NAME);
		resetInheritOrientation();
		stateUpdated();
	}
	
	private void resetInheritOrientation() {
		inheritedOrientation.set(rotationMtx);
	    inheritedPos.set(pos);
	    inheritedScale = scale;
	}
	
	/***************************************************
	 * Scene graph parent/child operations
	 ***************************************************/
	public void setParent(final INode parent){
		nodeTreeEntry.setAsParent(parent);
	}
	
	public INode getParent(){
		return nodeTreeEntry.getParent().getTreeNode();
	}

	public void addChild(final INode child){
		nodeTreeEntry.addLeaf(child.getTree());
		notifyChildrenOfUpdate();
	}
	
	public ITree<INode> getTree(){
		return nodeTreeEntry;
	}
	
	public void notifyChildrenOfUpdate(){
		final Collection<? extends ITree<INode>> childs = nodeTreeEntry.getSubTrees();
		for(final ITree<INode> tree : childs){
			tree.getTreeNode().setParentIsUpdated();
		}
	}
	
	public void setParentIsUpdated() {
		this.parentUpdated = true;
		stateUpdated();
	}
	
	private void handleParentUpdated(){
		final ITree<INode> parentTree = nodeTreeEntry.getParent();
		if(parentTree != null){
			final Node parent = (Node)parentTree.getTreeNode(); 
	
			setCalculatedInheritedOrientation(parent);
			setCalculatedInheritedScale(parent);
			setCalculatedInheritedPosition(parent);			
		}
		
		this.parentUpdated = false;
	}

	private void setCalculatedInheritedPosition(Node parent) {
		if(inheritOrientation){
			final Matrix3f tmpParent = new Matrix3f(parent.rotationMtx);
			tmpParent.mul(rotationMtx);
			inheritedOrientation.set(tmpParent);
		}
		else{
			inheritedOrientation.set(rotationMtx);
		}
	}

	private void setCalculatedInheritedScale(Node parent) {
		if(inheritScale){
			inheritedScale *= parent.scale;
		}
		else{
			inheritedScale = scale;
		}
	}

	private void setCalculatedInheritedOrientation(Node parent) {
		Matrix3f rm = new Matrix3f(parent.rotationMtx);
		Point3f out = new Point3f();
		rm.transform(new Point3f(pos), out);
		inheritedPos.set(out);
		inheritedPos.add(inheritedPos, parent.inheritedPos);
	}

	private boolean isParentUpdated() {
		return parentUpdated;
	}

	/***************************************************
	 * Rotaion, postion and scale update/set operations. Updates changed state.
	 ***************************************************/
	public void updatePos(final Vector3f pos) {
		this.pos.add(pos);
		stateUpdated();
	}

	public void setPos(final Vector3f pos) {
		this.pos.set(pos);
		stateUpdated();
	}

	public Vector3f getPosition(){
		return pos;
	}
	
	public void updateRotation(Matrix3f mtx){
		rotationMtx.mul(mtx);
		stateUpdated();
	}
	
	public void setRotatation(Matrix3f mtx) {
		rotationMtx.setIdentity();
		rotationMtx.set(mtx);
		stateUpdated();		
	}
	
	public void updateRotation(Vector3f axis, Radian angle){
		final Matrix4f rotUpdateMtx = new Matrix4f();
		rotUpdateMtx.setIdentity();
		rotUpdateMtx.setRotation(new AxisAngle4f(axis, angle.value));

		final Matrix3f rot3x3 = new Matrix3f();
		rotUpdateMtx.getRotationScale(rot3x3); 
		updateRotation(rot3x3);
	}
		
	public void setFullMatrixTransformationOverride(final Matrix4f mtx){	
		updateCache(mtx);
		updateNodeValuesFromMatrix(mtx);
		stateUpdateHandled();
	}
	
	public void setFullMatrixTransformationOverride(float[] mtx) {
		final Matrix4f tmp = new Matrix4f(mtx);
		setFullMatrixTransformationOverride(tmp);		
	}
	
	private void updateNodeValuesFromMatrix(Matrix4f mtx) {
		final Vector3f posFromMtx = LinjearAlg.getTranslationFromMatrix(mtx); 
		pos.set(posFromMtx);
	}

	public float getScale(){
		return scale;
	}
	
	public void setScale(float scale){
		this.scale = scale;
		stateUpdated();
	}
	
	@Override
	public Matrix4f callIfNotUpdated() {
		return cachedMtxTransformations;
	}
	
	@Override
	public Matrix4f callIfUpdated() {
		updateInheritedValues();
		updateCache();
		return cachedMtxTransformations;
	}
	
	private void updateInheritedValues() {
		if(isParentUpdated()){
			handleParentUpdated();
		}
		else if(isRoot()){
			resetInheritOrientation();
		}
	}

	private boolean isRoot() {
		return nodeTreeEntry.getParent() == null;
	}

	@Override
	public void callAtStateUpdate(){
		notifyChildrenOfUpdate();
	}
	
	private void updateCache(){
		cachedMtxTransformations.set(inheritedOrientation, inheritedPos, inheritedScale);
	}
	private void updateCache(final Matrix4f updatedMtx){
		cachedMtxTransformations.set(updatedMtx);
	}
	
	public Matrix4f getAllTransformsCopy(){
		return new Matrix4f(updateCheckCall());
	}
	
	public Matrix4f getAllTransforms(){
		return updateCheckCall();
	}

	public String getName() {
		return name;
	}

	public void setInheritOrientation(boolean inheritOrientation) {
		this.inheritOrientation = inheritOrientation;
	}

	public boolean isInheritOrientation() {
		return inheritOrientation;
	}

	public void setInheritScale(boolean inheritScale) {
		this.inheritScale = inheritScale;
	}

	public boolean isInheritScale() {
		return inheritScale;
	}
}
