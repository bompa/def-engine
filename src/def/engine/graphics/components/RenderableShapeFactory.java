package def.engine.graphics.components;


import javax.vecmath.Vector4f;

import def.Const;
import def.engine.graphics.ARenderable;
import def.engine.graphics.ARenderableFactory;
import def.engine.graphics.material.IMaterial;
import def.engine.graphics.material.ITexture;
import def.engine.graphics.material.Texture;
import def.engine.graphics.resources.ResourceManager;
import def.shared.interfaces.ITransform;
import def.ui.R;
import def.util.Util.NameGenerator;

public class RenderableShapeFactory extends ARenderableFactory<ARenderable>{
	
	public static RenderableShapeFactory create(ResourceManager rm) {
		return new RenderableShapeFactory(rm);
	}
	
	private RenderableShapeFactory(ResourceManager rm) {
		super(rm);
	}
	private ARenderable determineParamsAndConstructSquare(	final ITransform posImpl,
															final IMaterial m,
															final ITexture t){
		if( posImpl == null &&  m != null)
			return new Square(m);
		if( posImpl == null &&  t != null )
			return new Square(t);
		if(posImpl != null &&   m != null)
			return new Square(m, posImpl);			

		return null;
	}
	private ARenderable determineParamsAndConstructCube(ITransform posImpl, ITexture tex){
		return (posImpl == null)? new Cube(tex): new Cube(tex, posImpl);
	}
	private ARenderable determineParamsAndConstructBillBoard(ITransform posImpl, final ITexture texture){
		return (posImpl == null)? new BillBoard(texture): new BillBoard(texture, posImpl);
	}
	private ARenderable determineParamsAndConstructLines(ITransform posImpl, final Vector4f color){
		return new Lines(color);
	}
	private ARenderable determineParamsAndConstructModel(ITransform posImpl, final ITexture texture){
		return (posImpl == null)? new Model(texture): new Model(texture, posImpl);
	}
	
	private String updateRefName(final String current, final String base){
		if(current == null)
			return NameGenerator.instance().generateName(base);
		return current;
	}

	@Override
	protected ARenderable createConcreteRenderable(	String referenceName,
													String modelName, 
													ITransform positionableImpl, 
													IMaterial m,
													ITexture tex) 
	{
		ARenderable r = null;
		
		if(modelName == Const.SHAPE_SQUARE){
			r = determineParamsAndConstructSquare(positionableImpl, m, tex);
		}
		else if(modelName == Const.SHAPE_EMITTING_SQUARE){
			ARenderable tmp = determineParamsAndConstructSquare(positionableImpl, null, Texture.create(R.drawable.tile));
			final IMaterial material = tmp.getMaterial();
			material.setProperties(IMaterial.Properties.createSelfEmitting((Light.Properties.create())));
			r = tmp;
		}
		else if(modelName == Const.SHAPE_EMITTING_CUBE){
			ARenderable tmp = determineParamsAndConstructCube(positionableImpl, Texture.create(R.drawable.shine));
			final IMaterial material = tmp.getMaterial();
			material.setProperties(IMaterial.Properties.createSelfEmitting((Light.Properties.create())));
			r = tmp;
		}
		else if(modelName == Const.SHAPE_CUBE){
			r = determineParamsAndConstructCube(positionableImpl, Texture.create(R.drawable.tile));
		}
		else if(modelName == Const.SHAPE_LINES){
			r = determineParamsAndConstructLines(positionableImpl, new Vector4f());
		}
		else if(modelName == Const.SHAPE_BB){
			r = determineParamsAndConstructBillBoard(positionableImpl, tex);
		}
		else if(modelName == Const.SHAPE_MODEL){
			r = determineParamsAndConstructModel(positionableImpl, tex);
		}
		else if(modelName == Const.SHAPE_SKYBOX){
			r = new SkyBox();
		}
		else{
			throw new RuntimeException("Attempted to construct Unsupported modeltype in RenderableShapeFactory!");
		}
		
		referenceName = updateRefName(referenceName, modelName);
		
		if(referenceName == null){
			referenceName = "no-name";
		}
		r.setName(referenceName);
		return r;
	}
}
