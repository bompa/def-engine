package def.engine.graphics.components;

import javax.vecmath.Vector4f;

import def.engine.graphics.ARenderable;
import def.engine.graphics.RenderableData;
import def.engine.graphics.driver.IRenderableData;
import def.engine.graphics.driver.VertexData;
import def.engine.graphics.material.IMaterial;
import def.engine.graphics.material.Materials;

public class Lines extends ARenderable{
	
	private IMaterial material;

	public Lines(final Vector4f color) {	
		material = Materials.createWithBasicColorShaders(color);
	}
	
	protected void createMesh(IMeshComposer builder){
		final Vertex first = Vertex.create(-1.0f,  1.0f, 0.0f);
		final Vertex second = Vertex.create(-1.0f, -1.0f, 0.0f);
		
		builder.addVertex(first).addVertex(second);
	}
	
	@Override
	protected void initSharedRenderInfo(final IRenderableData rd){
		initRenderableDataShaders(rd);
	}

	@Override
	protected IRenderableData createRenderInfo(Mesh mesh) {
		final IRenderableData rendInfo = mesh.createRenderableData(
				RenderableData.RenderOperation.RO_LINES, RenderableData.RenderDirection.RD_CCW);

		initRenderableDataShaders(rendInfo);
		return rendInfo;
	}

	private void initRenderableDataShaders(final IRenderableData rendInfo) {
		rendInfo.setFragmentShader(material.getFragmentShader());
		rendInfo.setVertexShader(material.getVertexShader());
	}

	@Override
	public IMaterial getMaterialReadOnly() {
		return Materials.unmodifiableMaterial(material);
	}
	
	@Override
	public void setMaterial(IMaterial mat) {
		material = mat;
	}

	@Override
	public IMaterial getMaterial() {
		return material;
	}

	public boolean requiresPreparationOfMaterialForRendering() {
		return true;
	}

	public VertexData.UsedBuffersType getVertexDataType() {
		return VertexData.UsedBuffersType.VERT_POSITION_COLOR;
	}
}
