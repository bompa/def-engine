package def.engine.graphics.components;

import def.engine.graphics.driver.VertexData;

public interface IMeshComposer {

	/**
	 *  Validates that the veretx has the right data set
	 * @param v
	 * @return true if the vertex is valid
	 */
	public boolean validateVertex(Vertex v);

	/**
	 * Adds vertex if it qualifies to the builder settings.
	 * @param v
	 * @return this instance, for method chaining.
	 */
	public IMeshComposer addVertex(Vertex v);

	/**
	 * Use if a previouslt added vertex can be reused. Its index will be extracted.
	 * @param v
	 * @return  this instance, for method chaining.
	 */
	public IMeshComposer reuseVertex(Vertex v);
	
	/**
	 * Read vertex data to to create all vertices information  
	 * @param vd
	 * @return
	 */
	public IMeshComposer readVertexData(VertexData vd);
	
	/**
	 * Updates the state of the composer to read the coming indices as a new component,
	 * which can share buffers with the rest of the mesh but can be drawn separately.
	 * Requires that previous vertices has been added so that a new component can be created with the
	 * following vertices.
	 * @return
	 * @throws IllegalStateException if no previous vertexes has been added to the composer
	 */
	public IMeshComposer newComponent();
	
	/**
	 * Calculate tangent, binormals and normals when all trinangles has been added
	 */
	public void calculateTBNVectors();

}