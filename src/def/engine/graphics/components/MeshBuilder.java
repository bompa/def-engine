package def.engine.graphics.components;

import java.util.ArrayList;
import java.util.HashMap;

import javax.vecmath.Vector2f;
import javax.vecmath.Vector3f;

import android.util.Log;

import def.engine.graphics.driver.VertexData;
import def.engine.graphics.driver.VertexData.FaceTriangle;
import def.engine.graphics.driver.VertexData.Indices;
import def.util.IntPair;
import def.util.LogHandler;

public class MeshBuilder implements IMeshBuilder {
	private final LogHandler l = LogHandler.createInstanceShared();
	private static final int INITIAL_CAP = 3;
	
	private final ArrayList<Vertex> vertices;
	private final ArrayList<Short> indices;
	/**
	 * Start-end indices of sub-meshes, which can be drawn individually using the indices buffer
	 * Constraints: must contain at least on pair
	 */
	private final ArrayList<IntPair> componetIndices;
	private int lastStartComponentIdx = 0;
	
	private final VertexData.UsedBuffersType vertexDataType;
	
	public Mesh buildMesh(){
		if(lastStartComponentIdx == getLastIndicesIndex()){
			if(l.isLevelLoggable(Log.WARN))
				l.log("buildMesh: No new vertices added after construction or newComponent call", Log.WARN);
		}
		else{
			componetIndices.add(new IntPair(lastStartComponentIdx, getLastIndicesIndex()));
		}

		Mesh triangleMesh = Mesh.create(vertexDataType, vertices, indices, componetIndices);
		return triangleMesh;
	}

	private int getLastIndicesIndex() {
		if(indices.size() > 0)
			return indices.size() - 1;
		
		return 0;
	}
	
	public static IMeshBuilder create(VertexData.UsedBuffersType type){
		return new MeshBuilder(type);
	}
	
	private MeshBuilder(VertexData.UsedBuffersType type){
		vertexDataType = type;
		vertices = new ArrayList<Vertex>(INITIAL_CAP);
		indices = new ArrayList<Short>(INITIAL_CAP*2);
		componetIndices = new ArrayList<IntPair>();
	}
	
	public boolean validateVertex(Vertex v){
		switch(vertexDataType){
			case VERT_POSITION:
				return (v.getVertexNormal() == null) && (v.getVertexTexCoords() == null);
			case VERT_POSITION_COLOR:
				return (v.getVertexColor() != null) && (v.getVertexNormal() == null) && (v.getVertexTexCoords() == null);
			case VERT_POSITION_NORMAL:
				return (v.getVertexNormal() != null) && (v.getVertexTexCoords() == null);
			case VERT_POSITION_TEXCOORD:
				return (v.getVertexNormal() == null) && (v.getVertexTexCoords() != null);
			case VERT_POSITION_NORMAL_TEXCOORDS:
				return (v.getVertexNormal() != null) && (v.getVertexTexCoords() != null);
		}
		return false;
	}
	
	/**
	 * Adds vertex if it qualifies to the builder settings.
	 * @param v
	 * @return this instance, for method chaining.
	 * @see IMeshComposer
	 */
	public IMeshComposer addVertex(Vertex v){
		if(!validateVertex(v)){
			if(l.isLevelLoggable(Log.WARN))
				l.log("Deiscarding vertex since it misses data", Log.WARN);
			return this;
		}
		v.setIndex((short)vertices.size());
		indices.add(v.getIndex());
		vertices.add(v);
		return this;
	}
	
	/**
	 * Use if a previouslt added vertex can be reused. Its index will be extracted.
	 * @param v
	 * @return  this instance, for method chaining.
	 * @see IMeshComposer
	 */
	public IMeshComposer reuseVertex(Vertex v){
		final short index = v.getIndex();
		if(index == -1){
			throw new RuntimeException("Can not reuse vertex with illegal index");
		}
		indices.add(v.getIndex());
		return this;
	}

	public IMeshComposer readVertexData(VertexData vertexdata) {
		final HashMap<VertexData.Indices, Vertex> indexVertexMap = new HashMap<VertexData.Indices, Vertex>();
		
		for(FaceTriangle f : vertexdata.faces){
			addVertex(indexVertexMap, vertexdata, f.first);
			addVertex(indexVertexMap, vertexdata, f.second);
			addVertex(indexVertexMap, vertexdata, f.third);
		}
		return this;
	}

	private Vertex addVertex(final HashMap<VertexData.Indices, Vertex> indexVertexMap, VertexData vd, Indices indices) {
		Vertex vertex = indexVertexMap.get(indices);
		if(vertex == null){
			Vector3f vec = vd.positions.get(indices.v);
			vertex = Vertex.create(vec.x, vec.y, vec.z);
			if(vd.hasNormals()){
				Vector3f normal = vd.normals.get(indices.n);
				vertex.normal(normal.x, normal.y, normal.z);
			}
			if(vd.hasTexcoords()){
				Vector2f tex = vd.textureCoords.get(indices.t);
				vertex.texCoords(tex.x, tex.y);
			}
			
			addVertex(vertex);
			indexVertexMap.put(indices, vertex);
			return vertex;
		}
		
		reuseVertex(vertex);
		return vertex;
	}

	public IMeshComposer newComponent() {
		if(indices.size() <= 0){
			throw new IllegalStateException("Can not start a new mesh component with no previous vertex data");
		}
		
		final int lastIndex = getLastIndicesIndex();
		this.componetIndices.add(new IntPair(lastStartComponentIdx, lastIndex));
		this.lastStartComponentIdx = lastIndex;
		
		return this;
	}
	
	public void calculateTBNVectors(){
		if(indices.size() == 0){
			if( l.isLevelLoggable(Log.WARN) )
				l.log("No vertex data or indices. Can not calculate TBN vectors!");
			return;
		}
		
		if(indices.size() % TriangleFaceSize != 0){
			if( l.isLevelLoggable(Log.WARN) )
				l.log("Vertex count is not divisable by " + TriangleFaceSize + ". Can not calculate TBN vectors!");
			return;
		}
		
		for( int i = 0; i < indices.size(); i+=3 ){
			calculateTBNVectors(i);
		}
	}
	
	void calculateTBNVectors(final int indexOffset){
		final int maxOffset = indices.size() - TriangleFaceSize;
		if(indexOffset > maxOffset)
		{
			if( l.isLevelLoggable(Log.ERROR) )
				l.log("Incorrect indexoffset of index list: offset = " + indexOffset + " must be < " + maxOffset);
			
			return;
		}
		
		 //Compute TBN vectors
	    Vector3f tangent = new Vector3f();
	    Vector3f binormal = new Vector3f();

	    final Vector3f normal = new Vector3f(vertices.get(indexOffset).getVertexNormal());	//.normals[face.normalId[0]];
	    computeFaceTBNBasis(/*mesh,*/ indexOffset, tangent, binormal, null);
	    //Vector3f normal = new Vector3f();
	    //computeFaceTBNBasis(mesh, face, tangent, binormal, normal );

	    //Gram-Schmidt orthogonalization
	    //tangent.subtract(multiply(normal, dotProduct(normal, tangent))).normalize();
	    final Vector3f gsNormal = new Vector3f(normal);
	    final float normTangDot = gsNormal.dot(tangent);
	    gsNormal.scale(normTangDot);
	    tangent.sub(gsNormal);
	    tangent.normalize();

	    //Right handed TBN space ?
	    // boolean rigthHanded = dotProduct(crossProduct(tangent, binormal), normal) >= 0;
	    //binormal = crossProduct(normal, tangent);
	    //if(!rigthHanded)
	      //  binormal.multiply(-1);
	    
	    final Vector3f tangBinormalCross = new Vector3f(); tangBinormalCross.cross(tangent, binormal);
	    boolean rigthHanded = tangBinormalCross.dot(normal) >= 0;
	    binormal.cross(normal, tangent);
	    if(!rigthHanded)
	        binormal.scale(-1);
	    
	    // Set set same tanget/binormal for all face vertices
		final short faceV0Idx = indices.get(indexOffset);
		final short faceV1Idx = indices.get(indexOffset + 1);
		final short faceV2Idx = indices.get(indexOffset + 2);
		
	    vertices.get(faceV0Idx).tangent(tangent.x, tangent.y, tangent.z);
	    vertices.get(faceV0Idx).binormal(binormal.x, binormal.y, binormal.z);
	    
	    vertices.get(faceV1Idx).tangent(tangent.x, tangent.y, tangent.z);
	    vertices.get(faceV1Idx).binormal(binormal.x, binormal.y, binormal.z);
	    
	    vertices.get(faceV2Idx).tangent(tangent.x, tangent.y, tangent.z);
	    vertices.get(faceV2Idx).binormal(binormal.x, binormal.y, binormal.z);
	    
	}

	static final int TriangleFaceSize = 3;
	public void computeFaceTBNBasis(final int indexOffset , Vector3f tangent, Vector3f binormal, Vector3f normal)
	{ 
		final short faceV0Idx = indices.get(indexOffset);
		final short faceV1Idx = indices.get(indexOffset + 1);
		final short faceV2Idx = indices.get(indexOffset + 2);
		
		final Vector3f p21 = new Vector3f(); 
		p21.sub(getVertexPosFromIdx(faceV1Idx), getVertexPosFromIdx(faceV0Idx));
		final Vector3f p31 = new Vector3f();
		p31.sub(getVertexPosFromIdx(faceV2Idx), getVertexPosFromIdx(faceV0Idx));
		
		final Vector2f uv21 = new Vector2f();
		uv21.sub(getVertexTexuvFromIdx(faceV1Idx), getVertexTexuvFromIdx(faceV0Idx));
		final Vector2f uv31 = new Vector2f();
		uv31.sub(getVertexTexuvFromIdx(faceV2Idx), getVertexTexuvFromIdx(faceV0Idx));
		
		float f = uv21.x * uv31.y - uv21.y * uv31.x;
	    f = (f == 0) ? 1 : 1 / f;

	    if(tangent != null || normal != null){
	    	final Vector3f tangentCalcLhs = new Vector3f(p21);
	    	tangentCalcLhs.scale(uv31.y);
	    	final Vector3f tangentCalcRhs = new Vector3f(p31);
	    	tangentCalcRhs.scale(uv21.y);
	    	tangentCalcLhs.sub(tangentCalcRhs);
	    	tangentCalcLhs.scale(f);
	    	tangentCalcLhs.normalize();
	    	tangent.set(tangentCalcLhs);
	    }
	        
	    if(binormal != null || normal != null){
	    	final Vector3f binormalCalcLhs = new Vector3f(p31);
	    	binormalCalcLhs.scale(uv21.x);
	    	final Vector3f binormalCalcRhs = new Vector3f(p21);
	    	binormalCalcRhs.scale(uv31.x);
	    	binormalCalcLhs.sub(binormalCalcRhs);
	    	binormalCalcLhs.scale(f);
	    	binormalCalcLhs.normalize();
	    	binormal.set(binormalCalcLhs);
	    }
	    if(normal != null){
	    	normal.cross(tangent, binormal);
	    	normal.normalize();
	    }
	}

	private Vector2f getVertexTexuvFromIdx(short index) {
		return new Vector2f(vertices.get(index).getVertexTexCoords());
	}

	private Vector3f getVertexPosFromIdx(short index) {
		return new Vector3f(vertices.get(index).getVertexPos());
	}
}
