package def.engine.graphics.components;

import def.engine.graphics.ARenderable;
import def.engine.graphics.RenderableData;
import def.engine.graphics.driver.IRenderableData;
import def.engine.graphics.driver.VertexData;
import def.engine.graphics.material.IMaterial;
import def.engine.graphics.material.ITexture;
import def.engine.graphics.material.Materials;
import def.shared.interfaces.ITransform;
import def.util.FloatPair;
import def.util.Util;

public class Square extends ARenderable{
	
	// The material of this square
	private IMaterial material;

	public Square(final int textureResource, final ITransform posImpl){
		super(posImpl);
		material = Materials.createWithPerFragmentLightningShaders(textureResource);	
	}
	
	public Square(final ITexture tex) {	
		if(tex == null)
			throw Util.buildIllegalArgException(Square.class, "texture");
		
		material = Materials.createWithPerFragmentLightningShaders(tex);
	}
	
	public Square(final IMaterial material, final ITransform posImpl){
		super(posImpl);
		
		if(material == null)
			throw new IllegalArgumentException("Can not create Square with material as null argument");
		
		this.material = material;	
	}
	
	public Square(final IMaterial material) {	
		if(material == null)
			throw new IllegalArgumentException("Can not create Square with material as null argument");
		
		this.material = material;
	}
	
	public VertexData.UsedBuffersType getVertexDataType(){ return VertexData.UsedBuffersType.VERT_POSITION_NORMAL_TEXCOORDS; }
	
	protected void createMesh(IMeshComposer builder){
		createDefaultMesh(builder, material);
	}
	
	public static void createDefaultMesh(final IMeshComposer builder, final IMaterial material){
		createMesh(builder, material, 1.0f, 1.0f);
	}
	
	public static void createMesh(final IMeshComposer builder, final IMaterial material, float w, float h){
		// Get scale of texture for u and v coords
		final FloatPair uvs = material.getTexturesScale();
		final float us = uvs.first; 
		final float vs = uvs.second;
	
		final Vertex topLeft = Vertex.create(-1.0f, h * 1.0f, 0.0f).normal(0.0f, 0.0f, 1.0f).texCoords(0.0f * us, 1.0f * vs);
		final Vertex bottomLeft = Vertex.create(-1.0f, -1.0f, 0.0f).normal(0.0f, 0.0f, 1.0f).texCoords(0.0f * us, 0.0f * vs);
		final Vertex bottomRight = Vertex.create(w * 1.0f, -1.0f, 0.0f).normal(0.0f, 0.0f, 1.0f).texCoords(1.0f * us, 0.0f * vs);
		final Vertex topRight = Vertex.create(1.0f,  h * 1.0f, 0.0f).normal(0.0f, 0.0f, 1.0f).texCoords(1.0f * us, 1.0f * vs);
		
		builder.addVertex(topLeft).addVertex(bottomLeft).addVertex(bottomRight).
			reuseVertex(topLeft).reuseVertex(bottomRight).addVertex(topRight);
		
		if(material.requiresTangent()){
			builder.calculateTBNVectors();
		}
	}
	
	@Override
	protected void initSharedRenderInfo(final IRenderableData rd){
		initRenderableDataShaders(rd);
	}

	@Override
	protected IRenderableData createRenderInfo(Mesh squareMesh) {
		final IRenderableData rendInfo = squareMesh.createRenderableData(
				RenderableData.RenderOperation.RO_TRIANGLES, RenderableData.RenderDirection.RD_CCW);

		initRenderableDataShaders(rendInfo);
		return rendInfo;
	}

	private void initRenderableDataShaders(final IRenderableData rendInfo) {
		rendInfo.setFragmentShader(material.getFragmentShader());
		rendInfo.setVertexShader(material.getVertexShader());
	}

	@Override
	public IMaterial getMaterialReadOnly() {
		return Materials.unmodifiableMaterial(material);
	}
	
	@Override
	public void setMaterial(IMaterial mat) {
		material = mat;
	}

	@Override
	public IMaterial getMaterial() {
		return material;
	}

	public boolean requiresPreparationOfMaterialForRendering() {
		return true;
	}
}
