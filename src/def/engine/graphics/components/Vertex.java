package def.engine.graphics.components;

import def.Const;
import def.engine.graphics.driver.VertexData;

/**
 * Vertex postion in space with its optional attributes: normal, texcoords and color
 * @author jonas
 *
 */
public class Vertex {
	public static final int X = 0;
	public static final int Y = 1;
	public static final int Z = 2;
	
	public static final int A = 3;
	public static final int W = 3;
	
	public static final int U = 0;
	public static final int V = 1;
	
	private final float[] vertexPos;
	private float[] vertexNormal = null;
	private float[] vertexColor = null;
	private float[] vertexTexCoord = null;
	private float[] vertexTangent = null;
	private float[] vertexBinormal = null;
	private short index = -1;
	
	/**
	 * Create vertex with positon information.
	 * @param x
	 * @param y
	 * @param z
	 * @return the vertex.
	 */
	public static Vertex create(float x, float y, float z){
		return new Vertex(x, y, z);
	}
	
	/**
	 * Create vertex with by coping information from the array.
	 * @param vertexPos
	 * @return the vertex with position info.
	 */
	public static Vertex createCopy(float[] vertexPos){
		VertexData.validateVertexPosArray(vertexPos);

		return new Vertex(vertexPos);
	}
	
	/**
	 * Create vertex with by coping information from the array.
	 * @param vertexPos
	 * @param vertexNormal
	 * @return the vertex with position and normal information
	 */
	public static Vertex createCopy(float[] vertexPos, float[] vertexNormal){
		VertexData.validateNormalArray(vertexNormal);

		Vertex v = Vertex.createCopy(vertexPos);
		v.normal(vertexNormal[X], vertexNormal[Y], vertexNormal[Z]);
		return v;
	}
	
	/**
	 * Create vertex with by coping information from the array.
	 * @param vertexPos
	 * @param vertexNormal
	 * @param vertexTexCoord
	 * @return the vertex with position, normal and texture coordinate information
	 */
	public static Vertex createCopy(float[] vertexPos, float[] vertexNormal, float[] vertexTexCoord){
		VertexData.validateTexCoordArray(vertexTexCoord);

		Vertex v = createCopy(vertexPos, vertexNormal);
		v.texCoords(vertexTexCoord[U], vertexTexCoord[V]);
		return v;
	}
	
	private Vertex(float[] pos){
		vertexPos = new float[]{pos[X], pos[Y], pos[Z]};
	}
	private Vertex(float x, float y, float z){
		vertexPos = new float[]{x, y, z};
	}
	
	public Vertex normalShared(float[] normal) {
		VertexData.validateNormalArray(normal);
		vertexNormal = normal;
		return this;
	}
	
	public Vertex normal(float x, float y, float z) {
		if(vertexNormal == null){
			vertexNormal = new float[]{x, y, z};
			return this;
		}
		vertexNormal[X] = x;
		vertexNormal[Y] = y;
		vertexNormal[Z] = z;
		return this;
	}
	
	public Vertex binormal(float x, float y, float z) {
		if(vertexBinormal == null){
			vertexBinormal = new float[]{x, y, z};
			return this;
		}
		vertexBinormal[X] = x;
		vertexBinormal[Y] = y;
		vertexBinormal[Z] = z;
		return this;
	}
	
	public Vertex tangent(float x, float y, float z) {
		if(vertexTangent == null){
			vertexTangent = new float[]{x, y, z};
			return this;
		}
		vertexTangent[X] = x;
		vertexTangent[Y] = y;
		vertexTangent[Z] = z;
		return this;
	}
	
	public Vertex colorShared(float[] color) {
		VertexData.validateColorArray(color);
		vertexColor = color;
		return this;
	}
	
	public Vertex color(float x, float y, float z, float a) {
		if(vertexColor == null){
			vertexColor = new float[]{x, y, z, a};
			return this;
		}
		vertexColor[X] = x;
		vertexColor[Y] = y;
		vertexColor[Z] = z;
		vertexColor[A] = a;
		return this;
	}
	
	public Vertex texCoordsShared(float[] texCoords) {
		VertexData.validateTexCoordArray(texCoords);
		vertexTexCoord = texCoords;
		return this;
	}
	
	public Vertex texCoords(float u, float v) {
		if(vertexTexCoord == null){
			vertexTexCoord = new float[]{u, v};
			return this;
		}
		
		vertexTexCoord[U] = u;
		vertexTexCoord[V] = v;
		return this;
	}
	
	public float[] getVertexPos() {
		return vertexPos;
	}
	public float[] getVertexTexCoords() {
		return vertexTexCoord;
	}
	public float[] getVertexColor() {
		return vertexColor;
	}
	public float[] getVertexNormal() {
		return vertexNormal;
	}
	
	public float[] getVertexTangent(){
		return vertexTangent;
	}
	
	public float[] getVertexBinormal(){
		return vertexBinormal;
	}

	public void setIndex(short index) {
		this.index = index;
	}

	public short getIndex() {
		return index;
	}
	
	/**
	 * TODO: Not reliable, needs updating
	 * @return
	 */
	public int getSizeBytes(){
		if(vertexNormal != null && vertexTexCoord != null)
			return VertexData.VERTEX_MAX_SIZE * Const.FLOAT_BYTE_SIZE;
		else if(vertexNormal != null && vertexTexCoord == null)
			return (VertexData.VERTX_POS_SIZE + VertexData.VERTX_NOR_SIZE) * Const.FLOAT_BYTE_SIZE;
		else if(vertexNormal == null && vertexTexCoord != null)
			return (VertexData.VERTX_POS_SIZE + VertexData.VERTEX_TEX_COORD_SIZE) * Const.FLOAT_BYTE_SIZE;
		else	// vertexNormal_ == null && vertexTexCoord_ == null
			return VertexData.VERTX_POS_SIZE * Const.FLOAT_BYTE_SIZE;
	}
}
