package def.engine.graphics.components;

/**
 * IBuildAction which always has invalid id and will therefore not be cached
 * 
 * @author jonas
 *
 */
public abstract class ANonCachableBuildAction implements IBuildAction{
	public void setUniqueID(int ignore) { }
	
	public int getUniqueID(){
		return IBuildAction.INVALID_BUILD_ACTION_ID;
	}
}
