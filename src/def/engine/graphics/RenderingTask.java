package def.engine.graphics;

import java.util.Collection;

import def.engine.execution.IRenderCallTask;
import def.engine.graphics.driver.IGraphicsDriver;
import def.engine.graphics.driver.IRenderTarget;

/**
 * Class to represent a task that renders the current state.
 * Not thread-safe
 * @author jonas
 *
 */
public class RenderingTask implements IRenderCallTask{
	private Collection<IRenderBucket> toRender;
	private Collection<IRenderTarget> renderTargets; 

	public RenderingTask(){}
	
	public boolean render(IGraphicsDriver graphics) {
		if(hasObjectsToRender()){
			renderObjectsOnQueue(graphics);
		}
		return false;
	}
	
	public void setToRender(Collection<IRenderBucket> toRender) {
		this.toRender = toRender;
	}
	
	public void setRenderTargets(Collection<IRenderTarget> renderTargets) {
		this.renderTargets = renderTargets;
	}
	
	private boolean renderObjectsOnQueue(IGraphicsDriver graphics){
		for(final IRenderTarget t : renderTargets){
			
			if(null == t.getTargetCamera()){
				throw new IllegalStateException("Fatal error, render-target without camera used.");
			}
			
			if( null != t.getProjectionMatrix() )
			{
				graphics.setProjectionMatrix(t.getProjectionMatrix());
			}
			
			graphics.setRenderTarget(t);
			graphics.preDraw();
			graphics.setRenderCamera(t.getTargetCamera());
			for(final IRenderBucket rb : toRender){
				rb.renderContent(graphics);
			}
		}
		
		return false; 
	}
	
	private boolean hasObjectsToRender(){
		return	((toRender != null) && (!toRender.isEmpty())) &&
				((renderTargets != null) && (!renderTargets.isEmpty()));
	}
}
