package def.engine.graphics;

import def.engine.graphics.components.Light;
import def.engine.graphics.components.SkyBox;

public interface IRenderableVisitor {
	public void renderVisit(IRenderable renderable);
	public void renderVisit(Light l);
	public void renderVisit(SkyBox skyBox);
}
