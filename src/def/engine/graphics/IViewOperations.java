package def.engine.graphics;

import def.engine.logic.math.MatrixArray4f;

public interface IViewOperations {
	/**
	 * Returns the view matrix representing the camera position and view angle
	 * @return
	 */
	public MatrixArray4f getViewMatrix();
	public IViewFrustum getReadOnlyViewFrustum();
	public IViewFrustum getViewFrustum();
	void setViewFrustum(IViewFrustum vf);
	public void setViewPort(ViewPort vp);
	public ViewPort getViewPort();
	public MatrixArray4f updateAndGetProjectionMatrix();
	public MatrixArray4f getProjectionMatrix();	
}
