package def.engine.graphics;

import def.shared.util.IObserver;

public abstract class RenderSequenceListener implements IObserver<IRenderSequenceEvent>{

	public final void update(IRenderSequenceEvent rsa){
		renderSequenceUpdate(rsa);
	}
	
	public abstract void renderSequenceUpdate(IRenderSequenceEvent rsa);
}
