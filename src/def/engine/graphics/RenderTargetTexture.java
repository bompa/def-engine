package def.engine.graphics;

import def.engine.control.ICamera;
import def.engine.graphics.driver.FrameBufferObject;
import def.engine.graphics.driver.IRenderTarget;
import def.engine.graphics.material.IMaterial;
import def.engine.graphics.material.ITexture;
import def.engine.logic.math.MatrixArray4f;
import def.util.Util;

public class RenderTargetTexture implements IRenderTarget{
	
	private final String name;
	private final FrameBufferObject fbo = FrameBufferObject.create();
	private final ITexture renderTargetTextue;
	private ICamera camera;
	private CameraMode cameraMode = CameraMode.CAM_MODE_MAIN_CAMERA;
	
	public static RenderTargetTexture create(final String name, final ITexture renderTargetTextue){
		return new RenderTargetTexture(name, renderTargetTextue);
	}
	
	private RenderTargetTexture(final String name, final ITexture renderTargetTextue){
		this.name = name;
		this.renderTargetTextue = renderTargetTextue;
	}

	private void handleInvalidCameraState() {
		if(null == camera){
			throw new IllegalStateException("RenderTargetTexture has not been initalized. camera==null");
		}
	}
	
	public ViewPort getViewPort() {
		handleInvalidCameraState();
		
		return camera.getViewPort();
	}

	public void setViewPort(ViewPort viewPort) {
		if(viewPort == null)
			throw Util.buildIllegalNullArgException(RenderTargetTexture.class, "func:setViewPort", "viewPort");
		
		camera.setViewPort(viewPort);
	}
	
	public void setViewFrustm(ViewFrustum frustum) {
		handleInvalidCameraState();
		
		camera.setViewFrustum(frustum);
	}

	public String getName() {
		return name;
	}

	public FrameBufferObject getFrameBufferObject() {
		return fbo;
	}

	public boolean checkIfSupportedByTarget(IRenderable renderable) {
		if(null != renderable.getMaterial() ){
			final IMaterial mat = renderable.getMaterial();
			for( int i = 0; i < mat.getNrOfTextures(); i++){
				if(mat.getTexture(i) == renderTargetTextue){
					return false;
				}
			}
				
		}
			
		return true;
	}

	public void setTargetCamera(ICamera camera) {
		this.camera = camera;
		
	}

	public ICamera getTargetCamera() {
		return camera;
	}
	
	public MatrixArray4f getProjectionMatrix()	{
		return null;
	}

	public CameraMode getCameraMode() {
		return cameraMode;
	}

	public void setCameraMode(CameraMode cameraMode) {
		this.cameraMode = cameraMode;
	}

	public boolean requiresPrepartion() {
		return true;
	}
	
	public String toString(){
		return getName();
	}
}
