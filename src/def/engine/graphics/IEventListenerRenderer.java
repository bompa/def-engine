package def.engine.graphics;


import def.engine.execution.ARenderEventCoordinator;
import def.engine.graphics.driver.IGraphicsResources;
import def.engine.graphics.driver.RenderConfig;
import android.opengl.GLSurfaceView.Renderer;


public interface IEventListenerRenderer extends Renderer, IGraphicsResources {
	public RenderConfig getRenderConfig();
	public ARenderEventCoordinator getRenderEventListener();
	public void addRenderSequenceListener(RenderSequenceListener listener);
}
