package def.engine.graphics;

/**
 * Something that wants to be renderd can sen a renderevent to the queue.
 * @author jonas
 *
 */
public class RenderEvent {
	public RenderEvent(IRenderable r){
		renderObj = r;
	}
	private IRenderable renderObj;
	
	public IRenderable getRenderObj(){
		return renderObj;
	}
}
