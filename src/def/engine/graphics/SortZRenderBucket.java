package def.engine.graphics;

import java.util.AbstractQueue;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;

import android.opengl.Matrix;

import def.engine.graphics.driver.IGraphicsDriver;
import def.engine.logic.math.LinjearAlg;
import def.engine.logic.math.MatrixArray4f;

/**
 * SortZRenderBucket renders its content in one pass sorted from the camera view after z depth.
 * 
 * Calling renderContent is not thread-safe but add is thread-safe.
 * @author jonas
 *
 */
public class SortZRenderBucket implements IRenderBucket {
	private final PriorityBlockingQueue<IRenderable> renderables;
	private final IViewOperations view;
	private final int prio;
	private final static int INTIAL_CAP = 25;
	private final Comparator<IRenderable> zcomp = new Comparator<IRenderable>() {

		public int compare(IRenderable lhs, IRenderable rhs) {
			return Float.valueOf(lhs.getCalculatedZDepth()).compareTo(Float.valueOf(rhs.getCalculatedZDepth()));
		}};

	private IRenderableVisitor rendVisitor = null;
	
	public static SortZRenderBucket create(int prio, final IViewOperations view){
		return new SortZRenderBucket(prio, view);
	}
	
	private SortZRenderBucket(int prio, final IViewOperations view){
		if(view == null)
			throw new IllegalArgumentException("Could not create SortZRenderBucket since IViewOperations value is null");
		
		this.prio = prio;
		this.view = view;

		renderables = new PriorityBlockingQueue<IRenderable>(INTIAL_CAP, zcomp);
	}
	
	private SortZRenderBucket(int prio, final IViewOperations view, final AbstractQueue<IRenderable> renderables){
		if(view == null || renderables == null)
			throw new IllegalArgumentException("Could not create SortZRenderBucket since in arg value is null");
		
		this.prio = prio;
		this.view = view;
		final int initcap = renderables.size() > 0? renderables.size(): 1;
		this.renderables = new PriorityBlockingQueue<IRenderable>(initcap, zcomp);
		this.renderables.addAll(renderables);
	}

	public int getRenderPrio() {
		return prio;
	}

	public void renderContent(final IGraphicsDriver graphics) {
		if(this.rendVisitor == null){
			this.rendVisitor = new ObjectRenderer(graphics);
		}
		if(renderables.size() <= 0)
			return;
		
		List<IRenderable> orderedList = new ArrayList<IRenderable>(renderables.size());
		renderables.drainTo(orderedList);
		for(int i = orderedList.size() - 1; i >= 0; i--){
			orderedList.get(i).acceptRendering(rendVisitor);
		}
		renderables.addAll(orderedList);
	}

	public void add(IRenderable r) {
		calculateZDepth(r);
		renderables.add(r);
	}
	
	public void remove(IRenderable r) {
		renderables.remove(r);
	}
	
	private void calculateZDepth(IRenderable r) {
		
		// Model-view part, gives the object its z-value relative to the camera
		final float[] modelMatrix = LinjearAlg.matrixToArrayRearrange(r.getWorldTransforms());
		final float viewMtx[] = view.getViewMatrix().getArray();

		final float[] mVMatrix = (new MatrixArray4f()).getArray();
        Matrix.multiplyMM(mVMatrix, 0, viewMtx, 0, modelMatrix, 0);

		// Projection part, multiplied to model-view-proj
        final float[] projMtx = view.getProjectionMatrix().getArray();
        
        final float[] mVPMatrix = (new MatrixArray4f()).getArray();
        Matrix.multiplyMM(mVPMatrix, 0, projMtx, 0, mVMatrix, 0);
        
        //Get Z from matrx (in column first order) at row 3 column 2 (0-based)
        final int ncolumns = 4;
        final float z = mVPMatrix[ncolumns * 3 + 2];
        r.setCalculatedZDepth(z);
	}

	public void emptyBucket() {
		renderables.clear();
	}
	
	public IRenderBucket clone(){
		return new SortZRenderBucket(this.prio, this.view, this.renderables);
	}

	public Collection<IRenderable> getRenderables() {
		return renderables;
	}
}
