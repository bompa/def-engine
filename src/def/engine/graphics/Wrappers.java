package def.engine.graphics;

import def.engine.graphics.driver.IRenderableData;

public class Wrappers {

	public static IViewFrustum unmodifiableViewFrustum(IViewFrustum vf){
		return new ViewFrustumImmutableWrapper(vf);
	}

	public static IRenderSequenceEvent unmodifiableRenderSequenceEvent(IRenderSequenceEvent rse){
		return new RenderSequenceEventImmutableWrapper(rse);
	}

	public static IRenderableData unmodifiableRenderInfo(IRenderableData ri){
		return new RenderableDataImmutableWrapper(ri);
	}

}
