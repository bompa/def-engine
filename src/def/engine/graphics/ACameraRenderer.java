package def.engine.graphics;

import def.engine.control.ICamera;

public abstract class ACameraRenderer implements IEventListenerRenderer{
	protected ICamera mainCamera;
	private final RenderSequenceObservable renderSequence = new RenderSequenceObservable();
	
	public ICamera getCamera(){ return mainCamera; }
	public void setCamera(ICamera cam){ this.mainCamera = cam; }

	ACameraRenderer(){ }
	ACameraRenderer(ICamera cam){
		this.mainCamera = cam;
	}
	
	public void addRenderSequenceListener(RenderSequenceListener listener) {
		renderSequence.registerObserver(listener);
	}
	
	protected RenderSequenceObservable getRenderSequence() {
		return renderSequence;
	}
}
