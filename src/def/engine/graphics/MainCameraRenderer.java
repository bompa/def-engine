package def.engine.graphics;

import java.nio.Buffer;
import java.util.Collection;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.opengles.GL11;

import android.graphics.Bitmap;

import def.Const;
import def.engine.Engine;
import def.engine.control.ICamera;
import def.engine.execution.ARenderEventCoordinator;
import def.engine.execution.IRenderCallTask;
import def.engine.execution.ITaskExecutor;
import def.engine.graphics.RenderSequenceEvent;
import def.engine.graphics.driver.IGraphicsDriver;
import def.engine.graphics.driver.IRenderTarget;
import def.engine.graphics.driver.RenderConfig;
import def.engine.graphics.driver.Graphics.RenderSequenceAction;
import def.engine.graphics.driver.IRenderTarget.CameraMode;
import def.engine.graphics.driver.VertexData.BufferDataType;
import def.engine.graphics.driver.VertexData.BufferTarget;
import def.engine.graphics.material.ITexture;
import def.engine.graphics.shader.FragmentShader;
import def.engine.graphics.shader.IShaderProgram;
import def.engine.graphics.shader.VertexShader;
import def.engine.graphics.shader.IShaderProgram.Id;
import def.engine.graphics.shader.ShaderValue.ValueScope;
import def.shared.util.ITree;
import def.shared.util.Tree;
import def.util.IntPair;
import def.util.LogHandler;
import def.util.Stats;

public class MainCameraRenderer extends ACameraRenderer{
	private final LogHandler l = LogHandler.createInstanceShared();
	
	private final IGraphicsDriver graphicsDriver;
	private final RenderConfig renderConf;
	private final Stats stats = Stats.instance();
	private final ITaskExecutor engineTaskExec = Engine.instance().getTaskExecutor();
	private final IRenderTarget mainRenderTarget = RenderTargetWindow.create("MainWindow");
	
	public final static IRenderSequenceEvent RenderSeqSurfaceUpdate = 
		new RenderSequenceEvent(RenderSequenceAction.REND_ACTION_SURFACE_CREATED);
	public final static IRenderSequenceEvent RenderSeqWorkSynchedDrawing = 
		new RenderSequenceEvent(RenderSequenceAction.REND_ACTION_PRE_WORK_SYNCHED_DRAWING);
	public final static IRenderSequenceEvent RenderSeqFrameDone = 
		new RenderSequenceEvent(RenderSequenceAction.REND_ACTION_FRAME_RENDERED);
	

	
	public MainCameraRenderer(IGraphicsDriver igd, RenderConfig conf)
	{
		if(igd == null || conf == null)
			throw new IllegalArgumentException();
		
		graphicsDriver = igd;
		renderConf = conf;
		
		// Set main render-target
		engineTaskExec.getRenderEventCoordinator().addRenderTarget(mainRenderTarget);
		engineTaskExec.getRenderEventCoordinator().enableRenderTarget(mainRenderTarget);
	}
	
	public RenderConfig getRenderConfig()
	{
		return renderConf;
	}
	
	//private long logStatsFrameFreq = 25;

	/**
	 * Submit rendering work to system task executor when draw frame is requested.
	 */
	public void onDrawFrame(GL10 gl) {
		stats.updateFps();
		RenderSeqWorkSynchedDrawing.setRenderStep(stats.getLastRenderCallTime());

		IRenderCallTask renderingTask = engineTaskExec.fetchSynchedRenderWork();
		//stats.logStatistics(logStatsFrameFreq);
		
		getRenderSequence().notifyObservers(
				Wrappers.unmodifiableRenderSequenceEvent(RenderSeqWorkSynchedDrawing));
		
		renderingTask.render(graphicsDriver);
		
		graphicsDriver.drawFrameDone();
		
		getRenderSequence().notifyObservers(
				Wrappers.unmodifiableRenderSequenceEvent(RenderSeqFrameDone));
	}

	public void onSurfaceChanged(GL10 gl, int width, int height) {
		l.log("Surface changed.");
		
		final ViewPort vp = ViewPort.create(0, 0, height, width);
		mainCamera.setViewPort(vp);
		mainCamera.setViewFrustum(ViewFrustum.createDefault(vp.getWidth() / vp.getHeight()));
		
		Collection<IRenderTarget> renderTargets = engineTaskExec.getRenderEventCoordinator().getAllRenderTargets();
		for(IRenderTarget rt : renderTargets)
		{
			if(rt.getCameraMode() == CameraMode.CAM_MODE_MAIN_CAMERA){
				rt.setTargetCamera(mainCamera);
			}
			else if( rt.getCameraMode() == CameraMode.CAM_MODE_CUSTOM_USE_SCREEN_SIZE )
			{
				final ICamera customCam = rt.getTargetCamera();
				customCam.setViewPort(vp);
				customCam.setViewFrustum(ViewFrustum.createDefault(vp.getWidth() / vp.getHeight()));
			}
		}
		
		graphicsDriver.setProjectionMatrix(mainCamera.updateAndGetProjectionMatrix());
	}

	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		
		if(!(gl instanceof  GL11))
			throw new IllegalArgumentException("Minimum opengl version 1.1 not supported");
			
		GL11 gl11 = (GL11)gl;
		l.log("Creating Surface in Renderer");
		graphicsDriver.init(gl11, renderConf);

		// Init graph. TODO: Move to sceneFacade?		
		engineTaskExec.getRenderEventCoordinator().setRenderGraph(createDefaultRenderGraph());
		engineTaskExec.clearTasks();

		getRenderSequence().notifyObservers(RenderSeqSurfaceUpdate);
	}

	private ITree<IRenderBucket> createDefaultRenderGraph() {
		final IRenderBucket unsortedSkybox = UnsortedRenderBucket.create(Const.REND_PRIO_SKYBOX);
		final IRenderBucket unsortedLight = UnsortedRenderBucket.create(Const.REND_PRIO_LIGHT);
		final IRenderBucket unsorted = UnsortedRenderBucket.create(Const.REND_PRIO_DEFAULT);
		final IRenderBucket zsorted = SortZRenderBucket.create(Const.REND_PRIO_TRANSPARENT, mainCamera);
		
		final ITree<IRenderBucket> rendergraph = new Tree<IRenderBucket>(unsortedLight);
		final ITree<IRenderBucket> renderGraphSkyboxNode = rendergraph.addLeaf(unsortedSkybox);
		final ITree<IRenderBucket> rendergraphUnsortedNode = renderGraphSkyboxNode.addLeaf(unsorted);
		rendergraphUnsortedNode.addLeaf(zsorted);
		
		return rendergraph;
	}

	public ARenderEventCoordinator getRenderEventListener() {
		return engineTaskExec.getRenderEventCoordinator();
	}

	public ITexture.RenderId createTexture(Bitmap bitmap, int textureId, ITexture.Settings settings) {
		return graphicsDriver.createTexture(bitmap, textureId, settings);
	}

	public Id createProgramCompileShaders(VertexShader vs, FragmentShader fs) {
		if(graphicsDriver.supportsProgramablePipeline())
			return graphicsDriver.createProgramCompileShaders(vs, fs);
		return new Id(0);
	}
	
	public Id createProgramLinkCompiledShaders(IShaderProgram.Id vsId, IShaderProgram.Id fsId) {
		if(graphicsDriver.supportsProgramablePipeline())
			return graphicsDriver.createProgramLinkCompiledShaders(vsId, fsId);
		return new Id(0);
	}

	public int getSupporteRenderVersion() {
		return graphicsDriver.getSupporteRenderVersion();
	}

	public int getShaderParamId(Id program, String name, ValueScope vs) {
		if(graphicsDriver.supportsProgramablePipeline())
			return graphicsDriver.getShaderParamId(program, name, vs);
		return 0;
	}
	
	public int[] generateTextureIds(int nTextures) {
		return graphicsDriver.generateTextureIds(nTextures);
	}

	public int[] generateVBOs(int nVbos) {
		return graphicsDriver.generateVBOs(nVbos);
	}

	public void deleteVbos(int nVbos, int[] vbos) {
		graphicsDriver.deleteVbos(nVbos, vbos);
	}

	public void setVboData(int vboID, BufferTarget target, int size, Buffer data, BufferDataType usage) {
		graphicsDriver.setVboData(vboID, target, size, data, usage);
	}

	public IntPair getRenderWindowSize() {
		return graphicsDriver.getRenderWindowSize();
	}

	public int[] generateFrameBufferObjects(int nBuffers) {
		return graphicsDriver.generateFrameBufferObjects(nBuffers);
	}

	public int[] generateRenderBufferIDs(int nBuffers) {
		return graphicsDriver.generateRenderBufferIDs(nBuffers);
	}

	public boolean initRenderTarget(IRenderTarget renderTarget) {
		if(graphicsDriver.supportsProgramablePipeline()){
			return graphicsDriver.initRenderTarget(renderTarget);
		}
		return false;
	}
}
