package def.engine.graphics;

import android.util.Log;
import def.engine.execution.ARenderEventCoordinator;
import def.engine.graphics.components.ARenderableBuilder;
import def.engine.graphics.components.IBuildAction;
import def.engine.graphics.components.Mesh;
import def.engine.graphics.driver.IRenderableData;
import def.engine.graphics.material.IMaterial;
import def.engine.graphics.material.ITexture;
import def.engine.graphics.resources.ResourceManager;
import def.shared.interfaces.ITransform;
import def.shared.util.OutArg;
import def.util.LogHandler;
import def.util.Util;


public abstract class ARenderableFactory<E extends ARenderable> {
	private final LogHandler l = LogHandler.createInstanceShared();
	private final ResourceManager resourceMgr;
	private ARenderEventCoordinator defualtRenderEvListener = null;
	private boolean requestRenderingOnCreation = false;
	
	public ARenderableFactory(final ResourceManager rm){
		this.resourceMgr = rm;
	}
	
	/**
	 * This overload of createRenderable calls createConcreteRenderable to create the instance. And
	 * then initalizes listeners and so on.
	 * 
	 * @param refName
	 * @param modelName
	 * @param defaultListener
	 * @param positionableImpl
	 * @param buildAction how to build the mesh representing the renderable
	 * @return
	 */
	public final E createRenderable(final String refName, 
									final String modelName,
									final ARenderEventCoordinator defaultListener,
									final ITransform positionableImpl,
									IBuildAction buildAction,
									final IMaterial material,
									final ITexture texture)
	{

		E r = createConcreteRenderable(refName, modelName, positionableImpl, material, texture);

		if(r == null){
			if(l.isLevelLoggable(Log.WARN))
				l.log("AbstractRenderableFactory::createRenderable - Failed to create Renderable", Log.WARN);
			return null;
		}
		
		if(buildAction == null){
			buildAction = r.getIBuildAction();
		}
		
		try{
			initRenderableDataAndCache(modelName, buildAction, r);
		} catch(RuntimeException e){
			if(l.isLevelLoggable(Log.WARN))
				l.log("AbstractRenderableFactory::createRenderable - Failed to initalize Renderable", Log.WARN);
			return null;
		}
		
		if(defaultListener != null){
			r.registerObserver(defaultListener);
			if(requestRenderingOnCreation)
				r.requestRendering();
		}
		else{
			if(l.isLevelLoggable(Log.WARN))
				l.log("AbstractRenderableFactory::createRenderable - No default RenderEventListener supplied. Must be registred manualy.", Log.WARN);
		}
		return r;
	}

	private void initRenderableDataAndCache(final String modelName, IBuildAction buildAction, E r) {
		
		final int buildActionUniqueID = getBuildActionUniqueID(modelName, buildAction);
		final OutArg<IRenderableData> rendDataOut = Util.makeOutArg();
		
		if(resourceMgr.checkIRenderableDataRepo(buildActionUniqueID, rendDataOut)){
			r.init(RenderableData.createWithStaticSharedData(rendDataOut.value));
		}
		else{
			final Mesh mesh = createMesh(r, buildAction);
			final IRenderableData rendData = r.init(mesh);
			if(buildActionUniqueID != IBuildAction.INVALID_BUILD_ACTION_ID)
			{
				resourceMgr.updateIRenderableDataRepo(buildActionUniqueID, rendData);
			}
			
		}
	}

	private int getBuildActionUniqueID(final String modelName, IBuildAction buildAction) {
		
		int buildActionUniqueID = buildAction.getUniqueID();
		
		if(buildActionUniqueID == IBuildAction.DEFAULT_MODEL_BUILD_ACTION_ID){
			buildActionUniqueID = modelName.hashCode();
			buildAction.setUniqueID(buildActionUniqueID);
		}
		
		return buildActionUniqueID;
	}

	private Mesh createMesh(E r, final IBuildAction buildAction) {
		final ARenderableBuilder builder = ARenderableBuilder.create( r.getVertexDataType(), buildAction);
		return ARenderableBuilder.buildAndGetMesh(builder); 
	}
	
	/**
	 * Create a Renderable using given model, and using custom listener
	 * 
	 * @param modelName
	 * @param listener
	 * @param positionableImpl
	 * @return
	 */
	public final E createRenderable(	String modelName, 
										ARenderEventCoordinator listener, 
										ITransform positionableImpl)
	{
		return createRenderable(null, modelName, defualtRenderEvListener, positionableImpl, null, null, null);		
	}
	
	/***************************************************
	 * Public create methods, used to create renderables using varoius paramters
	 ***************************************************/
	
	/**
	 * Create a Renderable with custom build action
	 * 
	 * @param modelName
	 * @param buildAction
	 * @return renderable constructed accordingly to parameters, null on failure
	 */
	public final E createRenderable(final String modelName, final IBuildAction buildAction){
		return createRenderable(null, modelName, defualtRenderEvListener, null, buildAction, null, null); 
	}
	
	/**
	 * Create a Renderable with custom build action and supplied refernece namse
	 * 
	 * @param modelName
	 * @param buildAction
	 * @return renderable constructed accordingly to parameters, null on failure
	 */
	public final E createRenderable(final String refName, final String modelName, final IBuildAction buildAction){
		return createRenderable(refName, modelName, defualtRenderEvListener, null, buildAction, null, null); 
	}
	
	/**
	 * Create a Renderable with custom build action nad texture
	 * 
	 * @param modelName
	 * @param buildAction
	 * @return renderable constructed accordingly to parameters, null on failure
	 */
	public final E createRenderable(final String modelName, final IBuildAction buildAction, final ITexture tex){
		return createRenderable(null, modelName, defualtRenderEvListener, null, buildAction, null, tex);
	}
	
	/**
	 * Create a Renderable using modelName with supplied reference name
	 * 
	 * @param refName
	 * @param modelName
	 * @return
	 */
	public final E createRenderable(final String refName, final String modelName){
		return createRenderable(refName, modelName, defualtRenderEvListener, null, null, null, null); 
	}
	
	/**
	 * Create a Renderable
	 * 
	 * @param refName
	 * @param modelName
	 * @param positionableImpl
	 * @return
	 */
	public final E createRenderable(final String refName, final String modelName, final ITransform positionableImpl){
		return createRenderable(refName, modelName, defualtRenderEvListener, positionableImpl, null, null, null); 
	}
	
	/**
	 * Create a Renderable only using modelname, leaving all else to defalut
	 * 
	 * @param modelName
	 * @return
	 */
	public final E createRenderable(String modelName){
		return createRenderable(modelName, defualtRenderEvListener);
	}
	
	/**
	 * Create a Renderable with a shared IPositionable, useful for object which share all postion/orientation data.
	 * 
	 * @param modelName
	 * @param positionableImpl
	 * @return
	 */
	public final E createRenderable(String modelName, ITransform positionableImpl){
		return createRenderable(modelName, defualtRenderEvListener, positionableImpl);
	}

	/**
	 * Create a Renderable with modelname and custom listener
	 * 
	 * @param modelName name of model to render
	 * @param listener receives updates when renderable request rendering
	 * @return
	 */
	public final E createRenderable(String modelName, ARenderEventCoordinator listener){
		return createRenderable(modelName, listener, null);
	}
	
	/**
	 * Create a Renderable with modelname and custom texture
	 * 
	 * @param modelName
	 * @param texture
	 * @return
	 */
	public final E createRenderable(String modelName, ITexture texture){
		return createRenderable(null, modelName, defualtRenderEvListener, null, null, null, texture);
	}
	
	public final E createRenderable(String refName, String modelName, ITexture texture){
		return createRenderable(refName, modelName, defualtRenderEvListener, null, null, null, texture);
	}
	
	public final E createRenderable(String refName, String modelName, ITransform pos, ITexture texture){
		return createRenderable(refName, modelName, defualtRenderEvListener, pos, null, null, texture);
	}
	
	/**
	 * Create a Renderable with modelname, genreated reference name and a custom material
	 * 
	 * @param modelName
	 * @param material
	 * @return
	 */
	public final E createRenderable(String modelName, IMaterial material){
		return createRenderable(null, modelName, defualtRenderEvListener, null, null, material, null);
	}
	
	/**
	 * Create renderable with custom buildaction and material
	 * @param modelName
	 * @param buildaction
	 * @param material
	 * @return
	 */
	public final E createRenderable(String modelName, IBuildAction buildaction, IMaterial material){
		return createRenderable(null, modelName, defualtRenderEvListener, null, buildaction, material, null);
	}
	
	/**
	 * Create a renderable using modelname with given reference name
	 * @param refName
	 * @param modelName
	 * @param material
	 * @return
	 */
	public final E createRenderable(String refName, String modelName, IMaterial material){
		return createRenderable(null, modelName, defualtRenderEvListener, null, null, material, null);
	}

	/***************************************************
	 * Abstract methods to be implemented by concrete subclasses
	 ***************************************************/ 	
	
	/**
	 * Abstract method to be overridden by concrete factories of renderables.
	 * 
	 * @param referenceName is the name used to reference added model, can be null and will then be generated.
	 * @param modelName is the name of the type of the concrete renderable to be created
	 * @return
	 */
	protected abstract E createConcreteRenderable(	String referenceName, 
													String modelName, 
													ITransform positionableImpl, 
													IMaterial m, 
													ITexture tex);
	

	/***************************************************
	 * setters/getters
	 ***************************************************/ 	
	public void setDefualtRenderEvListener(ARenderEventCoordinator defualtRenderEvListener_) {
		this.defualtRenderEvListener = defualtRenderEvListener_;
	}

	public ARenderEventCoordinator getDefualtRenderEvListener() {
		return defualtRenderEvListener;
	}

	public void setRequestRenderingOnCreation(boolean requestRenderingOnCreation) {
		this.requestRenderingOnCreation = requestRenderingOnCreation;
	}

	public boolean isRequestRenderingOnCreation() {
		return requestRenderingOnCreation;
	}
}
