package def.engine.graphics;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

import javax.vecmath.Matrix4f;
import def.engine.graphics.driver.IRenderableData;
import def.engine.graphics.driver.VertexBufferData;
import def.engine.graphics.shader.FragmentShader;
import def.engine.graphics.shader.VertexShader;
import def.engine.logic.math.MatrixArray4f;
import def.util.IntPair;

public class RenderableDataImmutableWrapper implements IRenderableData {
	private static final String ILLEGAL_MODIFY_MSG = "Modification of immutable RenderInfo not allowed!";
	private IRenderableData wrapped_;
	
	public RenderableDataImmutableWrapper(IRenderableData wrapped){
		wrapped_ = wrapped;
	}
	
	private void handleUnsupported(){
		throw new UnsupportedOperationException(ILLEGAL_MODIFY_MSG);
	}

	public Matrix4f getModelMatrix() {
		return wrapped_.getModelMatrix();
	}

	public RenderDirection getRenderDirection() {
		return wrapped_.getRenderDirection();
	}

	public RenderOperation getRenderOperation() {
		return wrapped_.getRenderOperation();
	}

	public void setModelMatrix(Matrix4f modelViewMatrix) {
		handleUnsupported();
	}

	public ShortBuffer getActiveIndexBuffer() {
		return wrapped_.getActiveIndexBuffer();
	}

	public FloatBuffer getVertexBuffer() {
		return wrapped_.getVertexBuffer();
	}

	public FloatBuffer getColorBuffer() {
		return wrapped_.getColorBuffer();
	}

	public FloatBuffer getNormalsBuffer() {
		return wrapped_.getNormalsBuffer();
	}

	public void setColorBuffer(FloatBuffer colorBuffer) {
		handleUnsupported();		
	}

	public void setNormalsBuffer(FloatBuffer normalsBuffer) {
		handleUnsupported();
	}

	public FloatBuffer getTextureCoordsBuffer() {
		return wrapped_.getTextureCoordsBuffer();
	}

	public void setTextureCoordsBuffer(FloatBuffer tcBuffer) {
		handleUnsupported();
	}

	public FragmentShader getFragmentShader() {
		return wrapped_.getFragmentShader();
	}

	public VertexShader getVertexShader() {
		return wrapped_.getVertexShader();
	}

	public void setFragmentShader(FragmentShader fs) {
		handleUnsupported();
	}

	public void setVertexShader(VertexShader vs) {
		handleUnsupported();
	}

	public void adaptViewMatrix(MatrixArray4f viewMatrix) {
		wrapped_.adaptViewMatrix(viewMatrix);
	}

	public float getScale() {
		return wrapped_.getScale();
	}

	public void setScale(float scale) {
		handleUnsupported();
	}

	public int getNrOfComponents() {
		return wrapped_.getNrOfComponents();
	}

	public void setActiveComponent(int componentIndex) {
		wrapped_.setActiveComponent(componentIndex);
	}

	public FloatBuffer getTangetBuffer() {
		return wrapped_.getTangetBuffer();
	}

	public void setTangetBuffer(FloatBuffer tangetBuffer) {
		handleUnsupported();
		
	}

	public FloatBuffer getBinormalBuffer() {
		return wrapped_.getBinormalBuffer();
	}

	public void setBinormalBuffer(FloatBuffer binormalBuffer) {
		handleUnsupported();
	}

	public VertexBufferData getVertexBufferData() {
		return wrapped_.getVertexBufferData();
	}

	public IntPair getIndexBufferRange() {
		return wrapped_.getIndexBufferRange();
	}

	public ArrayList<IntPair> getComponentIndices() { 
		return wrapped_.getComponentIndices(); 
	}

	public short[] getIndices() {
		return wrapped_.getIndices();
	}
}
