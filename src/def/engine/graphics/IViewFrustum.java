package def.engine.graphics;

import def.engine.logic.math.MatrixArray4f;

public interface IViewFrustum {
	public void set(float fovy, float aspectRatio, float zNear, float zFar);
	
	public abstract MatrixArray4f getProjectionMatrix();
	public abstract MatrixArray4f calcualteProjectionMatrix(float fovy, float aspectRatio, float zNear, float zFar);

	public abstract void setAspectRatio(float aspectRatio_);
	public abstract float getAspectRatio();

	public abstract void setFovy(float fovy_);
	public abstract float getFovy();

	public abstract void setzNear(float zNear_);
	public abstract float getzNear();

	public abstract void setzFar(float zFar_);
	public abstract float getzFar();
}