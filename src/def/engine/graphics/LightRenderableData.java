package def.engine.graphics;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;

import javax.vecmath.Matrix4f;
import def.engine.graphics.driver.IRenderableData;
import def.engine.graphics.driver.VertexBufferData;
import def.engine.graphics.shader.FragmentShader;
import def.engine.graphics.shader.VertexShader;
import def.engine.logic.math.MatrixArray4f;
import def.util.IntPair;

public class LightRenderableData implements IRenderableData {

	public FloatBuffer getColorBuffer() {
		// TODO Auto-generated method stub
		return null;
	}

	public FragmentShader getFragmentShader() {
		// TODO Auto-generated method stub
		return null;
	}

	public ShortBuffer getActiveIndexBuffer() {
		// TODO Auto-generated method stub
		return null;
	}

	public Matrix4f getModelMatrix() {
		// TODO Auto-generated method stub
		return null;
	}

	public FloatBuffer getNormalsBuffer() {
		// TODO Auto-generated method stub
		return null;
	}

	public RenderDirection getRenderDirection() {
		// TODO Auto-generated method stub
		return null;
	}

	public RenderOperation getRenderOperation() {
		// TODO Auto-generated method stub
		return null;
	}

	public FloatBuffer getTextureCoordsBuffer() {
		// TODO Auto-generated method stub
		return null;
	}

	public FloatBuffer getVertexBuffer() {
		// TODO Auto-generated method stub
		return null;
	}

	public VertexShader getVertexShader() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setColorBuffer(FloatBuffer colorBuffer) {
		// TODO Auto-generated method stub

	}

	public void setFragmentShader(FragmentShader fs) {
		// TODO Auto-generated method stub

	}

	public void setModelMatrix(Matrix4f modelViewMatrix) {
		// TODO Auto-generated method stub

	}

	public void setNormalsBuffer(FloatBuffer normalsBuffer) {
		// TODO Auto-generated method stub

	}

	public void setTextureCoordsBuffer(FloatBuffer tcBuffer) {
		// TODO Auto-generated method stub

	}

	public void setVertexShader(VertexShader vs) {
		// TODO Auto-generated method stub

	}

	public void adaptViewMatrix(MatrixArray4f viewMatrix) {
		// TODO Auto-generated method stub
		
	}

	public float getScale() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setScale(float scale) {
		// TODO Auto-generated method stub
		
	}

	public int getNrOfComponents() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void setActiveComponent(int componentIndex) {
		// TODO Auto-generated method stub
		
	}

	public FloatBuffer getTangetBuffer() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setTangetBuffer(FloatBuffer tangetBuffer) {
		// TODO Auto-generated method stub
		
	}

	public FloatBuffer getBinormalBuffer() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setBinormalBuffer(FloatBuffer binormalBuffer) {
		// TODO Auto-generated method stub
		
	}

	public VertexBufferData getVertexBufferData() {
		// TODO Auto-generated method stub
		return null;
	}

	public IntPair getIndexBufferRange() {
		// TODO Auto-generated method stub
		return null;
	}

	public ArrayList<IntPair> getComponentIndices() {
		// TODO Auto-generated method stub
		return null;
	}

	public short[] getIndices() {
		// TODO Auto-generated method stub
		return null;
	}

}
