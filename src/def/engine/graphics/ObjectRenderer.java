package def.engine.graphics;

import javax.vecmath.Vector3f;

import android.util.Log;


import def.engine.control.ICamera;
import def.engine.graphics.components.Light;
import def.engine.graphics.components.SkyBox;
import def.engine.graphics.driver.IGraphicsDriver;
import def.engine.graphics.driver.IRenderTarget;
import def.engine.graphics.driver.IRenderableData;
import def.engine.graphics.driver.RenderConfig;
import def.engine.graphics.material.IMaterial;
import def.engine.graphics.shader.IShaderProgram.Id;
import def.engine.logic.math.LinjearAlg;
import def.util.LogHandler;
import def.util.Options;

public class ObjectRenderer implements IRenderableVisitor{
	
	private final LogHandler l = LogHandler.createInstanceShared(ObjectRenderer.class);
	private final IGraphicsDriver graphicsDriver;
	private final boolean driverIsProgamable;
	
	public ObjectRenderer(IGraphicsDriver gDriver){
		this.graphicsDriver = gDriver;
		this.driverIsProgamable = gDriver.supportsProgramablePipeline();
	}

	public void renderVisit(IRenderable renderable) {
		if( !checkIfTargteAcceptsRenderable(renderable) ){
			return;
		}
		final IRenderableData renderInfo = renderable.getRenderInfoReadOnly();
		final IMaterial mat = renderable.getMaterialReadOnly();
		
		drawRenderInfoUsingMaterial(renderInfo, mat, IMaterial.TEX_UNIT_ALL);
	}

	private boolean checkIfTargteAcceptsRenderable(IRenderable renderable) {
		IRenderTarget target = graphicsDriver.getRenderTarget();
		if(target == null){
			if(l.isLevelLoggable(Log.ERROR))
				l.log("No rendertarget!", Log.ERROR);
			return false;
		}
		return target.checkIfSupportedByTarget(renderable);
	}

	private void drawRenderInfoUsingMaterial(final IRenderableData renderInfo, final IMaterial mat, int texUnit) {
		try{
			useProgramIfApplicable(mat, mat.getShaderid());
			
			graphicsDriver.setMaterialModeState(mat.getMode());
			graphicsDriver.initMaterialForDrawing(mat, texUnit);
			graphicsDriver.draw(renderInfo);
		}finally{
			useProgramIfApplicable(mat, new Id(0));
		}
	}

	private void useProgramIfApplicable(IMaterial mat, Id id) {
		if(driverIsProgamable && mat.hasProgram()){
			graphicsDriver.useProgram(id);
		}
	}
	
	private final static Options.Val[] skyBoxOptions = RenderConfig.getUndefinedAttributesTemplate();
	static
	{
		skyBoxOptions[RenderConfig.REND_ATTR_BLENDING] = 	Options.Val.OFF;
		skyBoxOptions[RenderConfig.REND_ATTR_DEPTH_TEST] = 	Options.Val.OFF;
		skyBoxOptions[RenderConfig.REND_ATTR_LIGHTNING] = 	Options.Val.OFF;
		skyBoxOptions[RenderConfig.REND_ATTR_FACE_CULL] = 	Options.Val.OFF;	
	}
	
	public void renderVisit(SkyBox skyBox){
		if( !checkIfTargteAcceptsRenderable(skyBox) ){
			return;
		}
		
		final ICamera renderCamera = graphicsDriver.getRenderCamera();
		if(renderCamera != null){
			final Vector3f oldPos = new Vector3f(renderCamera.getPosition());
			final Options.Val[] oldOptions = graphicsDriver.getRenderConfig().getOptionAttributes();
			
			try{
				graphicsDriver.configure(skyBoxOptions);
				// Sky-box should be render from origo
				renderCamera.moveTo(LinjearAlg.ORIGO);
				graphicsDriver.setRenderCamera(renderCamera);
				
				final IRenderableData renderInfo = skyBox.getRenderInfoReadOnly();
				final IMaterial mat = skyBox.getMaterialReadOnly();
				
				final int nrComponents = renderInfo.getNrOfComponents();
				for(int i = 0;  i < nrComponents; i++){
					renderInfo.setActiveComponent(i);
					drawRenderInfoUsingMaterial(renderInfo, mat, IMaterial.TEX_UNIT_0 + i);
				}
			}
			finally{
				// Make sure real camera position is reset
				renderCamera.moveTo(oldPos);
				graphicsDriver.configure(oldOptions);
				graphicsDriver.setRenderCamera(renderCamera);
			}
		}
	}
	
	public void renderVisit(Light l){
		if( !checkIfTargteAcceptsRenderable(l) ){
			return;
		}
		
		graphicsDriver.enableLight(l);
	}
}
