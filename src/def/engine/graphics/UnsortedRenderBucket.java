package def.engine.graphics;

import java.util.AbstractQueue;
import java.util.Collection;
import java.util.concurrent.LinkedBlockingQueue;

import def.engine.graphics.driver.IGraphicsDriver;

/**
 * UnsortedRenderBucket renders its content in one pass without sorting.
 * 
 * Calling renderContent is not thread-safe but add is thread-safe.
 * @author jonas
 *
 */
public class UnsortedRenderBucket implements IRenderBucket {
	
	private final AbstractQueue<IRenderable> renderables;
	private final int prio;

	private IRenderableVisitor rendVisitor = null;
	
	public static UnsortedRenderBucket create(int prio){
		return new UnsortedRenderBucket(prio);
	}
	
	private UnsortedRenderBucket(int prio){
		this.prio = prio;
		this.renderables = new LinkedBlockingQueue<IRenderable>();
	}
	
	private UnsortedRenderBucket(int prio, final AbstractQueue<IRenderable> renderables){
		this.prio = prio;
		this.renderables = new LinkedBlockingQueue<IRenderable>(renderables);
	}

	public int getRenderPrio() {
		return prio;
	}

	public void renderContent(final IGraphicsDriver graphics) {
		if(this.rendVisitor == null){
			this.rendVisitor = new ObjectRenderer(graphics);
		}
		
		for(final IRenderable r : renderables){
			r.acceptRendering(rendVisitor);
		}
	}

	public void add(IRenderable r) {
		renderables.add(r);
	}
	
	public void remove(IRenderable r) {
		renderables.remove(r);
	}

	public void emptyBucket() {
		renderables.clear();
	}
	
	public Collection<IRenderable> getRenderables() {
		return renderables;
	}
	
	public IRenderBucket clone(){
		return new UnsortedRenderBucket(this.prio, this.renderables);
	}	
}
