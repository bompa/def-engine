package def.engine.graphics;

import javax.vecmath.Matrix4f;

import def.Const;
import def.engine.graphics.components.FreePositionable;
import def.engine.graphics.components.IBuildAction;
import def.engine.graphics.components.IMeshComposer;
import def.engine.graphics.components.Mesh;
import def.engine.graphics.driver.IRenderableData;
import def.engine.graphics.material.IMaterial;
import def.shared.interfaces.ITransform;
import def.shared.util.ConcurrentObservable;

public abstract class ARenderable extends ConcurrentObservable<RenderEvent> implements IRenderable {
	
	/**
	 * Should rendering be done every frame or only on request.
	 */
	protected boolean continiousRendering = true;
	
	/**
	 * Name 
	 */
	private String name = "no_name";
	
	/**
	 * The positionable behaviour determines how the renderable is placed in the world.
	 */
	private final ITransform postionableBehaviour;
	
	private float zdepth = 0.0f;
	
	/**
	 * The render info defines all render operations and data for the concrete renderable
	 */
	protected IRenderableData rendInfo;
	
	/**
	 * The buildAction which builds up the vertices for this renderable
	 */
	protected final IBuildAction buildAction;
	
	/**
	 * Create new default buildAction
	 * @return
	 */
	private IBuildAction newBuildAction(){
		return new IBuildAction() {
			int id = IBuildAction.INVALID_BUILD_ACTION_ID;
			
			public void build(IMeshComposer buildComposer) {
				createMesh(buildComposer);
			}

			/**
			 * Let the buildaction be identified by the default id of the implementing subclass 
			 */
			public int getUniqueID() {
				if(id == IBuildAction.INVALID_BUILD_ACTION_ID)
				{
					return IBuildAction.DEFAULT_MODEL_BUILD_ACTION_ID;
				}
				
				return id;
			}

			public void setUniqueID(int id) {
				this.id = id;
			}
		};
	}
	
	public ARenderable(){
		this.postionableBehaviour = new FreePositionable();
		buildAction = newBuildAction();
	}
	public ARenderable(final ITransform postionableBehaviour){
		this.postionableBehaviour = postionableBehaviour;
		buildAction = newBuildAction();
	}
	public ARenderable(final ITransform postionableBehaviour, boolean continious){
		this.continiousRendering = continious;
		this.postionableBehaviour = postionableBehaviour;
		buildAction = newBuildAction();
	}
	public ARenderable(boolean continious){
		this.continiousRendering = continious;
		this.postionableBehaviour = new FreePositionable();
		buildAction = newBuildAction();
	}
	
	/**
	 * Init the renderable with mesh, return new renderinfo created from mesh data
	 * @param mesh
	 * @return renderabledata created from mesh argument
	 */
	public IRenderableData init(Mesh mesh){
		rendInfo = createRenderInfo(mesh);
		if(rendInfo == null)
			throw new RuntimeException("Subclass of AbstractRenderable failed to return a RenderInfo object");
		return rendInfo;
	}
	
	/**
	 * Initialize with existing renderable-data, sharing static buffers 
	 * @param sharedRd
	 */
	public void init(final IRenderableData rendData){
		rendInfo = rendData;
		initSharedRenderInfo(rendInfo);
	}
		
	/**
	 * Implementation of IRenderable interface definition.
	 */
	public void requestRendering(){
		notifyObservers(new RenderEvent(this));
	}

	/**
	 * Implementation of IRenderable interface definition. Returns the model-view matrix of the object,
	 * representing its world transforms.
	 */
	public Matrix4f getWorldTransforms() {
		return postionableBehaviour.getModelMatrix();
	}
	
	public IRenderableData getRenderInfoReadOnly(){
		if(rendInfo != null){
			updateRenderInfo();
			return Wrappers.unmodifiableRenderInfo(rendInfo);
		}
		return null;
	}
	
	public IRenderableData getRenderInfo(){
		if(rendInfo != null){
			updateRenderInfo();
		}
		return rendInfo;
	}

	protected void updateRenderInfo() {
		rendInfo.setModelMatrix(getWorldTransforms());
		rendInfo.setScale(getIPositionable().getScale());
	}
	
	public ITransform getIPositionable(){
		return postionableBehaviour;
	}
	
	public void setContiniousRendering(boolean value) {
		continiousRendering = value;
	}
	
	public boolean getContiniousRendering(){
		return continiousRendering;
	}
	
	/**
	 * Implementation of IRenderable interface definition.
	 */
	public void acceptRendering(IRenderableVisitor renderVisitor) {
		renderVisitor.renderVisit(this);
	}
	
	public IBuildAction getIBuildAction() {
		return buildAction;
	}
	
	/**
	 * Overriden by concrete subclass to define how the mesh should be created. Can be
	 * overriden by subclass if a default construction is wanted, default is empty so the 
	 * renderable has to be created by getIBuildAction.
	 * 
	 * @see getIBuildAction
	 */
	protected void createMesh(IMeshComposer buildComposer){ }
	
	/**
	 * Implemetented by concrete subclass to define a RenderInfo with data about how the renderable should be
	 * rendered.
	 * @return an object filled in with render data.
	 */
	protected abstract IRenderableData createRenderInfo(Mesh mesh);
	
	/**
	 * Implemented to init a renderable-data shared with equivalent implementations
	 * @param rd
	 * @return
	 */
	protected abstract void initSharedRenderInfo(final IRenderableData rd);
	
	private final String UNSUPPORTED_OP = "The class concrete Renderable does not support this op.";

	public IMaterial getMaterial() {
		throw new UnsupportedOperationException(UNSUPPORTED_OP);
	}
	
	public void setMaterial(IMaterial mat) {
		throw new UnsupportedOperationException(UNSUPPORTED_OP);
	}

	public IMaterial getMaterialReadOnly() {
		throw new UnsupportedOperationException(UNSUPPORTED_OP);
	}
	
	public boolean requiresPreparationOfVertexBuffersForRendering() {
		return true;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public boolean isTransparent(){
		if(getMaterial().getProperties().getAlpha() < 1.0f)
			return true;
		return false;
	}
	
	public float getCalculatedZDepth(){
		return zdepth;
	}
	
	public void setCalculatedZDepth(float zdepth){
		this.zdepth = zdepth;
	}
	
	public int getRenderPrio(){
		if(isTransparent())
			return Const.REND_PRIO_TRANSPARENT;
		return Const.REND_PRIO_DEFAULT;
	}
	
	public int compareTo(IRenderable rhs) {
		return (Integer.valueOf(getRenderPrio()).compareTo(rhs.getRenderPrio()));
	}
	
	public String toString(){
		return getName();
	}
}
