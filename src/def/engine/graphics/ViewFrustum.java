package def.engine.graphics;

import def.engine.logic.math.LinjearAlg;
import def.engine.logic.math.MatrixArray4f;
import android.opengl.Matrix;

public class ViewFrustum implements IViewFrustum {
	private float fovy_ , aspectRatio_ , zNear_ , zFar_;
	private MatrixArray4f frustumReprProjectionMtx_ = new MatrixArray4f();
	
	public static ViewFrustum create(float fovy, float aspectRatio, float zNear, float zFar){
		return new ViewFrustum(fovy, aspectRatio, zNear, zFar);
	}
	
	public static ViewFrustum createDefault(float aspectRatio){
		return ViewFrustum.create(45.0f, aspectRatio, 0.1f, 350.0f);
	}
	
	private ViewFrustum(float fovy, float aspectRatio, float zNear, float zFar){
		init(fovy, aspectRatio, zNear, zFar);
		Matrix.setIdentityM(frustumReprProjectionMtx_.getArray(), 0);
	}
	
	public void set(float fovy, float aspectRatio, float zNear, float zFar){
		init(fovy, aspectRatio, zNear, zFar);
	}
	
	private void init(float fovy, float aspectRatio, float zNear, float zFar){
		setFovy(fovy);
		setAspectRatio(aspectRatio);
		setzNear(zNear);
		setzFar(zFar);
	}
	
	public MatrixArray4f getProjectionMatrix(){
		return LinjearAlg.calcualteProjectionMatrix(fovy_, aspectRatio_, zNear_, zFar_);
	}
	
	public MatrixArray4f calcualteProjectionMatrix(float fovy, float aspectRatio, float zNear, float zFar) {
		init(fovy, aspectRatio, zNear, zFar);
		return LinjearAlg.calcualteProjectionMatrix(fovy_, aspectRatio_, zNear_, zFar_);
	}
	
	/* (non-Javadoc)
	 * @see def.engine.graphics.IViewFrustum#setAspectRatio(float)
	 */
	public void setAspectRatio(float aspectRatio_) {
		this.aspectRatio_ = aspectRatio_;
	}
	/* (non-Javadoc)
	 * @see def.engine.graphics.IViewFrustum#getAspectRatio()
	 */
	public float getAspectRatio() {
		return aspectRatio_;
	}
	
	/* (non-Javadoc)
	 * @see def.engine.graphics.IViewFrustum#setFovy(float)
	 */
	public void setFovy(float fovy_) {
		this.fovy_ = fovy_;
	}
	/* (non-Javadoc)
	 * @see def.engine.graphics.IViewFrustum#getFovy()
	 */
	public float getFovy() {
		return fovy_;
	}
	
	/* (non-Javadoc)
	 * @see def.engine.graphics.IViewFrustum#setzNear(float)
	 */
	public void setzNear(float zNear_) {
		this.zNear_ = zNear_;
	}
	/* (non-Javadoc)
	 * @see def.engine.graphics.IViewFrustum#getzNear()
	 */
	public float getzNear() {
		return zNear_;
	}
	
	/* (non-Javadoc)
	 * @see def.engine.graphics.IViewFrustum#setzFar(float)
	 */
	public void setzFar(float zFar_) {
		this.zFar_ = zFar_;
	}
	/* (non-Javadoc)
	 * @see def.engine.graphics.IViewFrustum#getzFar()
	 */
	public float getzFar() {
		return zFar_;
	}
}
