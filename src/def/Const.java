package def;

public class Const {
	public final static String SHAPE_MODEL = "Model";
	public final static String SHAPE_SKYBOX = "SkyBox";
	public final static String SHAPE_SQUARE = "Square";
	public final static String SHAPE_BB = "BbShapeExpl";
	public final static String SHAPE_CUBE = "Cube";
	public final static String SHAPE_LIGHT1 = "Light";
	public final static String SHAPE_LINES = "Lines";
	public final static String SHAPE_EMITTING_SQUARE = "Self-Emitting-Square";
	public final static String SHAPE_EMITTING_CUBE = "Self-Emitting-Cube";
	public final static String SHAPE_LIGHT_VISUAL_REPRESENTATION_NAME = "-Self-Emitting-Cube";
	
	public final static int REND_PRIO_TRANSPARENT 	= 2000;
	public final static int REND_PRIO_DEFAULT 		= 200;
	public final static int REND_PRIO_SKYBOX		= 1;
	public final static int REND_PRIO_LIGHT			= 0;
	public final static int REND_PRIO_RTT_TARGET 	= -1;
	
	public final static int SHADER_DEF_NR_OF_LIGHTS	= 6;
	public final static float LIGHT_REPR_SCALE 		= 2.0f;
	
	public static final int FLOAT_BYTE_SIZE = Float.SIZE / Byte.SIZE;
	public static final int SHORT_BYTE_SIZE = Short.SIZE / Byte.SIZE;
}
