package def.network;

import android.content.Context;

public interface INetworkLayer {
	public void init(Context a);
	public void start();
	public void stop();
}
