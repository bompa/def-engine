package def.network.basic;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.DatagramSocket;
import java.net.Socket;
import java.util.ArrayList;


/**
 * Representing an open connection.
 *
 * @param <E>
 */
public class ConnectionAction {
	/**
	 * Send buffer on connection.
	 * 
	 * @param <E>
	 * @param b
	 * @param con
	 */
	static void send(IBuffer buff, IConnection<Socket> con){
		Socket sock = con.getSocket();
		try {
			//DefLogger.getLogger().log("Sending Tcp packet...\n");
			OutputStream out = sock.getOutputStream();
			out.write(buff.getBuffer(), 0, buff.getBuffer().length);
		} catch (IOException e) {
			//DefLogger.getLogger().log("Send error");
			e.printStackTrace();
		}
	}
	
	static void sendDatagram(IBuffer buff, IConnection<DatagramSocket> con){
		DatagramSocket s = con.getSocket();
		return;
	}


	/**
	 * Receive data in return buffer, using the supplied connection con.
	 * @param <E>
	 * @param con
	 * @return
	 */
	static<SocketT> IBuffer recv(IConnection<SocketT> con){
		return new TransportBuffer(new byte[10]);
	}
}
