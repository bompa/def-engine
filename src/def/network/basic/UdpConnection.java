package def.network.basic;

import java.io.IOException;
import java.net.DatagramSocket;

public class UdpConnection implements IConnection<DatagramSocket> {
	
	private final DatagramSocket socket_;
	
	UdpConnection(int port) throws IOException{
		socket_ = new DatagramSocket(port);
	}

	public void connect() {
		// TODO Auto-generated method stub

	}

	public void disconnect() {
		// TODO Auto-generated method stub

	}

	public ConnectionStatus status() {
		// TODO Auto-generated method stub
		return ConnectionStatus.DISCONNECTED;
	}

	public DatagramSocket getSocket() {
		return socket_;
	}
}
