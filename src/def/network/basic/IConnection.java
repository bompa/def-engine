package def.network.basic;

import java.io.IOException;

public interface IConnection<SocketT> {
	enum ConnectionStatus{ CONNECTED, DISCONNECTED, PENDING}
	void connect() throws IOException;
	void disconnect();
	SocketT getSocket();
	ConnectionStatus status();
}
