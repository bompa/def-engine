package def.network.basic;

public interface IBuffer {
	byte[] getBuffer();
	void setBuffer(byte[] buffer);
}
