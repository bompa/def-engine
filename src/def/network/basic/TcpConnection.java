package def.network.basic;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class TcpConnection implements IConnection<Socket>{
	
	private final Socket socket_;
	private final ConnectionInfo cInfo_;
	
	public TcpConnection(ConnectionInfo cInfo) {
		socket_ = new Socket();
		cInfo_ = cInfo;
	}

	public void connect() throws IOException {
		socket_.connect(new InetSocketAddress(cInfo_.getAddress(), cInfo_.getPort()));
		
	}

	public void disconnect() {
		// TODO Auto-generated method stub
		
	}

	public Socket getSocket() {
		return socket_;
	}

	public ConnectionStatus status() {
		// TODO Auto-generated method stub
		return null;
	}

}
