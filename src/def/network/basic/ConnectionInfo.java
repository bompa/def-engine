package def.network.basic;

public class ConnectionInfo {
	public ConnectionInfo(String address, int port){
		address_ = address;
		port_ = port;
	}
	
	public String getAddress(){
		return address_;
	}
	public int getPort(){
		return port_;
	}
	
	private final String address_;
	private final int port_;
		
}
