package def.network.basic;

public class NetworkOptions {
	public final static int ClientUdpPort = 10000;
	
	public final static int TestServerTcpPort = 10000;
	public final static String TestServerTcpAddr = "127.0.0.1";
}
