package def.network.basic;

import java.util.ArrayList;
import java.util.Arrays;


public final class TransportBuffer implements IBuffer{
	private byte[] buffer_;
	
	public TransportBuffer(byte[] buffer){
		this.buffer_ = buffer;
	}
	
	public byte[] getBuffer(){
		return buffer_; 
	}
	public void setBuffer(byte [] buffer) {
		buffer_ = buffer;
	}
}
