package def.network;

import java.io.IOException;

import def.network.basic.ConnectionInfo;
import def.network.basic.NetworkOptions;
import def.network.basic.TcpConnection;
import def.util.LogHandler;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class NetworkService extends Service {
	public static final String NETWORK_DEF_SERVICE = "def.network.NETWORK_DEF_SERVIC";
	
	private LogHandler l_ = LogHandler.createInstanceShared();
	private final TcpConnection tcpc_ = new TcpConnection(new ConnectionInfo(
			NetworkOptions.TestServerTcpAddr, NetworkOptions.TestServerTcpPort));
	
	@Override
	public void onStart(Intent intent, int startId){
		l_.log("Network service started...");
	}
	
	@Override
	public void onCreate(){
		try {
			tcpc_.connect();
		} catch (IOException e) {
			l_.log("Connection in network service failed");
			e.printStackTrace();
		}
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}
