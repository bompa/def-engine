package def.network;


import android.content.Context;
import android.content.Intent;

public class DefNetworkLayer implements INetworkLayer {
	
	private final NetworkService networkService_ = new NetworkService();

	public void init(Context owner) {
		Intent intent = new Intent();
    	intent.addCategory(NetworkService.NETWORK_DEF_SERVICE);
    	intent.setClass(owner, NetworkService.class);
    	owner.startService(intent);
	}

	public void start() {
	}

	public void stop() {
		networkService_.stopSelf();
	}

}
