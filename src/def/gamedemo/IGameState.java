package def.gamedemo;

import def.engine.control.ITouchControllerManager;

public interface IGameState {
	public void setStateActive(ITouchControllerManager touchManager);
	public void processState(StateEvent e);
}
