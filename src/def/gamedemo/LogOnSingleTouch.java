package def.gamedemo;

import android.view.MotionEvent;
import def.engine.control.AHandleSingleTouch;
import def.util.FloatPair;
import def.util.LogHandler;

public class LogOnSingleTouch extends AHandleSingleTouch{
	private final LogHandler l = LogHandler.createInstanceShared();
	
	private final String message;
	
	public static AHandleSingleTouch create(final String msg){
		return new LogOnSingleTouch(msg);
	}
	
	private LogOnSingleTouch(final String msg){
		this.message = msg;
	}

	public void singleTouch(FloatPair xy, FloatPair previousXy,
			long timeSinceLastSingleTouch) {
		l.log("[" + xy.first + ", " + xy.second + "]: " +message);
	}

	public void singleTouch(MotionEvent e) {
		l.log(message);
	}

	public void up() {}

}
