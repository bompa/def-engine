package def.gamedemo;

import android.view.MotionEvent;
import def.engine.control.AHandleSingleTouch;
import def.engine.logic.math.LinjearAlg;
import def.shared.interfaces.ITransform;
import def.shared.math.Radian;
import def.util.FloatPair;

public class RotateObjectOnSingleTouch extends AHandleSingleTouch {
	private final ITransform controlledObject;
	private static final float ROT_MP = 0.15f;
	private long maxTimeSinceLastTouchToRotateMs = 500;
	
	public static RotateObjectOnSingleTouch create(ITransform positionable){
		return new RotateObjectOnSingleTouch(positionable);
	}
	
	private RotateObjectOnSingleTouch(ITransform positionable){
		if(positionable == null)
			throw new IllegalArgumentException("OnTouchObjectController requires an object to controll, can not be null. positionable=null.");
		
		controlledObject = positionable;
	}

	public void singleTouch(final FloatPair xy, final FloatPair previousXy, long timeSinceLastSingleTouch) {		
		if(timeSinceLastSingleTouch < maxTimeSinceLastTouchToRotateMs){
			float dx = xy.first - previousXy.first;
			float dy = xy.second - previousXy.second;
			controlledObject.rotate(LinjearAlg.X_AXIS, new Radian(dy * ROT_MP));
			controlledObject.rotate(LinjearAlg.Y_AXIS, new Radian(dx * ROT_MP));
		}
	}

	public void up() {}

	public void singleTouch(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}
}
