package def.gamedemo;

import javax.vecmath.Vector3f;

import android.view.MotionEvent;

import def.engine.control.ICamera;
import def.engine.control.AHandleSingleTouch;
import def.engine.logic.math.LinjearAlg;
import def.util.FloatPair;

public class ControlCameraOnSingleTouch extends AHandleSingleTouch {
	private final ICamera cam;
	private static final float MOV_MP = 0.05f;
	private long maxTimeSinceLastTouchToRotateMs = 500;
	
	private final Vector3f xmov = new Vector3f();
	private final Vector3f ymov = new Vector3f();
	
	public static ControlCameraOnSingleTouch create(ICamera cam){
		return new ControlCameraOnSingleTouch(cam);
	}
	
	private ControlCameraOnSingleTouch(ICamera cam){
		if(cam == null)
			throw new IllegalArgumentException("ControlCameraOnSingleTouch requires a camera to controll, can not be null. cam=null.");
		
		this.cam = cam;
	}

	public void singleTouch(final FloatPair xy, final FloatPair previousXy, long timeSinceLastSingleTouch) {		
		if(timeSinceLastSingleTouch < maxTimeSinceLastTouchToRotateMs){
			float dx = xy.first - previousXy.first;
			float dy = xy.second - previousXy.second;
			
			xmov.scale(dx * MOV_MP, LinjearAlg.X_AXIS);
			ymov.scale(dy * MOV_MP, LinjearAlg.Y_AXIS);
			xmov.add(ymov);
			cam.moveRel(xmov);
		}
	}

	public void up() {}

	public void singleTouch(MotionEvent e) {
		// TODO Auto-generated method stub
		
	}
}
