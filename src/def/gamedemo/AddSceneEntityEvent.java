package def.gamedemo;

import javax.vecmath.Vector3f;

import def.engine.graphics.IRenderable;
import def.engine.physics.ShapeInfo;
import def.engine.scene.IRenderableSceneModiyEvent;
import def.engine.scene.ISceneModifyEvent;
import def.engine.scene.SceneFacade;
import def.engine.scene.IRenderableCreator;

/**
 * Event handled by the scene. Used to indicate the creation of a renderable shape, to be
 * added to the physical simulation.
 * @author jonas
 */
public class AddSceneEntityEvent implements ISceneModifyEvent{
	
	private final IRenderableSceneModiyEvent addRenderableEvent;
	private final PhysicalLightCubesPlayState playState;
	private long entityLifetime = SceneEntity.SCENE_ENT_INVALID_LIFETIME;
	private float entityScale = SceneEntity.SCENE_ENT_DEFAULT_SCALE;
	
	/**
	 * Create scene entity 
	 * @see AddSceneEntityEvent
	 */
	public static AddSceneEntityEvent createPhysRenderableDefault(	final PhysicalLightCubesPlayState ps, 
																	final String mn, 
																	final Vector3f p)
	{
		final IRenderableSceneModiyEvent ev = SceneFacade.createAddPhysicalRendableWithGeneratedNameEvent(mn, p);
		return new AddSceneEntityEvent(ps, mn, p, ev);
	}
	
	/**
	 * Create renderable attached to scene entity, customized according to ev and rendCreate
	 * 
	 * @param ps the PlayState managing the entities in the playing state
	 * @param mn name of the model to create a renderable of
	 * @param p inital position in world coords for the renderable
	 * @param ev the scene event implementation that creates the renderable
	 * @param rendCreate how the holds how the renderable vertex data should be composed, null means model default is used
	 * @return
	 */
	public static AddSceneEntityEvent createCustom(	final PhysicalLightCubesPlayState ps, 
													final String mn, 
													final Vector3f p,
													final IRenderableSceneModiyEvent ev,
													final IRenderableCreator rendCreate)
	{
		return new AddSceneEntityEvent(ps, mn, p, ev).setRenderableCreatorChainCall(rendCreate);
	}
	
	/**
	 * Create add-scene-entity event creatign a static shape, not added to physics sim.
	 * @see createCustom
	 */
	public static AddSceneEntityEvent createStaticCustom(	final PhysicalLightCubesPlayState ps, 
															final String mn, 
															final Vector3f p,
															final IRenderableCreator rendCreate)
	{
		final IRenderableSceneModiyEvent ev = SceneFacade.createAddStaticShapeEvent(mn, p, rendCreate);
		return new AddSceneEntityEvent(ps, mn, p, ev).setRenderableCreatorChainCall(rendCreate);
	}
	
	/**
	 * Create physics enabled renderable attached to scene entity customized according to rendCreate
	 * @see createCustom
	 * 
	 * @return
	 */
	public static AddSceneEntityEvent createWithPhysCustomDefaultShapeInfo(	
															final PhysicalLightCubesPlayState ps, 
															final String mn, 
															final Vector3f p, 
															final IRenderableCreator rendCreate)
	{
		final IRenderableSceneModiyEvent ev = SceneFacade.createAddPhysicalRendableWithGeneratedNameEvent(mn, p);
		return createCustom(ps, mn, p, ev, rendCreate);
	}
	
	/**
	 * Create physics enabled renderable attached to scene entity customized according to rendCreate and shapeInfo
	 * @see createCustom
	 * 
	 * @param shapeInfo shape for physics calculations
	 * @return
	 */
	public static AddSceneEntityEvent createWithPhysCustom(	final PhysicalLightCubesPlayState ps, 
															final String mn, 
															final Vector3f p,
															final ShapeInfo shapeInfo,
															final IRenderableCreator rendCreate)
	{
		final IRenderableSceneModiyEvent ev = SceneFacade.createAddPhysicalRendableWithGeneratedNameEvent(mn, p);
		ev.setShapeInfo(shapeInfo);
		return createCustom(ps, mn, p, ev, rendCreate);
	}
	
	/**
	 * Create renderable, beeing built with default vertex data, and adds it to playstate as scene-entity.
	 * @see AddSceneEntityEvent
	 */
	public static AddSceneEntityEvent createWithPhysDefault(	final PhysicalLightCubesPlayState ps, 
																final String mn, 
																final Vector3f p)
	{
		final IRenderableSceneModiyEvent ev = SceneFacade.createAddPhysicalRendableWithGeneratedNameEvent(mn, p);
		return new AddSceneEntityEvent(ps, mn, p, ev);
	}
	
	/**
	 * private Constructor
	 * 
	 * @param ps the PlayState managing the entities in the playing state
	 * @param mn name of the model to create a renderable of
	 * @param p inital position in world coords for the renderable
	 * @param ev the scene event implementation that creates the renderable
	 * 
	 * @see createWithPhysCustom, createCustom
	 */
	private AddSceneEntityEvent(	final PhysicalLightCubesPlayState ps, 
									final String mn, 
									final Vector3f p,
									final IRenderableSceneModiyEvent ev)
	{
		if(ps == null || p == null || mn == null)
			throw new IllegalArgumentException();
		
		playState = ps;
		addRenderableEvent = ev;
	}
	
	private AddSceneEntityEvent(final PhysicalLightCubesPlayState ps, final String n, final String mn, final Vector3f p){
		if(p == null || mn == null)
			throw new IllegalArgumentException();
		
		playState = ps;
		addRenderableEvent = SceneFacade.createAddPhysicalRendableEvent(n, mn, p);
	}
	
	private AddSceneEntityEvent setRenderableCreatorChainCall(IRenderableCreator rendCreator) {
		setRenderableCreator(rendCreator);
		return this;
	}
	
	public void setRenderableCreator(IRenderableCreator rendCreator) {
		addRenderableEvent.setRenderableCreator(rendCreator);
	}

	public void modifyScene(SceneFacade sceneHandler) {
		SceneEntity ent = playState.addSceneEntity();
		if(ent != null){
			addRenderableEvent.setPhysicsModifyDoneCallback(ent);
			addRenderableEvent.modifyScene(sceneHandler);
			final IRenderable r = addRenderableEvent.getRenderable();
			r.getIPositionable().setScale(this.entityScale);
			ent.setEntityRenderable(r);
			addRenderableEvent.updateModification(ent);
			
			if(entityLifetime != SceneEntity.SCENE_ENT_INVALID_LIFETIME){
				ent.setLifetime(entityLifetime);
			}
		}
	}

	public void initSceneModification(SceneFacade sceneHandler) {
		addRenderableEvent.initSceneModification(sceneHandler);
	}
	
	/**
	 * Set scaling of entity renderable and physics part(if any)
	 * @param scale
	 * @return
	 */
	public AddSceneEntityEvent setEntityScale(float scale) {
		this.entityScale = scale;
		return this;
	}

	/**
	 * Set the lifetime in milliseconds of the entity being created by this event
	 * 
	 * @param entityLifetime
	 * @return
	 */
	public AddSceneEntityEvent setEntityLifetime(long entityLifetime) {
		this.entityLifetime = entityLifetime;
		return this;
	}

	public long getEntityLifetime() {
		return entityLifetime;
	}
}
