package def.gamedemo;

import javax.vecmath.Vector3f;

import def.engine.control.AHandleMultiTouch;
import def.shared.interfaces.ITransform;
import def.util.FloatPair;

public class MoveObjectOnMultiTouch extends AHandleMultiTouch {

	private final ITransform controlledObject;
	private static final float MOV_MP = 0.1f;
	private long maxTimeSinceLastTouchToRotateMs = 500;
	
	public static MoveObjectOnMultiTouch create(ITransform positionable){
		return new MoveObjectOnMultiTouch(positionable);
	}
	
	private MoveObjectOnMultiTouch(ITransform positionable){
		if(positionable == null)
			throw new IllegalArgumentException("OnTouchObjectController requires an object to controll, can not be null. positionable=null.");
		
		controlledObject = positionable;
	}

	public void multiTouch(
			FloatPair xyOne, FloatPair previousXyOne,
			FloatPair xyTwo, FloatPair previousXyTwo,
			long timeSinceLastSingleTouch) {

		if(timeSinceLastSingleTouch < maxTimeSinceLastTouchToRotateMs){
			float dy = xyTwo.second - previousXyTwo.second;
			controlledObject.move(new Vector3f(0, 0, dy * MOV_MP));
		}
	}

	public void up() {
		
	}
}
