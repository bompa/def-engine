package def.gamedemo;

import android.view.MotionEvent;
import def.engine.control.ICamera;
import def.engine.control.AHandleMultiTouch;
import def.engine.control.AHandleSingleTouch;
import def.engine.control.IHandleTouch;
import def.engine.control.ConfigureableTouchListener.DispatchConfigType;
import def.engine.scene.SceneFacade;
import def.shared.interfaces.ITransform;
import def.util.FloatPair;

public class HandleTouchFactory {
	public static AHandleMultiTouch createMoveObject(ITransform controlled){
		return MoveObjectOnMultiTouch.create(controlled);
	}
	
	public static IHandleTouch createPhysEntitySpawner(final PhysicalLightCubesPlayState playState, final SceneFacade sceneHandler){
		return SpawnEntityOnMultiTouch.create(playState, sceneHandler);
	}
	
	public static AHandleSingleTouch createPhysEntitySpawnerSingleTouch(final PhysicalLightCubesPlayState playState, final SceneFacade sceneHandler){
		
		return new AHandleSingleTouch() {
			final SpawnEntityOnMultiTouch spawner = SpawnEntityOnMultiTouch.create(playState, sceneHandler);	
			public void up() { spawner.up(); }
			public void singleTouch(FloatPair xy, FloatPair previousXy, long timeSinceLastSingleTouch) { spawner.singleTouch(xy, previousXy, timeSinceLastSingleTouch); }
			public void singleTouch(MotionEvent e) { }
			public boolean getDispatchConfigOpt(DispatchConfigType type) { return false; }
		};
	}
	
	public static AHandleMultiTouch createDetectMultiTouchType(final IHandleTouch dispOnOneMove, final IHandleTouch dispOnTwoMove){
		return DetectOneOrTwoMoveMultiTouch.create(dispOnOneMove, dispOnTwoMove);
	}
	
	public static AHandleMultiTouch createZoomOnMultiMoveOnSingle(final ICamera controlled){
		return createDetectMultiTouchType(createCtrlCamera(controlled), createZoomCamera(controlled));
	}
	
	public static AHandleSingleTouch createRotateObject(ITransform controlled){
		return RotateObjectOnSingleTouch.create(controlled);
	}
	
	public static AHandleSingleTouch createCtrlCamera(ICamera controlled){
		return ControlCameraOnSingleTouch.create(controlled);
	}
	
	public static AHandleMultiTouch createZoomCamera(ICamera controlled){
		return ZoomCameraOnMultiTouch.create(controlled);
	}
}
