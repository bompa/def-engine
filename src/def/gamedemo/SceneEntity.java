package def.gamedemo;

import def.engine.graphics.IRenderable;
import def.engine.graphics.material.IMaterial;
import def.engine.physics.IPhysicsSimModifiedCallback;
import def.engine.scene.SceneFacade;
import def.shared.util.TimeMeasurement;

public class SceneEntity implements IPhysicsSimModifiedCallback{
	public final static long SCENE_ENT_DEFAULT_LIFETIME_MS = 6000;
	public final static float SCENE_ENT_DEFAULT_DELTA_ALPHA_FADE = 0.01f;
	public final static long SCENE_ENT_INVALID_LIFETIME = -1;
	public final static float SCENE_ENT_DEFAULT_SCALE = 1.0f;
	public final static int SCENE_ENT_DEFAULT_HEALTH = 100;
	public final static int SCENE_ENT_DELTA_HEALTH = 4;
	
	private long lifetime;
	private IRenderable entityRenderable;
	private Object entityBody;
	
	private long age;
	private int health = SCENE_ENT_DEFAULT_HEALTH;
	private float alphaFadeDelta = SCENE_ENT_DEFAULT_DELTA_ALPHA_FADE;
	
	private final TimeMeasurement agingTimer = TimeMeasurement.create();
	private boolean killed = false;
	
	public static SceneEntity createWithDefaultLifeTime(){
		return new SceneEntity(null);
	}
	
	public static SceneEntity createWithDefaultLifeTime(final IRenderable entityRenderable){
		return new SceneEntity(entityRenderable);
	}
	
	public static SceneEntity createWithLifeTime(long lifetime){
		return new SceneEntity(lifetime, null);
	}
	
	private SceneEntity(final IRenderable entityRenderable){
		this.lifetime = SCENE_ENT_DEFAULT_LIFETIME_MS;
		this.entityRenderable = entityRenderable;
		init();
	}
	
	private SceneEntity(final long lifetime, final IRenderable entityRenderable){
		if(lifetime < 0)
			throw new IllegalArgumentException();

		this.lifetime = lifetime;
		this.entityRenderable = entityRenderable;
		init();
	}
	
	private void init(){
		agingTimer.timeStamp();
	}
	
	/**
	 * Return if died
	 * @return true if has died of old age
	 */
	public boolean hasDied(){
		return age >= lifetime;
	}
	
	/**
	 * Checks if health if below zero
	 * @return true if has run out of health
	 */
	public boolean outOfHealth(){
		return health <= 0;
	}
	
	public void kill(){
		killed = true;
		age = lifetime;
		health = -1;
	}
	
	public void setLifetime(long lifetime){
		if(lifetime < 0)
			throw new IllegalArgumentException("Entity lifetime illegal value: < 0");
		
		this.lifetime = lifetime;
	}
	
	/**
	 * Uses internal timer to age the entity
	 * @return true if entity is not visible anymore, false otherwise
	 */
	public boolean ageEntity(){
		agingTimer.timeStamp();
		age += agingTimer.getPeriodTimeMs();
		if(hasDied()){
			final IMaterial mat = entityRenderable.getMaterial();
			if(mat == null || mat.getProperties() == null)	// If no material, nothing to fade
				return true;
			final float alpha = mat.getProperties().getAlpha(); 
			mat.getProperties().setAlpha(alpha - alphaFadeDelta);
			if(alpha <= 0.0f)
				return true;
		}
		return false;
	}
	
	public void stopRendering(final SceneFacade sceneFacade){
		if(entityRenderable != null)
			entityRenderable.setContiniousRendering(false);
		sceneFacade.removeFromPhysicsSimulationAsync(getPhysicalBody());
		sceneFacade.removeRenderableFromScene(entityRenderable);
	}

	public void setAlphaDeltaValue(float alphaValue) {
		this.alphaFadeDelta = alphaValue;
	}
	public float getAlphaDeltaValue() {
		return this.alphaFadeDelta;
	}

	public long getAge() {
		return age;
	}
	
	
	public IRenderable getRenderable(){
		return entityRenderable;
	}
	
	public void setEntityRenderable(final IRenderable entityRenderable){
		this.entityRenderable = entityRenderable;
	}

	public Object getPhysicalBody(){
		return entityBody;
	}

	/**
	 * Callback from physics simulation when the physics part of this entity is added to the simulation 
	 */
	public void modificationDone(Object modifiedbodyRefID) {
		entityBody = modifiedbodyRefID;
	}
	
	public boolean decreaseHealth() {
		return decreaseHealth(SCENE_ENT_DELTA_HEALTH);
		
	}
	
	public boolean decreaseHealth(int delta) {
		health -= delta;
		return outOfHealth();
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getHealth() {
		return health;
	}

	public boolean isKilled() {
		return killed;
	}
}
