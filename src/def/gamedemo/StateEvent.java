package def.gamedemo;

import def.engine.control.ISensorInput;

public class StateEvent {
	
	private final ISensorInput sensors;
	
	public static StateEvent create(ISensorInput sensors){
		return new StateEvent(sensors);
	}
	
	private StateEvent(ISensorInput sensors){
		if(sensors == null)
			throw new IllegalArgumentException("Sensor argument null in StateEvent");
		
		this.sensors = sensors;
	}

	public ISensorInput getSensors() {
		return sensors;
	}

}
