package def.gamedemo;

import java.security.SecureRandom;

import javax.vecmath.Vector3f;

import android.view.MotionEvent;

import def.Const;
import def.engine.control.IHandleTouch;
import def.engine.control.ConfigureableTouchListener.DispatchConfigType;
import def.engine.graphics.IRenderable;
import def.engine.graphics.components.AndroidResourceModelBuildAction;
import def.engine.graphics.material.Texture;
import def.engine.physics.ShapeInfo;
import def.engine.physics.ShapeInfo.Type;
import def.engine.scene.ARenderableModelNameCreator;
import def.engine.scene.IRenderableCreator;
import def.engine.scene.RenderableVertexBuilderCreator;
import def.engine.scene.SceneFacade;
import def.gamedemo.Game.SpawnablePhysObjectType;
import def.ui.R;
import def.util.FloatPair;

public class SpawnEntityOnMultiTouch implements IHandleTouch{
	private final SceneFacade sceneFacade;
	private final PhysicalLightCubesPlayState playState;
	private SpawnablePhysObjectType objectType = SpawnablePhysObjectType.Undefined;
	
	public static SpawnEntityOnMultiTouch create(final PhysicalLightCubesPlayState playState, final SceneFacade sf){
		return new SpawnEntityOnMultiTouch(playState, sf);
	}
	
	private SpawnEntityOnMultiTouch(final PhysicalLightCubesPlayState playState, final SceneFacade sf){
		this.sceneFacade = sf;
		this.playState = playState;
	}

	public void multiTouch(	FloatPair xyOne, FloatPair previousXyOne,
							FloatPair xyTwo, FloatPair previousXyTwo,
							long timeSinceLastSingleTouch) 
	{	
		singleTouch(xyOne, previousXyOne, timeSinceLastSingleTouch);
	}

	public void up() {}

	public void singleTouch(FloatPair xy, FloatPair previousXy,
							long timeSinceLastSingleTouch) 
	{		
		final SpawnObject spawnObject = getSpawnObject(); 
		
		sceneFacade.modifySceneAsync(
			AddSceneEntityEvent.createWithPhysCustom(
				playState, 
				spawnObject.shapeModelName,
				getRandomizedPosition(),
				spawnObject.shapeInfo, 
				spawnObject.renderableCreator
			).setEntityScale(Const.LIGHT_REPR_SCALE)
		);
	}

	private Vector3f getRandomizedPosition() {
		SecureRandom randGen = new SecureRandom();
		float randomX = randGen.nextFloat();
		float randomY = randGen.nextFloat();
		float randomZ = randGen.nextFloat();
		
		return new Vector3f(0 + randomX * 2, 10 + randomY, -10 + randomZ * 2);
	}

	private static class SpawnObject{
		String shapeModelName;
		ShapeInfo shapeInfo;
		IRenderableCreator renderableCreator;
	}
	
	private SpawnObject getSpawnObject() {
		final SpawnObject spawn = new SpawnObject();
		
		switch(getSpawnObjectType()){
			case Astroid:
				final float radius = 2.0f;
				spawn.renderableCreator = RenderableVertexBuilderCreator.create(
						AndroidResourceModelBuildAction.create(R.raw.asteroid200origo)
					).setTexture(Texture.create(R.drawable.wall_2));
				spawn.shapeInfo = ShapeInfo.create(Type.SH_SPHERE, new float[]{radius});
				spawn.shapeModelName = Const.SHAPE_MODEL;
				break;
			default:	// Fall-through
			case Cube:
				spawn.renderableCreator = new ARenderableModelNameCreator() {
					public IRenderable create(SceneFacade sceneHandlerFacade, String modelName) {
						return sceneHandlerFacade.addRenderableLight(modelName);
					}
				};
				spawn.shapeInfo = ShapeInfo.create(Type.SH_CUBE, new float[]{Const.LIGHT_REPR_SCALE, Const.LIGHT_REPR_SCALE, Const.LIGHT_REPR_SCALE});
				spawn.shapeModelName = Const.SHAPE_LIGHT1;
				break;
		}
		
		return spawn;
	}

	private SpawnablePhysObjectType getSpawnObjectType() {
		if(objectType != SpawnablePhysObjectType.Undefined){
			return objectType;
		}
		
		SecureRandom randGen = new SecureRandom();
		float random = randGen.nextFloat();
		if(random <= 0.5f){
			return SpawnablePhysObjectType.Astroid;
		}
			
		return SpawnablePhysObjectType.Cube;
	}

	public void singleTouch(MotionEvent e) { }
	public void multiTouch(MotionEvent e) { }
	public boolean getDispatchConfigOpt(DispatchConfigType type) { return false; }

	public SpawnablePhysObjectType getObjectSpawnType() {
		return objectType;
	}

	public void setObjectSpawnType(SpawnablePhysObjectType objectType) {
		this.objectType = objectType;
	}
}
