package def.gamedemo;

import javax.vecmath.Vector3f;

import def.shared.interfaces.ITransform;
import def.ui.R;
import android.os.Bundle;
import def.Const;
import def.engine.graphics.ACameraRenderer;
import def.engine.graphics.IRenderable;
import def.engine.graphics.RenderTargetTexture;
import def.engine.graphics.components.ANonCachableBuildAction;
import def.engine.graphics.components.IMeshComposer;
import def.engine.graphics.components.Square;
import def.engine.graphics.driver.FrameBufferData;
import def.engine.graphics.driver.FrameBufferObject;
import def.engine.graphics.material.IMaterial;
import def.engine.graphics.material.ITexture;
import def.engine.graphics.material.Materials;
import def.engine.graphics.material.IMaterial.Properties;
import def.engine.graphics.resources.ResourceManager;
import def.engine.scene.ContentConstruction;
import def.engine.scene.ILevel;
import def.engine.scene.SceneFacade;
import def.util.IntPair;
import def.util.LogHandler;
import def.util.Util.NameGenerator;
import android.util.Pair;

public class CubeSkyboxRttDemoLevel implements ILevel {
	
	private final LogHandler l = LogHandler.createInstanceShared();
	
	/**
	 * The starting position for the camera.
	 */
	private static final Vector3f CAMERA_INIT_POS = new Vector3f(0, 0, 1.5f);
	public static final Vector3f GROUND_INIT_POS = new Vector3f(0, -5.0f, 5);

	/**
	 * Inital shapes the scene is created with - selectable.
	 */
	private static final String[] IN_SHAPES = {Const.SHAPE_CUBE};
	/**
	 * Inital ligts the scene is created with - selectable.
	 */
	private static final String[] IN_LIGHTS = {};//{Const.SHAPE_LIGHT1};
	
	/**
	 * Get the inital ligts the scene is created with.
	 */
	public static String[] getInitialSceneLights(){
		return IN_LIGHTS;
	}
	
	/**
	 * Get the inital shapes the scene is created with.
	 */
	public static String[] getInitialSceneShapes(){
		return IN_SHAPES;
	}
	
	/**
	 * Get the inital objects (shapes, lights, ...) the scene is created with.
	 */
	public String[] getInitialSceneContent(){
		final String[] concatinated = new String[IN_SHAPES.length + IN_LIGHTS.length];
		System.arraycopy(IN_SHAPES, 0, concatinated, 0, IN_SHAPES.length);
		System.arraycopy(IN_LIGHTS, 0, concatinated, IN_SHAPES.length, IN_LIGHTS.length);
		return concatinated;
	}
	
	public void createScene(	final SceneFacade sceneFacade,
								final ACameraRenderer renderer, 
								final GameStateManager gameManager,
								final IntPair winSize,
								final Bundle extras)
	{
		
		final ResourceManager matMgr = sceneFacade.getResourceManager();
		sceneFacade.getCamera().moveTo(CAMERA_INIT_POS);
		
		ContentConstruction.createSkybox(sceneFacade);
		//final IPositionable controlled = initSceneContent(sceneFacade, extras);
		//attachLightAsChildNodeToObject(controlled, sceneFacade);
		ContentConstruction.createGroundShape(sceneFacade);
		
		createSquareRTT(sceneFacade, matMgr, winSize);
		
		// Create billboard with repeatedly animated texture
		createBillboardAnimation(sceneFacade, matMgr);

		IGameState playstate = PhysicalLightCubesPlayState.create(sceneFacade, null);
		gameManager.addAndActivateGameState("Playing", playstate);
	}

	private void createSquareRTT(final SceneFacade sceneFacade, final ResourceManager matMgr, IntPair renderWinsSz) {

		final Pair<Integer, ITexture> t = matMgr.createTextureCustom(renderWinsSz.first, renderWinsSz.second, ITexture.RenderType.TEX_RTT);
		
		final IMaterial material = Materials.createBasicTexturingNoLightning(t.second);
		
		final float scaledW = (float)renderWinsSz.first * 2.0f / 1000.0f; 
		final float scaledH = (float)renderWinsSz.second * 2.0f / 1000.0f;
		IRenderable r = sceneFacade.addRenderableShape(Const.SHAPE_SQUARE, new ANonCachableBuildAction(){
				public void build(IMeshComposer buildComposer) {
					Square.createMesh(buildComposer, material, scaledW, scaledH);
				}
			}, material); 

		r.setName("RTT-square");
		
		final RenderTargetTexture renderTarget = RenderTargetTexture.create("CustomRTT", t.second);
		final ITexture.Settings texureSettings = t.second.getSettings();
		
		final FrameBufferObject fbo = renderTarget.getFrameBufferObject();
		fbo.addTargetBuffer(FrameBufferData.createTextureFrameBufferData(t.second));
		fbo.addTargetBuffer(FrameBufferData.createDepthFrameBufferData(texureSettings.getWidth(), texureSettings.getHeight()));
		
		sceneFacade.addRenderTarget(renderTarget);
	}
	
	private void createBillboardAnimation(final SceneFacade sceneFacade, final ResourceManager matMgr) {
		Pair<Long, ITexture> idTexPair = matMgr.createAnimatedTexture(R.drawable.explosion0, 8);
		IRenderable explo = sceneFacade.addRenderableShape(Const.SHAPE_BB, "bilboard-explosion", idTexPair.second);
		explo.getIPositionable().setPosition(new Vector3f(10, 0, -5.0f));
		explo.getIPositionable().setScale(4.0f);
		final IMaterial material = explo.getMaterial();
		final Properties p = material.getProperties();
		p.setAlpha(0.8f);
	}
	
	private void attachLightAsChildNodeToObject(final ITransform controlled, final SceneFacade sceneFacade ) {
		final IRenderable light = sceneFacade.getRenderalbeInScene(NameGenerator.instance().getLastGeneratedName(Const.SHAPE_LIGHT1));
		if(light != null){
			controlled.addSceneChild(light.getIPositionable());
			light.getIPositionable().setSceneParent(controlled);
		}
	}	
}
