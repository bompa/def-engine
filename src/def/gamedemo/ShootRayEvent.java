package def.gamedemo;

import java.util.ArrayList;
import java.util.List;

import javax.vecmath.Vector3f;

import def.engine.physics.IPhysicsSimModifyEvent;
import def.engine.physics.IPhysicsSimulation;

public class ShootRayEvent implements IPhysicsSimModifyEvent {
	
	private final Vector3f from;
	private final Vector3f to;
	
	public static ShootRayEvent create(final Vector3f from, final Vector3f to){
		return new ShootRayEvent(from, to);
	}
	
	private ShootRayEvent(final Vector3f from, final Vector3f to){
		assert(from != null && to != null);
		
		this.from = from;
		this.to = to;
	}
	
	public void modifySimulation(IPhysicsSimulation p) {
		final List<Vector3f> hits = new ArrayList<Vector3f>();
		p.shootRay(from, to, hits);
	}


	public void initSimulationModification(IPhysicsSimulation physis) {	}
}
