package def.gamedemo;

import def.engine.control.AHandleSingleTouch;
import def.engine.control.CtrlUtil;
import def.engine.control.ConfigureableTouchListener.DispatchConfigType;
import def.util.FloatPair;

import android.view.GestureDetector;
import android.view.MotionEvent;

public class GestureHandling extends AHandleSingleTouch{
	
	private final GestureDetector gestureDetector;
	
	public static GestureHandling createWithBuiltDispatcher(final GestureEventDispatcher gestureEventDisp){
		return new GestureHandling(gestureEventDisp);
	}
	
	private GestureHandling(final GestureEventDispatcher gestureEventDisp){
		if(gestureEventDisp == null)
			throw new IllegalArgumentException("GestureEventDispatcher null in GeastureHandlingOnSingleTouch ctr!");
		
		this.gestureDetector = new GestureDetector(gestureEventDisp);
	}
	
	/**
	 *	On the query if pure event should be dispatched by the touchController, return true 
	 */
	public boolean getDispatchConfigOpt(DispatchConfigType type) { return true; }
	
	/**
	 * Dispatch to gesture handler on single touch event, called since 
	 * getDispatchConfigOpt(CONF_ON_TOUCH_SINGLE_DISPATCH_EV) -> returns true
	 */
	public void singleTouch(MotionEvent e) { 
		gestureDetector.onTouchEvent(e);
	}
	public void singleTouch(FloatPair xy, FloatPair previousXy,
			long timeSinceLastSingleTouch) { }

	public void up() { }
	
	/**
	 * Convenience class to set mandatory handlers and build GestureEventDispatcher
	 * @author jonas
	 *
	 */
	public static class GestureEventDispatcherBuilder{
		private AHandleSingleTouch handleOnDown = null;
		private AHandleSingleTouch handleOnDoubleTap = null;
		private AHandleSingleTouch handleOnLongPress = null;
		
		public static GestureEventDispatcherBuilder create(){
			return new GestureEventDispatcherBuilder();
		}
		private GestureEventDispatcherBuilder(){}
		
		public GestureEventDispatcherBuilder onDown(final AHandleSingleTouch onDown){
			this.handleOnDown= onDown;
			return this;
		}
		
		public GestureEventDispatcherBuilder onDoubleTap(final AHandleSingleTouch onDoubleTap){
			this.handleOnDoubleTap = onDoubleTap;
			return this;
		}
		
		public GestureEventDispatcherBuilder onLongPress(final AHandleSingleTouch onLongPress){
			this.handleOnLongPress = onLongPress;
			return this;
		}
		
		public boolean hasAllManadatoryConfig(){
			return (handleOnDown != null) && (handleOnDoubleTap != null) && (handleOnLongPress != null);
		}
		
		public GestureEventDispatcher buildGestureEventDispatcher(){
			return GestureEventDispatcher.createWithAllHandlers(handleOnDown, handleOnDoubleTap, handleOnLongPress);
		}
	}

	/**
	 * Internal class that extends simpleGeastureListener so that it can dispatch event accordingly to how
	 * the GeastureHandlingOnSingleTouch is configured.
	 * @author jonas
	 *
	 */
	private static class GestureEventDispatcher extends GestureDetector.SimpleOnGestureListener 
	{	
		// Handlers
		private final AHandleSingleTouch handleOnDown;
		private final AHandleSingleTouch handleOnDoubleTap;
		private final AHandleSingleTouch handleOnLongPress;
		
		private static GestureEventDispatcher createWithAllHandlers(AHandleSingleTouch handleOnDown,
											AHandleSingleTouch handleOnDoubleTap,
											AHandleSingleTouch handleOnLongPress)
		{
			return new GestureEventDispatcher(handleOnDown, handleOnDoubleTap, handleOnLongPress);
		}
		
		/**
		 * Constructor requiring all handlers, none may be null
		 * @param handleOnDown
		 * @param handleOnDoubleTap
		 * @param handleOnLongPress
		 */
		private GestureEventDispatcher(	AHandleSingleTouch handleOnDown,
										AHandleSingleTouch handleOnDoubleTap,
										AHandleSingleTouch handleOnLongPress)
		{
			if( (handleOnDown == null) || (handleOnDoubleTap == null) || (handleOnLongPress == null) )
				throw new IllegalArgumentException("GeastureEventDispatcher null arg!");
			
			this.handleOnDown = handleOnDown;
			this.handleOnDoubleTap = handleOnDoubleTap;
			this.handleOnLongPress = handleOnLongPress;
		}
	
		
		/**
	     * Callback when on down gesture is detected 
	     */
	    public boolean onDown(MotionEvent e) {
	    	dispatchToucEvent(e, handleOnDown);
	        return true;
	    }
	    
	    /**
	     * Callback when double tap gesture is detected 
	     */
	    public boolean onDoubleTap(MotionEvent e) {
	    	dispatchToucEvent(e, handleOnDoubleTap);
	        return true;
	    }
	    
	    /**
	     * Callback when long press gesture is detected 
	     */
	    public void onLongPress(MotionEvent e){
	    	dispatchToucEvent(e, handleOnLongPress);
	    }

		private void dispatchToucEvent(MotionEvent e, AHandleSingleTouch handler) {
			final FloatPair xy = CtrlUtil.getEventXyAtPointerIndex(e, 0);
			handler.singleTouch(xy, xy, 0);
		}
	    
	}
}