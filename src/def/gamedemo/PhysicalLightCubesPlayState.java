package def.gamedemo;

import java.util.Iterator;
import java.util.concurrent.LinkedBlockingQueue;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

import android.opengl.Matrix;

import def.Const;
import def.engine.control.ConfigureableTouchListener;
import def.engine.control.ISensorInput;
import def.engine.control.ITouchControllerManager;
import def.engine.control.OnTouchController;
import def.engine.graphics.IRenderable;
import def.engine.graphics.components.Light;
import def.engine.graphics.material.IMaterial;
import def.engine.graphics.material.ITexture;
import def.engine.graphics.material.Materials;
import def.engine.graphics.material.IMaterial.Properties;
import def.engine.graphics.resources.ResourceManager;
import def.engine.logic.math.LinjearAlg;
import def.engine.logic.math.MatrixArray4f;
import def.engine.logic.math.VectorArray4f;
import def.engine.physics.IPhysicsEventEvent.EventType;
import def.engine.physics.IPhysicsEventEvent.PairRayRes;
import def.engine.physics.PhysicsEventListener;
import def.engine.physics.IPhysicsEventEvent;
import def.engine.physics.IPhysicsSimulation;
import def.engine.scene.ARenderableModelNameCreator;
import def.engine.scene.SceneFacade;
import def.gamedemo.GestureHandling.GestureEventDispatcherBuilder;
import def.shared.interfaces.IPhysicsObjectData;
import def.shared.interfaces.ITransform;
import def.ui.R;
import android.util.Pair;
import def.util.Stats;

public class PhysicalLightCubesPlayState extends PhysicsEventListener implements IGameState {
	private final SceneFacade sceneFacade;
	private final ConfigureableTouchListener playTouchController;
	private final LinkedBlockingQueue<SceneEntity> sceneEntities = new LinkedBlockingQueue<SceneEntity>();
	
	private final Vector4f gravity = new Vector4f(0.0f, -10.0f, 0.0f, 1.0f);
	private final float gravityCalibrateX = -90.0f;
	private final float gravityCalibrateY = -90.0f;
	private final float gravityCalibrateZ = 180.0f;
	
	public static IGameState create(final SceneFacade sceneFacade, final ITransform controlled){
		return new PhysicalLightCubesPlayState(sceneFacade, controlled);
	}
	
	private PhysicalLightCubesPlayState(final SceneFacade sceneFacade, final ITransform controlled){
		if(sceneFacade == null)
			throw new IllegalArgumentException();
		
		this.sceneFacade = sceneFacade;
		this.sceneFacade.getPhysicsSimulation().addPhysicsEventListener(this);
		
		final GestureEventDispatcherBuilder builder = GestureEventDispatcherBuilder.create();
		builder.onDoubleTap(HandleTouchFactory.createPhysEntitySpawnerSingleTouch(this, this.sceneFacade));
		builder.onDown(LogOnSingleTouch.create("On-Down"));
		builder.onLongPress(DrawLineFromCameraOnSingleTouch.create(this, sceneFacade));
		
		this.playTouchController = OnTouchController.createAndConfigure(
				GestureHandling.createWithBuiltDispatcher(builder.buildGestureEventDispatcher()),
				HandleTouchFactory.createZoomOnMultiMoveOnSingle(sceneFacade.getCamera())
			);
	}
	
	public SceneEntity addSceneEntityFromRenderable(IRenderable r){
		Stats.instance().incNrOfActiveObjects();
		SceneEntity ent = SceneEntity.createWithDefaultLifeTime(r);
		sceneEntities.add(ent);
		return ent;
	}
	
	public SceneEntity addSceneEntity(){
		Stats.instance().incNrOfActiveObjects();
		SceneEntity ent = SceneEntity.createWithDefaultLifeTime();
		sceneEntities.add(ent);
		return ent;
	}

	/**
	 * Iterate throw entities and see if any entity has died.
	 */
	public void processState(final StateEvent e) {
		adaptGravityToDevice(e.getSensors());
		
		final Iterator<SceneEntity> it = sceneEntities.iterator();
		while(it.hasNext()){
			SceneEntity ent = it.next();
			if(ent.ageEntity()){
				Stats.instance().decNrOfActiveObjects();
				ent.stopRendering(sceneFacade);
				it.remove();
			}
		}
	}
	
	private void adaptGravityToDevice(final ISensorInput sensorInput) {
		
		final IPhysicsSimulation physics = sceneFacade.getPhysicsSimulation();
		
		final MatrixArray4f rotation = new MatrixArray4f();
		sensorInput.getDeviceRotationMtx(rotation);
		Matrix.rotateM(rotation.getArray(), 0, gravityCalibrateX, 1, 0, 0);
		Matrix.rotateM(rotation.getArray(), 0, gravityCalibrateY, 0, 1, 0);
		Matrix.rotateM(rotation.getArray(), 0, gravityCalibrateZ, 0, 0, 1);
		
		final VectorArray4f adaptedGravityVec = new VectorArray4f();
		Matrix.multiplyMV(adaptedGravityVec.getArray(), 0, rotation.getArray(), 0, LinjearAlg.vectorToArray(gravity), 0);
		
		//TODO: Synchronize, or modify as event
		physics.setGravity(new Vector3f(adaptedGravityVec.getArray()));
	}

	public void setStateActive(ITouchControllerManager touchManager) {
		touchManager.updateOnTouchController(playTouchController);
	}

	@Override
	public void physicsEvent(IPhysicsEventEvent<Vector3f> ev) {
		if(ev.getType() == EventType.EV_TYPE_COLLISION){
			final IPhysicsEventEvent.PairRes collision = ev.getCollisionPair();
			
			final IPhysicsObjectData visualDataA = collision.first;
			final IPhysicsObjectData visualDataB = collision.second;
			if(visualDataA == null || visualDataB == null){
				return;
			}
			
			handlePhysicsEvent(visualDataA, visualDataB);
		}
		else if(ev.getType() == EventType.EV_TYPE_RAYHIT){
			
			final SceneEntity ent = (SceneEntity)ev.getRayHitResult().second.getOptional();
			
			final IRenderable ra = ent.getRenderable();
			
			// Entity A
			if(!ent.isKilled())
			{
				// Increase material emission
				IMaterial.Properties props = ra.getMaterial().getProperties();
				Vector4f newEmission = props.getEmission();
				if(newEmission.y > 0.5f){
					newEmission.sub(new Vector4f(0.0f, 0.7f, 0.00f, 0));
					props.setEmission(newEmission);
				}
			}
		
			final PairRayRes res = ev.getRayHitResult();
			
			final float f = 50.0f;
			final Vector3f hitDir = res.first;
			
			ev.setResult(new Vector3f(hitDir.x * f, hitDir.y * f, hitDir.z * f));
		}
	}
	
	private void handleEntityOutOfHealth(final SceneEntity ent) {
		ent.kill();
		ent.setAlphaDeltaValue(Materials.MATERIAL_FULL_APLHA);
		
		final IRenderable r = ent.getRenderable();
		sceneFacade.modifySceneAsync(	
				AddSceneEntityEvent.createStaticCustom(this, Const.SHAPE_BB, r.getIPositionable().getPosition(), 
						new ARenderableModelNameCreator() {
					
							public IRenderable create(SceneFacade sceneHandler, String modelName) {
								// Create billboard with animated texture
								final ResourceManager matMgr = sceneHandler.getResourceManager();
								final Pair<Long, ITexture> idTexPair = matMgr.createAnimatedTexture(R.drawable.explosion0, 8, 1);
								final IRenderable explo = sceneFacade.addRenderableShape(Const.SHAPE_BB, idTexPair.second);
								final IMaterial material = explo.getMaterial();
								final Properties p = material.getProperties();
								p.setAlpha(0.8f);
								
								return explo;
							}
						}).setEntityScale(8.0f)
		);
	}

	private void handlePhysicsEvent(final IPhysicsObjectData visualDataA, final IPhysicsObjectData visualDataB) {
		final SceneEntity entA = (SceneEntity)visualDataA.getOptional();
		final SceneEntity entB = (SceneEntity)visualDataB.getOptional();
				
		final IRenderable ra = entA.getRenderable();
		final IRenderable rb = entB.getRenderable();
		
		// Entity A
		if(!entA.isKilled())
		{
			// Increase material emission
			IMaterial.Properties props = ra.getMaterial().getProperties();
			Vector4f newEmission = props.getEmission();
			if(newEmission.y > 0.5f){
				newEmission.sub(new Vector4f(0.0f, 0.05f, 0.05f, 0));
				props.setEmission(newEmission);
			}

			// Incrase light diffuse, if is a light source
			final Light la = sceneFacade.getLightInScene(ra.getName());
			if(la != null){
				// Is light source
				Vector4f newDiffuse = la.getProperties().getDiffuse();
				if(newDiffuse.x < 2.5f){
					newDiffuse.add(new Vector4f(0.2f, 0.0f, 0.0f, 0));
					newDiffuse.sub(new Vector4f(0.0f, 0.1f, 0.1f, 0));
					la.getProperties().setDiffuse(newDiffuse);
				}
			}
			
			// Decrease health on impact and handle if out of health
			if(entA.decreaseHealth()){
				handleEntityOutOfHealth(entA);
			}
		}

		// Entity B
		if(!entB.isKilled())
		{
			IMaterial.Properties props = rb.getMaterial().getProperties();
			Vector4f newEmission = props.getEmission();
			if(newEmission.y > 0.5f){
				newEmission.sub(new Vector4f(0.0f, 0.05f, 0.05f, 0));
				props.setEmission(newEmission);
			}
			
			// Is light source
			final Light lb = sceneFacade.getLightInScene(rb.getName());
			if(lb != null){
				// Is light source
				Vector4f newDiffuse = lb.getProperties().getDiffuse();
				if(newDiffuse.x < 2.5f){
					newDiffuse.add(new Vector4f(0.2f, 0.0f, 0.0f, 0));
					newDiffuse.sub(new Vector4f(0.0f, 0.1f, 0.1f, 0));
					lb.getProperties().setDiffuse(newDiffuse);
				}
			}

			if(entB.decreaseHealth()){
				handleEntityOutOfHealth(entB);
			}
		}
	}
}
