package def.gamedemo;

import javax.vecmath.Vector3f;

import def.engine.control.AHandleMultiTouch;
import def.engine.control.ICamera;
import def.engine.logic.math.LinjearAlg;
import def.util.FloatPair;

public class ZoomCameraOnMultiTouch extends AHandleMultiTouch{
	private final ICamera cam;
	private static final float MOV_MP = 0.2f;
	private long maxTimeSinceLastTouchToRotateMs = 500;
	
	private final Vector3f zmov = new Vector3f();
	
	public static ZoomCameraOnMultiTouch create(ICamera cam){
		return new ZoomCameraOnMultiTouch(cam);
	}
	
	private ZoomCameraOnMultiTouch(ICamera cam){
		if(cam == null)
			throw new IllegalArgumentException("ControlCameraOnSingleTouch requires a camera to controll, can not be null. cam=null.");
		
		this.cam = cam;
	}

	public void up() {}

	public void multiTouch(FloatPair xyOne, FloatPair previousXyOne,
			FloatPair xyTwo, FloatPair previousXyTwo,
			long timeSinceLastSingleTouch) {
		
		if(timeSinceLastSingleTouch < maxTimeSinceLastTouchToRotateMs){
			
			final float prevDelta = previousXyOne.first - previousXyTwo.first; 
			final float currDelta = xyOne.first - xyTwo.first;
			final float xyOnePrevDelta = xyOne.first - previousXyOne.first;
			
			float sign = 1.0f;
			if(	(xyOne.first < xyTwo.first && xyOnePrevDelta > 0) ||
				(xyOne.first > xyTwo.first && xyOnePrevDelta < 0))
			{
				sign = -1.0f;
			}
			
			zmov.scale(sign * Math.abs(prevDelta - currDelta) * MOV_MP, LinjearAlg.Z_AXIS);
			cam.moveRel(zmov);
		}
	}
}
