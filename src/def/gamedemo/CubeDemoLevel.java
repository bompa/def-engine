package def.gamedemo;

import javax.vecmath.Vector3f;

import android.os.Bundle;
import def.engine.graphics.ACameraRenderer;
import def.engine.graphics.IRenderable;
import def.engine.graphics.components.FreePositionable;
import def.engine.scene.ContentConstruction;
import def.engine.scene.ILevel;
import def.engine.scene.SceneFacade;
import def.util.IntPair;

public class CubeDemoLevel implements ILevel {
	
	//private final LogHandler l = LogHandler.createInstanceShared();
	
	/**
	 * The starting position for the camera.
	 */
	private static final Vector3f CAMERA_INIT_POS = new Vector3f(0, 0, 1.5f);
	
	public String[] getInitialSceneContent(){
		return new String[]{ new String() };
	}
	
	public void createScene(	final SceneFacade sceneFacade,
								final ACameraRenderer renderer, 
								final GameStateManager gameManager,
								final IntPair winSize,
								final Bundle extras)
	{
		
		sceneFacade.getCamera().moveTo(CAMERA_INIT_POS);
		
		//ContentConstruction.createSkybox(sceneFacade);
		
		final IRenderable r = ContentConstruction.createBillboardAnimation(sceneFacade);
		r.getIPositionable().setPosition(new Vector3f(0.25f, 0.0f, -30.0f));
		r.getIPositionable().setScale(4.0f);
		
		//ContentConstruction.createSquareRTT(sceneFacade, sceneFacade.getResourceManager(), winSize);
		
		ContentConstruction.createGroundShape(sceneFacade);
		
		renderer.addRenderSequenceListener(gameManager);
		IGameState playstate = PhysicalLightCubesPlayState.create(sceneFacade, new FreePositionable());
		gameManager.addAndActivateGameState("Playing", playstate);
	}
}
