package def.gamedemo;


import def.engine.control.AHandleMultiTouch;
import def.engine.control.IHandleTouch;
import def.util.FloatPair;

public class DetectOneOrTwoMoveMultiTouch extends AHandleMultiTouch{
	final IHandleTouch dispatchOnOneMove, disptchOnTwoMove;
	
	public static DetectOneOrTwoMoveMultiTouch create(	final IHandleTouch dispatchOnOneMove, final IHandleTouch disptchOnTwoMove){
		
		return new DetectOneOrTwoMoveMultiTouch(dispatchOnOneMove, disptchOnTwoMove);
	}
	
	private DetectOneOrTwoMoveMultiTouch(final IHandleTouch dispatchOnOneMove, final IHandleTouch disptchOnTwoMove){
		this.dispatchOnOneMove = dispatchOnOneMove;
		this.disptchOnTwoMove = disptchOnTwoMove;
	}

	public void multiTouch( final FloatPair xyOne, final FloatPair previousXyOne,
							final FloatPair xyTwo, final FloatPair previousXyTwo,
							final long timeSinceLastSingleTouch) {
		
		final float dxOne = xyOne.first - previousXyOne.first;
		final float dyOne = xyOne.second - previousXyOne.second;
		final double sumDxyOne = Math.abs(dxOne) + Math.abs(dyOne);
		
		final float dxTwo = xyTwo.first - previousXyTwo.first;
		final float dyTwo = xyTwo.second - previousXyTwo.second;
		final double sumDxyTwo = Math.abs(dxTwo) + Math.abs(dyTwo);
		
		final boolean twoHasMoved = (sumDxyOne > 0) && (sumDxyTwo > 0);
		if(twoHasMoved){
			disptchOnTwoMove.multiTouch(xyOne, previousXyOne, xyTwo, previousXyTwo, timeSinceLastSingleTouch);
			return;
		}
		
		if(sumDxyOne > 0){
			dispatchOnOneMove.singleTouch(xyOne, previousXyOne, timeSinceLastSingleTouch);
		}
		else if(sumDxyTwo > 0){
			dispatchOnOneMove.singleTouch(xyTwo, previousXyTwo, timeSinceLastSingleTouch);
		}
	}

	public void up() {}
}
