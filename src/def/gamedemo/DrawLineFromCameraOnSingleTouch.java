package def.gamedemo;

import javax.vecmath.Matrix4f;
import javax.vecmath.Vector3f;

import android.view.MotionEvent;

import def.Const;
import def.engine.control.AHandleSingleTouch;
import def.engine.control.ICamera;
import def.engine.graphics.components.IBuildAction;
import def.engine.graphics.components.IMeshComposer;
import def.engine.graphics.components.ANonCachableBuildAction;
import def.engine.graphics.components.Vertex;
import def.engine.logic.math.LinjearAlg;
import def.engine.physics.IPhysicsSimulation;
import def.engine.scene.LightModelNameCreator;
import def.engine.scene.RenderableVertexBuilderCreator;
import def.engine.scene.SceneFacade;
import def.util.FloatPair;

public class DrawLineFromCameraOnSingleTouch extends AHandleSingleTouch{
	static final float DRAW_LINE_Y_CAM_OFFSET = 0.5f; 
	static final float DRAW_LINE_Z_CAM_DIST_OFFSET = 200.0f;

	private final ICamera cam;
	private final SceneFacade sceneFacade;
	private final PhysicalLightCubesPlayState playState;
	
	public static DrawLineFromCameraOnSingleTouch create(final PhysicalLightCubesPlayState playState, final SceneFacade sf){
		return new DrawLineFromCameraOnSingleTouch(playState, sf);
	}
	
	private DrawLineFromCameraOnSingleTouch(final PhysicalLightCubesPlayState playState, final SceneFacade sf){
		if(sf == null || playState == null)
			throw new IllegalArgumentException("ControlCameraOnSingleTouch requires a camera to controll, can not be null. cam=null.");
		
		this.playState = playState;
		this.sceneFacade = sf;
		this.cam = sceneFacade.getCamera();
	}

	public void up() {}

	public void singleTouch(FloatPair xyOne, FloatPair previousXyOne, long timeSinceLastSingleTouch) {
		final Vector3f sV = new Vector3f(cam.getPosition());
		sV.y -= DRAW_LINE_Y_CAM_OFFSET;

		// Distance from camera to shoot ray
		final float endz = sV.z - DRAW_LINE_Z_CAM_DIST_OFFSET;
				
		final Vector3f endpoint = LinjearAlg.screenToWorld(	new Matrix4f(cam.getViewMatrix().getArray()), 
															new Matrix4f(cam.getProjectionMatrix().getArray()), 
															cam.getViewPort(), 
															new Vector3f(xyOne.first, xyOne.second, endz)
		);
		
		final IBuildAction build = new ANonCachableBuildAction(){
			// Set start/end point for line
			public void build(IMeshComposer buildComposer) {
				buildComposer.addVertex(Vertex.create(sV.x, sV.y, sV.z).color(1, 0, 0, 1)).
					addVertex(Vertex.create(endpoint.x, endpoint.y, endpoint.z).color(0, 0, 1, 1));
			}
		};
		
		sceneFacade.modifySceneAsync(
			AddSceneEntityEvent.createStaticCustom(	
				playState, Const.SHAPE_LINES, LinjearAlg.ORIGO, RenderableVertexBuilderCreator.create(build)
			).setEntityLifetime(400)
		);
		
		sceneFacade.modifySceneAsync(
				AddSceneEntityEvent.createStaticCustom(	
					playState, Const.SHAPE_LIGHT1, endpoint, LightModelNameCreator.create()
				).setEntityLifetime(400).setEntityScale(0.1f)
		);
		 
		final IPhysicsSimulation p = sceneFacade.getPhysicsSimulation();
		p.addModifyEvent(ShootRayEvent.create(sV, endpoint));
	}

	public void singleTouch(MotionEvent e) {}
}
