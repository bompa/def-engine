package def.gamedemo;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

import def.engine.control.ISensorInput;
import def.engine.control.ITouchControllerManager;
import def.engine.graphics.IRenderSequenceEvent;
import def.engine.graphics.RenderSequenceListener;
import def.engine.graphics.driver.Graphics.RenderSequenceAction;

public class GameStateManager extends RenderSequenceListener{
	private final ConcurrentHashMap<String, IGameState> statesMap = new ConcurrentHashMap<String, IGameState>();
	private final ITouchControllerManager touchManager;
	private IGameState activeState = null;
	final ISensorInput sensorInput;
	
	public static GameStateManager create(final ITouchControllerManager touchManager, final ISensorInput sensorInput){
		return new GameStateManager(touchManager, sensorInput);
	}
	
	private GameStateManager(final ITouchControllerManager touchManager, final ISensorInput sensorInput){
		this.touchManager = touchManager;
		this.sensorInput = sensorInput;
	}
	
	public void addGameState(final String name, final IGameState state){
		statesMap.put(name, state);
	}
	
	public void addAndActivateGameState(final String name, final IGameState state){
		statesMap.put(name, state);
		activateGameState(name);
	}
	
	public void activateGameState(final String name){
		final IGameState state = statesMap.get(name);
		if(state == null)
			throw new IllegalStateException();
		
		activeState = state;
		activeState.setStateActive(touchManager);
	}

	/**
	 * Process states after each frame has been rendered.
	 * TODO: Should probably only process active state..
	 */
	@Override
	public void renderSequenceUpdate(IRenderSequenceEvent rsa) {
		if(rsa.getAction() == RenderSequenceAction.REND_ACTION_FRAME_RENDERED){
			Collection<IGameState> states = statesMap.values();
			for(IGameState state : states){
				state.processState(StateEvent.create(sensorInput));
			}
		}
	}
}
