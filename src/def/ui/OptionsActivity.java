package def.ui;

import def.util.LogHandler;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class OptionsActivity extends FragmentActivity{
	OptionsFragment options = null;
	private final LogHandler l = LogHandler.createInstanceShared();
    
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if( savedInstanceState == null ){
        	final Bundle ext = getIntent().getExtras();
        	OptionsResult optionResult = null;
        	if(ext != null){
        		optionResult = (OptionsResult) ext.getSerializable(InstanceBundleArgs.ID.ARG_OPTION_RESULT);
        	}
        	else{
        		optionResult = OptionsResult.createDefault();
        	}
	    	
	    	options = options == null? OptionsFragment.create(optionResult): options;
            options.setArguments(getIntent().getExtras());
            
            getSupportFragmentManager().beginTransaction().add(android.R.id.content, options).commit();
        }
    }
    
    @Override
    public void onBackPressed() {
  
    	if(options != null){
        	final Intent intent = new Intent();
        	final OptionsResult result = options.getResult();
        	intent.putExtra(InstanceBundleArgs.ID.ARG_OPTION_RESULT, result);
        	setResult(Activity.RESULT_OK, intent);    		
    	}
    	else{
    		l.log("OptionResult fragment was null when return from OptionActiviy");
    	}

    	super.onBackPressed();
    }
}
