package def.ui;

import def.engine.Engine;

public class InstanceBundleArgs {
	
	public static class Request {
		public final static int OPTIONS_RESULT = 0; 
	}

	public static class ID {
		public final static String ACTION_CHOOSEN_RENDER_STR = "GlActRenderArg";
		public final static String ACTION_CHOOSEN_PHYSLIB_STR = "GlActPhyslibArg";
		public final static String ACTION_CHOOSEN_LEVEL_STR = "GlActLevelArg";
		public final static String ARG_OPTION_RESULT = "OptionsItemListener";
	}
	
	public static class Value{
		public final static String GLES_20_STR = "GlEs20Arg";
		public final static String GLES_10_STR = "GlEs10Arg";
		public final static String BULLET_STR = "BulletArg";
		public final static String JBULLET_STR = "JBulletArg";
		public final static String LEVEL_CUBE_SKYBOX_RTT_STR = Engine.getLevels()[Engine.LEVEL_CUBE_RTT_SKYBOX];
		public final static String LEVEL_CUBE_SKYBOX_STR = Engine.getLevels()[Engine.LEVEL_CUBE_SKYBOX];
		public final static String LEVEL_CUBE_STR = Engine.getLevels()[Engine.LEVEL_CUBE_SIMPLE];
		
	}

}
