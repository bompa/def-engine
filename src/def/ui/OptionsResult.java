package def.ui;

import java.io.Serializable;

public class OptionsResult implements Serializable {

	private static final long serialVersionUID = 3404048146266710207L;
	
	public static String DefaultSelection = "__EmptySelection__";
	
	private String selectedRender = DefaultSelection;
	private String selectedPhysics = DefaultSelection;
	
	private boolean drawNormals = false;
	private boolean useFaceCullling = true;
	private boolean useLightning = true;
	private boolean useVbos = true;
	
	public static OptionsResult createDefault(){
		return new OptionsResult();
	}
	
	public static OptionsResult create(String render, String phyics){
		return new OptionsResult(render, phyics);
	}
	
	private OptionsResult(){ }
	
	private OptionsResult(String render, String phyics){
		this.selectedRender = render;
		this.selectedPhysics =phyics;
	}

	public String getSelectedRender() {
		return selectedRender;
	}

	public void setSelectedRender(String selectedRender) {
		this.selectedRender = selectedRender;
	}

	public String getSelectedPhysics() {
		return selectedPhysics;
	}

	public void setSelectedPhysics(String selectedPhysics) {
		this.selectedPhysics = selectedPhysics;
	}

	public boolean isDrawNormals() {
		return drawNormals;
	}

	public void setDrawNormals(boolean drawNormals) {
		this.drawNormals = drawNormals;
	}

	public boolean isUseFaceCullling() {
		return useFaceCullling;
	}

	public void setUseFaceCullling(boolean useFaceCullling) {
		this.useFaceCullling = useFaceCullling;
	}

	public boolean isUseLightning() {
		return useLightning;
	}

	public void setUseLightning(boolean useLightning) {
		this.useLightning = useLightning;
	}

	public boolean isUseVbos() {
		return useVbos;
	}

	public void setUseVbos(boolean useVbos) {
		this.useVbos = useVbos;
	}
}
