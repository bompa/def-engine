package def.ui;

import def.util.AndroidUiUtil;
import def.util.Util;
import android.os.Bundle;
import def.engine.Engine;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.Spinner;

public class OptionsFragment extends Fragment{
	
	private final OptionsResult optionsResult;
	//So that stuff are initialized in onStart when the activity can be accessed using getActivity()
	private boolean restored = false;
	
	/**
	 * Listener class for listening on spinner selections
	 * @param v
	 */
	private final OnItemSelectedListener itemSelectedListener; 
	
	public static OptionsFragment create(){
		return new OptionsFragment();
	}
	
	public static OptionsFragment create(final OptionsResult optionResult){
		return new OptionsFragment(optionResult);
	}
	
	public OptionsFragment(final OptionsResult optionResult){
		if(optionResult == null){
			throw Util.buildIllegalNullArgException(OptionsFragment.class, "optionResult");
		}
		
		this.optionsResult = optionResult;
		this.itemSelectedListener = OptionResultItemSelectedListener.createWithSharedResult(optionsResult);
	}
	
	public OptionsFragment(){
		optionsResult = OptionsResult.createDefault();
		this.itemSelectedListener = OptionResultItemSelectedListener.createWithSharedResult(optionsResult);
	}
	
	public OptionsResult getResult(){
		updateResultFromView();
		return optionsResult;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
	}
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.optionslayout, container, false);
    }
    
    @Override
    public void onStart(){
    	super.onStart();
    	
    	// Initialize only if not restored from a user task switch
    	if(!restored){
	    	final Spinner render = AndroidUiUtil.createSpinnerFromStringArray(	getActivity(), 
		    											R.id.options_select_graphics_drv_spinner, 
		    											Engine.getSupportedRenders(), 
		    											itemSelectedListener);
		    	
			final Spinner physics = AndroidUiUtil.createSpinnerFromStringArray(	getActivity(), 
	    												R.id.options_select_physics_drv_spinner, 
	    												Engine.getSupportedPhysicEngines(), 
	    												itemSelectedListener);
	
			render.setSelection(Engine.getRenderIndex(optionsResult.getSelectedRender()));
			physics.setSelection(Engine.getPhyicsEngineIndex(optionsResult.getSelectedPhysics()));
	    	
	    	// Set checkBoxDefaultValuse
	    	final CheckBox drawNormals = (CheckBox) getActivity().findViewById(R.id.options_draw_normal_checkbox);
	    	final CheckBox faceCulling = (CheckBox) getActivity().findViewById(R.id.options_facec_checkbox);
	    	final CheckBox lightning = (CheckBox) getActivity().findViewById(R.id.options_lightning_checkbox);
	    	final CheckBox vbos = (CheckBox) getActivity().findViewById(R.id.options_VBOs_checkbox);
	    	
	    	drawNormals.setChecked(optionsResult.isDrawNormals());
	    	faceCulling.setChecked(optionsResult.isUseFaceCullling());
	    	lightning.setChecked(optionsResult.isUseLightning());
	    	vbos.setChecked(optionsResult.isUseVbos());
	    	
	    	restored = false;
    	}
    }
    
    @Override
    public void onPause(){
    	restored = true;
    	super.onPause();
    }

	private void updateResultFromView() {
		final CheckBox drawNormals = (CheckBox) getActivity().findViewById(R.id.options_draw_normal_checkbox);
    	final CheckBox faceCulling = (CheckBox) getActivity().findViewById(R.id.options_facec_checkbox);
    	final CheckBox lightning = (CheckBox) getActivity().findViewById(R.id.options_lightning_checkbox);
    	final CheckBox vbos = (CheckBox) getActivity().findViewById(R.id.options_VBOs_checkbox);
    	
    	optionsResult.setDrawNormals(drawNormals.isChecked());
    	optionsResult.setUseFaceCullling(faceCulling.isChecked());
    	optionsResult.setUseLightning(lightning.isChecked());
    	optionsResult.setUseVbos(vbos.isChecked());
	}
}
