package def.ui;

import def.engine.control.SensorInputManager;
import def.engine.graphics.IEventListenerRenderer;
import def.engine.graphics.MainCameraRenderer;
import def.engine.graphics.driver.GraphicsDriverFactory;
import def.engine.graphics.driver.RenderConfig;
import def.engine.graphics.resources.ResourceManager;
import def.engine.scene.ILevel;
import def.engine.scene.SceneFacade;
import def.gamedemo.CubeDemoLevel;
import def.gamedemo.CubeSkyboxDemoLevel;
import def.gamedemo.GameStateManager;
import def.gamedemo.CubeSkyboxRttDemoLevel;
import def.ui.InstanceBundleArgs.Value;
import def.util.Configuration;
import def.util.IntPair;
import def.util.LogHandler;
import def.util.Util.NameGenerator;

import android.app.Activity;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.util.Log;

public class MainGameGLActivity extends Activity {
	private final LogHandler l = LogHandler
			.createInstanceShared(MainGameGLActivity.class);
	private GLSurfaceView gLSurfaceView;
	private WakeLock wakeLock;
	private PowerManager powerManager;
	private volatile boolean useWakeLock = true;

	/**
	 * Create the correct renderer implementation, according to the arguments
	 * from the intent for starting this activity.
	 * 
	 * @param extras
	 *            is the Bundle instance from the intent, holding the arguments.
	 * @return the created renderer.
	 */
	private MainCameraRenderer createRender(Bundle extras) {
		final RenderConfig rc = RenderConfig
				.createFromConfiguration(Configuration.instance());
		int w = getWindowManager().getDefaultDisplay().getWidth();
		int h = getWindowManager().getDefaultDisplay().getHeight();
		rc.setRenderWinSize(new IntPair(w, h));

		MainCameraRenderer renderer = null;
		String target = "";

		if (extras != null)
			target = extras
					.getString(InstanceBundleArgs.ID.ACTION_CHOOSEN_RENDER_STR);

		if (target.equals(Value.GLES_10_STR))
			renderer = new MainCameraRenderer(
					GraphicsDriverFactory.createOpenGl10Driver(), rc);
		else if (target.equals(Value.GLES_20_STR))
			renderer = new MainCameraRenderer(
					GraphicsDriverFactory.createOpenGl20Driver(), rc);
		else {
			if (l.isLevelLoggable(Log.WARN))
				l.log("Error in driver parameter. Falling back to defalut driver, GLES1.0",
						Log.WARN);
			renderer = new MainCameraRenderer(
					GraphicsDriverFactory.createOpenGl10Driver(), rc);
		}
		return renderer;
	}

	/**
	 * Check which gl version the renderer supports and initialize the
	 * glSurfaceView accordingly
	 * 
	 * @param gLSurfaceView
	 *            GlSurfaceView used for rendering
	 * @param renderer
	 *            the renderer to query for supported render version
	 */
	private void checkSupportedGlVersion(final GLSurfaceView gLSurfaceView,
			final MainCameraRenderer renderer) {
		if (renderer.getSupporteRenderVersion() > 1)
			gLSurfaceView.setEGLContextClientVersion(renderer
					.getSupporteRenderVersion());
		gLSurfaceView.setRenderer(renderer);
	}

	private SceneFacade createSceneHandlingFacade(
			final IEventListenerRenderer renderer,
			final ResourceManager matMgr, final Bundle extras) {
		String target = InstanceBundleArgs.Value.JBULLET_STR;
		if (extras != null)
			target = extras
					.getString(InstanceBundleArgs.ID.ACTION_CHOOSEN_PHYSLIB_STR);

		if (target.equals(InstanceBundleArgs.Value.BULLET_STR)) {
			l.log("Using native Bullet physics library", Log.INFO);
			return SceneFacade.createWithBulletPhysics(
					renderer.getRenderEventListener(), matMgr);
		} else {
			l.log("Using JBullet physics library", Log.INFO);
			return SceneFacade.createWithJBulletPhysics(
					renderer.getRenderEventListener(), matMgr);
		}

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		l.log("Creating GlSurfaceView activity..");

		powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(
				PowerManager.SCREEN_BRIGHT_WAKE_LOCK, getClass().getName());

		NameGenerator.instance().reset();
		gLSurfaceView = new GLSurfaceView(this);
		final Bundle ext = getIntent().getExtras();

		MainCameraRenderer renderer = createRender(ext);
		checkSupportedGlVersion(gLSurfaceView, renderer);

		GameStateManager gameManager = GameStateManager.create(
				ViewOnTouchManager.create(gLSurfaceView),
				SensorInputManager.create(this));
		renderer.addRenderSequenceListener(gameManager);

		final ResourceManager matMgr = ResourceManager.create(renderer,
				getApplicationContext());
		renderer.addRenderSequenceListener(matMgr);
		final SceneFacade sceneFacade = createSceneHandlingFacade(renderer,
				matMgr, ext);

		renderer.addRenderSequenceListener(sceneFacade);
		renderer.setCamera(sceneFacade.getCamera());

		final IntPair winSize = renderer.getRenderConfig().getRenderWinSize();

		final String selectedlevel = getSelectedShapeString(ext);

		ILevel level = null;
		if (selectedlevel
				.equals(InstanceBundleArgs.Value.LEVEL_CUBE_SKYBOX_RTT_STR))
			level = new CubeSkyboxRttDemoLevel();
		else if (selectedlevel
				.equals(InstanceBundleArgs.Value.LEVEL_CUBE_SKYBOX_STR))
			level = new CubeSkyboxDemoLevel();
		else
			level = new CubeDemoLevel();

		level.createScene(sceneFacade, renderer, gameManager, winSize, ext);

		setContentView(gLSurfaceView);
	}

	private static String getSelectedShapeString(Bundle extras) {
		if (extras != null)
			return extras
					.getString(InstanceBundleArgs.ID.ACTION_CHOOSEN_LEVEL_STR);
		else
			return InstanceBundleArgs.Value.LEVEL_CUBE_SKYBOX_RTT_STR;
	}

	// Ideally a game should implement onResume() and onPause()
	// to take appropriate action when the activity looses focus
	@Override
	protected void onResume() {
		super.onResume();

		// Prevent screen from turning off
		try {
			if (useWakeLock)
				wakeLock.acquire();
		} catch (Exception e) {
			l.log("Could not acquire wackLock (Exception: " + e.getMessage()
					+ "), disabling wakelock usage..", Log.WARN);
			useWakeLock = false;
		} finally {
			gLSurfaceView.onResume();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();

		try {
			if (useWakeLock)
				wakeLock.release();
		} catch (Exception e) {
			l.log("Could not release wackLock (Exception: " + e.getMessage()
					+ ")", Log.WARN);
			useWakeLock = false;
		} finally {
			gLSurfaceView.onPause();
		}
	}
}
