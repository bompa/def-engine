package def.ui;

import def.engine.Engine;
import def.engine.graphics.driver.RenderConfig;
import def.network.DefNetworkLayer;
import def.network.INetworkLayer;
import def.ui.InstanceBundleArgs.Value;
import def.ui.R;
import def.util.AndroidUiUtil;
import def.util.Configuration;
import def.util.LogHandler;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;

import android.support.v4.app.FragmentActivity;

public class MainMenu extends FragmentActivity  {
	private final static LogHandler l = LogHandler.createInstanceShared(MainMenu.class);
	
	private final INetworkLayer networkLayer = new DefNetworkLayer();
	
	private OptionsResult optionResult = OptionsResult.createDefault();
	
	/**
	 * String storing which shape is selected with the spinner.
	 */
	private String selectedLevel = OptionsResult.DefaultSelection;
	
	/**
	 * Listener class for listening on spinner selections
	 * @param v
	 */
	public class LevelItemSelectedListener implements OnItemSelectedListener {

        public void onItemSelected(AdapterView<?> parent, View v, int pos, long row) {
        	MainMenu.this.selectedLevel = parent.getItemAtPosition(pos).toString();
        }
        public void onNothingSelected(AdapterView<?> parent) {}
    }
	
	/**
	 * Call back for button, defined in main.xml
	 * @param v
	 */
	public void onClickLaunchESv1(View v){
		l.log("Launching ES1.0...");
		launchScreen(Value.GLES_10_STR, Value.BULLET_STR, selectedLevel);
	}
	
	/**
	 * Call back for button, defined in main.xml
	 * @param v
	 */
	public void onClickLaunchESv2(View v){
		l.log("Launching ES2.0...");
		launchScreen(Value.GLES_20_STR, Value.BULLET_STR, selectedLevel);
	}
	
	private static String choosenPhysicsToValueString(final String physics){
		final String bullet = Engine.getSupportedPhysicEngines()[Engine.PHYS_LIB_BULLET];
		final String jbullet = Engine.getSupportedPhysicEngines()[Engine.PHYS_LIB_JBULLET];
		
		if(physics.equals(OptionsResult.DefaultSelection) || physics.equals(bullet)){
			return Value.BULLET_STR;
		}
		else if(physics.equals(jbullet)){
			return Value.JBULLET_STR;
		}
		
		return Value.JBULLET_STR;
	}
	
	private static String choosenLevelValueString(final String level){
		if(	level.equals(OptionsResult.DefaultSelection))
		{
			return Value.LEVEL_CUBE_SKYBOX_RTT_STR;
		}
		return level;
		/*if(	level.equals(OptionsResult.DefaultSelection) || 
			level.equals(Engine.getLevels()[Engine.LEVEL_CUBE_RTT_SKYBOX]))
		{
			return Value.LEVEL_CUBE_SKYBOX_RTT_STR;
		}
		else if(level.equals(Engine.getLevels()[Engine.LEVEL_CUBE_RTT_SKYBOX])){
			return Value.LEVEL_CUBE_SKYBOX_STR;
		}
		else if(level.equals(Engine.getLevels()[Engine.LEVEL_CUBE_SIMPLE])){
			return Value.LEVEL_CUBE_STR;
		}*/
		
		//return Value.JBULLET_STR;
	}
	
	private static String choosenRenderToValueString(final String render){
		
		final String opengles20 = Engine.getSupportedRenders()[Engine.RENDER_OPENGLES20];
		final String opengles1x = Engine.getSupportedRenders()[Engine.RENDER_OPENGLES1X];
		
		if(render.equals(OptionsResult.DefaultSelection) || render.equals(opengles20)){
			return Value.GLES_20_STR;
		}
		else if(render.equals(opengles1x)){
			return Value.GLES_10_STR;
		}
		
		return Value.GLES_10_STR;
	}

	/**
	 * Call back for button, defined in main.xml
	 * @param v
	 */
	public void onClickLaunch(View v){
		l.log("Launching...");
		
		final String renderValue = choosenRenderToValueString(optionResult.getSelectedRender());
		final String physLibValue = choosenPhysicsToValueString(optionResult.getSelectedPhysics());
		final String levelArg = choosenLevelValueString(selectedLevel);
		
		launchScreen(renderValue, physLibValue, levelArg);
	}	

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (resultCode == Activity.RESULT_OK && requestCode == InstanceBundleArgs.Request.OPTIONS_RESULT) {
	    	final Bundle extras = data.getExtras();
	    	optionResult = (OptionsResult) extras.getSerializable(InstanceBundleArgs.ID.ARG_OPTION_RESULT);
		}
	}
	
	/**
	 * Options button callback
	 * @param v
	 */
	public void onClickOptions(View v){
		Intent intent = new Intent();
        intent.setClass(getApplicationContext(), OptionsActivity.class);
        intent.putExtra(InstanceBundleArgs.ID.ARG_OPTION_RESULT, optionResult);
        startActivityForResult(intent, InstanceBundleArgs.Request.OPTIONS_RESULT);
	}

	private void generateXmlRenderConfig(){
		final CharSequence[] items = RenderConfig.getAvailableOptionNames();
		
		final OptionsResult op = optionResult;
		final boolean[] checkedItems = new boolean[]{ op.isUseLightning(), op.isDrawNormals(), op.isUseFaceCullling() };
		
		Configuration.instance().createRenderConfigXml(items, checkedItems);
	}
	
	/**
	 * Launch opengl main activity with chosen config and options
	 * @param renderArg string representing the chosen render to use
	 * @param shapeArg string representing the choosen default shape to control
	 */
	private void launchScreen(final String renderArg, final String physLibArg, final String shapeArg){
		generateXmlRenderConfig();
		Intent intent = new Intent();
		intent.setClass(MainMenu.this, MainGameGLActivity.class);
		intent.putExtra(InstanceBundleArgs.ID.ACTION_CHOOSEN_RENDER_STR, renderArg);
		intent.putExtra(InstanceBundleArgs.ID.ACTION_CHOOSEN_PHYSLIB_STR, physLibArg);
		intent.putExtra(InstanceBundleArgs.ID.ACTION_CHOOSEN_LEVEL_STR, shapeArg);
		
		startActivity(intent);
	}
	
	private void createChooseLevelSpinner(){
		AndroidUiUtil.createSpinnerFromStringArray(	this, 
				R.id.Spinner01, 
				Engine.getLevels(), 
				new LevelItemSelectedListener());
	}
		
    // Called when the activity is created, or relaunched etc 
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        createChooseLevelSpinner();
        
        if(Engine.instance().init(getApplicationContext())){
        	l.log("Main app initalized succesfuly!");
        }
        else{
        	l.log("Main app initalization error!");
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.network_menu, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
        case R.id.connect:
        	networkLayer.init(this);
            return true;
        case R.id.disconnect:
        	networkLayer.stop();
        default:
            return super.onOptionsItemSelected(item);
        }
    }
}