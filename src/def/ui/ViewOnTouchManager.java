package def.ui;

import def.engine.control.ITouchControllerManager;
import android.view.View;
import android.view.View.OnTouchListener;

public class ViewOnTouchManager implements ITouchControllerManager{
	private final View view;
	
	public static ViewOnTouchManager create(final View v){
		return new ViewOnTouchManager(v);
	}
	
	private ViewOnTouchManager(final View v){
		this.view = v;
	}

	public void updateOnTouchController(OnTouchListener listener) {
		view.setOnTouchListener(listener);
	}

}
