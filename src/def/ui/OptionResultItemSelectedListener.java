package def.ui;

import def.util.LogHandler;
import def.util.Util;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;

public class OptionResultItemSelectedListener implements OnItemSelectedListener{	
	private static final LogHandler l = LogHandler.createInstanceShared();
	private final OptionsResult result;
	
	public static OptionResultItemSelectedListener createWithSharedResult(final OptionsResult result){
		if(result == null)
			throw Util.buildIllegalNullArgException(OptionResultItemSelectedListener.class, "result");
		
		return new OptionResultItemSelectedListener(result);
	}
				
	private OptionResultItemSelectedListener(final OptionsResult result){
		this.result = result;
	}

    public void onItemSelected(AdapterView<?> parent, View v, int pos, long row) {
    	if( R.id.options_select_graphics_drv_spinner == parent.getId()){
    		setSelectedRender(parent.getItemAtPosition(pos).toString());
    	}
    	else if( R.id.options_select_physics_drv_spinner == parent.getId() ){
    		setSelectedPhysics(parent.getItemAtPosition(pos).toString());
    	}
    	else{
    		l.log("No valid ID found when selecting option!", Log.WARN);
    	}	
    }
    
    public void onNothingSelected(AdapterView<?> parent) {}
    
    public OptionsResult getResult(){
    	return result;
    }

	private void setSelectedRender(String selected) {
		this.result.setSelectedRender(selected);
	}

	private void setSelectedPhysics(String selectedPhysics) {
		result.setSelectedPhysics(selectedPhysics);
	}
}