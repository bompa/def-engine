package def.util;

public class Options {
	// Attribute option values
	public enum Val{ON, OFF, UNDEF}

	public static Val[] getUndefValuesOptValArray(int length) {
		Val[] attributes = new Val[length];
		
		for(int i = 0; i < attributes.length; i++){
			attributes[i] = Val.UNDEF;
		}
		return attributes;
	}

	public static boolean getAttrBool(Val val) {
		if(val == Val.ON)
			return true;
		
		return false;
	}

	public static Val toOptVal(boolean on){
		if(on){
			return Val.ON;
		}
		
		return Val.OFF;
	}

	public static void updateAttributeIfValid(Val[] attributesOld, Val[] attributesNew, int pos) {
		if(attributesNew[pos] != Val.UNDEF){
			attributesOld[pos] = attributesNew[pos];
		}
	}
}
