package def.util;

import java.util.concurrent.atomic.AtomicInteger;

import def.shared.util.TimeMeasurement;

import android.util.Log;

public class Stats {
	private static final Stats instance = new Stats();
	private static final LogHandler l = LogHandler.createInstanceShared();
	
	private final TimeMeasurement fpsTimer = TimeMeasurement.create();
	private final TimeMeasurement compTaskWaitTimer = TimeMeasurement.create();
	private final AtomicInteger activeObjects = new AtomicInteger(0);

	public void updateFps(){
		fpsTimer.timeStamp();
	}
	public void updateComputationTaskWait(){
		compTaskWaitTimer.timeStamp(); 
	}
	public void incNrOfActiveObjects(){
		activeObjects.incrementAndGet();
	}
	public void decNrOfActiveObjects(){
		activeObjects.decrementAndGet();
	}
	
	public long getFps() {
		return fpsTimer.getFrequency();
	}
	public long getLastRenderCallTime(){
		return fpsTimer.getPeriodTimeMs();
	}
	public int getNrOfActiveObjects() {
		return activeObjects.get();
	}

	public static Stats instance() {
		return instance;
	}
	
	/**
	 * log stats every logStatsFrameFreq�th frame
	 * @param logStatsFrameFreq
	 */
	public void logStatistics(final long logStatsFrameFreq){
		if(fpsTimer.getNrOfTimeStampUpdates() >= logStatsFrameFreq){
			l.log("Statistics: " + toString(), Log.ERROR);
			fpsTimer.resetNrOfTimeStampUpdates();
		}
	}
	
	@Override
	public String toString(){
		return 
			"\n\tFPS: " + fpsTimer.getFrequency() + 
			"\n\tWait: " + compTaskWaitTimer.getPeriodTimeMs() +
			"\n\tActive Objs: " + activeObjects.get();
	}
}
