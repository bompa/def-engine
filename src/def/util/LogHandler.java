package def.util;

import android.util.Log;

public class LogHandler {
	private final ILogger loggerImpl;
	
	private static int logLevel = Log.INFO;
	private static ILogger defaultSharedloggerImpl = LoggerFactory.createASyncLogger();
	
	/**
	 * Create instance with shared background ILogger task
	 * @return
	 */
	public static LogHandler createInstanceShared(){
		return new LogHandler(defaultSharedloggerImpl);
	}
	
	/**
	 * Create instance with shared background ILogger task
	 * @param cls class type used which uses the created instance for logging
	 * @return
	 */
	public static LogHandler createInstanceShared(Class<?> cls){
		final LogHandler inst = LogHandler.createInstanceShared();
		if(inst.loggerImpl != null){
			inst.loggerImpl.setCallingClass(cls);
		}
		return inst;
	}
	
	private LogHandler(final ILogger logger){
		loggerImpl = logger;
	}
	
	//private LogHandler(){}
	
	public static boolean isLogLevelWithinRange(final int level){
		return level >= Log.VERBOSE && level <= Log.ASSERT;
	}
	
	public boolean isLevelLoggable(final int level){
		return isLogLevelWithinRange(level) && level >= logLevel;
	}

	public void log(String msg){
		log(msg, Log.DEBUG);		
	}
	
	public void log(String msg, int level){
		if(isLevelLoggable(level))
			loggerImpl.log(msg, level);
	}
	
	public void logAtFreq(String msg, int level, int skipLogfequency){
		log(new ModSkipLogEvent(msg, level, skipLogfequency));
	}
	
	public void logAtFreq(String msg, int level, int skipLogfequency, String skipMatchPattern){
		log(new ModSkipLogEvent(msg, level, skipLogfequency, skipMatchPattern));
	}
	
	public void log(LoggerEvent ev){
		if(isLevelLoggable(ev.getLevel()))
			loggerImpl.log(ev);
	}
	
	public static void setLogLevel(final int level){
		logLevel = level;
	}
	
	public static void init(final int logLevel) {
		setLogLevel(logLevel);
		defaultSharedloggerImpl.startLogging();
	}

	public static void init(ILogger logger, final int logLevel) {
		defaultSharedloggerImpl = logger;
		setLogLevel(logLevel);
		defaultSharedloggerImpl.startLogging();
	}
	
	public static void shutDown(){
		defaultSharedloggerImpl.flush();
		defaultSharedloggerImpl.setLogging(false);
	}
}
