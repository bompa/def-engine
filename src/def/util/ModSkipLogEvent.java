package def.util;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class ModSkipLogEvent extends LoggerEvent {
	
	private static final ConcurrentHashMap<String, AtomicInteger> logMsgRepository = new ConcurrentHashMap<String, AtomicInteger>();

	/**
	 * 
	 * @param msg Message to log.
	 * @param level The debug level of the message.
	 * @param skipLogfequency Nr of identical messages to skip before doing actual log. 
	 */
	ModSkipLogEvent(String msg, int level, int skipLogfequency) {
		super(null, 0);
		checkFreqAndUpdate(msg, level, skipLogfequency, null);
	}
	
	ModSkipLogEvent(String msg, int level, int skipLogfequency, String matchPattern) {
		super(null, 0);
		checkFreqAndUpdate(msg, level, skipLogfequency, matchPattern);
	}
	
	private void checkFreqAndUpdate(String msg, int level, int skipLogfequency, String matchPattern){
		if(skipLogfequency <= 0){
			setInfo(msg, level);
			return;
		}
		updateLogFrequency(msg, level, skipLogfequency, matchPattern);

	}

	private void updateLogFrequency(String msg, int level, int skipLogfequency, String matchPattern){
		String key = matchPattern != null? matchPattern: msg;
		AtomicInteger atomInt = logMsgRepository.putIfAbsent(key, new AtomicInteger(0));
		if(atomInt != null){
			int val = atomInt.incrementAndGet();
			if(val >= skipLogfequency){	// Message should be logged
				atomInt.set(0);
				setInfo(msg, level);
			}
		}
	}
}
