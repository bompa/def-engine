package def.util;


import def.shared.util.IEvent;
import def.shared.util.LogInfo;
import android.util.Log;

public class LoggerEvent  implements IEvent<LogInfo> {
	protected LogInfo info_;
		
	LoggerEvent(String msg, int level){
		if(msg == null)
			msg = new String("Invalid log msg received!");
		if(level < 0)
			level = Log.WARN;
		
		setInfo(msg, level);
	}
	
	protected void setInfo(String msg, int level){
		info_ = new LogInfo(msg, level);
	}
	
	public String getMsg(){
		return info_.msg_;
	}
	public int getLevel(){
		return info_.level_;
	}
	
	public boolean valid(){
		return info_.valid();
	}
}
