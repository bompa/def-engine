package def.util;

import java.io.InputStream;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import def.engine.graphics.components.NodeImmutableWrapper;
import def.shared.interfaces.INode;
import def.shared.util.OutArg;

import android.content.Context;
import android.util.Pair;

public class Util {
	
	public static int[] integerListToIntArray(List<Integer> list){
		final int[] intsOut = new int[list.size()];
		for(int i = 0; i < list.size(); i++){
			intsOut[i] = list.get(i);
		}
		return intsOut;
	}
	
	public static INode unmodifiableNode(INode n){
		return new NodeImmutableWrapper(n);
	}

	public static InputStream openRawResource(Context ctx, int resource){
		return ctx.getResources().openRawResource(resource);
	}
	
	public static <E> OutArg<E> makeOutArg(){
		return new OutArg<E>(null);
	}
	
	public static <E> OutArg<E> makeOutArg(E arg){
		return new OutArg<E>(arg);
	}
	
	public static <E, U> Pair<E, U> makePair(E first, U second){
		return new Pair<E, U>(first, second);
	}
	
	/**
	 * Thread-safe Singleton used to generate names by adding number after basenames.
	 * Keeps track of all added basenames and increments counters for each.
	 * @author jonas
	 *
	 */
	public static class NameGenerator{
		private static final NameGenerator instance = new NameGenerator();
		
		private final ConcurrentHashMap<String, AtomicInteger> nameCountMap =
			new ConcurrentHashMap<String, AtomicInteger>();
		
		public String generateName(final String baseName){
			final AtomicInteger newNameCount = new AtomicInteger(0);
			AtomicInteger currentNameCount = nameCountMap.putIfAbsent(baseName, newNameCount);
			if(currentNameCount == null)
				currentNameCount = newNameCount;
			return baseName + "-" + currentNameCount.getAndIncrement();
		}
		
		public String getLastGeneratedName(final String baseName){
			AtomicInteger count = nameCountMap.get(baseName);
			if(count != null && count.get() > 0)
				return baseName + "-" + (count.get() - 1);
			return "";
		}
		
		public void reset(){
			nameCountMap.clear();
		}

		public static NameGenerator instance() {
			return instance;
		}
	}

	/**
	 * Helper to allocate buffer
	 * @param sz
	 * @return
	 */
	public static ByteBuffer allocateDirectNativeOrder(int sz){
		ByteBuffer bb = ByteBuffer.allocateDirect(sz);
		bb.order(ByteOrder.nativeOrder());
		return bb;
	}
	
	/**
	 * Helper to get a buffer to string
	 * @param buff
	 * @return
	 */
	public static String bufferToString(FloatBuffer buff) {
		buff.position(0);
		String data = "";
		for(int i = 0; i < buff.capacity(); i++){
			data += buff.get() + ", ";
		}
		buff.position(0);
		return data;
	}
	
	/**
	 * Helper to get a buffer to string as pairs of values
	 * @param buff
	 * @return
	 */
	public static String pairBufferToString(FloatBuffer buff) {
		if(buff.capacity() % 2 != 0)
			return "Error: Buffer not divisable by 2";
		
		buff.position(0);
		String data = "";
		for(int i = 0; i < (buff.capacity() / 2); i++){
			data += "[ " + buff.get() + ", " + buff.get() + "]";
		}
		//l.log("Data: " + data, Log.ERROR);
		buff.position(0);
		return data;
	}
	
	public static RuntimeException buildIllegalArgException(Class<?> cls, String... args){
		String argsStr = "";
		if(args.length < 2 || args.length % 2 != 0){
			return new IllegalArgumentException(cls.getName() + ": Illegal arguments received");
		}
		else{
			for( int i = 0; i < args.length; i += 2 ){
				String arg = args[i];
				String argAssert = args[i + 1];
				argsStr += (arg + "(was " + argAssert + ")");
				if(i < (args.length - 2)){
					argsStr += ", ";
				}
			}
		}
		
		return new IllegalArgumentException(cls.getName() + ": Illegal arguments received: " + argsStr);
	}
	
	public static RuntimeException buildIllegalNullArgException(Class<?> cls, String... args){
		String argsStr = "";
		for( int i = 0; i < args.length; i++ ){
			String arg = args[i];
			argsStr += arg;
			if(i < (args.length - 1)){
				argsStr += ", ";
			}		
		}
		
		return new IllegalArgumentException(cls.getName() + ": Illegal null arguments received - " + argsStr);
	}

	public static int calcIndexCount(final Buffer indices) {
		return indices.remaining();
	}
	
	public static ByteBuffer deepCopy( ByteBuffer orig )
	{
	    int pos = orig.position(), lim = orig.limit();
	    try
	    {
	        orig.position(0).limit(orig.capacity()); // set range to entire buffer
	        ByteBuffer toReturn = deepCopyVisible(orig); // deep copy range
	        toReturn.position(pos).limit(lim); // set range to original
	        return toReturn;
	    }
	    finally // do in finally in case something goes wrong we don't bork the orig
	    {
	        orig.position(pos).limit(lim); // restore original
	    }
	}

	public static ByteBuffer deepCopyVisible( ByteBuffer orig )
	{
	    int pos = orig.position();
	    try
	    {
	        ByteBuffer toReturn;
	        // try to maintain implementation to keep performance
	        if( orig.isDirect() )
	            toReturn = ByteBuffer.allocateDirect(orig.remaining());
	        else
	            toReturn = ByteBuffer.allocate(orig.remaining());

	        toReturn.put(orig);
	        toReturn.order(orig.order());

	        return (ByteBuffer) toReturn.position(0);
	    }
	    finally
	    {
	        orig.position(pos);
	    }
	}
}
