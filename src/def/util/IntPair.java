package def.util;

import android.util.Pair;

public class IntPair extends Pair<Integer, Integer>{

	public IntPair(Integer first, Integer second) {
		super(first, second);
	}
}
