package def.util;

import def.shared.util.Exceptions;
import android.util.Log;

public class DefUnCaughtExceptionHandler implements Thread.UncaughtExceptionHandler{
	LogHandler l = LogHandler.createInstanceShared();
	
	public void uncaughtException(Thread t, Throwable tr) {
		try {
			l.log("Uncaught exception received: " + tr.toString()  + "\nMessage: " + tr.getMessage() + "\n" + Exceptions.stackTraceToString(tr), Log.ERROR);
			LogHandler.shutDown();
		} catch (Throwable innert) {}	// don't let the exception get thrown out, will cause infinite looping!
		System.exit(1);
	}
}
