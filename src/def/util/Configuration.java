package def.util;

import java.io.StringWriter;
import java.util.Locale;
import java.util.concurrent.Callable;

import org.w3c.dom.*;

import android.util.Log;

import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

import javax.xml.parsers.*;

public class Configuration {
	private static final Configuration config = new Configuration();
	private static final LogHandler l = LogHandler.createInstanceShared();
	
	private final Document doc;
	private final Element root;
	private Element chachedRenderConfigElement;
	
	public static final String ROOT_CONFIG_ELEMENT = "configuration";
	public static final String RENDER_CONFIG_ELEMENT = "render";
	
	public static Configuration instance(){
		return config;
	}
	
	private Configuration(){
        DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
		
        try {
			docBuilder = dbfac.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			if(l.isLevelLoggable(Log.ERROR))
				l.log("Error occured when trying to create Conig document builder.", Log.ERROR);
			
		}
        doc = docBuilder.newDocument();

        //Creating the root element
        root = doc.createElement(ROOT_CONFIG_ELEMENT);
        doc.appendChild(root);
        
        clearChacheRenderElement();
	}
	
	private static Node getFirst(NodeList nl){
		if(nl.getLength() > 0)
			return nl.item(0);
		return null;
	}
	
	private Element getFirstNodeByName(String name){
		return getFirstNodeByName(name, new Callable<Element>() {
			public Element call() throws Exception {
				return null;
			}
		});
	}
	
	private Element produceIfNotFound(Callable<Element> factoryIfNotFound){
		Element e = null;
		try {
			e = factoryIfNotFound.call();
		} catch (Exception e1) {
			if(l.isLevelLoggable(Log.WARN))
				l.log("Failed to create Element for xml-tree", Log.WARN);
		}
		return e;
	}
	
	private Element getFirstNodeByName(String name, Callable<Element> factoryIfNotFound){
		Element child = null;
		NodeList elements = root.getElementsByTagName(name); 
		if(elements.getLength() == 0){	
			child = produceIfNotFound(factoryIfNotFound);
		}
		else{
			child = (Element) elements.item(0);
		}	
		return child;
	}
	
	/**
	 * Clear render element cache.
	 */
	private void clearChacheRenderElement(){
		chachedRenderConfigElement = null;
	}
	
	/**
	 * Cache the renderElement, since it is used frequently
	 */
	private Element cacheRenderElement(){
		if(chachedRenderConfigElement == null){
			chachedRenderConfigElement = getFirstNodeByName(RENDER_CONFIG_ELEMENT, 
					new Callable<Element>() {
							public Element call() throws Exception {
								Element child = doc.createElement(RENDER_CONFIG_ELEMENT);
								child.setAttribute("name", "render-config");
								root.appendChild(child);
								return child;
							}
						}
			);			
		}
		return chachedRenderConfigElement;
	}
	
	/**
	 * Add element under the RENDER_CONFIG_ELEMENT node.
	 * ex 	<render name="render-config">
	 * 			<attribName>value</attribName>
	 * 		</render>
	 * @param attribName
	 * @param value
	 */
	public void addRenderConfigOptionElement(final String attribName, final String value){
		Element renderElement = cacheRenderElement();
		final String convertedAttribName = convertStringToConfigStd(attribName);
		
		//<convertedAttribName></convertedAttribName> := configValueElement 
		Node configValueElement = getFirst(renderElement.getElementsByTagName(convertedAttribName));
		if(configValueElement == null){	// Add if not found
			configValueElement = doc.createElement(convertedAttribName);
			renderElement.appendChild(configValueElement);
		}

		//<convertedAttribName>value</convertedAttribName>: value =  configValueTextNode
		Node configValueTextNode = configValueElement.getFirstChild();
		if(configValueTextNode == null){
			configValueElement.appendChild(doc.createTextNode(value));
		}
		else{
			configValueTextNode.setNodeValue(value);
		}
	}
	
	/**
	 * Get value of config option attribName.
	 * @param attribName name of attribute config option holding the render option.
	 * @return
	 */
	public String getRenderConfigOptionValue(final String attribName){
		Element renderElement = cacheRenderElement();
		NodeList list = renderElement.getElementsByTagName(attribName);
		if(list.getLength() > 0 && getFirst(list).getChildNodes().getLength() > 0){
			NodeList tmp = getFirst(list).getChildNodes();
			return getFirst(tmp).getNodeValue();
		}
		return "";
	}

	/**
	 * Get boolean value of config option attribName.
	 * @param attribName
	 * @return
	 */
	public boolean getRenderConfigOptionBool(final String attribName){
		String val = getRenderConfigOptionValue(attribName);
		return Boolean.parseBoolean(val);
	}

	
	/**
	 * Adapts the string to the configuration standard
	 * @param str the string to be converted
	 * @return the converted string
	 */
	public static String convertStringToConfigStd(final String str){
		return str.replace(' ', '-').toLowerCase(Locale.US);
	}
	/**
	 * Create xml tree under render element, which is root for all option items being added.
	 * @param items
	 * @param checkedItems
	 */
	public void createRenderConfigXml(final CharSequence[] items, final boolean[] checkedItems){
		if(items == null || checkedItems == null)
			throw new IllegalArgumentException("Null arguments received in Conf::createRenderConfig!");
		if(items.length != checkedItems.length)
			throw new IllegalArgumentException("Invalid length of arrays received in Conf::createRenderConfig, must be equal.");

		for(int i = 0; i < items.length; i++){
			addRenderConfigOptionElement(items[i].toString(), Boolean.toString(checkedItems[i]));	
		}
	}
	
	public String getRenderConfigString(){
		return getConfigString(RENDER_CONFIG_ELEMENT);
	}
	
	public String getConfigString(String configElement){
        //set up a transformer
        TransformerFactory transfac = TransformerFactory.newInstance();
        Transformer trans;
		try {
			trans = transfac.newTransformer();
		} catch (TransformerConfigurationException e) {
			if(l.isLevelLoggable(Log.WARN))
				l.log("Failed to create transform-factory due to TransformerConfigurationException", Log.WARN);
			return "";
		}
        trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        trans.setOutputProperty(OutputKeys.INDENT, "yes");

        //create string from xml tree
        StringWriter sw = new StringWriter();
        StreamResult result = new StreamResult(sw);
        DOMSource source = new DOMSource(getFirstNodeByName(configElement));
        try {
			trans.transform(source, result);
		} catch (TransformerException e) {
			if(l.isLevelLoggable(Log.WARN))
				l.log("Failed to transform xml-config into String", Log.WARN);
			return "";
		}
        return sw.toString();		
	}
}
