package def.util;

import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import android.os.AsyncTask;
import android.util.*;

public class LoggerASyncTask extends AsyncTask<Void, Void, Void> implements ILogger{
	enum LogLevel{WARNING, ERROR, INFO1, INFO2, INFO3}
	private static final String TAG = "Def";
	
	private LogLevel level = LogLevel.INFO1;
	
	private volatile Class<?> cls = null;
	private volatile boolean logging = true;
	
	private final LinkedBlockingQueue<LoggerEvent> queue = new LinkedBlockingQueue<LoggerEvent>();
	private final AtomicLong nrLoggedMsg = new AtomicLong();
	private final AtomicLong nrDroppedLoggedMsg = new AtomicLong();
	
	@Override
	protected Void doInBackground(Void... params) {
		runLogLoop();
		return null;
	}
	
	public void startLogging(){
		this.execute((Void)null);
	}
	
	public long getNrOfLoggedMessages(){
		return nrLoggedMsg.get();
	}
	public long getNrODroppedfLoggedMessages(){
		return nrDroppedLoggedMsg.get();
	}
	
	public LogLevel getLogLevel(){
		return level;
	}
	
	public void setLogLevel(LogLevel level){
		this.level = level;
	}
	
	public void setLogging(boolean logging_) {
		this.logging = logging_;
	}

	public boolean isLogging() {
		return logging;
	}
	
	public void log(String msg, int level){
		if(msg == null)
			return;
		log(new LoggerEvent(msg, level));
	}
	
	public void log(String msg, int level, int equalMessagesToSkip){
		if(msg == null)
			return;
		log(new ModSkipLogEvent(msg, level, equalMessagesToSkip));
	}
	
	public void log(LoggerEvent ev){
		try {
			queue.put(ev);
		} catch (InterruptedException e) {
			nrDroppedLoggedMsg.incrementAndGet();
			String msg = ev.getMsg();
			if(msg != null)
				Log.e(TAG, "Dropped log message: " + msg);
			e.printStackTrace();
		}
		nrLoggedMsg.incrementAndGet();				
	}
	
	private void runLogLoop(){
		while(logging){
			try {
				final int timeToWait = 10;
				LoggerEvent lEv = queue.poll(timeToWait, TimeUnit.SECONDS);
				if(lEv == null)
					lEv = new LoggerEvent("Waiting for log events...", Log.DEBUG);
				if(!lEv.valid())
					continue;
				if(Checks.stringNonNullAndLevelLoggable(lEv.getMsg(), TAG, lEv.getLevel())){
					final String msg = cls == null? lEv.getMsg(): cls.getName() + ": " + lEv.getMsg();
					dispatchLogMsg(msg, lEv.getLevel());
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void dispatchLogMsg(String msg, int level){
		switch(level){
			case Log.DEBUG: 
				Log.d(TAG, msg);
				break;
			case Log.ERROR:
				Log.e(TAG, msg);
				break;
			case Log.WARN:
				Log.w(TAG, msg);
				break;
			default:
				Log.i(TAG, msg);
					
		}
	}

	public void flush() {
		ArrayList<LoggerEvent> arr = new ArrayList<LoggerEvent>();
		queue.drainTo(arr);
		for(LoggerEvent e : arr){
			if(Checks.stringNonNullAndLevelLoggable(e.getMsg(), TAG, e.getLevel()))
				dispatchLogMsg(e.getMsg(), e.getLevel());
		}

	}

	public void setCallingClass(Class<?> cls) {
		this.cls = cls;
	}
}
