package def.util;

import android.util.Pair;


public class FloatPair extends Pair<Float, Float>{

	public FloatPair(Float first, Float second) {
		super(first, second);
	}
	
	@Override
	public String toString(){
		return first + ", " + second;
	}
}
