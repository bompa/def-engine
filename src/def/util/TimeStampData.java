package def.util;

import android.os.SystemClock;

public class TimeStampData<E> {
	/**
	 * Data that is associated with the time-stamp
	 */
	private E data_ = null;
	private long timeStampMs_ = 0;
	
	public TimeStampData(E data){	
		updateAndSetTimeStamp(data);
	}
		
	public long timeMs(){
		return timeStampMs_;
	}
	
	public void resetTimeStamp(){
		timeStampMs_ = Long.MAX_VALUE;
	}
	
	public E get(){
		return data_;
	}
	
	public void set(E data){
		updateAndSetTimeStamp(data);
	}
	
	void updateAndSetTimeStamp(E data){
		this.data_ = data;
		timeStampMs_ = SystemClock.uptimeMillis();
	}
	
	/**
	 * Static factory helper function.
	 * Create maximum value with time as early as possible (minimum value of long)
	 */
	public static TimeStampData<FloatPair> createTimedFloatPairInfinfy(){
		TimeStampData<FloatPair> tmp = new TimeStampData<FloatPair>(new FloatPair(Float.POSITIVE_INFINITY, Float.POSITIVE_INFINITY));
		tmp.timeStampMs_ = Long.MAX_VALUE;
		return tmp;
	}
}
