package def.util;

import def.util.LoggerASyncTask.LogLevel;

public interface ILogger {
	public long getNrOfLoggedMessages();
	public long getNrODroppedfLoggedMessages();
	
	public void setCallingClass(final Class<?> cls);
	
	public LogLevel getLogLevel();
	public void setLogLevel(LogLevel level);
	
	public void setLogging(boolean logging_);
	public boolean isLogging();

	public void log(LoggerEvent ev);
	public void log(String msg, int level);
	public void startLogging();
	public void flush();
}
