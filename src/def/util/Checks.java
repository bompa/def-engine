package def.util;

public class Checks {
	public static boolean stringNonNullAndLevelLoggable(String msg, String tag, int l){
		return msg != null;// && Log.isLoggable(tag, l);
	}
	
	public static boolean valid(Float f){
		return !f.isNaN();
	}
	public static boolean valid(FloatPair fp){
		return !fp.first.isNaN() || !fp.second.isNaN();
	}
}
