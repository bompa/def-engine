package def.util;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

public class AndroidUiUtil {
	
	/**
	 * Create spinner in given activiy
	 * @param activity activity context holding the spinner
	 * @param androidSpinnerResourceId spinner resource id
	 * @param content inital content of spinner
	 * @param itemSelected callback to handle selected item
	 */
	public static Spinner createSpinnerFromStringArray(	
			final Activity activity,
			int androidSpinnerResourceId, 
			final String[] content,
			final OnItemSelectedListener itemSelected)
	{
		final Spinner spinner = (Spinner) activity.findViewById(androidSpinnerResourceId);
		ArrayAdapter<CharSequence> adapter = new ArrayAdapter<CharSequence>(activity, android.R.layout.simple_spinner_dropdown_item, content);
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(itemSelected);
		
		return spinner;
	}
}
