package def.util;

public class ShortArrayList {
	private short[] array = new short[16];
	private int size;
	
	public void add(short value) {
		if (size == array.length) {
			expand();
		}
		
		array[size++] = value;
	}
	
	private void expand() {
		short[] newArray = new short[array.length << 1];
		System.arraycopy(array, 0, newArray, 0, array.length);
		array = newArray;
	}

	public short remove(int index) {
		if (index >= size) throw new IndexOutOfBoundsException();
		short old = array[index];
		System.arraycopy(array, index+1, array, index, size - index - 1);
		size--;
		return old;
	}

	public short get(int index) {
		if (index >= size) throw new IndexOutOfBoundsException();
		return array[index];
	}

	public void set(int index, short value) {
		if (index >= size) throw new IndexOutOfBoundsException();
		array[index] = value;
	}

	public int size() {
		return size;
	}
	
	public void clear() {
		size = 0;
	}
}
