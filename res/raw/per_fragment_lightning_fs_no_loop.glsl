precision mediump float;

struct Light
{
	vec3 uLightPos;
	vec4 diffuse;
	vec4 ambient;
};

const int maxNLights = 2;
const vec4 globlalAmbientLight = vec4(0.05, 0.05, 0.05, 0.0);

uniform sampler2D sTexture;
uniform vec4 uMatEmmison;
uniform vec4 uMatDiffuse;
uniform vec4 uMatBrightness;

uniform float uAlpha;
uniform int uNrOfLights;

uniform Light lightArr[maxNLights];

varying vec2 vTextureCoord;
varying vec3 vPosition;
varying vec3 vNormal;

vec3 comuteLighContribution(int lightIdx, int nLightsUsed) {
  if(lightIdx < nLightsUsed) {
    float distance = length(lightArr[lightIdx].uLightPos - vPosition);
    vec3 lightVector = normalize(lightArr[lightIdx].uLightPos - vPosition);

    float attenuation = (1.0 / (1.0 + (0.1 * distance)));
    vec3 diffuse = attenuation * max(dot(vNormal, lightVector), 0.1) * vec3(lightArr[lightIdx].diffuse);

    return diffuse + vec3(lightArr[lightIdx].ambient);
  }
  
  return vec3(0, 0, 0);
}

void main() {
  vec3 totalLighting = vec3(0, 0, 0);
  int nLightsUsed = uNrOfLights;
  if(nLightsUsed >= maxNLights || nLightsUsed < 0)
  {
    nLightsUsed = maxNLights - 1;
  }
 
   // Accumulate light from all light sources - no for loop
  totalLighting += comuteLighContribution(0, nLightsUsed);
  totalLighting += comuteLighContribution(1, nLightsUsed);
  //totalLighting += comuteLighContribution(2, nLightsUsed);
  //totalLighting += comuteLighContribution(3, nLightsUsed);
  //totalLighting += comuteLighContribution(4, nLightsUsed);
  //totalLighting += comuteLighContribution(5, nLightsUsed);
  
  vec4 matEmmison = uMatEmmison;
  matEmmison.a = 0.0;
  vec4 texColor = texture2D(sTexture, vTextureCoord) + globlalAmbientLight;
  vec4 totalLightingV4 = vec4(totalLighting, 1) + globlalAmbientLight;
  vec4 finalColor = totalLightingV4 * texColor + uMatBrightness * texColor + matEmmison;
  
  float oneMinAlpha = 1.0 - uAlpha;
  finalColor.a = finalColor.a - oneMinAlpha;
  
  gl_FragColor =  finalColor;
}
