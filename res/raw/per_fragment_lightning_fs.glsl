precision mediump float;

struct Light
{
	vec3 uLightPos;
	vec4 diffuse;
	vec4 ambient;
};

const int maxNLights = 16;
const vec4 globlalAmbientLight = vec4(0.1, 0.1, 0.1, 0.0);

//uniform vec3 uLight1Pos; 
uniform sampler2D sTexture;
uniform vec4 uMatEmmison;
uniform vec4 uMatDiffuse;
uniform vec4 uMatBrightness;

uniform float uAlpha;
uniform int uNrOfLights;

uniform Light lightArr[maxNLights];

varying vec2 vTextureCoord;
varying vec3 vPosition;
varying vec3 vNormal;

void main() {
  vec3 totalLighting = vec3(0, 0, 0);
  int nLightsUsed = uNrOfLights;
  if(nLightsUsed >= maxNLights || nLightsUsed < 0)
  {
    nLightsUsed = maxNLights - 1;
  }
 
   // Accumulate light from all light sources
  int index = 0;
  for (; index < nLightsUsed; index++)
  {
    float distance = length(lightArr[index].uLightPos - vPosition);

	float attenuation = (1.0 / (1.0 + (0.1 * distance * distance)));
	if( attenuation > 0.001 )
	{
		vec3 lightVector = normalize(lightArr[index].uLightPos - vPosition);
		vec3 diffuse = attenuation * max(dot(vNormal, lightVector), 0.0) * vec3(lightArr[index].diffuse);
		totalLighting = totalLighting + diffuse;
	}
	totalLighting = totalLighting + vec3(lightArr[index].ambient);
	
  }
  
  vec4 matEmmison = uMatEmmison;
  matEmmison.a = 0.0;
  vec4 texColor = texture2D(sTexture, vTextureCoord) + globlalAmbientLight;
  vec4 totalLightingV4 = vec4(totalLighting, 1) + globlalAmbientLight;
  vec4 finalColor = totalLightingV4 * texColor + uMatBrightness * texColor + matEmmison;
  
  float oneMinAlpha = 1.0 - uAlpha;
  finalColor.a = finalColor.a - oneMinAlpha;
  
  gl_FragColor =  finalColor;
}
