uniform mat4 uMVPMatrix;  										 
uniform mat4 uMVMatrix;  										 
		
attribute vec4 aPosition;  										 
attribute vec2 aTextureCoord;  									 
attribute vec3 aNormal;
attribute vec3 aTangent;
attribute vec3 aBinormal;

varying vec2 vTextureCoord;  									 
varying vec3 vPosition;

varying vec3 vNormal;
varying vec3 vTangent;
varying vec3 vBinormal;

void main() {  													 

  vPosition = vec3(uMVMatrix * aPosition); 						 
  vNormal = vec3(uMVMatrix * vec4(aNormal, 0.0)); 				 
  vTextureCoord = aTextureCoord;
  
  vTangent = vec3(uMVMatrix * vec4(aTangent, 0.0)); 				 
  vBinormal = vec3(uMVMatrix * vec4(aBinormal, 0.0)); 				 

  gl_Position = uMVPMatrix * aPosition;  						 
}
