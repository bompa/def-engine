precision mediump float;

uniform sampler2D sTexture0;
uniform sampler2D sTexture1;

varying vec2 vTextureCoord;
varying vec3 vPosition;
varying vec3 vNormal;

void main() {
  vec4 texColor = texture2D(sTexture0, vTextureCoord);
  gl_FragColor =  texColor;
}
